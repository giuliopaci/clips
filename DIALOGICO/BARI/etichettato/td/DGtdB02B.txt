TEXT_inf.

MAT:            td
MAP:            B
TRM:
Ndl:            02
Ntr:
Nls:
REG:            B


SPEAKERS_inf.

INp1:       T. S.M., F, 24, Bari, spontaneo, fluente
INp2:       P. N., M, 24, Bari, spontaneo, fluente


RECORDING_inf.

TYP:        DAT
LOC:        Bari/Universit�
DAT:        10/05/01
DUR:        10.02,718
CON:        ottime


TRANSCRIPTION_inf.

DAT:        14/01/02
CMT:        il segnale � spesso saturato
Nst:        242



p1#1:  allora , Nicola cominciamo dalla nuvola <sp> #<p2#2> <tongue-click> <sp> in alto# a #<p2#2> sinistra#


p2#2:  #<p1#1> s�# <P> #<p1#1> <tongue-click> s�#


<sp>


p1#3:  <mh> <sp> allora la mia <sp> ha<aa> , subito a sinistra <inspiration> <eeh> dei segnetti


p2#4:  quattro


<sp>


p1#5:  quattro segnetti


p2#6:  <mh> <sp> #<p1#7> allora# non c'� #<p1#7> nessuna differenza#


p1#7:  #<p2#6> <mh># <sp> #<p2#6> <inspiration> <sp> <eeh> poi � di forma allungata#


p2#8:  s�


p1#9:  ha come un<nn> beccuccio con <eeh>


<lp>


p2#10:  sulla #<p1#11> destra#


p1#11:  #<p2#10> sulla# destra s�


<sp>


p2#12:  che termina per� non arrotondato che termina #<p1#13> come un<nn>#


p1#13:  #<p2#12> con due tondini#


<lp>


p2#14:  e termina con uno a me <sp> <inspiration> #<p1#15> sai tipo# fosse una mammella <sp> #<p1#15> che ti devo di'#


p1#15:  #<p2#14> e allora# <lp> #<p2#14> e all+# <lp> e allora dovrebbe essere la prima differenza questa


<sp>


p2#16:  <mb�> <sp> <inspiration> <sp> poi <breath> <sp> <inspiration> <sp> <eeh> prendiamoci l'uovo


<sp>


p1#17:  s�


p2#18:  <tongue-click> <sp> allora io c'ho l'uovo con la punta superiore verso <sp> destra


p1#19:  s�


p2#20:  <tongue-click> <inspiration> c'ha tre chiazze


p1#21:  <mhmh>


p2#22:  grosse <inspiration> e poi c'ha <sp> <inspiration> <sp> tra la chiazza che sta pi� a destra e quella pi� in #<p1#23> basso due linee#


p1#23:  s� <lp> #<p2#22> <mhmh># <lp> s� 


p2#24:  che <sp> uniscono le due <sp> le due palle / le due #<p1#25> chiazze <inspiration># 


p1#25:  #<p2#24> s�#


p2#26:  poi <inspiration> partendo da quella pi� in basso ce ne sono sempre due 


p1#27:  s�


p2#28:  <tongue-click> <sp> di cui per� <sp> <tongue-click> 
        la <lp> pi� lunga � quella che � in basso <sp> #<p1#29> la inferiore# <inspiration> l'altra invece superiore 
        #<p1#29> � pi�<uu>#


p1#29:  <tongue-click> #<p2#28> s�# <lp> #<p2#28> � un po' pi� cor+#


p2#30:  corta <inspiration> <sp> c'� una piccola ombra <sp> <inspiration> che inizia per� dalla base in cui 
        tocca l'uovo la terra #<p1#31> <inspiration># e che ha un <eeh> un cono d'ombra pi� o meno fino alla seconda chiazza 
        <inspiration> aspetta che cosa intendo l+ / la seconda chiazza quella che sta su' destra <breath>


p1#31:  #<p2#30> <mhmh># <lp> s�


p2#32:  <mh> <inspiration> se tu tiri una perpendicolare che va fino a de+ / fino all<ll> / al d+ <NOISE> / {<NOISE> a / 
        come si dice ?} <inspiration> comunque fino al terreno


p1#33:  s�


<lp>


p2#34:  i<ii> <sp> tratteggiata<aa> cos� provvisoria<aa> a fa+ a fantasia , l'ombra arriva pi� o meno fino l�


<sp>


p1#35:  s� <sp> � uguale


p2#36:  � uguale


<lp>


p1#37:  poi la #<p2#38> barchetta#


p2#38:  #<p1#37> <NOISE> <sp> <vocal> aspetta# aspett+ <sp> <clear-throat> <sp> <vocal> poi dall'uovo <sp> 
        parte / che � una linea poi del terreno una zigrinatura / parte una piccola lineetta<aa> <sp> <vocal> pi� lunga 
        sembra quasi la codi+ <sp> #<p1#39> sembra quasi# uno spermatozoo Stefania #<p1#39> sembra la coda 
        dello spermatozoo#


p1#39:  #<p2#38> s�# <lp> #<p2#38> <laugh># #<p2#40> s�#


p2#40:  #<p1#39> <inspiration># <eh> <inspiration> <sp> finisce poi ci sono altri due pezzettini


p1#41:  s�


p2#42:  allo' niente <inspiration> e poi ci sono invece sempre pi� o meno a 
        quell'altezza <inspiration> <sp> alla destra <sp> #<p1#43> al margine# del foglio <sp> 
        tre lineette piccoline #<p1#43> pi� o meno# all'altezza del mezzo uovo 


p1#43:  #<p2#42> <mhmh># <lp> #<p2#42> s�# <sp> s�


<sp>


p2#44:  allo' niente , <inspiration> <mb�> di' tu mo'


p1#45:  <tongue-click> allo' la barchetta


<sp>


p2#46:  <tongue-click> <sp> <ah> !


<sp>


p1#47:  <inspiration> allora la barchetta <sp> ha<aa> la bandierina


p2#48:  s�


p1#49:  con la 
        punta verso sinistra


p2#50:  <tongue-click> a me ce l'ha a destra


<sp>


p1#51:  <ah> , allora � la prima , <eeh> differenza


<sp>


p2#52:  la seconda


<sp>


p1#53:  la seconda


<sp>


p2#54:  <tongue-click> <sp> #<p1#55> poi ?#


p1#55:  #<p2#54> <inspiration># <sp> <ehm> la vela 


<sp>


p2#56:  <mh>


<sp>


p1#57:  <tongue-click> <sp> �<ee> pi� o meno triangolare


<sp>


p2#58:  s�


<sp>


p1#59:  e ha<aa> uno due tre <sp> cinque #<p2#60> lineette#


p2#60:  #<p1#59> s�# <sp> <inspiration> <sp> triangolo isoscele pi� o meno


<sp>


p1#61:  s�


<sp>


p2#62:  <mh> <sp> <inspiration> <sp> e poi per� ha una piccola<aa> / sembra / sar� la seconda vela per� #<p1#63> presa in <eeh># <sp> 
        #<p1#63> prospettiva l� che cos'�#

    
p1#63:  #<p2#62> s�#  <lp> #<p2#62> s+ <sp> <eeh> sulla destra#


p2#64:  sulla destra s� 


p1#65:  e<ee> finisce dove inizia lo scafo <lp> #<p2#66> giusto ?#


p2#66:  <clear-throat> <sp> #<p1#65> finisce# dove inizia lo scafo , #<p1#67> <mh>#


p1#67:  #<p2#66> <mh># <sp> <inspiration> <sp> lo scafo <sp> � dritto o sulla destra ?


<sp>


p2#68:  dri_tto s+ / la punta dove ce l'ha ?


<sp>


p1#69:  la punta ce l'ha a sinistra


<sp>


p2#70:  s�  <sp> <clear-throat>


p1#71:  <mh> <sp> <tongue-click> <inspiration> #<p2#72> poi#


p2#72:  <tongue-click> #<p1#71> e<ee># per� aspetta la vela arriva pi� o meno a un cent+ / 
        <inspiration> prendi la punta ?


<sp>


p1#73:  s�


p2#74:  <tongue-click> <inspiration>  la punta della vela quella che c'ha le cinque linee 
        #<p1#75> dista# dalla punta della barca un centimetro pi� o #<p1#75> meno#


p1#75:  #<p2#74> s�# <P> #<p2#74> s�# , pi� o meno


p2#76:  <mh> #<p1#77> <inspiration> poi# sotto vedi tre onde c'hanno tre punte


p1#77:  #<p2#76> {[whispering] allora � ugual+}# <lp> <tongue-click> <sp> s�

"allora � ugual+" � pronunciato velocemente e con un volume di voce molto basso.


p2#78:  le tre ondette ? <sp> allo' niente


p1#79:  la nuvoletta a #<p2#80> destra ?#


p2#80:  #<p1#79> <eh> s� vedi# l�


p1#81:  <inspiration> <sp> allora la nuvoletta a destra<aa> <sp> <ehm> <P> dunque <sp> 
        <inspiration> <sp> ha<aa> <sp> uno due<ee> <lp> tre <lp> quattro cinque sei<ii> ondine <sp>
        #<p2#82> tutto intorno#


p2#82:  #<p1#81> aspetta aspe+ parti# da sotto parti da #<p1#83> sotto#


p1#83:  #<p2#82> parto# da #<p2#84> sotto allora# , uno due tre <sp> <tongue-click> <sp> #<p2#84> piccoline#


p2#84:  #<p1#83> <mh># <P> #<p1#83> piccole# <sp> s�


p1#85:  poi ce n'� una pi� grande sulla sinistra


p2#86:  che � la punta #<p1#87> praticamente#


p1#87:  #<p2#86> della nuvola# s�


<sp>


p2#88:  s� <inspiration> e poi fa come due ro+ <P> sai le macchine quando disegni la #<p1#89> macchina le rotelle ? 
        #


p1#89:  #<p2#88> s� le ruote della macchina s�#


p2#90:  #<p1#89> <mh> <sp> vabb�# non {[dialect] val' 'a pe'}


<lp>


p1#91:  allora � uguale


<sp>


p2#92:  poi aspetta <sp> <tongue-click> <inspiration> passiamo sotto al<ll> alla sabbia <sp> #<p1#93> e poi# 
        ci facciamo il bambino qua


p1#93:  #<p2#92> <mh># <P> allora a #<p2#94> sinistra# <sp> sulla / l+ / s+ <sp> #<p2#94> la sabbia# , a 
        #<p2#94> sinistra#


p2#94:  #<p1#93> <clear-throat># <P> #<p1#93> <tongue-click> allo'# <P> 
        #<p1#93> <ah> questa# qua s� <P> #<p1#95> in basso#


p1#95:  #<p2#94> allora# <sp> ci sono due<ee> <sp> come delle ondine #<p2#96> della sabbia#


p2#96:  #<p1#95> s�# <sp> s�


p1#97:  allora quella di sopra <sp> � fatta di piccoli tratti


<lp>


p2#98:  <eeh> uno pi� lungo / aspetta #<p1#99> uno interrompe#


p1#99:  #<p2#98> ce ne sono due un po' pi� lunghi# 


p2#100:  s� #<p1#101> e poi uno due# tre quattro cinque #<p1#101> pi� o meno#


p1#101:  #<p2#100> e poi de+# <P> #<p2#100> pi� corti#


<sp>


p2#102:  <mh>


p1#103:  quella di sotto � <sp> un poco pi� lunga


<sp>


p2#104:  ha una parte iniziale pi� #<p1#105> arcata e sotto ci s+#


p1#105:  #<p2#104> che sembra un sasso#


<sp>


p2#106:  <eh> ?


<sp>


p1#107:  sembra un sasso


p2#108:  s� <inspiration> e sembra poi che ci siano <sp> all'interno come delle erbette <sp> #<p1#109> con quattro#


p1#109:  s� #<p2#108> ce ne sono quattro#


p2#110:  e poi sai sempre i piccoli sassini sono uno <sp> pi� piccolo , #<p1#111> poi un altro# ancora pi� 
         piccolo e poi inizia una specie di linea sinuosa


p1#111:  #<p2#110> s�# <lp> <tongue-click> s�


<sp>


p2#112:  allora niente <inspiration> che altro ? #<p1#113> <ah> andiamo#


p1#113:  #<p2#112> <eeh> il sasso#


p2#114:  <eh> ?


<sp>


p1#115:  il sasso <lp> #<p2#116> che sta sul#


p2#116:  andiamo #<p1#115> alla destra# quell'altro #<p1#117> pezzettino l� che non abbiamo fatt+#


p1#117:  #<p2#116> s� c'� un sasso a# #<p2#118> destra#


p2#118:  #<p1#117> <eh> ! <sp> il# sasso brava !


<sp>


p1#119:  con tre lineette


<lp>


p2#120:  due


p1#121:  vabb� s� la terza � nascosta dal margine del <eeh> <sp> #<p2#122> del disegno# <sp> vabb� due #<p2#122> allora 
         � uguale#


p2#122:  #<p1#121> <mh># <sp> <inspiration> <lp> #<p1#121> <mh> <sp> <inspiration># <sp> 
         allora aspetta che cosa ti pu� sembrare ? <sp> un panzerotto Stefa' !


<sp>


p1#123:  s� , pi� o meno


<sp>


p2#124:  <tongue-click> <sp> <ah> ?


<sp>


p1#125:  s�


<sp>


p2#126:  <tongue-click> allo' {<NOISE> niente}


<sp>


p1#127:  � uguale <inspiration> <eeh> <lp> e allora passiamo alla papera


<lp>


p2#128:  <tongue-click> <ah> ! la parte inferiore com'� a te ?


<sp>


p1#129:  <eeh> ha una<aa> ha un'ondetta <lp> ha #<p2#130> un<nn>a piccola#


p2#130:  #<p1#129> no<oo> a me � pi� o meno# dritta


<sp>


p1#131:  no la mia non � dritta


<sp>


p2#132:  <mh> <lp> <inspiration> la parte invece superiore � un po' #<p1#133> sinuosa#


p1#133:  #<p2#132> <tongue-click># s�


<sp>


p2#134:  <mh> <sp> <tongue-click> <inspiration> la punta � a de+ / a #<p1#135> sinistra#


p1#135:  #<p2#134> <tongue-click> a sinistra#


p2#136:  <clear-throat> <sp> poi il bambino


<sp>


p1#137:  s� 


p2#138:  <breath> <tongue-click> allo' cominciamo dalle gambe che sono magre


p1#139:  s�


p2#140:  c'ha <P> si vede i due / i / si vedono i due pollici


<sp>


p1#141:  s�


<sp>


p2#142:  e poi si vedono i<ii> due ditini <P> #<p1#143> le due dita del piede#


p1#143:  #<p2#142> s�<ii> , s� s�# <lp> s�


p2#144:  <tongue-click> quello che #<p1#145> sta in basso#


p1#145:  #<p2#144> del piede sinistro#


<sp>


p2#146:  poi si vede l'osso della caviglia


<sp>


p1#147:  s�


<sp>


p2#148:  dalla parte esterna sul piede pi� in basso e dalla parte interna dalla parte<ee> <sp> 
         #<p1#149> <inspiration> sul piede pi� 
         in alto#


p1#149:  #<p2#148> dell'altro piede# <lp> s�


p2#150:  i due cosi del+ gin+ / due segnetti che<ee> marcano i gin+ le ginocchia


<sp>


p1#151:  s�


<sp>


p2#152:  e basta , poi c'ha <lp> il pollice aperto verso <inspiration> il salva+ , <ah> ! <eeh>
         facciamo il salvagente allora aspetta <P> facciamo il #<p1#153> salvagente !#


p1#153:  #<p2#152> s�#


<sp>


p2#154:  il tappo<oo> per l'aria


p1#155:  s�


p2#156:  ha una parte cilindrica pi� o meno inferiore


p1#157:  s�


p2#158:  poi ha un tubicino di un centimetro


<sp>


p1#159:  s�


p2#160:  e poi ha praticamente una parte superiore che assomiglia alla testa di un funghetto


<sp>


p1#161:  s�


p2#162:  e ha <ehm> sai il manico della tazza ?


p1#163:  s� <lp> <tongue-click> #<p2#164> � uguale#


p2#164:  <mh> <P> #<p1#163> ce l'ha# <lp> <tongue-click> <inspiration> <lp> <eeh> la parte cilindrica in basso 
         <lp> �<ee> pi� grossa pi� #<p1#165> picco+ ? anda' [dialect] a me# sar� cinque<ee> <P> #<p1#165> mezzo centimetro#


p1#165:  #<p2#164> <tongue-click> <sp> �<ee># <lp> #<p2#164> �# <lp> <tongue-click> � un po' pi� grande rispetto 
         diciamo al collo del<ll> del beccuccio <sp> � abbastanza alta


<sp>


p2#166:  <tongue-click> <ah> a me no a me � piccola


<sp>


p1#167:  no a me � abbastanza alta


<sp>


p2#168:  no <sp> {<NOISE> <inspiration>} allo' vedi le dimensioni ce {<NOISE> le devo' dire se no} qua


p1#169:  s�


p2#170:  <clear-throat> <sp> <tongue-click> <sp> poi <inspiration> il <eeh> la ciambella qua / 
         il salvagente ha la forma di un cigno <sp> o #<p1#171> di una papera# o di un'oca


p1#171:  #<p2#170> s�# <lp> s�


<sp>


p2#172:  ha <sp> la / il classico becco 


p1#173:  s�


<sp>


p2#174:  <mh> <inspiration> <eeh> <sp> con due<ee> parti di cui per� la superiore


<sp>


p1#175:  <mhmh>


p2#176:  allora da sopra hai una linea dritta , #<p1#177> poi# in basso diventa arcuata


p1#177:  #<p2#176> s�# <P> s�


<sp>


p2#178:  <mh> <inspiration> e ha la parte invece inferiore


<sp>


p1#179:  s�


p2#180:  che assomiglia a un pollice , in basso Ste' non in basso<oo> <breath> <tongue-click> <inspiration> no 
         aspetta <sp> <inspiration> <P> {[screaming] Stefa'} #<p1#181> � chiusa# <sp> bene


p1#181:  #<p2#180> allora# <P> s� ha la<aa> / il becco chiuso


p2#182:  <mh> #<p1#183> <inspiration># i<ii>+ pi� o meno lo spessore � di un centimetro la parte superiore nella parte pi� 
         alta <sp> e poi sai man mano � come una mezzaluna #<p1#183> brava ecco !# una mezzaluna <inspiration> e la 
         parte inferiore invece � piccolina insomma


p1#183:  #<p2#182> <mh># <P> #<p2#182> s�#  <P> no la mia non � molto 
         piccola <sp> non � molto pi� piccola del / di quella superiore <sp> #<p2#184> �<ee> <sp> <ehm>#


p2#184:  no #<p1#183> non � molto pi�# piccola #<p1#185> per� insomma � pi� pi+#


p1#185:  #<p2#184> una linea dritta# sotto


<sp>


p2#186:  dritta sotto ?


p1#187:  d+ / sotto s� � quasi completamente #<p2#188> dritta#


p2#188:  #<p1#187> <ah># no a me no


<sp>


p1#189:  ed �<ee> <ehm> <P> � perpendicolare la linea / la fine della b+ <P> del #<p2#190> becco#


p2#190:  <clear-throat> <P> #<p1#189> no a me# no


<lp>


p1#191:  {[whispering] #<p2#192> e allora � una differenza#}


p2#192:  #<p1#191> <eeh> <sp> poi senti un po' !# <sp> quando le due parti della bocca si incontrano a me fa sai come una 
         fossetta #<p1#193> come 'na cosa# pi� in basso


p1#193:  #<p2#192> s�# <P> s� sotto l'occhio


<lp>


p2#194:  sotto l'occhio s� un<nn> un mezz+ / sette #<p1#195> millimetri pi� in basso#


p1#195:  #<p2#194> come se<ee> / come la bocca#


<sp>


p2#196:  <tongue-click> <eh> brava ! come se la fossetta quando chiudi <vocal> <inspiration> capito ?


p1#197:  s� s�


<sp>


p2#198:  <mh> <sp> <tongue-click> <inspiration> poi l'occhio praticamente ha il bulbo il <eeh> / la palla ro+ / nera


p1#199:  s�


<sp>


p2#200:  in basso


<sp>


p1#201:  s�


<sp>


p2#202:  <mh> <inspiration> che altro ? <inspiration> <ah> ! <eeh> allora

La seconda parte del turno � pronunciata in modo da essere percepita come una domanda. 
Non ha, comunque, intonazione interrogativa.


<sp>


p1#203:  <tongue-click> la testa � tonda


p2#204:  la testa <sp> ovale �


<sp>


p1#205:  s�


<sp>


p2#206:  <NOISE> <mh> <inspiration> poi ha le macchie allora praticamente la prima inizia <sp> da dove finisce / vedi 
         l'occhio ? <sp> #<p1#207> tra+ <sp> traccia# una linea , #<p1#207> pi� o meno inizia# da l� <sp> #<p1#207> 
         {<NOISE> sul collo}#


p1#207:  #<p2#206> s�# <P> #<p2#206> s�# <P> #<p2#206> s�# <sp> s�


p2#208:  a {<NOISE> destra}


<sp>


p1#209:  s�


<sp>


p2#210:  <tongue-click> <sp> allora <NOISE> pi� o meno all'altezza del collo / all'altezza dell'occhio traccia una linea 
         che va verso la fine del collo sulla destra #<p1#211> inizia# da l� <sp> <inspiration> e finisce praticamente 
         {<NOISE> dove} hai il confine / l'#<p1#211> orizzonte#


p1#211:  #<p2#210> s�# <P> s� <P> #<p2#210> <tongue-click> s�#


p2#212:  <inspiration> <mh> <sp> <inspiration> ed ha un diametro di c+ / ha un <eeh> / s� uno spessore pi� 
         o meno nella parte su+ / nella parte pi�<uu> ampia <inspiration> <sp> di un centimetro e mezzo sar�


<sp>


p1#213:  s�


p2#214:  <clear-throat> <sp>  <tongue-click> poi la seconda


<sp>


p1#215:  s�


<lp>


p2#216:  � quasi <P> <vocal> � grande quasi da toccare tutta la superficie del collo


<sp>


p1#217:  <ss>s� <sp> #<p2#218> s� s� s�#


p2#218:  #<p1#217> <mh> ? cio� manca# proprio quasi un #<p1#219> millimetro a toccare# anche <nn>nella parte pi� ampia


p1#219:  #<p2#218> s� s� s�# <P> s� <sp> #<p2#220> � uguale#


p2#220:  #<p1#219> <mh> <inspiration># inizia <lp> pi� o meno <sp> <ah> ! <sp> 
         allora praticamente vedi #<p1#221> adesso ci so+#


p1#221:  #<p2#220> c'� un'ondina#


<sp>


p2#222:  c'� una linea che � un'ondina <sp> <inspiration> e poi ce ne sta un'altra pi� 
         #<p1#223> piccola#


p1#223:  #<p2#222> s�#


<sp>


p2#224:  allora quella pi� grande che inizia dal margine


<sp>


p1#225:  s�


<sp>


p2#226:  e finisce pi� o meno alla met� della seconda che sta sotto


<sp>


p1#227:  s�


<sp>


p2#228:  la seconda va<aa> quasi a toccare il collo


<sp>


p1#229:  s�


p2#230:  <inspiration> <clear-throat> <sp> <tongue-click> allora pi� o meno , quella seconda onda sta nel bel mezzo della 
         macchia


<sp>


p1#231:  s�


p2#232:  <inspiration> quindi tu , pi� o meno , #<p1#233> traccia#


p1#233:  #<p2#232> vabb� la macchia# parte dalla prima<aa> linea #<p2#234> praticamente#


p2#234:  #<p1#233> s� <sp> e# finisce <sp> un centimetro sotto <inspiration> l'altra ondetta che sta sotto una terza onda che 
         � zigrinata che #<p1#235> sembra una /Z/#


p1#235:  #<p2#234> s�# <sp> s� arriva quasi l�


<sp>


p2#236:  un po' pi� sopra per�


<sp>


p1#237:  s� s� un po'<oo> un po' pi� su


p2#238:  <clear-throat> niente <inspiration> questa � quindi la terza onda


p1#239:  il petto � dritto

Il turno � pronunciato in modo da essere contemporaneamente un'affermazione e una richiesta di conferma. Non ha, per�, 
intonazione interrogativa.


<sp>


p2#240:  <ii>il petto � dritto s� <sp> #<p1#241> e finisce# <sp> all'altra onda , che � un'altra Zeta 


p1#241:  #<p2#240> e arriva all'altra onda# <lp> s�


<sp>


p2#242:  che � parallela praticamente all'alt+ / alla ter+

