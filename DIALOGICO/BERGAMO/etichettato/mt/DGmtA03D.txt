TEXT_inf.

MAT:            mt
MAP:            A
TRM:
Ndl:            03
Ntr:
Nls:
REG:            D


SPEAKERS_inf.

INp1:  M. G., F, 20, Trescore Balneario (BG), spontaneo, non fluente, G>F
INp2:  F. D., F, 19, Bergamo, spontaneo, fluente, F>G


RECORDING_inf.

TYP:  DAT
LOC:  Bergamo
DAT:  09/10/01
DUR:  11.40,971
CON:  buone


TRANSCRIPTION_inf.

DAT:  08/02/02
CMT:
Nst:  256


p1G#1:  okay <unclear> parto io


<sp>


p2F#2:  vai


<sp>


p1G#3:  � una specie di percorso di offshores


<sp>


p2F#4:  {[whispering] <ss>s� <sp> #<G#5> di pers+}#


p1G#5:  #<F#4> allora partenza# televisione <lp> passi sotto


<sp>


p2F#6:  allora partenza televisione <lp> devo passare<e> / #<G#7> do aspetta#


p1G#7:  #<F#6> sotto la# #<F#8> televisione#


p2F#8:  #<G#7> devo# *vassare sotto la #<G#9> televisione da che pun+#


p1G#9:  #<F#8> <tongue-click> andando verso# destra


<lp>


p2F#10:  okay {[whispering] <unclear> verso} #<G#11> destra va bene#


p1G#11:  #<F#10> okay poi fai# una specie di parabola <breath>


<sp>


p2F#12:  s�<ii>


<sp>


p1G#13:  <eeh> senza superare l'altezza massima della torta


<lp>


p2F#14:  aspetta devo passar sopra gli sci o sotto ?


<lp>


p1G#15:  sci<ii> ? <sp> ecco non ci sono {[screaming] brava} allora ci saranno
         gli sci da te passi sopra gli sci 


p2F#16:  sopra gli sci / io parto da sotto la televisione passo #<G#17> sopra
         gli sci#


p1G#17:  #<F#16> sopra gli sci# <sp> brava


p2F#18:  s�


p1G#19:  poi hai una torta ?


p2F#20:  s� ce #<G#21> l'ho#


p1G#21:  #<F#20> okay# passi intorno alla torta a destra passando da sotto ,
         #<F#22> sotto e poi vai sopra#


p2F#22:  #<G#21> passato da sotto# <sp> <inspiration> poi vado #<G#23> sopra#


p1G#23:  #<F#22> ecco fermati# per� / fermati<ii> <sp> <eeh> sopra la torta


p2F#24:  {[screaming] sopra ?}


<sp>


p1G#25:  <tongue-click> s�


p2F#26:  <mh>


<sp>


p1G#27:  <eeh> devi tornare a sinistra


<lp>


p2F#28:  s�


<lp>


p1G#29:  <ehm> sopra la partenza tu cos'hai <lp> andando<oo> in linea retta
         verso l'alto ?


<sp>


p2F#30:  la partenza � la tiv�<uu> ?


<sp>


p1G#31:  la partenza <sp> prima della tiv� <lp> #<F#32> un pochino prima della
         tiv�#


p2F#32:  #<G#31> {[whispering] prima della tiv� <sp> <eh># un pochino io ci son
         partita da sotto} <inspiration> #<G33> <eeh>#


p1G#33:  #<F#32> {[screaming] <eh> !} allora# se sei partita da sotto
         <inspiration> #<F#34> <ehm> <vocal>#


p2F#34:  #<G#33> {[screaming] ho un pettine}#


<sp>


p1G#35:  hai un pettine


<sp>


p2F#36:  {[screaming] tra} / tra la televisione e la torta


<sp>


p1G#37:  #<F#38> <ss>s�#


p2F#38:  #<G#37> pi�# verso la televisione c'� un pettine sopra


<lp>


p1G#39:  okay <sp> #<F#40> e<ee>#


p2F#40:  #<G#39> che � sotto# praticamente <sp> l'arrivo


<lp>


p1G#41:  {[whispering] <mh>}


p2F#42:  {[whispering] sotto 'l<ll> <inspiration> <sp> puntino puntino nero
         questo}


p1G#43:  <tongue-click> allora aspetta esci <lp> {[whispering] vabbe' <sp>
         facciam che son gli sci} <sp> poi hai un pettine


p2F#44:  sopra #<G#45> s� <lp> s�#


p1G#45:  #<F#44> sopra <breath> tu hai# un / anche una macchina rossa ? #<F#46>
         <sp> due#


p2F#46:  #<G#47> s� che � molto# a sinistra


p1G#47:  molto a sinistra okay perfetto ed � metti <inspiration> <eeh> {<NOISE>
         un<nn>} <NOISE> <sp> dieci centimetri sopra la televisione un po' a
         #<F#48> sinistra ?#


p2F#48:  #<G#47> s�# <sp> s� <sp> #<G#49> s� s�# 


p1G#49:  #<F#48> okay# <sp> ecco dalla torta tu devi andare a sinistra


<sp>


p2F#50:  <eh>


p1G#51:  e<ee> <sp> <ehm> <tongue-click> <sp> il primo pezzo <sp> orizzontale
         <sp> a meno che tu abbia il pettine <sp> #<F#52> ma il pettine#
         praticamente il pettine dovrebbe #<F#52> essere<ee>#


p2F#52:  #<G#51> <mh># <lp> #<G#51> il pettine# � il / in <eeh> in perfetta
         diagonale


<sp>


p1G#53:  #<F#54> s�#


p2F#54:  #<G#53> tra# <ll>la torta e la macchina rossa


<sp>


p1G#55:  tra la torta e la #<F#56> macchina rossa <inspiration>#


p2F#56:  #<G#55> cio� praticamente# in mezzo #<G#57> se tiri una diagonale#


p1G#57:  #<F#56> {[screaming] okay allora} tu passa# sotto il pettine


<sp>


p2F#58:  sotto il #<G#59> pettine <lp> <eh>#


p1G#59:  #<F#58> sotto il pettine# <inspiration> <eeh> arrivi alla macchina
         rossa <sp> e le fai il giro da sotto sopra


<sp>


p2F#60:  da sotto sopra {[whispering] quindi passando cos� #<G#61> <sp> vado
         sempre}#


p1G#61:  #<F#60> okay fermati sopra# la macchina rossa <inspiration>


p2F#62:  {[whispering] mi #<G#63> fermo}#


p1G#63:  #<F#62> {[screaming] hai}# <sp> a destra sopra la torta <sp> un'altra
         macchina rossa ?


<sp>


p2F#64:  {[screaming] blu}


<sp>


p1G#65:  blu ?


<sp>


p2F#66:  c'ho una macchina #<G#67> blu <sp> {[screaming] e un orologio}#


p1G#67:  #<F#66> okay okay <lp># okay <sp> allora <sp> io tra la torta e la tua
         macchina blu ho una macchina rossa

<lp>


p2F#68:  s� che probabilmente sar� il mio #<G#69> orologio#


p1G#69:  #<F#68> che# sar� il tuo #<F#70> orologio#


p2F#70:  #<G#69> che dista# praticamente<ee> un tre <sp> <tongue-click> <sp>
         #<G#71> tre centimetri {[screaming] quattro dal}#


p1G#71:  #<F#70> {[screaming <ss>s� s� � proprio praticamente}# a met�


<sp>


p2F#72:  <eh> #<G#73> okay <sp> <ah> s� {[whispering] certo che te non ce
         l'hai}#


p1G#73:  #<F#72> tra una e l'altra <sp> okay <inspiration> allora tu# dalla
         macchina rossa a sinistra


p2F#74:  s�


p1G#75:  devi tornare al<ll> <eeh> <sp> allo+ / devi fare <sp> <eeh> devi
         tornare al tuo orologio passando da sotto


<sp>


p2F#76:  okay


<sp>


p1G#77:  okay ? <sp> e ti fermi sopra l'orologio


<lp>


p2F#78:  okay mi fermo sopra #<G#79> l'orologio#


p1G#79:  #<F#78> <ss>sopra l'orologio# <sp> okay <inspiration> ora il mio punto
         nero di arrivo <sp> #<F#80> �#


p2F#80:  #<G#79> s�#


p1G#81:  <ehm> <sp> rispetto alla tua macchina blu


<sp>


p2F#82:  s�


p1G#83:  <inspiration> un due centimetri a<aa> sinistra


<sp>


p2F#84:  #<G#85> s�#


p1G#85:  #<F#84> e# un tre o quattro centimetri <sp> in alto <lp> tu l� dovresti
         aver qualcosa credo


<lp>


p2F#86:  allora #<G#87> aspetta#


p1G#87:  #<F#86> o<oo># {[whispering] non lo so}


<lp>


p2F#88:  dunque il #<G#89> mio#


p1G#89:  #<F#88> cio� da# sopra l'orologio


<sp>


p2F#90:  #<G#91> s�#


p1G#91:  #<F#90> tu# ti sposti <inspiration> di<ii> tre centimetri a sinistra


p2F#92:  s�


p1G#93:  <inspiration> <eeh> <sp> facciamo<oo> cinque verso l'alto <lp> ma il
         puntino dovrebbe esser lo stesso #<F#94> no ?#


p2F#94:  #<G#93> s�# <sp> #<G#95> s� s� s�#


p1G#95:  #<F#94> okay#


<sp>


p2F#96:  <vocal> <eh> #<G#97> s� sono<oo> <sp> okay#


p1G#97:  #<F#96> sei arrivata ? adesso {<laugh> tocca# a me} <laugh>


p2F#98:  quindi dall'orologio sopra vado dritta al #<G#99> punto#


p1G#99:  #<F#98> esatto#

a questo punto si ha l'inversione tra p1 e p2: p2 diventa Giver e p2 Follower

<lp>


p2G#100:  {[whispering] vado al punto} <lp> okay {<NOISE> <inspiration>} allora
          <sp> <inspiration> al punto tu hai una farfalla ?


p1F#101:  <nn>no


<sp>


p2G#102:  non hai una farfalla allora sopra <lp> la mia macchinina blu<uu>


p1F#103:  #<G#104> s�<ii>#


p2G#104:  #<F#103> hai# presente dov'era ? c'� una farfalla allora il punto nero


<sp>


p1F#105:  s�


p2G#106:  <tongue-click> <sp> in parte alla #<F#107> farfalla#


p1F#107:  #<G#106> okay#


p2G#108:  <eeh> tu c'hai<ii> s+ / <uu>un disegnino l�<ii>


p1F#109:  <tongue-click> non ho nulla


<sp>


p2G#110:  non hai nulla <tongue-click> hai solo il punto nero


p1F#111:  esatto


p2G#112:  <inspiration> allora #<F#113> <ehm> <lp> {[screaming] verso}#


p1F#113:  #<G#112> 'spetta hai de+ / hai de+ <sp> s� <sp> okay# okay <sp>
          #<G#114> vai#


p2G#114:  #<F#113> dunque# <lp> <tongue-click> a destra leggermente in diagonale


<sp>


p1F#115:  #<G#116> s�#


p2G#116:  #<F#115> di un# <lp> cinque centimetri hai qualcosa ? io ho un camion


<lp>


p1F#117:  s� esatto <sp> anch'io ho un camion


p2G#118:  ecco allora ti dirigi passando da sotto


<lp>


p1F#119:  #<G#120> s�#


p2G#120:  #<F#119> verso# il camion e fermati #<F#121> sul lato#


p1F#121:  #<G#120> <tongue-click> la farfalla# la lascio stare ?


p2G#122:  #<F#123> s� <unclear> s� {[whispering] perch� se il punto nero# � in
          comune non avevo pensato a questo} <inspiration> ti fermi in parte


p1F#123:   #<G#122> okay <sp> va bene# <P> <tongue-click> al #<G#124> camion#


p2G#124:  #<F#123> sulla destra# al camion


<sp>


p1F#125:  okay


<sp>


p2G#126:  <tongue-click> poi <ehm> <breath> <inspiration> in cima al foglio
          sulla sinistra rispetto al camion <sp> #<F#127> io ho un dado#


p1F#127:  #<G#126> <tongue-click> un dado# <sp> #<G#128> anch'io#


p2G#128:  #<F#127> perfetto# allora ci passi sopra


<sp>


p1F#129:  s�


p2G#130:  e ti fermi <inspiration> <eeh> sopra il dado


p1F#131:  okay


<sp>


p2G#132:  okay <inspiration> {[screaming] io} a destra del foglio ho un lupo


<lp>


p1F#133:  a destra ?


p2G#134:  tip+ / no scusami #<F#135> a sinistra <tongue-click> ho un lupo#


p1F#135:  #<G#134> a sinistra anch'io con dietro il bosco#


p2G#136:  <inspiration> okay a <sp> <ehm> <inspiration> e in parte al lupo sulla
          destra tu hai per caso un paio di occhiali ?


<sp>


p1F#137:  s�


p2G#138:  <inspiration> #<F#139> ecco#


p1F#139:  #<G#138> <eeh># i+ / un pochino in #<G#140> basso rispetto al lupo
          <inspiration> okay#


p2G#140:  #<F#139> un pochino in basso rispetto al <vocal> al# p+/ <aa>l lupo
          <inspiration> allora qua praticamente co' la matita


p1F#141:  #<G#142> s�<ii>#


p2G#142:  #<F#141> tu dovresti# dirigerti


p1F#143:  #<G#144> s�#


p2G#144:  #<F#143> <ehm># <tongue-click> leggermente <ss>sopra <sp> il paio di
          occhiali rimani <inspiration> a un centimetro #<F#145> <sp> dalle
          stanghette#


p1F#145:  #<G#144> okay io ho <sp> <eeh># tra il dado e il lupo ho una
          bicicletta <tongue-click> in diagonale <sp> #<G#146> passer� sotto ?#


p2G#146:  #<F#145> ecco passaci# sotto #<F#147> se tu qua hai la bicicletta#


p1F#147:  #<G#146> okay <lp> va bene#


<sp>


p2G#148:  passi sotto e ti rimani a un centimetro dal <eeh> / dagli #<F#149>
          occhiali#


p1F#149:  #<G#148> okay#


<lp>


p2G#150:  <inspiration> quando sei arrivata l� ti dirigi sopra al lupo


<sp>


p1F#151:  <ss>s�


<lp>


p2G#152:  ci passi intorno {[whispering] quindi passando andando a #<F#153>
          sinistra}#


p1F#153:  #<G#152> <ss>s�#


<sp>


p2G#154:  ci passi intorno <inspiration> {[screaming] poi} <eeh> ti dirigi
          praticamente a un mezzo centimetro sotto degli occhiali <lp> passando
          sotto gli occhiali


<sp>


p1F#155:  <ss>s�


<lp>


p2G#156:  <tongue-click> ecco e ti fermi l� <inspiration> poi tu hai un <ehm>
          <sp> una massa stellare #<F#157> non so come chiamarla# ecco perfetto
          <inspiration> allora alla massa stella+ ti dirigi sopra la massa
          stellare per�


p1F#157:  #<G#156> s�# <P> #<G#158> s�#


p2G#158:  #<F#157> devi# <sp> l+ / <eeh> la tua matita deve passare
          <inspiration> <ehm> <lp> <tongue-click> dunque allora c'� una stella
          grossa giusto ? <sp> a sinistra ? <sp> c'� <lp> pi� grossa poi una
          piccolina e poi una ancora grossa


<sp>


p1F#159:  #<G#160> s�#


p2G#160:  #<F#159> giusto ?# <inspiration> ecco <sp> passi praticamente tra la
          stella grossa a sinistra <sp> e quella piccolina in centro


<lp>


p1F#161:  no aspetta allora {[screaming] <ah> ! okay}


p2G#162:  okay ? #<F#163> la stella grossa a sinistra la prima#


p1F#163:  #<G#162> okay <inspiration> <eeh> passando dall'alto# ? #<G#164> cio�
          ci passo in mezzo ?#


p2G#164:  #<F#163> dall'alto s� perch� tu vieni# dagli occhiali


<lp>


p1F#165:  {[whispering] okay} perch� io tra<aa> la costellazione e il lupo ho un
          pettine <sp> {[whispering] me ne frega niente ?} <lp> #<G#166> cio� io
          dagli# occhiali scendo gi� in linea praticamente #<G#166> retta ?#


p2G#166:  #<F#165> <eeh># <P> #<F#165> {[screaming] no}# t+ tu tu dal / s� dagli
          occhiali s� praticamente passi sotto gli occhiali praticamente
          <inspiration> <sp> percorrili tutti gli occhiali <inspiration> cio�
          colla linea passa <sp> <eeh> come si chiama


p1F#167:  #<G#168> s� s� s� s� s�#


p2G#168:  #<F#167> percorri tutti gli occhiali e poi quando# li hai finiti
          scendi in linea retta


<lp>


p1F#169:  {[whispering] okay}


<lp>


p2G#170:  e passi praticamente #<F#171> tra la<aa> / tra la st+#


p1F#171:  #<G#170> a s+ / che sono# tra la prima e la seconda stella a #<G#172>
          sinistra#


p2G#172:  #<F#171> tra la# prima e la seconda stella a #<F#173> sinistra#


p1F#173:  #<G#172> okay#


p2G#174:  <inspiration> poi scendi e ti ritrovi in mezzo alle due centrali
          giusto ? <lp> <tongue-click> <eeh> in mezzo alle due<ee> <sp> ti
          ritrovi praticamente in mezzo


p1F#175:  in mezzo alla #<G#176> costellazione ?#


p2G#176:  #<F#175> <ss>s�# pi� #<F#177> o meno#


p1F#177:  #<G#176> <ah> ! allora# aspetta ho capito <sp> perch� io devo averla f+
          / {[whispering] no <sp> forse � messa in un modo #<G#178> sbagliato la
          costellaz+}#


p2G#178:  #<F#177> a meno che l'abbiamo# #<F#179> diversa#


p1F#179:  #<G#178> <inspiration> {[screaming] cio� io}# l'ho messa come<ee>
          <inspiration> una<aa> / un sorriso praticamente


p2G#180:  <tongue-click> <eh> vedi #<F#181> allora � un po' dive+#


p1F#181:  #<G#180> {<NOISE> con}# <inspiration> cio� ho la parte<ee> <ehm>
          <inspiration> come una ci+ / come una tazza un po' storta


p2G#182:  #<F#183> <ah> be' s� no � uguale#


p1F#183:  #<G#182> tu inv+ / cio� concava verso# l'alto <sp> hai #<G#184>
          il<ll>#


p2G#184:  #<F#183> s� no# � uguale #<F#185> uguale allora#


p1F#185:  #<G#184> okay <sp> okay# okay okay <lp> #<G#186> {[whispering] va
          bene}#


p2G#186:  #<F#185> vabbe' quando# vabb� quando hai passato tra la <sp> la prima
          e la #<F#187> seconda a sinistra#


p1F#187:  #<G#186> <ss>s� <breath>#


<sp>


p2G#188:  <inspiration> <eeh> ti dirigi sempre verso il basso <sp> <inspiration>
          passi a sinistra di quella centrale praticamente ce n'� una centrale
          giusto ?


<lp>


p1F#189:  #<G#190> s� <sp> passo a sinistra di quella in#


p2G#190:  #<F#189> una stella pi� o meno centrale <inspiration> passi a
          sinistra# e vai in gi� e passi in mezzo


<lp>


p1F#191:  #<G#192> s�#


p2G#192:  #<F#191> alle due pi�# grandi alla seconda e alla terza partendo da
          sinistra <P> <eh> ! {<laugh> {<NOISE> quest+} <inspiration> <vocal>
          <inspiration>} #<F#193> {[screaming] allora}#


p1F#193:  #<G#192> {<laugh> aspetta}# allora io ti dico #<G#194> come ho# 'ste
          stelle qua perch� <inspiration> <ehm> <sp> <tongue-click> <eeh> cio�
          <sp> al+ / innanzitutto sono tre tre sei <inspiration> sette otto
          stelle


p2G#194:  #<F#193> s�# <P> {[whispering] una due tre quattro cinque sei} sette
          otto #<F#195> s�#


p1F#195:  #<G#194> okay# <inspiration> poi <unclear> alla mia sinistra


p2G#196:  s�


p1F#197:  <tongue-click> io ne ho <sp> una<aa> mezzanella <sp> una<aa> <sp> un
          po' pi� #<G#198> piccola e una mezzanella#


p2G#198:  #<F#197> {<laugh> mezzanella}# <P> tutte sulla sini+ / non ne hai
          <inspiration> tre in fi+ / tre partendo #<F#199> dall'alto non ne hai
          tre in fila#


p1F#199:  #<G#198> <inspiration> nel senso io no+# <P> partendo dall'alto tre in
          #<G#200> fila#


p2G#200:  #<F#199> nel sento# ve+ venendo gi� dagli occhiali


p1F#201:  <eh>


p2G#202:  <inspiration> non ne hai in orizzontale


<sp>


p1F#203:  no ecco perch� le mie stelle sono spostate <inspiration> cio� non ho
          orizzontale <sp> verticale verticale


<sp>


p2G#204:  #<F#205> no neanch'io quello#


p1F#205:  #<G#204> ma ho obliquo obliquo# obliquo


p2G#206:  s� anche le mie sono un po' in #<F#207> obliquo#


p1F#207:  #<G#206> <ah> okay# <inspiration> ma venendo gi� dagli #<G#208>
          occhiali#


p2G#208:  #<F#207> venendo# gi� dagli occhiali tu passi <sp> in mezzo <sp> alle
          due stelle pi� a sinistra


p1F#209:  <ss>s� <inspiration> per� se io passo in mezzo a quelle


p2G#210:  #<F#211> <eh>#


p1F#211:  #<G#210> mi# ritrovo fuori


<sp>


p2G#212:  ti ritrovi #<F#213> fuori ?#


p1F#213:  #<G#212> dalla# #<G#214> costellazione#


p2G#214:  #<F#213> <ah># quindi � diverso


p1F#215:  <eh> s� perch� tu l'avrai <inspiration> ho capito tu come ce l'hai
          <sp> io ho <sp> la parte sinistra pi� bassa di quella destra


<sp>


p2G#216:  <tongue-click> s� pure io quello 


<lp>


p1F#217:  e allora se io ci passo in mezzo come faccio ad #<G#218> uscire<ee>#


p2G#218:  #<F#217> scu+# / da #<F#219> me se ti#


p1F#219:  #<G#218> <inspiration> allora# devo passar sotto ? <sp> in mezzo ma da
          sotto


<sp>


p2G#220:  {[whispering] in mezzo ma da sotto} <sp> da me<ee> cadi proprio cio�
          se vieni gi� in par+ / in <ehm> <inspiration> in verticale dagli
          occhiali <inspiration> <sp> ci passi in mezzo alla costellazione <lp>
          capito ?


p1F#221:  s� <sp> per� io <sp> io una volta che ci son passata in mezzo mi
          ritrovo dall'altra parte verso<oo>


<lp>


p2G#222:  s� <ah> ma ho capito t+ {<laugh> <unclear> sei sei gi� uscita tu
          <inspiration> <sp> Mari vai piano} quando tu sei passata in mezzo alle
          due #<F#223> stelle#


p1F#223:  #<G#222> s�<ii>#


<sp>


p2G#224:  ti ritrovi <inspiration> in mez+ / cio� <tongue-click> la
          costellazione non � fatta solo da una striscia quindi se passi solo in
          mezzo alle prime due stelle ti ritrovi in mezzo alla costellazione
          <inspiration> non attraversarla tutta passa #<F#225> solo le <sp> la
          prima fila#


p1F#225:  #<G#224> <eh> no <mm>ma per quello# <sp> perch� la mia parte destra


p2G#226:  <tongue-click> <eh>


p1F#227:  � pi� in alto della sinistra


<sp>


p2G#228:  ma tanto tu passi a sinistra <sp> cio� <inspiration> la linea � pi� a
          sinistra <lp> o no ? <lp> cio� la tua linea passa <tongue-click>
          <inspiration> pi� #<F#229> a sinist+ / verso sinistra della {<laugh>
          costellazio+}#


p1F#229:  #<G#228> <laugh> {<laugh> scusami <unclear>}# <laugh>


p2G#230:  <laugh> <sp> no vai tra vai tra #<F#231> <inspiration> allora#


p1F#231:  #<G#230> <laugh># fai cos� <inspiration> la mia<aa> / cio� tipo la mia
          costellazione  � una specie di #<G#232> luna no ?#


p2G#232:  #<F#231> <inspiration <ah># <sp> ho trovato come dirtelo facciam
          #<F#233> cos�#


p1F#233:  #<G#232> <eh> !# dai <sp> #<G#234> dimmi#


p2G#234:  #<F#233> praticamente# alla tua #<F#235> sinistra <P> <eh> infatti �
          quello che ti stavo dicendo <inspiration> alla tua sinistra#


p1F#235:  #<G#234> fai cos� contiamo da sinistra a destra le stelle uno due tre
          quattro cinque sei# sette otto <inspiration> #<G#236> okay#


p2G#236:  #<F#235> alla# sinistra della linea <sp> <tongue-click> lascia tre
          stelle <lp> <tongue-click> alla sinistra della linea deve es+ / devono
          esserci tre stelle che sono due grandi e una<aa> #<F#237>
          {[whispering] media}#


p1F#237:  #<G#236> pi� <mh># pochino pi� #<G#238> piccola non troppo#


p2G#238:  #<F#237> po_chino pi� piccola# s�


<lp>


p1F#239:  {[whispering] <ah>} okay <sp> fatto


p2G#240:  e dall'altra parte <sp> da questo punto #<F#241> ne rimangon' cinque#


p1F#241:  #<G#240> uno due tre quattro e# cinque <sp> s�


<sp>


p2G#242:  che sono un gruppettino <sp> in cima


<sp>


p1F#243:  s�<ii>


<sp>


p2G#244:  e due sotto una *grossona grossa #<F#245> grossa e u+#


p1F#245:  #<G#244> esatto# #<G#246> brava#


p2G#246:  #<F#245> {[screaming] bona [dialect]} ci# siam #<F#247> dentro#


p1F#247:  #<G#246> <unclear># perch� mi avevi detto di passar tra la prima e la
          seconda a #<G#248> sinistra#


p2G#248:  #<F#247> e perch� era tra la prima# e la seconda a sinistra


<sp>


p1F#249:  #<G#250> {[whispering] e da me no}#


p2G#250:  #<F#249> questa � la destra questa � la# sinist+


<lp>


p1F#251:  {[whispering] per� fa niente} <laugh> #<G#252> okay#


p2G#252:  #<F#251> vabbe' <inspiration> non ci pensiamo# {[whispering] ho la
          lateralit� poco sviluppa+} <inspiration> vabbe' <inspiration> <eeh>
          poi praticamente <lp> <tongue-click> adesso sei tra <sp> la stella
          grossa vi+ sei vicino alla stella #<F#253> grossa grossa giusto#


p1F#253:  #<G#252> s� <sp> esatto#


p2G#254:  ecco vai leggermente a destra di<ii> un centimetro e mezzo <sp>
          {[whispering] e quello � l'arrivo}


<lp>


p1F#255:  {[whispering] okay}


<sp>


p2G#256:  bona [dialect] <sp> arrivati
