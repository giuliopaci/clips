TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            03
Ntr:
Nls:
REG:            C


SPEAKERS_inf.

INp1:  F. C., M, 18, Cagliari, spontaneo, fluente
INp2:  N. P., M, 21, Cagliari, spontaneo, fluente


RECORDING_inf.

TYP:	DAT
LOC:	Cagliari/universit�
DAT:	23/10/01
DUR:	10.15,735
CON: 	buone


TRANSCRIPTION_inf.

DAT:  29/01/02
CMT:
Nst:  282


p2#1:  #<p1#2> vai#


p1#2:  #<p2#1> Nico ?#


<sp>


p2#3:  dimmi


<lp>


p1#4:  <tongue-click> prendiamo il tetto della casa


p2#5:  perfetto vai


<lp>


p1#6:  <inspiration> io ho <sp> due comignoli <sp> #<p#2#7> al centro# <sp>
       #<p2#7> tu ce li hai ?#


p2#7:  #<p1#6> <ss>s�# <sp> #<p1#6> <ss>s�# <sp> s� tutti e due


<lp>


p1#8:  di che forma ?


<sp>


p2#9:  <inspiration> c'hanno il triangolino e il cili+ e #<p1#10> il<ll> coso
       s+#


p1#10:  #<p2#9> il ciclindretto# #<p2#11> sotto o_kay#


p2#11:  #<p1#10> s� sotto# <lp> l'antenna com'� ?


<sp>


p1#12:  l'antenna �<ee> <ehm> <sp> <vocal> come <vocal> <sp> due -Ti-
        sovrapposte una pi� in basso una pi� in #<p2#13> alto#


p2#13:  #<p1#12> perfetto# ce l'ho anche io


<sp>


p1#14:  #<p2#15> o_kay dai#


p2#15:  #<p1#14> <inspiration> allora# scendi gi� le finestre quante ne hai ? la
        la prima fila orizzontale


p1#16:  {[whispering] uno due tre} <sp> cinque ?


<sp>


p2#17:  la seconda ?


<sp>


p1#18:  due


p2#19:  la terza ?


p1#20:  una


<sp>


p2#21:  o_kay l'antenna della <rr>radio e del televisore ?


<lp>


p1#22:  <tongue-click> arriva fim+ fino alle<ee> due<ee>


p2#23:  o_kay perfetto


<sp>


p1#24:  aspe+ <lp> <breath> chi c'hai raffigurato tu<uu> nella radio<oo>
        {[whispering] boh 'na #<p2#25> specie}#


p2#25:  #<p1#24> boh 'na# specie di cane


<lp>


p1#26:  {[whispering] s�<ii> ?}


<lp>


p2#27:  #<p1#28> {[whispering] s�}#


p1#28:  #<p2#27> i# tasti come sono sono otto neri ?


<sp>


p2#29:  sono otto neri <sp> e tre bianchi sotto


<lp>


p1#30:  {[whispering] s�<ii>}


<sp>


p2#31:  <inspiration> e lo+ e il<ll> tipo l'orecchio come ce l'ha c'ha due<ee> /
        si vede proprio il lobo ? <lp> il il #<p1#32> padi+#


p1#32:  #<p2#31> come ?#


<sp>


p2#33:  <inspiration> l'orecchio del tipo com'� ?


<lp>


p1#34:  normale


<lp>


p2#35:  c'ha due linee <vocal> curve insomma <sp> una dentro l'altra


<lp>


p1#36:  {[whispering] s�} <sp> #<p2#37> {[whispering] meno male}#


p2#37:  #<p1#36> o_kay# <sp> <inspiration> allora <vocal> {[screaming] la
        statua}


<lp>


p1#38:  {[whispering] prendiamo la statua eia [dialect]}


<sp>


p2#39:  allora il cappello <sp> ce l'ha ?


<lp>


p1#40:  {[whispering] s�}


<lp>


p2#41:  <inspiration> e con la visierina


<lp>


p1#42:  visierina dove ?


<sp>


p2#43:  sopra il naso


<lp>


p1#44:  {[whispering] visierina<aa>} <sp> cio� tipo cappellino ?


<sp>


p2#45:  <eh>


<sp>


p1#46:  o_kay


<sp>


p2#47:  <inspiration> la spada ce l'ha ?


<lp>


p1#48:  {[whispering] <ss>s�} <sp> come � fatta la tua spada ?


<sp>


p2#49:  � lunga sino a quasi alla fine del foglio


<lp>


p1#50:  {[whispering] s� � <sp> che #<p2#51> forma# ha ?}


p2#51:  #<p1#50> {[whispering] <mh>}# <lp> boh una #<p1#52> di spada#


p1#52:  #<p2#51> normale# spada


<sp>


p2#53:  <inspiration> le orecchie del cavallo ci sono


<lp>


p1#54:  {[whispering] le orecchie ? <sp> <ss>s�}


<lp>


p2#55:  tutti e due okay


<sp>


p1#56:  gli occhi del cavallo ci #<p2#57> sono ?#


p2#57:  #<p1#56> no# da te ?


<lp>


p1#58:  {[whispering] no}


<lp>


p2#59:  <inspiration> okay e<ee> allora cosa prendiamo ?


<sp>


p1#60:  la macchina


<sp>


p2#61:  la macchina lo specchietto ce l'ha ?


<sp>


p1#62:  <tongue-click> s�


<lp>


p2#63:  o_kay <inspiration> #<p1#64> i due fa+#


p1#64:  #<p2#63> ci si# vede il tettuccio da #<p2#65> te<ee> ?#


p2#65:  #<p1#64> no# da te ?


<sp>


p1#66:  {[whispering] no}


<sp>


p2#67:  {[whispering] neanche da me} <inspiration> i<ii>


<sp>


p1#68:  i fari <sp> #<p2#69> prendiamo i fari#


p2#69:  #<p1#68> i fari tutti# e due ce li ha <sp> normali senza cose dentro
        <eh>


<lp>


p1#70:  {[whispering] e le ruote ?}


<lp>


p2#71:  le ruote<ee> normali <lp> tranne quella di sinistra che un po' non si
        vede la fine


<lp>


p1#72:  {[whispering] s�} <sp> e la / quella ruota che si vede ?


<lp>


p2#73:  la #<p1#74> ruota che s+#


p1#74:  #<p2#73> c'ha due# cerchi ?


<sp>


p2#75:  s� dentro eia [dialect]


<sp>


p1#76:  uno grande e uno #<p2#77> {[whispering] piccolo}#


p2#77:  #<p1#76> {[whispering] <ss>s� s�}# <lp> <inspiration> {[screaming] sopra
        la ruota} <sp> #<p1#78> a sinistra#


p1#78:  #<p2#77> {[whispering] s�}# <lp> c'� un<nn> pezzettino #<p2#79> nero#


p2#79:  #<p1#78> nero# <lp> okay #<p1#80> {<laugh> anche quello} <inspiration>
        <eeh># ascolta il cane


p1#80:  #<p2#79> {[whispering] <laugh> <unclear> ridi}# <lp> asp�' il tetto
        della casa


<lp>


p2#81:  <eh>


<sp>


p1#82:  c'ha le #<p2#83> strisce ?#


p2#83:  #<p1#82> s�# il tuo ?


<lp>


p1#84:  #<p2#85> {[whispering] s� s� s�}#


p2#85:  #<p1#84> {[whispering] <eh> okay}# <inspiration> #<p1#86> as+#


p1#86:  #<p2#85> il cane#


<sp>


p2#87:  il cane <sp> <vocal> il / ha il collo strano sembra che gli manchi un
        pezzo di co+ di collare


<lp>


p1#88:  {[whispering] s� boh}


<lp>


p2#89:  {[whispering] <mh>


p1#90:  {[whispering] s� s�}


<sp>


p2#91:  <inspiration> ascolta la statua


<lp>


p1#92:  <eh>


p2#93:  alla alla base


<lp>


p1#94:  c'ha una scritta da te ?


<sp>


p2#95:  no da te ?


p1#96:  {[whispering] no}


<sp>


p2#97:  <inspiration> e c'ha un cerchio al centro {[screaming] <uu>un quadra+ un
        mezzo quadrato dentro ?}


<lp>


p1#98:  {[whispering] s�}


<sp>


p2#99:  {[whispering] <mh> <unclear> boh} <sp> <inspiration> #<p1#100> <eeh>#


p1#100:  #<p2#99> <inspiration> aspetta# un secondo <eeh> <lp> gi� del foglio a
         destra


<sp>


p2#101:  s� ci son sei punti / no <lp> sette punti


<sp>


p1#102:  sette puntini ?


p2#103:  {[whispering] neri}


<lp>


p1#104:  secondo me dobbiamo vedere pi� le forme <lp> perch� magari<ii>
         #<p2#105> il contenuto � lo stesso#


p2#105:  #<p1#104> <inspiration> <eh> perch� c'� scritto# differente
         orientamento spaziale degli oggetti <sp> <inspiration> capito ? quindi
         magari i comignoli possono essere pi� a si+ / pi� a destra o pi� a
         sinistra cos� come<ee>


<sp>


p1#106:  <eh> <sp> i comignoli sono<oo> al centro tra<aa> la<aa> cosa e l'albero
         ? <lp> tra l'antenna e #<p2#107> l'albero ?#


p2#107:  #<p1#106> <eh> un p+# s� <sp> al centro un po' un poco poco pi� a
         sinistra ma #<p1#108> non tanto#


p1#108:  #<p2#107> {[whispering] s� s�# okay okay}


<sp>


p2#109:  <inspiration> ascolta il / la panchina ha tutte le linee ? <sp>
         verticali <sp> oblique


<sp>


p1#110:  e la panchina ?


<sp>


p2#111:  a tutte / dietro la panchina / ci son tutte linee verticali #<p1#112>
         oblique#


p1#112:  #<p2#111> <ss>s�#


<lp>


p2#113:  #<p1#114> o_kay#


p1#114:  #<p2#113> <tongue-click> ma# tutte tratteggiate fino all'ultimo oppure
         le prime due sono<oo> <lp> non sono intere ?


<lp>


p2#115:  s� esatto la la seconda non arriva sino alla fine


<sp>


p1#116:  e la prima ?


<sp>


p2#117:  nemmeno


<lp>


p1#118:  {[whispering] o_kay} <lp> e l'albero ?


<lp>


p2#119:  <eh> l'albero vai vai capiscilo <sp> � pieno di curve <inspiration>
         {[screaming] l'albero / il tronco � curvo ?}


<sp>


p1#120:  {[whispering] s� verso<oo> destra}


p2#121:  {<laugh> c+ s� non lo so allora}


<lp>


p1#122:  <inspiration> il tipo


<sp>


p2#123:  <eh> <lp> � seduto con le gambe incrociate


<lp>


p1#124:  {[whispering] <ss>s�} e <vocal> i lacci delle scarpe ?


<sp>


p2#125:  i lacci delle scarpe sono<oo> tipo<oo> hai presente le c+ le corde di
         una chitarra ? <sp> gli accordi


p1#126:  {[whispering] <ss>s� chiaro} <lp> #<p2#127> son# tre


p2#127:  #<p1#126> <eeh># <lp> s� <lp> con una linea al centro che passa / che
         <vocal> #<p1#128> scende# verso gi� poi un po'


p1#128:  #<p2#127> {[whispering] s�}# <lp> verso destra


<sp>


p2#129:  {[whispering] <ss>s�} <sp> <inspiration> il cane sta sorridendo


<lp>


p1#130:  {[whispering] boh s�}


<sp>


p2#131:  {[whispering] <eh>} <sp> <inspiration> <eeh> a<aa> / il cane ha le
         dita<aa>


<lp>


p1#132:  quante dita ha ? <sp> #<p2#133> due#


p2#133:  #<p1#132> due#


<lp>


p1#134:  {[whispering] <ah> o_kay} <sp> #<p2#135> la coda ?#


p2#135:  #<p1#134> <eeh> la# coda � all'in s� verso un po' a s+ / a destra <lp>
         arricciolata un po' verso destra


<sp>


p1#136:  <inspiration> arricciolata ?


<sp>


p2#137:  non arricciolata aspetta <inspiration> fai conto #<p1#138> che va<aa>#


p1#138:  #<p2#137> � dritta# la coda


<lp>


p2#139:  non � dritta


<sp>


p1#140:  non � dritta ?


<sp>


p2#141:  � un po' il / � verso l'alto <lp> gira verso l'altro


<sp>


p1#142:  okay allora quello � uno


<lp>


p2#143:  perfetto


<sp>


p1#144:  {[whispering] cosa facciamo<oo> <vocal>}


<lp>


p2#145:  #<p1#146> <eh># continua ce ne #<p1#146> sono / chiss� quanti ce ne#
         sono <lp> #<p1#146> <eeh>#


p1#146:  #<p2#145>{[whispering] boh}# <lp> #<p2#145> {[whispering] o_kay dai}#
         <lp> #<p2#145> dunque# le #<p2#147> scarpe#


p2#147:  #<p1#146> {[screaming] aspetta# aspetta} il<ll> / la panchina


<sp>


p1#148:  o_kay


<sp>


p2#149:  la la {<NOISE> gamba} della #<p1#150> {<NOISE> panchina}#


p1#150:  #<p2#149> <ss>s�#


<sp>


p2#151:  come ce l'hai {<NOISE> tu} ?


<lp>


p1#152:  come ce l'ho<oo> ?


<sp>


p2#153:  <eh> cio� la mia va / � tutta verticale <inspiration> e poi all'ultimo
         momento alla fine gira un po' verso sinistra


<sp>


p1#154:  {[whispering] <ss>s� okay}


<sp>


p2#155:  {[whispering] o_kay}


<lp>


p1#156:  <inspiration> il praticello


<lp>


p2#157:  <eh>


<sp>


p1#158:  sotto<oo> la statua


<sp>


p2#159:  s�<ii>


<lp>


p1#160:  ce l'hai tu ?


<sp>


p2#161:  {[whispering] s� s�}


<sp>


p1#162:  ma c'�<ee> vicino qualcosa ?


<lp>


p2#163:  cio� ?


<lp>


p1#164:  nel praticello vedi qualcosa di strano che magari io / da me non c'�


<lp>


p2#165:  no no c'� tutto il praticello che fa<aa> tipo <unclear> va into+ /
         sembra che vada intorno alla statua


<lp>


p1#166:  quanta erba c'� <laugh>


<sp>


p2#167:  {[whispering] <vocal> un grammo <laugh> <vocal>}


<lp>


p1#168:  {<laugh> lo specchietto della macchina}


<lp>


p2#169:  {<laugh> <inspiration> <vocal> lo specchietto della macchina} � normale


<lp>


p1#170:  {[whispering] <laugh> e va <laugh>}


<lp>


p2#171:  <eeh> aspetta dai <ehm>


<lp>


p1#172:  {[whispering] dunque}


p2#173:  {[screaming] ce l'ha la sella} il tuo cavallo ?


<sp>


p1#174:  #<p2#175> come ?#


p2#175:  #<p1#174> ce l'ha la# sella il tuo #<p1#176> cavallo#


p1#176:  #<p2#175> la# stella ?


p2#177:  la <ss>sella


<lp>


p1#178:  s� come � fatta ?


<lp>


p2#179:  {[whispering] <eh> boh vabb�} all'americana


<lp>


p1#180:  la classica sella #<p2#181> {<laugh> americana}#


p2#181:  #<p1#180> eia [dialect]# <sp> si si vede la mano sinistra che tiene un
         po' sopra capito al pomo sembra


<lp>


p1#182:  {[whispering] s�}


<sp>


p2#183:  <inspiration> #<p1#184> {[whispering] o_kay}#


p1#184:  #<p2#183> classica# americanata


<sp>


p2#185:  {[whispering] eia [dialect]} <lp> {[whispering] poi} <sp> <vocal> /
         le<ee> il ti+ <vocal> guarda il tipo / aspetta il televisore da te


<sp>


p1#186:  <eh>


<sp>


p2#187:  <inspiration> sembra #<p1#188> come un libro# sembra aperto come un
         #<p1#188> libro ? <sp> <mh>#


p1#188:  #<p2#187> <inspiration> o+# <lp> #<p2#187> aspetta aspetta# <laugh> la
         faccia del cagnolino <lp> #<p2#189> sta sorridendo#


p2#189:  <eeh> #<p1#188> <unclear> <ss>s�# c'hai gli occhiettini stupidi<ii>
         normali uno vicino all'altro


<sp>


p1#190:  {<laugh> <inspiration> i classici occhietti stupidi #<p2#191> normali}#


p2#191:  #<p1#190> <inspiration> ascolta# un'altra cosa allora il gomito del
         tipo


<sp>


p1#192:  <eh>


<sp>


p2#193:  ce l'hai presente ?


<sp>


p1#194:  s�<ii>


<sp>


p2#195: la / il p+ / la piegatura del braccio


<sp>


p1#196:  s� come � da te ?


<sp>


p2#197:  cio� c'� / ci sono due pieghettine di cui una � un po' arricciata e una
         verso gi�<uu> ?


<lp>


p1#198:  una verso gi� e l'altra<aa> #<p2#199> verso# su curvata


p2#199:  #<p1#198> <vocal># <lp> s� allora � #<p1#200> uguale#


p1#200:  #<p2#199> quante# dita c'ha<aa> il tipo ?


<sp>


p2#201:  quattro in tutto


<lp>


p1#202:  {[whispering] che si vedono}


<sp>


p2#203:  {[whispering] s� #<p1#204> s�}#


p1#204:  #<p2#203> e<ee># il<ll> pollice come � rivolto


p2#205:  <vocal> il pollice � sul terzo pulsante <sp> #<p1#206> <vocal>#


p1#206:  #<p2#205> <vocal># partendo da sinistra


<sp>


p2#207:  partendo da sinistra gi� per� #<p1#208> capito ?#


p1#208:  #<p2#207> s�<ii># chiaro


<sp>


p2#209:  {[whispering] o_kay}


<lp>


p1#210:  <ehm> <sp> {[whispering] <eh> s�}


p2#211:  <ehm> <lp> la scritta {[screaming] -A- trattino} -Pi- due ce l'hai tu ?
         <laugh> 


<lp>


p1#212:  {<laugh> no c'ho quella -Pi- uno}


<sp>


p2#213:  davvero ? <lp> #<p1#214> c'hai -A-Pi-# uno tu ?


p1#214:  #<p2#213> {[whispering] s�}# <lp> {[whispering] <eh> beh} <sp>
         #<p2#215> {[whispering] son diversi}#


p2#215:  #<p1#214> allora � una differenza# anche quella


<sp>


p1#216:  {<laugh> s� beh classico} <inspiration>


<lp>


p2#217:  {<laugh> due differenze}


<lp>


p1#218:  ma<aa> <lp> <tongue-click> il cane no ?


<sp>


p2#219:  il cane #<p1#220> <vocal>#


p1#220:  #<p2#219> il naso# come #<p2#221> ce l'ha ?#


p2#221:  #<p1#220> alla fin# fine ce l'ha nero


<lp>


p1#222:  con un trattino bianco #<p2#223> dentro#


p2#223:  #<p1#222> s�<ii># <sp> una specie di mezzaluna


<lp>


p1#224:  chiaro


<sp>


p2#225:  {[screaming] e l'occhio ?}


<sp>


p1#226:  <tongue-click> e l'occhio � un puntino nero


<sp>


p2#227:  un puntino nero {[screaming] sopra quella angolatura<aa> tra} il<ll> /
         la fronte e il muso


<lp>


p1#228:  {[whispering] boh s�<ii>}


<sp>


p2#229:  {[whispering]<mh>} <sp> le orecchie dritte tutte e due


<sp>


p1#230:  {[whispering] s�<ii>}


<sp>


p2#231:  <inspiration> tipo pastore tedesco


<lp>


p1#232:  {[whispering] s� il classico tipo boxer}


<sp>


p2#233:  <ah> <lp> ma <vocal> scusa <sp> <inspiration> <tongue-click> tu la vedi
         / {[whispering] <ah> s� che si vede un po' <breath> #<p1#234> no
         vabb�}#


p1#234:  #<p2#233> aspetta aspetta# aspetta il cane


<sp>


p2#235:  {[whispering] <mh>}


<lp>


p1#236:  sopra la bocca c'ha<aa> dei #<p2#237> pun_tini ?#


p2#237:  #<p1#236> s� i baffi# <lp> #<p1#238> quanti ne ha da te ?#


p1#238:  #<p2#237> <ah> son baffi# #<p2#239> quelli ?#


p2#239:  #<p1#238> s� quanti# ne ha da te ?


<sp>


p1#240:  sette


<lp>


p2#241:  {[whispering] s� anche da me}


<lp>


p1#242:  {[whispering] anche da te sette}


<lp>


p2#243:  sono <vocal> <sp> sei quasi uguali e uno piccolino<oo> a #<p1#244>
         sinistra#


p1#244:  #<p2#243> {[whispering] s� s�# s� s�}


<sp>


p2#245:  e allora non lo so


<sp>


p1#246:  #<p2#247> <inspiration> come s+#


p2#247:  #<p1#246> ascolta# la tua fines+ / allora e / hai tre finestre tu ?


<lp>


p1#248:  tre finestre #<p2#249> <vocal>#


p2#249:  #<p1#248> no scusami# sto dicendo una stronzata <inspiration> hai <sp>
         cinque finestre su <sp> due finestre e una


<sp>


p1#250:  s�<ii>


p2#251:  <ss>sotto la finestra c'� un puntino ?


<lp>


p1#252:  sotto<oo> la<aa> #<p2#253> seconda<aa># <lp> {[whispering] <vocal>
         <unclear>} <inspiration>


p2#253:  #<p1#252> s�<ii># bravo quella l� <lp> {[whispering] <ah> allora non �
         #<p1#254> niente}#


p1#254:  #<p2#253> il<ll># cio� c'ha<aa> occhio naso qualcosa insomma di
         delineato il cavallo sopra<aa>


<sp>


p2#255:  <vocal> no � tutto cie+ � c+ / sembra cieco c'ha un po' il<ll> sor+ /
         la boccuccia cos� pronunciata ma


<lp>


p1#256:  pronunciata ? <sp> si vede una boccuccia<aa> #<p2#257> oppure# il mento
         segue i classici menti da<aa> <sp> #<p2#257> {[whispering] cartone}#


p2#257:  #<p1#256> s+# <lp> #<p1#256> no ha<aa># <sp> <inspiration> come se
         avesse il labbro superiore pi� sporgente <lp> #<p1#258> ca+#


p1#258:  #<p2#257> {[whispering] sai disegnare# vecchio ?}


<lp>


p2#259:  {[whispering] io s�}


<lp>


p1#260:  {[whispering] e ti sembra il labbro {<laugh> superiore sporgente}
         quello ?}


<sp>


p2#261:  #<p1#262> {[whispering] allora no}#


p1#262:  #<p2#261> <inspiration> oppure# <sp> {[whispering] aspetta dai magari
         c'� que+ / #<p2#263> � quello}#


p2#263:  #<p1#262> {[whispering] <eh>}# <sp> cio� allora praticamente <sp>
         prendi il naso <sp> vedi che fa una specie di<ii> collinetta il naso ?


<lp>


p1#264:  #<p2#265> collinetta ?#


p2#265:  #<p1#264> <eh># vabb� come te lo posso #<p1#266> spiegare ?#


p1#266:  #<p2#265> <eh> il# naso


<sp>


p2#267:  <eh> <sp> <inspiration> poi scende


<lp>


p1#268:  {[whispering] <eh>}


<sp>


p2#269:  verso la bocca fa un trattino oriz+ / un<nn> po' ver+ / come te lo
         posso spiegare #<p1#270> fa un trattino#


p1#270:  #<p2#269> orizzont+#


<sp>


p2#271:  <eh> <sp> un #<p1#272> trattino#


p1#272:  #<p2#271> orizzontale#


<lp>


p2#273:  no non � orizzontale � un #<p1#274> trattino#


p1#274:  #<p2#273> c'ha la bocca# insomma o #<p2#275> no ?#


p2#275:  #<p1#274> <eh># s� {[screaming] ce l'ha ma � <vocal> piccolissima} si
         vede poco


<lp>


p1#276:  {[whispering] si vede poco la boccuccia ?}


<sp>


p2#277:  {[whispering] <ss>s�}


<sp>


p1#278:  {[whispering]e io non ce l'ho mi sa}


<lp>


p2#279:  {[whispering] e allora forse � una di+} forse non lo so <lp> #<p1#280>
         me lo diresti# gua+ sicuramente se <vocal> � una bocca si vede che c'ha
         la bocca l� <inspiration> perch� da te � tutta una linea continua scusa
         ?


p1#280:  #<p2#279> e i+# <lp> � una linea continua non si vede il tratteggio
         orizzontale rispetto all'altra linea


p2#281:  <ah> allora <unclear> � una differenza


<lp>


p1#282:  {[whispering] o_kay dai} <lp> <eeh>
