TEXT_inf.

MAT:            td
MAP:            B
TRM:
Ndl:            04
Ntr:
Nls:
REG:            C


SPEAKERS_inf.

INp1:  F. M., F, 19, Cagliari, spontaneo, fluente
INp2:  S. D., F, 19, Cagliari, spontaneo, fluente


RECORDING_inf.

TYP:  DAT
LOC:  Cagliari/universit�
DAT:  23/10/01
DUR:  16.13,368
CON:  buone


TRANSCRIPTION_inf.

DAT:  04/01/02
CMT:
Nst:  371


p2#1:  allora <breath>


<sp>


p1#2:  ascoltami un po'


<sp>


p2#3:  <eh> guardiamo le cose pi� #<p1#4> evidenti pi� {[whispering] grandi}
       <breath>#


p1#4:  #<p2#3> allora <sp> c'� l'uovo# <sp> {[creaky-voice] e} l'anatra


<lp>


p2#5:  l'uovo ?


<lp>


p1#6:  <tongue-click> <eh> quella specie di cosa #<p2#7> che c'� per te+#


p2#7:  #<p1#6> {<laugh> <breath> � un pallone}#


p1#8:  � un pallo+ #<p2#9> no � un uovo ecco vedi il mio vale <inspiration>#


p2#9:  #<p1#8> <laugh> <inspiration> okay per� ha la forma# di uovo hai
       ragione


<sp>


p1#10:  #<p2#11> okay#


p2#11:  #<p1#10> <NOISE> quante# palline ha <sp> nere ?


<lp>


p1#12:  cosa sempre l'uovo #<p2#13> pallone ?#


p2#13:  #<p1#12> s� <breath>#


p1#14:  {<laugh> <breath> okay tre}


p2#15:  <inspiration> {<laugh> okay} <inspiration> e le linee all'interno
        <sp> due ?


<lp>


p1#16:  {[creaky-voice] <ehm>} <tongue-click> aspetta <sp> ne ha due per�<oo>
        passano<oo> / cio� c'� 'na palla #<p2#17> in me+ c'� u+#


p2#17:  #<p1#16> s�# <sp> okay


<sp>


p1#18:  okay <inspiration> <ehm> <tongue-click> poi nell'anatra quante ce
        ne sono ?


<sp>


p2#19:  il salvagente ? #<p1#20> <breath>#


p1#20:  #<p2#19> <eh> !#


<sp>


p2#21:  <inspiration> allora <sp> uno {[whispering] due tre quattro ci+} <sp>
        <inspiration> cinque <breath>


<lp>


p1#22:  cinque palline nere ?


p2#23:  {[dialect] eia}


p1#24:  da me quattro


p2#25:  okay <breath>


<lp>


p1#26:  <eeh> poi cvediamo<oo> <ehm>} <sp> <inspiration> <vocal>
        <ehm> boh guarda com'� fatto il becco verso su ? <lp> c'ha il becco in
        su e la parte di gi� dritta praticamente <inspiration> #<p2#27> e s+#


p2#27:  #<p1#26> <nn>no# � nor+ <ehm> cio� normale hanno / sono dritti tutte e
        {[creaky-voice] due} <breath> <lp> il becco<oo> vabb� ha un po' /
        naturalmente quella parte di sopra pi�<uu> rivolta verso l'altro
        #<p1#28> ma niente di<ii>#


p1#28:  #<p2#27> okay# <sp> <tongue-click> l'occhio � normale guarda verso gi�
        la {[creaky-voice] palli+} / #<p2#29> cio� cerchietto <vocal>#


p2#29:  #<p1#28> <ss>s� <sp> <inspiration> il bambino# � triste
        � contento ?


p1#30:  triste


p2#31:  #<p1#32> o_kay <breath>#


p1#32:  #<p2#31> <inspiration> <tongue-click> e ha<aa># s� <sp> {[creaky-voice]
        triste capelli<ii>} c'ha questo #<p2#33> ciuffone#


p2#33:  #<p1#32> <laugh># {[whispering] alla Elvis} #<p1#34> <inspiration>#


p1#34:  #<p2#33> va bene# <inspiration> <sp> e<ee> #<p2#35> poi<ii> <unclear>#


p2#35:  #<p1#34> la la# <ll>la bocca sotto la bocca 'na fossettina ?


<sp>


p1#36:  s�


p2#37:  okay <sp> #<p1#38> ie+#


p1#38:  #<p2#37> l'ombelico# ce l'ha<aa> ?


<sp>


p2#39:  s� <sp> <laugh> <inspiration> e su ha una<aa> <ehm> <breath> <sp>
        <inspiration> non lo so la pieghettina della pe+ <vocal> non so come
        definirla sopra l'ombelico ?


<lp>


p1#40:  s� <breath>


<sp>


p2#41:  okay <sp> e poi ha un #<p1#42> specie di# <ehm> <lp> <tongue-click> ha
        un segno nelle / nel #<p1#42> braccio ?#


p1#42:  #<p2#41> pettorali# <sp> <laugh> <P> #<p2#41> s�# <lp> il sopracciglio
        <sp> ce #<p2#43> l'ha#


p2#43:  #<p1#42> ne ha# solo uno perch� si #<p1#44> vede praticamente# <breath>
        <sp> <inspiration> e poi<ii> <ehm> <breath> i piedi <breath>


p1#44:  #<p2#43> <ss>s�# <P> ha due segnettini sui piedini che sarebbero le
        caviglie <breath> <sp> #<p2#45> dovrebbero essere#


p2#45:  #<p1#44> <laugh> <inspiration> {[whispering] s�}# #<p1#46>
        <inspiration>#


p1#46:  e anche nelle ginocchia #<p2#47> ce li ha#


p2#47:  #<p1#46> okay# <inspiration> #<p1#48> <ehm> in un p+#


p1#48:  #<p2#47> nella barca# vai nella barca


<sp>


p2#49:  s� <breath>


p1#50:  la barca sono praticamente due vele <lp> sembrano due vele <lp> #<p2#51>
        <tongue-click> attaccate#


p2#51:  #<p1#50> <eh># <breath> s� si vede un pezzettino #<p1#52> dell'altra#


p1#52:  #<p2#51> poi c'� sopra<aa># <ll>la velettina pi� #<p2#53> piccola#


p2#53:  #<p1#52> s� <breath>#


p1#54:  <inspiration> e poi ci son delle lin+ / dei puntini sulla vela


<sp>


p2#55:  s� quanti ?


<lp>


p1#56:  <uu>uno due tre<ee> quattro cinque


<sp>


p2#57:  s� <breath>


<sp>


p1#58:  okay la barca<aa> normalissima / un po' <vocal> #<p2#59> da 'na parte �
        un po' pi� a# punta {[whispering] s�}


p2#59:  #<p1#58> ha la punta<aa> <breath> <lp># <eh> la t+ <vocal> <inspiration>
        poi la / guarda quella nuvola a sinistra su<uu> <breath>


<sp>


p1#60:  s�


<lp>


p2#61:  <inspiration> con<nn> <ehm> boh la #<p1#62> forma un po' <breath>#


p1#62:  #<p2#61> � un po' allungata# <sp> da 'na parte <sp> verso / va verso
        #<p2#63> destra#


p2#63:  #<p1#62> verso destra# ha molte <ehm> ondine <breath> ?


<lp>


p1#64:  s�


<lp>


p2#65:  <eeh> quel segnetti su <breath> ?


p1#66:  s� <ehm> cio� su<uu> / #<p2#67> gi�<uu> a sinistra ?#


p2#67:  #<p1#66> neh <sp> s�#


p1#68:  <inspiration> uno due tre e quattro #<p2#69> ne ha#


p2#69:  #<p1#68> o_kay <breath>#


p1#70:  e poi ce n'� una pi� piccolina<aa> a destra di nuvola dall'altra parte
        del foglio {[whispering] praticamente}


<sp>


p2#71:  s� c'� 'na nuvola ma niente<ee> #<p1#72> <ehm>#


p1#72:  #<p2#71> non c'�# #<p2#73> niente no � normalissima# #<p2#73>
        <inspiration> okay#

p2#73:  #<p1#72> non c'� niente# <lp> #<p1#72> <tongue-click> <inspiration>
        <eeh># <vocal> {[creaky-voice] e} poi guar+ i pantaloncini delle del
        ragazzino ? <breath>


p1#74:  ha dei si+ <vocal> / cio� una specie di lineetta


<lp>


p2#75:  e poi {[creaky-voice] <vocal>} e poi sotto ha dei puntini <breath> ?


<lp>


p1#76:  sotto s� vabb� s� nella vita ha #<p2#77> dei puntini#


p2#77:  #<p1#76> le mani# si vedono tutti e due o se ne vede {[creaky-voice]
        una} <breath> ?


<lp>


p1#78:  se ne vede una


<sp>


p2#79:  okay ci sono tutte le dita #<p1#80> segnate#


p1#80:  #<p2#79> <inspiration> <ah># guarda ci sono delle linee nel <ehm> nel
        {[creaky-voice] salvagente}


<lp>


p2#81:  <ss>s�


<sp>


p1#82:  le vedi in ba+ in {[creaky-voice] basso} ?


p2#83:  #<p1#84> <ss>s�#


p1#84:  #<p2#83> ce n'�# una che passa attraverso 'na palla


p2#85:  s� <breath>


p1#86:  <inspiration> e poi ce ne son due pi� {[creaky-voice] piccoline}


p2#87:  <tongue-click> s� <breath>


p1#88:  {[creaky-voice] okay}


p2#89:  <inspiration> e poi gli il salvagente ha il<ll> #<p1#90> <ehm> <breath>
        <tongue-click> {[creaky-voice] al} <sp> il tappo# com'�<ee> ? <breath>


p1#90:  #<p2#89> <tongue-click> il tappo dietro# <lp> <tongue-click> <eeh> c'�
        {[creaky-voice] una} specie di pallina poi c'�<ee> una cosa pi�
        {[creaky-voice] lunga} 'n'altra pallina il cerchie+ cio� mezzo {[creaky-
        voice] cerchietto} cos�<ii> ? <sp> <inspiration> <lp> <laugh> #<p2#91>
        <inspiration> {<laugh> ti faccio ridere}#


p2#91:  #<p1#90> <laugh> <breath> <sp> <inspiration> <breath> <inspiration>
        {<laugh> mi fai ridere# la palli+} <inspiration> ascolta <ehm> <tongue-
        click> <inspiration> <breath> <sp> <breath> <lp> <tongue-click> #<p1#92>
        le onde#


p1#92:  #<p2#91> c'� 'na pietra# <sp> no c'� una pietra


p2#93:  dove ?


p1#94:  a<aa> {[creaky-voice] destra}


<sp>


p2#95:  io non {[creaky-voice] ce} l'ho la {[creaky-voice] pietra a} destra dove
        ?


p1#96:  <inspiration> a destra in basso


<sp>


p2#97:  no #<p1#98> io non#


p1#98:  #<p2#97> sopra al# pallone praticamente pi� in {[creaky-voice] alto del
        pallone}


<lp>


p2#99:  <breath> ma pietra proprio pietra #<p1#100> tonda <breath> ?#


p1#100:  #<p2#99> e no# � una {[creaky-voice] specie di s+ <vocal> / una crepa
         'na cosa strana} sembra 'na pietra <breath>


<lp>


p2#101:  boh io non<nn> <eeh> cio� {[creaky-voice] i} <sp> <breath> io vedo<oo>
         <ehm> <breath> <tongue-click> <inspiration> non so come definirlo un
         <ehm> <breath> <lp> #<p1#102> e sembra la# forma dell'onda <nn>non lo
         {[creaky-voice] so} anche se #<p1#102> {[creaky-voice] <vocal>}
         bambino#


p1#102:  #<p2#101> <tongue-click> 'na cre+# <P> #<p2#101> dell'onda ?# <sp> � la
         sabbia #<p2#103> questa#


p2#103:  #<p1#102> <inspiration> <eeh> della sabbia# volevo dire <breath> <sp> �
         grandina <breath> <sp> #<p1#104> e in<nn> <eeh>#


p1#104:  #<p2#103> <eh> ma chiusa# � chiusa , capito ?


<sp>


p2#105:  � lunga per�


<lp>


p1#106:  {[whispering] <eh> ! <sp> mica #<p2#107> tant+}#


p2#107:  #<p1#106> <tongue-click> � sottile# lunga


<lp>


p1#108:  {[whispering] non � cos� sottile questa} #<p2#109> {<laugh> non lo so
         se}#

p2#109:  #<p1#108> <inspiration> allora vuol dire# #<p1#110> {<laugh> che
         ci ho du+}#


p1#110:  #<p2#109> <inspiration> ha tre <sp> ha# <sp> due lineette {[creaky-
         voice] dentro ?}


<lp>


p2#111:  <ss>s�


<sp>


p1#112:  allora {[whispering] non #<p2#113> {[creaky-voice] �<ee>} s+ <sp> ce
         {[creaky-voice] l'hai} anche tu<uu> okay}#


p2#113:  #<p1#112> � la stessa <inspiration> <breath># <lp> #<p1#114> <tongue-
         click> prov+#


p1#114:  #<p2#113> <inspiration> poi ci sono# sotto la pie+ / sotto questa
         specie {[creaky-voice] di<ii>} crepa #<p2#115> insomma <inspiration>#
         ci son tre #<p2#115> {[creaky-voice] lineette ?}#


p2#115:  #<p1#114> <tongue-click> s�# <lp> #<p1#114> <tongue-click> s� <breath>#


p1#116:  a fianco al pallone ?


p2#117:  <inspiration> s�


p1#118:  ci {[creaky-voice] sono<oo>} tre lineette ? <sp> #<p2#119> una pi�
         lunga e due picc+#


p2#119:  #<p1#118> s� <inspiration> c'� l'ombra# del pallone ?


<lp>


p1#120: <eeh> un pezzettino <sp> #<p2#121> piccolino# {[creaky-voice] s�}


p2#121:  #<p1#120> okay# <sp> <inspiration> <ehm> #<p1#122> <breath>#


p1#122:  #<p2#121> <ehm># <sp> a fianco i piedi del ragazzo


p2#123:  s�


<sp>


p1#124:  ci sono quelle linee ? <lp> vicino #<p2#125> sopra i pie+#


p2#125:  #<p1#124> a fianco <aa>alla# destra ?


<lp>


p1#126:  no a sinistra


<lp>


p2#127:  s�


<sp>


p1#128:  <inspiration> ci sono {[creaky-voice] delle<ee> <ehm>} delle specie di
         montagnette di sabbia ?


<sp>


p2#129:  s�


<sp>


p1#130:  <ehm> quella pi� in basso sotto<oo> ha {[creaky-voice] delle<ee>}
         <inspiration>#<p2#131> {[creaky-voice] <vocal>}#


p2#131:  #<p1#130> <tongue-click> una lineetta# #<p1#132> co+ <sp> s�#


p1#132:  #<p2#131> <eh> ! <sp> delle {[creaky-voice] lineettine}# tre<ee>
         #<p2#133> quattro {[whispering] sono}#


p2#133:  #<p1#132> <mh># <sp> s� <sp> esatto


p1#134:  #<p2#135> okay#


p2#135:  #<p1#134> e poi c'�# un <sp> <ehm> nel piede del ragazzo <tongue-click>
         <sp> {[creaky-voice] alla} sua destra , il piede pi� gi�


<sp>


p1#136:  <tongue-click> c'� l'ombra ?


<sp>


p2#137:  c'� l'ombra s� <breath>


p1#138:  okay


p2#139:  uff poi<ii> #<p1#140> {[whispering] aspett+} <breath>#


p1#140:  #<p2#139> vediamo nel# mare ?


<sp>


p2#141:  <tongue-click> 'ndiamo al #<p1#142> mare#


p1#142:  #<p2#141> a destra# <sp> nella parte di #<p2#143> destra#


p2#143:  #<p1#142> <mhmh>#


p1#144:  <tongue-click> ci sono una due tre<ee> <inspiration> <eeh> <sp> <ehm>
         anche i puntini conto <eh> quattro <inspiration> <sp> cinque sei sette
         otto<oo> lineette <lp> #<p2#145> del mare#


p2#145:  #<p1#144> allora ce n'�# una spezzata


<lp>


p1#146:  ce ne so+ ce / s� ce n'� una con un {[creaky-voice] puntino}


<lp>


p2#147:  con due puntini <breath>


<lp>


p1#148:  {[whispering] <tongue-click> come con due puntini ?}


<lp>


p2#149:  {[whispering] s�}


<lp>


p1#150:  e allora<aa> <sp> cio� praticamente <vocal> vicino la barca ce n'� una
         a destra una a sinistra #<p2#151> <inspiration> e una sotto# con un
         puntino


p2#151:  #<p1#150> okay# <P> la mia ne ha due <breath>


p1#152:  okay {[creaky-voice] allora � una<aa>} #<p2#153> differenza#


p2#153:  #<p1#152> <tongue-click> non so se � la fotocopia# {<laugh> mi sa che �
         la #<p1#154> ma+ / non lo so <inspiration>}#


p1#154:  #<p2#153> <laugh> <sp> non penso# <sp> {[whispering] #<p2#155> si
         {[creaky-voice] vedre+} si vedr�}#


p2#155:  #<p1#154> poi gi�# <breath>


<sp>


p1#156:  ce ne {[creaky-voice] sono<oo>} <inspiration> <sp> tre per�<oo> <vocal>
         una<aa> � divisa praticamen+ / #<p2#157> {[creaky-voice] �} separata#


p2#157:  #<p1#156> s� <breath># <sp> s� <breath>


p1#158:  e quella che sta al centro � piccolissima #<p2#159> {[creaky-voice]
         praticamente}#


p2#159:  #<p1#158> <tongue-click> s� s� s�# s�


p1#160:  okay poi ce n'� una sotto<oo> al salvagente


<lp>


p2#161:  <ss>s� <vocal> lunga


<lp>


p1#162:  {[whispering] <eeh> lunga} <sp> 'na specie di -Esse-<ee> allungata


<sp>


p2#163:  s�


<sp>


p1#164:  {[whispering] poco a poco} okay <inspiration> poi ce n'�


<lp>


p2#165:  a #<p1#166> sinistra del salvagente <breath>#


p1#166:  #<p2#165> dove / sotto la faccia# {[creaky-voice] della<aa> della<aa>}
         <sp> <vocal> {[whispering] dell'oca l� della<aa>} / #<p2#167> cos'�
         <sp> del cigno cos'� que+ questa cosa# <inspiration> {[creaky-voice]
         okay e} son due <sp> <inspiration> poi ce n'� una a forma {[creaky-
         voice] di -Esse-} <sp> nel #<p2#167> collo#


p2#167:  #<p1#166> {<laugh> <breath> scendi questa co+ <sp> come ? <breath>}
         <laugh># <P> #<p1#166> {[whispering] s�}#


p1#168:  <inspiration> un'altra sotto


<sp>


p2#169:  <mhmh>


p1#170:  <vocal> altre due <sp> #<p2#171> piccoline#


<sp>


p2#171:  #<p1#170> <ss>s�#


p1#172:  {[creaky-voice] e<ee>} poi altre due e una<aa> �<ee> divisa


<lp>


p2#173:  <tongue-click> s� <lp> mannaggia {<laugh> <vocal> dove sono le altre}
         <sp> <inspiration>


p1#174:  non c'� pi� {[creaky-voice] niente} / #<p2#175> nel cielo#


p2#175:  #<p1#174> <inspiration> <ah> ! <ehm># il becco del<ll> <sp> il becco
         finisce con<nn> la fossettina <breath> ?


<sp>


p1#176:  <inspiration> in gi�


<lp>


p2#177:  s�


<lp>


p1#178:  {[whispering] <inspiration> poi<ii> #<p2#179> vedia+}#


p2#179:  #<p1#178> � circolare# questo salvag+ / {[whispering] pe+ perch�
         potrebbe essere anche una <sp> {[creaky-voice] un<nn>} #<p1#180>
         rettangolare}#


p1#180:  #<p2#179> � tondo# s� #<p2#181> normali+ un# so+ <vocal> salvagente
         normalissimo <lp> {[whispering] <ehm> cio�} <inspiration> <ah> ! aspetta
         un po' <sp> <tongue-click> guardiamo come sono messi i cerchietti


p2#181:  #<p1#180> s�# <P> <breath> <P> che cerchietti ?


p1#182:  i cerchiettini del salvagente


<lp>


p2#183:  <mh>


<sp>


p1#184:  capito / ce n'� #<p2#185> uno <inspiration> vicino# alla testa


p2#185:  #<p1#184> <inspiration> <ah> s� <breath># <lp> <tongue-click> a met�
         <breath>


<lp>


p1#186:  {[whispering] come met� ?}


<lp>


p2#187:  non � tutto<oo> / non � un {[creaky-voice] cerchietto<oo>}


<sp>


p1#188:  'na pallina <vocal> #<p2#189> s� <ah> ! s�# {[creaky-voice] <ehm> me+
         mezza} palla #<p2#189> {[whispering] s� <inspiration>}#


p2#189:  #<p1#188> <tongue-click> � met�# <lp> #<p1#188> {[whispering] mezza
         pallina}#


<sp>


p1#190:  <inspiration> okay <sp> <inspiration> {[creaky-voice] poi} uno<oo>
         <nn>n+ <sp> nel <breath> nel <eeh> nel collo


p2#191:  #<p1#192> s� <laugh>#


p1#192:  #<p2#191> {[whispering] una parte<ee># s� <eh>} <inspiration> uno
         sotto <sp> #<p2#193> gi�#


p2#193:  #<p1#192> s�#


<sp>


p1#194:  c'� quasi tutto ne manca un #<p2#195> pezzo#


p2#195:  #<p1#194> <tongue-click> s� <breath>#


p1#196:  <inspiration> poi ce n'� uno vicino<oo> al pantalone del bambino


<lp>


p2#197:  <mhmh>


<lp>


p1#198:  <inspiration> e poi uno<oo> <sp> verso il sedere del {<laugh> bambino}
         <laugh> <inspiration> in gi� <breath>


<sp>


p2#199:  <breath> <laugh> <lp> {<laugh> <inspiration> <ss>s�}


p1#200:  e poi c'� quello che io non ho <sp> {[whispering] praticamente}


<lp>


p2#201:  <eh> <sp> no <breath>


<lp>


p1#202:  non erano cinque ?


<sp>


p2#203:  <eh>


<sp>


p1#204:  <inspiration> allora lo sai che non c'� un errore ? <sp> abbiamo
         sbagliato <NOISE> <sp> <inspiration> #<p2#205> perch� io s+#


p2#205:  #<p1#204> ma se# / {[creaky-voice] <vocal>} minimo in questo gioco
         {<laugh> non ci sono errori} <inspiration> ascolta sotto la faccia del
         bambino c'� 'na pallina ?


<lp>


p1#206:  {[whispering] <tongue-click> cio� ?} <lp> una pallina ?


<sp>


p2#207:  {<laugh> <eheh>}


<sp>


p1#208:  <inspiration> un puntino ?


p2#209:  <eh> un puntino


p1#210:  <inspiration> s� <lp> s� ce n'� uno anche sai dove ?


p2#211:  {[whispering] dove ?}


<sp>


p1#212:  e<ee> #<p2#213> tra il pal+#


p2#213:  #<p1#212> sotto il#


<lp>


p1#214:  <NOISE> tra il pallone e la<aa> e la spiaggia #<p2#215> pratic+#


p2#215:  #<p1#214> s�<ii># #<p1#216> s� s�#


p1#216:  #<p2#215> {[whispering] <ah> ! {[creaky-voice] okay}}# <sp> <laugh>
         <inspiration> {<laugh> <unclear> <breath> bellissimo} <inspiration>
         <lp> #<p2#217> <ehm>#


p2#217:  #<p1#216> {[whispering] <inspiration> oddio# non so cosa <breath>}


<lp>


p1#218:  {[whispering] <mh> non c'� #<p2#219> pi� nie+}#


p2#219:  #<p1#218> <inspiration> la linea# del<ll> <ehm> del mare diciamo verso
         la fine � dritta ? <breath> <lp> <vocal> #<p1#220> dietro {[creaky-
         voice] la}#


p1#220:  #<p2#219> <ah> !# nel tramonto ?


<sp>


p2#221:  #<p1#222> <inspiration> <ss>s� <breath>#


p1#222:  #<p2#221> �# drittissima


<lp>


p2#223:  {[whispering] nel {[creaky-voice] mare} <breath>}

p1#224:  <eh> invece gi� ? <lp> da sinistra <lp> va sotto<oo> il <eeh>
         salvagente


<lp>


p2#225:  <mhmh>


<sp>


p1#226:  fa 'na specie di curvetta <sp> poi va a s+ <vocal> {[creaky-voice] �}
         <vocal> � quasi {[creaky-voice] dritta<aa>} <vocal> e passa attraverso
         le gambe


<sp>


p2#227:  <tongue-click> <ah> #<p1#228> s� <breath>#


p1#228:  #<p2#227> <inspiration> poi all'altezza# del pallone ha<aa> due curve


<sp>


p2#229:  <tongue-click> s�


<sp>


p1#230:  e poi<ii> va dritta di {[creaky-voice] nuovo}


p2#231:  {[whispering] <tongue-click> s� <breath>}


<sp>


p1#232:  okay <P> c'� pi� niente <laugh>


<sp>


p2#233:  #<p1#234> <vocal> <inspiration> {[whispering] non tro+}#


p1#234:  #<p2#233> <inspiration> caspita# <lp> <inspiration> {[whispering]
         <eeh> <mh>} <sp> <tongue-click> nel braccio l'abbiamo {[creaky-voice]
         detta / il} punti+ #<p2#235> {[whispering] [creaky-voice] l'abbiamo
         de+} son due ?# son due quasi attaccati sembrano due ?


p2#235:  #<p1#234> <ehm> s� l'abbiamo detto# <P> {[whispering] <tongue-click>
         che cosa <breath> ?}


<sp>


p1#236:  dei puntini <sp> che ha nel braccio<oo> il bambino


<lp>


p2#237:  s� <breath> <lp> {[whispering] sembrano due}


<sp>


p1#238:  {[whispering] <mh>}


<sp>


p2#239:  <tongue-click> <breath> <vocal> poi nel cielo non c'� niente ?


p1#240:  niente niente <lp> {[whispering] nient'altro proprio}


<lp>


p2#241:  <inspiration> in mare ? <laugh> <unclear> <inspiration>


<lp>


p1#242:  {[whispering] no<oo> proprio nie+} / <nn>nessun puntino proprio niente
         <lp> {[whispering] pa+} <lp> <tongue-click> <lp> {[whispering] boh <lp>
         <tongue-click> vediamo}


<lp>


p2#243:  {[whispering] non trovo #<p1#244> niente}#


p1#244:  #<p2#243> ombre# strane no #<p2#245> niente#


p2#245:  #<p1#244> {[whispering] <inspiration> neanche}# <P> <tongue-click> <lp>
         {[whispering] #<p1#246> niente <breath>#}


p1#246:  #<p2#245> <inspiration> la<aa># <ehm> <tongue-click> <vocal> <sp>
         <vocal> la barca no ?


<sp>


p2#247:  <mh> <breath>


<lp>


p1#248:  {[whispering] {[creaky-voice] praticamente �} la<aa> <ehm> <sp>
         <tongue-click> proprio � la barca no ?} <sp> no non l+ non la vela
         <laugh> <inspiration> {<laugh> voglio dire<ee> insomma la barca}
         <inspiration> � quadrata quadrata e un po' a punta <sp> verso sinistra


p2#249:  no <sp> {<laugh> � rettangola+ <breath> e questo non � un #<p1#250>
         quadrato} <inspiration> <eh> <inspiration>#


p1#250:  #<p2#249> <eh> ! s� � rettangolare# scusa <sp> � rettangolare e un po' a
         punta


<sp>


p2#251:  <tongue-click> la parte di sinistra <breath>


p1#252:  s�


<sp>


p2#253:  <inspiration> <mhmh> <breath>


<sp>


p1#254:  <inspiration> <ah> ecco la vela <sp> quella pi� piccolina in alto � a
         sinistra o a destra ? <breath>


<sp>


p2#255:  <tongue-click> a destra


<lp>


p1#256:  {[whispering] <ah> ! da me a sinistra <lp> <laugh>} <sp> <tongue-click>
         <lp> <inspiration> <sp> <breath> <tongue-click> <sp> <inspiration> e
         poi <sp> <ah> ! l'orecchio <lp> sotto l'orecchio ci sono i capelli ?


<sp>


p2#257:  <tongue-click> s� <breath>


<lp>


p1#258:  {[whispering] <mh>} <sp> <tongue-click> e l'orecchio dentro ce l'ha le
         pieghette ?


<sp>


p2#259:  {<laugh> #<p1#260> <ss>s�#}


p1#260:  #<p2#259> � una# sola vero ? <lp> sembra una A<aa> quasi ?


<lp>


p2#261:  s� <breath> <sp> #<p1#262> <mhmh>#

p1#262:  #<p2#261> {[whispering] <ah>}# <sp> non � divisa non ha nessun
         puntino niente ?


<sp>


p2#263:  <tongue-click> � divisa ma<aa> non � tutta una A perfetta <breath>


<lp>


p1#264:  come , � divisa ?


<lp>


p2#265:  <inspiration> cio� <sp> alla fine <breath> <sp> � divisa perch� c'� la
         pelle <lp> del ragazzino


p1#266:  <ah> s� #<p2#267> ho capito okay# okay <inspiration> <sp> per� sembra
         una A<aa> !


p2#267:  #<p1#266> <laugh> okay <inspiration># <P> s� s� <breath>


<lp>


p1#268:  <tongue-click> {[whispering] <eh> okay} <lp> o una {[creaky-voice]
         Acca} {[whispering] no <sp> non sembra} un'Acca #<p2#269> perch� l'Acca
         � al contrario <breath>#


p2#269:  #<p1#268> {[whispering] <eh> s+ <vocal># <sp> infatti no <breath>}


<sp>


p1#270:  <inspiration> {[creaky-voice] okay} e poi<ii> boh ? <lp> <tongue-click>
         <lp> <ah> il bambino c'ha il pettorale <lp> <tongue-click> <sp> c'ha
         un po' di pettorale guarda lo vedi ?


<sp>


p2#271:  #<p1#272> {<laugh> s�}#


p1#272:  #<p2#271> <laugh># okay <inspiration> ha il gomito <breath> {<laugh> ha
         tutto ha i #<p2#273> polpacci}#


p2#273:  #<p1#272> ha 'na spalla pi�# gi� una spalla pi� su


<sp>


p1#274:  <eh> ma proprio poco poco


p2#275:  {[whispering] <inspiration> s� #<p1#276> va be+}#


p1#276:  #<p2#275> {[whispering] <vocal> non si nota# quasi <sp> okay} <sp>
         <tongue-click> la forma del viso a punta ?


<lp>


p2#277:  il mento <breath> ?


p1#278:  s�


p2#279:  la fine del mento s�


<lp>


p1#280:  <eeh> il collo ? <sp> <inspiration> <sp> <tongue-click> cio� la linea
         del viso tocca quasi con il collo ?


p2#281:  <mhmh> <breath>


p1#282:  <inspiration> {[whispering] o_kay} <lp> <eeh> <vocal> <ah> i piedi <sp>
         <tongue-click> <inspiration> <sp> #<p2#283> <eeh> vediamo#


p2#283:  #<p1#282> <inspiration> <breath> in uno si# vedono un po'<oo> le
         #<p1#284> dita <breath>#


p1#284:  #<p2#283> e# nell'altro no si vede solo il pol+ l'alluce <breath>


p2#285:  #<p1#286> {[whispering] s�} <breath> <laugh>#


p1#286:  #<p2#285> {<laugh> il pollice} <sp># <inspiration> <breath> okay <sp>
         <eeh> poi<ii> boh <P> <tongue-click> c'� una divisione tra le tra le
         due caviglie <sp> {[whispering] proprio un buchettino piccolino}


<sp>


p2#287:  {[whispering] <ss>s�}


p1#288:  {[whispering] che le divide insomma} <P> {[whispering] <tongue-click>
         non c'� nient'altro} <P> <tongue-click> <inspiration>
         le<ee> / {[whispering] l'abbiamo #<p2#289> detto questo}#


p2#289:  #<p1#288> {[creaky-voice] e} la# mano che tiene il salvagente ?
         <breath>


p1#290:  si {[creaky-voice] vedono} le dita


<sp>


p2#291:  {[whispering] s� <breath>}


p1#292:  <inspiration> ed � {[creaky-voice] anche<ee>} <lp> larga


<lp>


p2#293:  <tongue-click> <mh> <breath> <sp> c'� solo il pollice praticamente


p1#294:  s� per� si vede il segno delle dita


p2#295:  <mhmh> <breath> <sp> <tongue-click> <inspiration> {[whispering] s�
         <breath>}


p1#296:  e poi<ii> <inspiration> la linea<aa> <ehm> <sp> <inspiration> <sp>
         praticamente<ee> il cerchio interno del salvagente che passa
         attorno al ragazzo


<sp>


p2#297:  {[whispering] s�} <breath>


<sp>


p1#298:  <vocal> non � f+ non fini+ / cio� non<nn> / #<p2#299> <vocal> certo


p2#299:  #<p1#298> non arriva# fino alla fine insomma


<sp>


p1#300:  <eeh> non <ehm> / quando<oo> arriva quasi vicino al ragazzo<oo> non c'�
         <lp> hai #<p2#301> capito <breath> ? {<laugh> non mi so spiegare}
         <inspiration>#


p2#301:  #<p1#300> <tongue-click> s� <breath> no no ti sto capendo#


<sp>


p1#302:  <inspiration> okay


<sp>


p2#303:  e c'ha due<ee> lineette verticali <breath>


p1#304:  okay <sp> <inspiration> poi sai<ii> lo spaghetto che c'� tra<aa> <ehm>
         <tongue-click> <inspiration> <ehm> cio� nel tappo {[tongue-click]
         del<ll> <ehm>} <sp> #<p2#305> del salvagente ?#


p2#305:  #<p1#304> <vocal> <breath> <mh> <breath> <mhmh> <breath># <mh>
         #<p1#306> <breath> s� <breath>#


p1#306:  <p2#305> � nero# in mezzo vero ?


p2#307:  s� <breath>


p1#308:  {[whispering] okay} <lp> <breath> <lp> <inspiration> {[whispering] sa
         che non c'� nient'altro}


<sp>


p2#309:  <inspiration> {[whispering] qualcosa ci {[creaky-voice] deve} essere
         per forza} <lp> <inspiration> <breath> #<p1#310> non so per�# cosa<aa>
         <ehm> <breath> <tongue-click> cosa guardare <sp> <clear-throat>


p1#310:  #<p2#309> {[whispering] quel bracc+}# <P> {[whispering] <mh>} <lp>
         <tongue-click> <inspiration> ascolta dove c'� il bambino


<lp>

p2#311:  {[whispering] s� <breath>}


<sp>


p1#312:  <inspiration> il braccio che non si vede <lp> #<p2#313> la mano
         che non si vede#


p2#313:  #<p1#312> {[whispering] s�} <tongue-click> <ah> s� <breath>#


<sp>


p1#314:  <inspiration> c'�<ee> una specie di<ii> spazietto ? <sp> tra<aa> la
         pancia<aa> e il braccio ?


<sp>


p2#315:  s�


<sp>


p1#316:  {[whispering] <ah>} <lp> <tongue-click> <sp> {[whispering] <mm>ma} <lp>
         tiro


<lp>


p2#317:  <tongue-click> cosa ?


<sp>


p1#318:  {[whispering] non ne vedo altre <inspiration> aspetta fammi
         pensare} <sp> <tongue-click> la testa � un po' inclinata verso<oo>
         #<p2#319> verso<oo>#


p2#319:  #<p1#318> verso il bas+# <sp> sto guardando #<p1#320> gi� <vocal> sta
         guardando# quel {[whispering] pallone a #<p1#320> forma di uo+}#


p1#320:  #<p2#319> s�# <sp> <inspiration> #<p2#319> tutti e due gli occhi#
         guardano verso il #<p2#321> basso#


p2#321:  #<p1#320> s� <breath>#


p1#322:  <inspiration> e il naso � una specie {[creaky-voice] di l+ <vocal> /
         di<ii>} accento sembra


<sp>


p2#323:  <tongue-click> s� <breath>

<lp>


p1#324:  {[whispering] okay} <lp> <breath> <lp> {[whispering] guarda non lo so
         non vedo nient'altro} <sp> vedi qualcosa ?


<lp>


p2#325:  cosa ?


<sp>


p1#326:  vedi qualcosa ?


<lp>


p2#327:  <inspiration> {[whispering] no} <breath> <lp> <tongue-click>
         {[whispering] non so proprio cosa}


<lp>


p1#328:  {[whispering] non vedo nient'altro}


<lp>


p2#329:  {[whispering] neanch'io} <breath> <lp> {[whispering] <ehm> boh sto
         cercando<oo> <vocal> <breath>}


<lp>


p1#330:  <tongue-click> <inspiration> {[whispering] <ah> ! ecco guarda} <sp>
         <tongue-click> <inspiration> i<ii> <ehm> <tongue-click> la palla <sp>
         <tongue-click> <sp> � attaccata<aa> al<ll> al bordo del disegno ?


<lp>


p2#331:  {[whispering] <tongue-click> c'� uno spazietto} <breath>


<lp>


p1#332:  tra il bordo del disegno<oo> e la #<p2#333> palla ?#


p2#333:  #<p1#332> {[whispering] <tongue-click> s� <breath>}#


<sp>


p1#334:  <inspiration> {[whispering] <eh> ! un altro erro+ / vedi da me �
         attaccata}


<lp>


p2#335:  <tongue-click> {[whispering] secondo me questa cosa non � /
         <unclear> son vere secondo me <vocal> <laugh> <vocal> della #<p1#336>
         fotocopia <sp> <inspiration>#


p1#336:  #<p2#335> <eh> ?# <lp> <NOISE> <sp> secondo te ?


<lp>


p2#337:  {[whispering] <vocal> e ha qualcosa nella fotocopia non lo so}


<sp>


p1#338:  <inspiration> dici che � la #<p2#339> fotocopia ?#


p2#339:  #<p1#338> <tongue-click> <ehm> boh dai# tu mettilo #<p1#340> {<laugh>
         fregatene} <sp> <inspiration> <laugh>#


p1#340:  #<p2#339> <tongue-click> <eh> ! infatti tanto# / cio� <unclear> niente
         da perdere {[whispering] quindi} <lp> <tongue-click> <lp> {[whispering]
         <mh> poi non so} <sp> boh perch�<ee>


p2#341:  <inspiration> ascolta quella cosa che avevi detto prima #<p1#342>
         del<ll> <ehm>#


p1#342:  #<p2#341> <tongue-click> come ?#


<sp>


p2#343:  la<aa> {[creaky-voice] <ehm>} <unclear> avevi detto che c'era la
         {[creaky-voice] pietra nella #<p1#344> sabbia#}


p1#344:  #<p2#343> <tongue-click> <ah># s�


p2#345:  � sulla destra del disegno <breath>


<lp>


p1#346:  s�


<sp>


p2#347:  <inspiration> ma <lp> praticamente come se fosse una<aa> {[creaky-
         voice] <ehm>} <breath> <sp> <tongue-click> un buco {[creaky-voice]
         alla} sabbia


p1#348:  <tongue-click> ecco brava {[whispering] un buco nella sabbia}


p2#349:  <inspiration> {[whispering] <ah> {[creaky-voice] okay}} <breath>


p1#350:  <inspiration> che<ee> �<ee> attaccato al disegno <aa>al al #<p2#351>
         bordo del disegno#


p2#351:  #<p1#350> <tongue-click> s� s�# {[creaky-voice] s�} <breath>


<lp>


p1#352:  si dovrebbero vedere tre lineette per� una � attaccata #<p2#353>
         proprio al bordo <inspiration>#


p2#353:  #<p1#352> <tongue-click> s� s�# s�


<sp>


p1#354:  {[whispering] o_kay}

<lp>


p2#355:  {[whispering] boh} <sp> <breath>

p1#356:  {[whispering] non vedo nient'altro}


<lp>


p2#357:  non � che la tua fotocopia � a colori no ?


<sp>


p1#358:  no no <sp> <laugh> {<laugh> <tongue-click> te l'avrei detto} <sp>
         <inspiration> <lp> {[whispering] bianco e nero}


<lp>


p2#359:  cosa c'� di nero proprio tutto nero le palline gli occhi <lp> #<p1#360>
         capelli#


p1#360:  #<p2#359> <inspiration> <tongue-click> gli occhi# <lp> i capelli<ii>
         e<ee> basta <lp> {[whispering] nient'altro} <P> e le ombre anche
         {[whispering] sono nere}


<sp>


p2#361:  <mh> {[whispering] vabb�} <breath>


<lp>


p1#362:  <vocal> ma<aa> i capelli sembrano quasi ricci ?


<lp>


p2#363:  {[creaky-voice] i} capelli ?


p1#364:  sembrano un po' ricci no ? <sp> {[whispering] sono un po' strani}


<sp>


p2#365:  <inspiration> l'ondina sotto l'orecchio un po' riccia <breath> per�
         #<p1#366> questo {[creaky-voice] ciuffo<oo>}#


p1#366:  #<p2#365> {[whispering] <ah>} <lp> anche su# <P> ma il ciuffo com'�
         sembra<aa> <sp> #<p2#367> <tongue-click> ci+#


p2#367:  #<p1#366> <tongue-click> alla# {<laugh> Elvis} <laugh> {<laugh> un po'}
         <sp> #<p1#368> <inspiration> <breath>#


p1#368:  #<p2#367> <inspiration> come ?#


p2#369:  {<laugh> <inspiration> un po' alla Elvis}


<sp>


p1#370:  <eh> {[whispering] <ah> okay perfet+} <inspiration> che {[creaky-voice]
         c'ha<aa>}<eeh>} sembra che ha la riga ? <sp> #<p2#371> in me+ <sp> cio�
         da 'na parte {[whispering] diciamo<oo># verso<oo>} <lp> che va verso
         destra


p2#371:  #<p1#370> <tongue-click> s� <breath> <sp> <mhmh>#
