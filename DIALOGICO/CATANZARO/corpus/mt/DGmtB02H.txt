TEXT_inf.

MAT:            mt
MAP:            B
TRM:
Ndl:            02
Ntr:		
Nls:		
REG:            H


SPEAKERS_inf.

INp1:		D. V., M, 26, Franfeld, spontaneo, fluente, G>F
INp2:		A. T., M, 27, Soveria Simeri, spontaneo, fluente, F>G		


RECORDING_inf.

TYP:		DAT
LOC:		Soveria Simeri/privato
DAT:		09/06/01
DUR:		4.24,155
CON:		buone


TRANSCRIPTION_inf.

DAT:		10/07/01
CMT:		
Nst:		87


p1G#1:  <inspiration> allora Antonio <sp> <inspiration> <eeh> dal_la<aa> <clear-
        throath> da sopra i limoni <inspiration> vai <sp> verso il<ll> centro 
        del foglio , giri intorno ai limoni e ti porti sulla parte es_terna del
        foglio <sp> ci sei ? scendi fino al gatto


<sp>


p2F#2:  s� , aspetta <eh> ? quindi sulla parte este+ sulla parte sinistra
        #<G#3> quindi#


p1G#3:  #<F#4> sulla parte# sinistra del #<F#5> foglio#


p2F#4:  #<G#3> s�# 


p1G#5:  scendi fino al gatto , ci giri intorno


p2F#6:  dove ? intorno dove ?


p1G#7:  sotto #<F#8> sotto#


p2F#8:  #<G#7> alla# mia destra o alla mia #<G#9> sinistra giro ?#


p1G#9:  #<F#8> scendi fino a# sotto al gatto , intorno alla intorno al gatto 
        <sp> dalla parte di sotto del gatto e ritorni a salire


p2F#10:  s�


<sp>


p1G#11:  giri intorno alla freccia <sp> vedi che c'� una freccia al #<F#12> 
         centro del foglio# <inspiration> e scendi #<F#12> lasciando# il cuore
         sulla destra <sp> ci sei ? <inspiration> passi tra il cuore e la mano , 
         #<F#12> scendi intorno alla sedia#


p2F#12:  #<G#11> s�# <P> #<G#11> quin+# <P> #<G#11> aspe+ <sp> aspetta aspetta 
         allora , dal gatto #<G#13> io risalgo#


p1G#13:  #<F#12> risali# #<F#14> intorno# alla freccia <sp> <NOISE> <sp> giri


p2F#14:  #<G#13> s�# <P> s� ma io alla freccia ci devo passare a destra o a si+ 
         / la f+ la freccia me la #<G#15> lascio a destra o a#


p1G#15:  #<F#14> lasciala# <sp> lasciala sulla destra


<sp>


p2F#16:  sulla #<G#17> destra , quindi p+# <lp> #<G#17> quindi#


p1G#17:  #<F#16> salendo# sulla destra , #<F#16> poi ci# giri intorno <sp> e
         rimane sulla sinistra logicamente <inspiration> <sp> va bene ?


<sp> 


p2F#18:  s� sc+ <sp> #<p1G#19> e p+#


p1G#19:  #<F#18> scendi#


<sp>


p2F#20:  s�



p2G#21:  lasci il cuore a destra e la mano a sinistra <inspiration> giri intorno 
         alla sedia


<sp>


p2F#22:  {[whispering] il cuore a destra e la mano a sinistra} <lp> no , ascolta 
         se io giro intorno alla freccia poi riscendendo ritrovo la barca a 
         destra 


<sp>


p1G#23:  la ?


<sp>


p2F#24:  una barca alla mia destra


<sp>


p1G#25:  no , la barca � sopra i limoni , ce l'ho io scusa


<sp> 


p2F#26:  <tongue-click> no , infatti<ii> , sono posizionati diversamente 


p1G#27:  <ah> ! ecco qua ! 


<sp>


p2F#28:  quindi , allora io


p1G#29:  allora , ricominciamo


p2F#30:  <tongue-click> allora , s�


<sp>


p1G#31:  <clear throath> <sp> vai a destra , poi scendi


p2F#32:  allora io mi trovo s+ / parto dai limoni <sp> #<G#33> giro# alla destra 
         dei limoni scendendo


p1G#33:  #<F#32> s+# <P> s� e riscendi #<F34> gi� , io# ci / mi trovo un gatto 
         <inspiration> #<F#34> intorno al g+# 


p2F#34:  #<G#33> s�# <lp> #<G#33> s� alla fine# <sp> #<G#35> mi trovo il gatto#



p1G#35:  #<F#34> ecco i+ un gatto# , risali


p2F#36:  risalgo , lasciandomi il gatto sulla sinistra


p1G#37:  s�


<sp>


p2F#38:  e poi #<G#39> arrivo fino alla freccia#


p1G#39:  #<F#38> io al centro del# foglio c'ho una freccia


p2F#40:  e anche io c'ho una freccia #<G#41> {[whispering] al <unclear>}#


p1G#41:  #<F#40> ecco , giri# intorno a destra


<sp>


p2F#42:  ecco , se io adesso giro intorno alla freccia per tornare gi� in 
         #<G#43> basso# <sp> io mi ritrovo scendendo immediatamente sulla mia 
         destra una barca , subito dopo la freccia


p1G#43:  #<F#42> <eh># <P> <mh> no , io c'ho un cuore , comunque quindi tu 
         scendi <inspiration> c'hai una mano


<sp>


p2F#44:  no <sp> io invece la mano me la trovo in alto a destra


p1G#45:  ce ne sono due


<sp>


p2F#46:  no


<sp>


p1G#47:  vabb� , comunque tu scendi fino a<aa> / in basso <sp> lascia la barca 
        <sp> io c'ho il cuore <inspiration> scendi fino a gi� <sp> poi io mi 
        trovo una sedia 


<sp>


p2F#48:  <inspiration> io allora io mi trovo al <sp> in basso al centro <sp> 
         diciamo un un treno , diciamo #<G#49> <sp> e# 


p1G#49:  #<F#48> <mh># <sp> <tongue-click> e io c'ho la mano , quindi #<F#50> 
         scendi poi# in basso a destra l'ultimo disegno sulla destra <sp> 
         #<F#50> io c'ho# 


p2F#50:  #<G#49> ecco# <P> #<G#49> � una# #<G#51> sedia#


p1G#51:  #<F#50> una# sedia , #<F#52> giri# intorno alla sedia


p2F#52:  #<G#51> s�# <P> e mi lascio la sedia sulla #<G#53> sinistra quindi#


p1G#53:  #<F#52> s� <sp> e risali#


p2F#54:  e risalgo #<G#55> s�#


p1G#55:  #<F#54> okay# , io sono arrivato al punto , ora tocca a te


Inversione di ruoli


p2G#56:  s� , io sono / va bene <sp> <inspiration> allora <sp> adesso partendo
         da questo #<F#57> punto#


p1F#57:  #<G#56> s�#


<sp>


p2G#58:  io m+ / io {<NOISE> allora} s+ / alla tua sinistra tu dovresti avere quindi la 
         mano , giusto ? <sp> io mi trovo una barca


<sp>


p1F#59:  no <eeh> il cuore c'ho io <sp> #<G#60> vabb�#


p2G#60:  #<F#59> <mh> vabb�# allora <sp> fai cos�


<sp>


p1F#61:  #<G#62> salgo#


p2G#62:  #<F#61> <ehm> sal+# sali diritto lasciandoti <sp> quelle immagini che
         hai sulla tua #<F#63> sinistra#


<sp>


p1F#63:  s�


<sp>



p2G#64:  poi io mio trovo <sp> subito dopo <sp> poco sopra ma sulla destra ,
         #<G#64> spostata# #<G#64> sull'estrema destra , una pipa#


p1F#65:  #<F#63> s�# <sp> #<F#63> perfetto# <sp> perfetto #<F#65> s� , ce
         l'ho# anch'io


p2G#66:  #<G#64> allora# <sp> con il tuo percorso <sp> passa qui in mezzo <sp>
         <inspiration> e *pose+ e prosegui dritto 


<sp>


p1F#67:  s� <sp> fino al centro del foglio 


<sp>


p2G#68:  s� , ma non<nn> non andare verso il centro <sp> #<F#69> prosegui# parallelamente
         al lato del foglio 


p1F#69:  #<G#68> mh# <lp> s�


<sp>


p2G#70:  va bene ? <inspiration> a un certo punto io mi trovo <sp> quasi<ii> sulla sulla mia
         mia sinistra mi trovo un angolo bar , #<F#71> diciamo#


p1F#71:  #<G#70> s� , perfetto#


<sp>


p2G#72:  ecco , lasciati <sp> questo angolo bar sulla tua sinistra e spostati verso <sp> la
         parte destra del foglio , #<F#73> fino ad# arrivare all'angolo destro


p1F#73:  #<G#72> <mh># <lp> in alto


p2G#74:  s� , #<F#75> pi� o meno#


p1F#75:  #<G#74> e io c'ho pure una# forchetta con la mano


p2G#76:  ecco , io invece l� c'ho solo una mano gira intorno a questo disegno


<sp>


p1F#77:  la mano ?


<sp>


p2G#78:  s� <sp> #<F#79> lasciati# quindi la mano sulla tua sinistra #<F#79> s+#
         scendendo


p1F#79:  #<G#78> <mhmh># <lp> #<G#78> s�# 


<sp>


p2G#80:  continuando <sp> diciamo sempre diritto pi� o meno , io mi trovo due cuori <sp> uno 
         sopra e uno sotto , dovresti passare in mezzo a queste due figure #<F#81> comunque#


p1F#81:  #<G#80> perfetto#


<sp>


p2G#82:  <tongue-click> passi in mezzo a queste due figure e io nell'angolo a sinistra mi trovo 
         un cono #<F#83> gelato# <sp> in alto


p1F#83:  #<G#82> s�#


<sp>


p2G#84:  ecco , lasciati il cono gelato sulla tua destra 


<sp>


p1F#85:  <mhmh>


<sp>


p2G#86:  e<ee> <sp> gira intorno al gelato e sei arrivato al punto di #<F#87> arrivo#


p1F#87:  #<G#86> okay# <breath> <sp> s�




