TEXT_inf.

MAT:            td
MAP:            B
TRM:
Ndl:            03
Ntr:		
Nls:		
REG:            H


SPEAKERS_inf.

INp1:		A. C., M, 27, Soveria Simeri, spontaneo, fluente 
INp2:		T. D., F, 27, Catanzaro, spontaneo, fluente		


RECORDING_inf.

TYP:		DAT
LOC:		Soveria Simeri
DAT:		09/06/01
DUR:		11.44,723
CON:		buone


TRANSCRIPTION_inf.

DAT:		12/07/01
CMT:		
Nst:		239


p1#1:  #<p2#2> allora# <sp> <inspiration> Tere' , #<p2#2> la<aa># la barca ce 
       l'ha la bandierina in alto ?


p2#2:  #<p1#1> andiamo# <lp> <inspiration> <sp> #<p1#1> dim+# <lp> s� <lp> e 
       la punta � rivolta alla mia <sp> destra


p1#3:  alla tua destra s� <lp> e sono due le le v+ le vele ?


p2#4:  va b+ <lp> <inspiration> <sp> <ss>s�


p1#5:  nella vela davanti #<p2#6> ci sono# dei tra+ dei<ii> #<p2#6> trattini ?#


p2#6:  #<p1#5> {[whispering] asp�}# <lp> #<p1#5> s� s� ,# uno due tre quattro e 
       cinque , sono cinque


p1#7:  la barca � verso la tua destra <sp> con la punta #<p2#8> con la#


p2#8:  #<p1#7> no �# verso la mia<aa> sinistra


p1#9:  allora il primo e+ / la prima differenza � questa


p2#10:  okay


<sp>


p1#11:  vai


p2#12:  vado allora <sp> <inspiration> <sp> e<ee> <sp> <tongue-click> la nuvola 
        <sp> <inspiration> quella in alto a sinistra <sp>


p1#13:  <inspiration> <lp> s� <lp> s�


p2#14:  <inspiration> <sp> ha delle righe piccoline ? <sp>


p1#15:  <lp> s� 


p2#16:  all'angolo sinistro <sp> 


p1#17:  <lp> s� <lp> 


p2#18:  quante sono ?


p1#19:  uno due tre quattro 


p2#20:  sono quattro pure le mie <sp> <inspiration> e la punta<aa> <sp> dov'� ?


p1#21:  alla mia #<p2#22> destra#


p2#22:  #<p1#21> st+ # <sp> alla tua destra , va bene <inspiration> <sp> e<ee> 
        l'altra nuvola


p1#23:  s�


p2#24:  <mh> <tongue-click> <inspiration> <sp> � rivolta verso destra o verso 
        sinistra


p1#25:  verso sinistra


<sp>


p2#26:  va bene <sp> <inspiration> e<ee> <sp> quel pallone che � a terra che 
        sembra un uovo di Pasqua


p1#27:  s�


p2#28:  ecco <inspiration> <sp> ha tre punti neri grandi ?


p1#29:  s�


p2#30:  <inspiration> e poi ci sono<oo> de+


p1#31:  strisce due


p2#32:  due <sp> due da una parte e due dall'altra ?


p1#33:  due da una parte e due dall'altra


p2#34:  okay <inspiration> e


p1#35:  <eeh> le le le strisce dall'altra parte <sp> dalla parte m+ / dalla mia 
        sinistra , congiungono l'altra macchia oppure si interrompono ?


p2#36:  s� <lp> <inspiration> no si interrompono e quella di<ii> / che � pi� in 
        alto � pi� corta


<sp>


p1#37:  s�


p2#38:  va bene <sp> <inspiration> e<ee> <ehm> <sp> vediamo


p1#39:  senti


p2#40:  il / la paperella che ha il bimbo <sp> no ? questo salvagente <sp>
        <inspiration> {[whispering] uno due tre quattro e cinque} ha cinque 
        punti neri


p1#41:  <eh> <lp> s�


p2#42:  va bene ?


p1#43:  senti


p2#44:  dimmi


<sp>


p1#45:  al punto di sotto <sp> al punto nero di sotto ci so+ c'� una striscia 
        che va da una parte all'altra ?


p2#46:  <mh> <lp> s�


p1#47:  e due sopra?


p2#48:  no


<sp>


p1#49:  no ? <lp> due sulla / due piccole strisce sull'alt+ sulla<aa> l'altra 
        linea ?


p2#50:  <inspiration> ci sono <sp> <eeh> <sp> due strisce <lp> sulla mia
        sinistra <sp> di cui ti ho detto che una � pi� corta <sp> <inspiration>
        e poi <sp> due strisce che congiungono i due punti neri <sp> e basta


p1#51:  dove ?


p2#52:  sul pallone che � a terra


p1#53:  no , sul salvagente dico , gi�


p2#54:  <ah> ! no , sul salvagente scusa <sp> allora <sp> <inspiration>


p1#55:  sulla paperina qua


p2#56:  sulla paperina <lp> c'� una striscia sulla mia #<p1#57> sinistra#


p1#57:  #<p2#56> allora alla# alla t+ <sp> scendendo dalla testa , no ?


p2#58:  s�


p1#59:  c'� la terza macchina


p2#60:  s�


p1#61:  c'� una striscia che va da una parte all'altra


<lp>


p2#62:  s� , ma non congiunge l'altro punto <sp> #<p1#63> si ferma#


p1#63:  no , #<p2#62> sopra# <sp> ci sono due piccole<ee> strisce ?


p2#64:  no , una ne ho io


p1#65:  una ?


p2#66:  s� una


p1#67:  allora questa � l'altra differenza


p2#68:  <inspiration> <sp> im+


p1#69:  il tappo della<aa> del salvagente qua , c'ha un coso rotondo intorno ?


p2#70:  <ss>� <lp> s�


p1#71:  che chiude ?


p2#72:  s� chiude


<sp>


p1#73:  <mh>


p2#74:  <inspiration> <eeh> vediamo un po'


p1#75:  <ehm>


p2#76:  conta le dita del bimbo che tiene la paperella , tante volte #<p1#77> ne 
        ha una in meno#


p1#77:  #<p2#76> uno due tre# <sp> quattro


<lp>


p2#78:  vabb� <sp> uno due tre quattro e poi c'� il pollice cinque


p1#79:  senti , il bimbo � sorridente <sp> o � triste ?


p2#80:  � triste


p1#81:  ce l'ha il sopracciglio ?


p2#82:  <inspiration> <sp> uno


p1#83:  s�


p2#84:  <inspiration> <sp> e<ee> <sp> vedi l'orecchio <sp> <inspiration> <ehm> 
        c'� un segno che � come una -A- in stampatello <sp> minuscola


p1#85:  s� <lp> s� s� #<p2#86> s� s� s�#


p2#86:  #<p1#85> <ah> ! va bene# <sp> <inspiration> e<ee> poi vediamo


p1#87:  la b+ cio� la<aa> la bocca ha due<ee> strisce ?


p2#88:  s� ha due strisce , #<p1#89> una# <sp> <eeh> pi� orizzontale e una �<ee> 
        #<p1#89> curva#


p1#89:  #<p2#88> s�# <sp> #<p2#88> s� s�#


p2#90:  va bene <inspiration>


p1#91:  ce l'ha l'ombelico ?


p2#92:  s�


<sp>


p1#93:  sopra l'ombelico c'�<ee>


p2#94:  c'� un segnetto


p1#95:  s�


p2#96:  {[laughing] non ti so dire che rappresenti , per� #<p1#97> � uno solo# }


p1#97:  #<p2#96> le caviglie# sono riportate ?


p2#98:  <inspiration> <sp> s�


p1#99:  tutt'e due ?


p2#100:  s� s� sono riportate tutt'e due <lp> <tongue-click> <sp> a uno c'� 
         un segnetto come un uncino e all'altro<oo> <ehm> <lp> pi� orizzontale 
         <lp> <inspiration> <sp> <ah> ! il ginocchio <sp> c'� un puntino per 
         parte


p1#101:  <eh> <lp> s�


p2#102:  <mh> va bene <sp> <inspiration> <sp> e<ee> <lp> all� [dialect] andiamo 
         sulle onde

p1#103:  <ehm> <lp> s�


p2#104:  <inspiration> <lp> vedi<ii> / partendo dalla tua <sp> destra <lp> 
         <inspiration> <lp> ci sono delle on+ / uno due tre quattro <lp> 
         quattro onde <lp> e #<p1#105> poi ce ne sono# / ecco due laterali 
         <lp> #<p1#105> ai lati# della barca


p1#105:  s� <lp> uno due tre quattro cinque sei #<p2#104> ne ho io# 
         <lp> #<p2#104> s�# <lp> s�


p2#106:  va bene


p1#107:  due laterali ?


<sp>


p2#108:  s� diciamo che<ee>


p1#109:  s� s� #<p2#110> s� s�# s�


p2#110:  #<p1#109> <eh> !# <sp> s� s� <lp> <tongue-click> <sp> poi vediamo 
         un'#<p1#111> altra cosa# <inspiration>


p1#111:  #<p2#110> se+ / la pietra# che � sopra alla<aa> all'uovo di pasqua l� 
         <lp> c'ha delle striscine dentro ?


<lp>


p2#112:  <ehm> <lp> <tongue-click> <sp> non � proprio una striscia <sp> io ho 
         sul mio lato #<p1#113> destro#


p1#113:  #<p2#112> dei cerchietti#


p2#114:  <inspiration> <sp> e non sono nemmeno cerchietti , sul mio lato destro 
         <sp> � come<ee> <NOISE> <lp> <inspiration> <eeh> #<p1#115> come# se 
         fosse un po' spezzata <sp> #<p1#115> rigata#


p1#115:  #<p2#114> <mh># <lp> #<p2#114> no sulla#<aa> <sp> sulla pietra che hai 
         sul<ll> sulla tua de+ sulla tua des+ <sp> sulla tua sinistra


<sp>


p2#116:  <inspiration> no io sulla mia sinistra non ha nessuna pietra


p1#117:  allora questa � l'altra differenza <sp> trovata !


p2#118:  ed � vicino il pallone ?


p1#119:  s�


<lp>


p2#120:  <inspiration> <sp> <eeh> <sp> il muso della paperella


p1#121:  no , � finito Teresa , sono tre le differenze


<lp>


p2#122:  tre differenze sono ? <lp> no , sono tre indicazioni {[laughing] che 
         danno le differenze <inspiration> non tre differenze <inspiration>} 
         <lp> <eh> <sp> il muso<oo> <sp> della paperella <lp> <inspiration> <sp> 
         � un muso che sorride ?


<sp>


p1#123:  s� <lp> s�


p2#124:  <ah> ! va bene <lp> <inspiration> <lp> c'ha un occhio nero <sp> la 
         paperella ?


p1#125:  s�


p2#126:  va bene <lp> <tongue-click> e<ee> <ehm>


p1#127:  se+ al braccio c'ha dei segnetti il<ll> ragazzo ?


p2#128:  <inspiration> <sp> ha un segnetto soltanto


p1#129:  uno ?


p2#130:  s� uno


p1#131:  no ne deve avere due , questa � l'altra differenza


p2#132:  <NOISE> <sp> allora ne ha uno <sp> <inspiration> <sp> poi<ii> <sp> / il 
         ciuffo del bimbo <sp> #<p1#133> �# tipo<oo> Little Tony ? <sp> <NOISE>


p1#133:  #<p2#132> s�# <lp> s� s� s� s� s�


<lp>


p2#134:  <mh> <inspiration> cos� va bene allora <sp> <inspiration> <sp> e<ee>


p1#135:  quanti occhi ha ?


p2#136:  <laugh> <sp> {[laughing] due <lp>} <inspiration> due occhi <lp> 
         <inspiration> tristi


p1#137:  <laugh> <lp> s�


p2#138:  vediamo un po' <lp> <ehm> <sp> vicino la punta del pallone <sp> 
         <tongue-click> #<p1#139> c'�<ee># come una pietra sulla mia destra


p1#139:  #<p2#138> s�# <lp> e te l'ho gi� detta Tere' [dialect] <lp> tu hai 
         detto che non c'�


p2#140:  e no , ma tu mi dicevi una pietra sulla sinistra !


p1#141:  sulla tua sinistra


p2#142:  no <sp> sulla mia sinistra non c'� nessuna pietra <sp> io ti dico in 
         alto <lp> <inspiration> <sp> e<ee> a destra / in alto <ehm> a destra 
         del pallone


<sp>


p1#143:  s�


p2#144:  <eh> ! c'� una pietra <sp> <inspiration> <sp> <eeh> o comunque<ee> s� ,
         penso che sia una pietra <sp> anche se non � proprio rotonda


p1#145:  s� s�


p2#146:  <eh>


<lp>


p1#147:  <eh> che vuoi sapere ?


p2#148:  e<ee> <ehm> <sp> all'estremit�<aa> / all'angolo destro <lp> � un 
         po'<oo> spezzata con<nn> #<p1#149> con una riga# ?


p1#149:  s� <lp> #<p2#148> s�# <lp> #<p2#150> s�#


p2#150:  <ah> #<p1#149> va bene# allora niente <inspiration> e poi <sp> 
         <inspiration> pi� in basso <lp> sempre al lato<oo> sul lato destro 
         del pallone , #<p1#151> ci sono# delle righe ?


p1#151:  s� <lp> #<p2#150> s�# <lp> s� <sp> delle specie <eeh>


p2#152:  #<p1#153> tre come delle onde# nella s+ <eh> ! ho capito <lp> 
         <inspiration> e po+


p1#153:  #<p2#152> tre <lp> s�# <lp> e scendendo a sinistra ce l'hai un<nn>


p2#154:  c'� <sp> s� infatti <sp> e<ee> <sp> <ah> ! s�


p1#155:  quante ne hai una due tre <sp> una grande e #<p2#156> due piccole ?#


p2#156:  #<p1#155> <ss>s�# <sp> <inspiration> � come se ci fosse un filo <sp> 
         attaccato al pallone e poi ci sono due<ee>


p1#157:  <eh> <lp> <eh> <lp> staccate per�


p2#158:  s� , due segnetti staccati <inspiration> <sp> e poi


p1#159:  a sinistra del bambino <lp> #<p2#160> del ragazzo#


p2#160:  #<p1#159> a sinistra s�# <lp> s� , in basso


p1#161:  <eh>


p2#162:  <eh> <inspiration>


p1#163:  hai 'na specie #<p2#164> di<ii># <sp> segnetti ?


p2#164:  #<p1#163> s� co+# <lp> s� come delle montagnette #<p1#165> fatte nella# 
         sabbia


p1#165:  #<p2#164> <eh># <sp> all'interno , alla / prima vedi quello pi� grande


p2#166:  s� all'interno c'� come un pettine rotto


p1#167:  <eh> <lp> s� , poi seguono<oo> #<p2#168> le<ee> le cose ?#


p2#168:  <eh> <sp> <inspiration> con <sp> #<p1#167> uno due tre#
         quattro<oo> <sp> s� con quattro<oo> punte #<p1#169> diciamo#


p1#169:  #<p2#168> s�#


p2#170:  <inspiration> e poi sopra ci sono altri segnetti #<p1#171> sempre come# 
         delle onde <lp> e<ee> e contiamo ? <lp> uno due tre quattro 
         cinque sei sette <sp> alc+ alcune attaccate e altre no


p1#171:  #<p2#170> s�# <lp> s� <lp> <mh> <P> senti alla<aa> / sempre alla 
         sinistra del<ll> / guardando il ragazzo <lp> hai <sp> uno due tre 
         quattro cinque sei sette otto onde ?


p2#172:  del bim+ <lp> s� <P> uno due tre quattro cinque sei sette 
         otto e nove io in alto <inspiration> proprio sotto il muso , 
         #<p1#173> sotto# l'orizzonte s� ne ho due


p1#173:  #<p2#172> due# <lp> poi uno due tre quattro cinque e sei , no 
         devono / io ne ho sei


p2#174:  allora due quattro sei sette <sp> io ne ho sette compresa quella 
         della riva proprio<oo>


p1#175:  <ah> ! la riva non l'ho contata io <sp> va bene se 


p2#176:  <eh> contala <sp> � sempre un'onda


p1#177:  se+ <sp> sotto dove c'�<ee> la chiusura del salvagente <lp> quasi 
         vicino la mano <lp> hai un'onda ?


p2#178:  <ss>s� <lp> s� <sp> s� <sp> ce l'ho


p1#179:  <mh>


p2#180:  ce l'ho quest'onda <sp> <inspiration> <sp> <ehm> <NOISE> <lp> vediamo 
         un po' cos'altro possiamo <ah> ! ecco <sp> il pantaloncino del bimbo 
         <sp> <inspiration> <sp> m<ehm> <sp> c'� come<ee> <lp> ci sono dei 
         puntini <sp> #come una#<p1#181> cucitura ?


p1#181:  s� <lp> #<p2#180> s�# <lp> s�


p2#182:  <ah> ! ho capito <lp> <inspiration> <lp> vediamo un po' <sp> 
         <inspiration> ti risulta una spalla pi� alta e una pi� bassa ?


p1#183:  s�


p2#184:  vabb�


<lp>


p1#185:  sotto l'orecchio ti contiene dei capelli ?


p2#186:  s� <lp> <inspiration> il mento � leggermente staccato , la prosecuzione 
         del viso rispetto all'orecchio , no ? <sp> � leggermente staccata dal 
         collo


<sp>


p1#187:  s�


p2#188:  <ah> ! va bene <sp> <inspiration> <sp> e poi vediamo un po' <sp> 
         <inspiration> e<ee> <sp> vedi il sorriso della paperella ? <lp> 
         <inspiration> c'� poi una<aa> lineetta<aa>


p1#189:  s� <lp> che scende verso gi� ?


p2#190:  s�


p1#191:  s�


p2#192:  okay


p1#193:  vabb�


p2#194:  <ehm> <sp> mi sembra che non ci sia altro #<p1#195> non lo so#


p1#195:  #<p2#194> no# , abbiamo detto tutto <sp> penso che altre differenze non 
         ce ne sono


<lp>


p2#196:  vabb� il polpaccio � bello<oo> <sp> sai <sp> quei polpacci


p1#197:  s� <lp> tra una gamba e l'altra c'� una piccola #<p2#198> differe+ / 
         cio� una staccatura# si nota


p2#198:  #<p1#197> <ss>s+ / cio�# <lp> s� una staccatura si vede un po' di 
         bianco


p1#199:  s�


p2#200:  <mh>


p1#201:  bene <lp> penso che non c'� nient'altro


<lp>


p2#202:  <mh> mi sembra di no <sp> il naso ce l'ha , s� ?


p1#203:  s�


p2#204:  sai com'� <sp> tante volte <lp> <inspiration> <sp> e poi vediamo un po' 
         #<NOISE> <lp> <eeh> <lp># il mare molto calmo


<sp>


p1#205:  s� #<NOISE> <lp># ma io non riesco a trovare pi� niente <lp> boh ?


<lp>


p2#206:  allora vediamo<oo> <sp> <inspiration> <sp> e<ee> <lp> ci sono due / te 
         l'ho detto se ci sono due segnetti<ii> sulle ginocchia ?


<sp>


p1#207:  s� , ci sono


p2#208:  <tongue-click> s� <sp> <inspiration> e poi il polpaccio<oo> � un 
         pochino<oo> pi�<uu> <ehm> <tongue-click> pi� grande <sp> alla parte 
         superiore ?


<lp>


p1#209:  � a posto


p2#210:  s� <lp> vabb� <sp> <inspiration> <sp> e vediamo qualcos'altro <lp> 
         <inspiration> <lp> <NOISE> <lp> la linea dell'orizzonte <sp> � continua
         ?


p1#211:  s�


<sp>


p2#212:  <mh> <lp> <inspiration> <sp> <ehm> <lp> ce l'hai il sole ?


<sp>


p1#213:  no


p2#214:  <tongue-click> <sp> nemmeno io #<NOISE> <lp># e secondo te perch� � 
         triste il bimbo allora ? <lp> <tongue-click> ha il salvagente , ha il 
         pallone , perch� � cos� triste ?


<lp>


p1#215:  non � che tu hai qualche secchiello <sp> qualcosa <sp> no ?


p2#216:  no , non ho niente io <sp> forse � triste per questo perch� si 
         trova<aa> <sp> in ma+ al mare <sp> <inspiration> senza un 
         secchiello<oo> senza<aa> una


p1#217:  ha fatto 'o cattivo , secchiello niente <lp> #<p2#218> {[dialect] ha 
         vagnato <unclear>}#


p2#218:  #<p1#217> <eh> <sp> probabilmente# <lp> <inspiration> per� � cos� 
         grande quel salvagente <sp> non lo so perch�


<sp>


p1#219:  vabbu� [dialect]


p2#220:  comunque <inspiration> <sp> e poi questo pallone a forma di uovo !


p1#221:  ma non � nu' pallone <sp> boh?


<lp>


p2#222:  secondo me � un aquilone <inspiration> <ah> ! gli si � spezzato 
         l'aquilone <sp> e<ee> si � messo a piangere <lp> forse


<lp>


p1#223:  non #<p2#224> sembra# un aquilone


p2#224:  #<p1#223> <eh># <lp> e per� c'� il filo <lp> sembra <lp> sembra un filo 
         , un #<p1#225> filo# spezzato


p1#225:  #<p2#224> <mah> !#


<lp>


p2#226:  ci sono le nuvole forse<ee> tra poco arriver� la pioggia ! <lp> dato 
         che il disegno � in bianco e nero noi non riusciamo a vedere se � 
         nuvoloso allora il bimbo si � rattristato


p1#227:  quante nuvole hai tu ?


p2#228:  <inspiration> <sp> io ? ho due nuvole <lp> per� il bimbo � triste 
         guardando il pallone , per questo ti dico #<p1#229> che quello deve#


p1#229:  #<p2#228> s� , e dimmi qualche / cos'hai# , cos'hai tu ? io cio�<ee> ci 
         so+ appu+ / non ho nient'altro di differenza


<sp>


p2#230:  <inspiration> e io nemmeno mi sembra <sp> <tongue-click> <sp> <eh> 
         <eeh> l'ho guardato bene <lp> <tongue-click> <inspiration> 
         l'interno<oo> <lp> <tongue-click> del<ll> <sp> <inspiration> <lp> 
         <tongue-click> del salvagente <lp> no ? <lp> <inspiration> <eeh> <sp> 
      c'� qualche linea particolare ? <lp> no , #<p1#231> mi sembra di no#


p1#231:  s� <lp> <mh> <lp> #<p2#230> se+ tra la mano# <lp> e l'altro<oo> punto 
         nero del #<p2#232> salvagente hai una linea ?#


p2#232:  {[whispering] tra la mano <inspiration>] <lp> #<p1#231> <ah> s� , quello 
         sulla destra# quello sulla destra


p1#233:  <eh> #<p2#234> hai una linea ?#


p2#234:  #<p1#233> s� per� � continua# <sp> s�


p1#235:   s� s� s�


p2#236:  congiunge la mano al punto nero <sp> e poi <sp> alla destra dell'ultimo 
         punto nero <lp> c'� una linea che congiunge appunto questo <sp> questo 
         punto nero <sp> ecco ripeto punto <sp> <inspiration> <sp> e poi<ii> 
         il<ll> salvagente <sp> capito ?


p1#237:  s� <lp> s� <lp> s� <lp> s� s�


<lp>


p2#238:  <mh> <lp> <tongue-click> <inspiration> e poi vediamo un po'


p1#239:  stop
