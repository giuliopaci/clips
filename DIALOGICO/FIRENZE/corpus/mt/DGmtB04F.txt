TEXT_inf.

MAT:            mt
MAP:            B
TRM:
Ndl:            04
Ntr:          
Nls:
REG:            F


SPEAKERS_inf.

INp1:         L. M., M, 20, Firenze, non spontaneo, non fluente, G>F
INp2:         C. L., F, 25, Firenze, spontaneo, poco fluente, F>G


RECORDING_inf.

TYP:          DAT
LOC:          Firenze/abitazione
DAT:          14/02/01            
DUR:          11.44,382
CON:          la registrazione � molto sporca


TRANSCRIPTION_inf.

DAT:          08/04/02
CMT:          L'annotazione del Noise solo laddove � particolarmente evidente
Nst:          262



p1G#1:  allora <inspiration> {<NOISE> <eeh> si parte<ee> <lp> nella parte}
        <breath> sotto il foglio <breath> {<NOISE> sulla} destra <breath>


p2F#2:  s�


p1G#3:  non proprio in fondo per� <inspiration> <NOISE> circa un dieci                    centimetri<ii> <breath> {<NOISE> dodici} centimetri <inspiration> pi�             verso il centro


p2F#4:  <inspiration> aspetta <eh> !


p1G#5:  s�<ii> <breath>


p2F#6:  io non ho <sp> per� sufficiente spazio ora per <sp> disegnare <sp>
        <inspiration> allora <sp> <ehm> in fondo rispetto al #<G#7> foglio#


p1G#7:  #<F#6> s�<ii># <vocal> #<F#8> sulla destra#


p2F#8:  #<G#7> s� <sp> sulla# #<G#9> destra# 


p1G#9:  #<F#8> s�<ii># 


p2F#10:  <mh> 


p1G#11:  {[dialect] da i'} lato destro {<NOISE> <vocal>} ci saranno cinque
         centimetri


p2F#12:  s� 


p1G#13:  <mh> <inspiration> <sp> e alla <eeh> rispetto al fondo {[dialect] de 
         i'} foglio una decina , dodici centimetri 


p2F#14:  s�


p1G#15:  <NOISE> s� <NOISE> <tongue-click> <vocal> bene <inspiration> <NOISE>
         allora poi <breath> si scende gi�


p2F#16:  ma il tuo foglio {[voiceless-velar-stop] oc+} / � occupato tutto dal percorso ?


p1G#17:  quasi tutto


p2F#18:  cio� , met+ #<G#19> met� ?#


p1G#19:  #<F#18> la parte# <eh> , s�<ii> <NOISE> tre quarti


p2F#20:  <mh> <sp> e #<G#21> il<ll> percorso#


p1G#21:  #<F#20> i tre quarti di sotto# , un quarto<oo> sopra no


p2F#22:  un quarto sopra no , ovviamente , solo un quarto quindi <inspiration>
         io non so dove disegnare per� tre quarti di+ / del del tuo foglio <sp>
         <breath> cio� , non ho lo {<NOISE> spazio} comunque <inspiration> va
         bene <lp> <vocal> #<G#23> <eeh> <breath>#


p1G#23:  #<F#22> {<hypo-articulated> allora} s�# <sp> #<F#24> si comincia# , vai


p2F#24:  #<G#23> dimmi# <sp> s�


p1G#25:  <NOISE> allora <inspiration> <eeh> immagina di scendere <breath>
         un sette otto centimetri


p2F#26:  s�


p1G#27:  <mh> <inspiration> <NOISE> a sinistra di questa<aa> / di questo
         a righe / di questo percorso <inspiration> c'� una sedia


p2F#28:  s�


p1G#29:  <mh> <inspiration> <tongue-click> <sp> bisogna girare intorno alla                sedia <sp> e andare verso la parte sinistra del foglio


p2F#30:  <inspiration> allora <sp> girare intorno <sp> alla #<G#31> sedia#


p1G#31:  #<F#30> s�# <inspiration> sei sotto la sedia ?


p2F#32:  s�


p1G#33:  ecco , ora <vocal> <inspiration> diciamo dalla sedia <sp> fai una una
         riga <sp> continuando i' [dialect] percorso , per quarantacinque gradi
         vai


p2F#34:  <inspiration> allora la sedia <sp> accanto c'� il treno , no ?


p1G#35:  no <sp> accanto alla sedia c'ho una mano


<sp>


p2F#36:  accanto alla sedia c'hai una mano ?


p1G#37:  s� <breath> <sp> e sopra un cuore


<lp>


p2F#38:  <mm>mah ! <breath> <inspiration> allora <sp> <ehm> <breath>
         no , perch� io c'ho ho la se+ la stessa sedia rossa


p1G#39:  s�


p2F#40:  <inspiration> il cuore ce l'ho in alto per�


<sp>


p1G#41:  di quanto rispetto alla #<F#42> sedia ?#


p2F#42:  #<G#41> <eeh># in fon+ / per me in fondo al mio percorso


<sp>


p1G#43:  io ce n'ho due di cuori , ce n'ho uno {<laugh> rosso e uno
         giallino} <breath> <NOISE> #<F#44> arancione#


p2F#44:  #<G#43> ecco <sp> uno# / quello giallino sopra e quello rosso sotto


p1G#45:  s�<ii>


p2F#46:  e sono <sp> in cima al foglio tuo ?


p1G#47:  quello gia+ <vocal> quello giallino , s� <breath> quell'altro � un
         po' a / diciamo vicino al centro


p2F#48:  vicino al #<G#49> centro#


p1G#49:  #<F#48> nella# parte di sotto , {<hypo-articulated> per�}


p2F#50:  <mh> <inspiration> e poi <vocal> <eeh> <sp> per / no , per vedere se 
         abbiamo gli stessi #<G#51> oggetti#


p1G#51:  #<F#50> s� s� s�# ho capito <inspiration> in alto a destra c'ho una               mano


p2F#52:  s�


p1G#53:  sotto la mano <sp> una forchetta


p2F#54:  <mh> no <sp> #<G#55> io la forchetta non ce l'ho#


p1G#55:  #<F#54> no <eeh> <breath># e io sotto c'ho una pipa


p2F#56:  s�


p1G#57:  <mh> <inspiration> poi il gelato su in alto a sinistra


p2F#58:  s�


p1G#59:  sotto i limoni


p2F#60:  s�


p1G#61:  che tra l'altro i limoni sono il mio punto di partenza


p2F#62:  <ah> <sp> <tongue-click> <inspiration> quindi <NOISE> il tuo punto di             partenza � il limone


p1G#63:  #<F#64> s�#


p2F#64:  #<G#63> e# perch� m'hai fatto partire dalla sedia ?


p1G#65:  <eeh> {<laugh> perch� perch�} {<hypo-articulated> credevo} di partire da Pi uno , vabb� !
         <inspiration> #<F#66> <NOISE> tanto ci si arrivav+ / vabb�#


p2F#66:  #<G#65> <mh> <inspiration> quindi , allora , il limone# / e il punto di
         partenza � sopra o sotto il limone ?


p1G#67:  allora <NOISE> <ii>il / � sopra


p2F#68:  � sopra ai #<G#69> limoni#


p1G#69:  #<F#68> s�# <sp> i limoni sono comunque subito {<NOISE> accanto}                  <NOISE> #<F#70> appena# appena accanto sulla destra


p2F#70:  #<G#69> <tongue-click> s�# <lp> s�


p1G#71:  <mh> <sp> <inspiration> <tongue-click> allora , te imma+ <sp> <vocal>
         se+ / vai l+ / nelle / diciamo , nel quadrante sopra del foglio


p2F#72:  s�


p1G#73:  bene <sp> sulla<aa> sulla sinistra


p2F#74:  s�


p1G#75:  vai <inspiration> <tongue-click> scendi un dieci centimetri


p2F#76:  <inspiration> dal limone scendo gi� in direzione frigorifero ?


p1G#77:  no , io 'un [dialect] ce l'ho i' [dialect] frigorifero


p2F#78:  il gatto ?


p1G#79:  s+ / c'ho i' [dialect] bar / s� , va bene , il gatto ce l'ho , s� <sp>
         � il secondo simbolo che devo attraversare


p2F#80:  quindi il gatto ce l'hai gi� nel<ll> / a<aa> sinistra del #<G#81>
         foglio ?#


p1G#81:  #<F#80> s�# <sp> #<F#82> in basso#


p2F#82:  #<G#81> in basso# <inspiration> allora , io vado verso il gatto <sp>
         <tongue-click> #<G#83> cont+# continuativamente o devo interromperlo
         il segmento ?


p1G#83:  #<F#82> s�# <breath> <lp> no no no no , vai gi� verso i' [dialect]
         #<F#84> gatto# 


p2F#84:  #<G#83> s�# fino #<G#85> all'estremit�# del<ll> rettangolino quale                sinistra ?
   

p1G#85:  #<F#84> passagli# <lp> s� <sp> s� <sp> s� , ti / passi s+ / gli passi
         intorno {[dialect] a i'} gatto


p2F#86:  #<G#87> {<NOISE> s�}#


p1G#87:  #<F#86> per�# dalla parte di fuori , capito ? #<F#88> <inspiration>               cio�#


p2F#88:  #<G#87> s� s� , d+# / #<G#89> esternamente gli passo <eh> ! e 
         continuativamente# o tratteggiato ? <lp> #<G#89> il#


p1G#89:  #<F#88> s+ <sp> <eh> ! s� , esternamen+# <inspiration> <lp>
         #<F#88> tratteggiato# <breath> � tutto tratteggiato <breath>


p2F#90:  va bene <sp> <vocal> allora , siamo arrivati a qualcosa #<G#91> vad+# 
         vado intorno al #<G#91> gatto# 


p1G#91:  #<F#90> s�# <lp> #<F#90> s�# 


p2F#92:  {<NOISE> <mh>}


p1G#93:  ecco , passato il gatto ritorna su / in su <sp> verso la parte / sempre 
         {[dialect] co' co' i' co' i'} tratteggio spostandoti per� leggermente 
         <inspiration> verso destra <sp> continua i'[dialect] tratteggio piano 
         piano


p2F#94:  allora il<ll> <ehm> <vocal> verso dest+ <sp> devo girargli intorno
         al gatto ?


p1G#95:  <tongue-click> s� , gli hai gi� girato #<F#96> intorno#


p2F#96:  #<G#95> s�#


p1G#97:  ecco , te ora con le / con i' [dialect] tratteggio a che punto sei ?
         stan <vocal> #<F#98> stai<ii>#


p2F#98:  #<G#97> sono parallela# alla fine del<ll> <eeh> del<ll> gatto


p1G#99:  s� <sp> bene


p2F#100:  <mh> <tongue-click> <inspiration> <sp> e poi c'ho questo treno che tu             non hai


p1G#101:  no <sp> 'un [dialect] ce l'ho io , #<F#102> io# c'ho una freccia
          che ora bisogna #<F#102> passarci#


p2F#102:  #<G#101> <mh># <lp> #<G#101> ecco# allora , io devo andare verso la
          #<G#103> freccia ?#


p1G#103:  #<F#102> s�# <sp> e devi <sp> devi pass�' intorno anche alla freccia


p2F#104:  s� <lp> <vocal> di / e quindi � d+ / � una diagonale questa ?


p1G#105:  s� <sp> s� <inspiration> per� <NOISE> considera una cosa 


p2F#106:  #<G#107> <mh>#


p1G#107:  #<F#106> che# te il gatto 'un [dialect] ci sei passata 
          di sotto #<F#108> diciamo <sp> dalla# freccia ci devi passare di 
          #<F#108> sopra#


p2F#108:  #<G#107> s�# <sp> <inspiration> #<G#107> <ah># <sp> ecco <inspiration>
          la fu+ / quindi <vocal> in realt� non � una diagonale <sp> cio� 
          #<G#109> mi �<ee> / devo an+#


p1G#109:  #<F#108> s� <sp> � un po' spostata# , per quello 'un [dialect] / devi
          girare un p+ / a destra un pochino


<sp>


p2F#110:  s�


p1G#111:  <mh>


p2F#112:  allora , devo passarci di sopra <sp> <NOISE> io ho la punta rivolta               verso il basso della freccia


<sp>


p1G#113:  <ss>s�


p2F#114:  e invece devo passare quindi <lp> intorno <breath>


p1G#115:  s� <sp> di sopra


p2F#116:  <vocal> di #<G#117> sopra#


p1G#117:  #<F#116> <mhmh>#


p2F#118:  <mh>


p1G#119:  {[whispering] ecco} <vocal>


p2F#120:  e <sp> <ehm> <sp> piego poi / praticamente vado su<uu> <sp> dal
          gatto ve+ / vado su / in #<G#121> su in<nn># in me+ / molto
          stretto come<ee> 


p1G#121:  #<F#120> s�# <inspiration> <lp> s� 


p2F#122:  cio� #<G#123> <ehm>#


p1G#123:  #<F#122> s�# , ci sar� <ehm>


p2F#124:  <tongue-click> � quasi verticale , no ?


p1G#125:  s� <sp> #<F#126> s�#


p2F#126:  #<G#125> e# poi <sp> vado per orizzontale intorno alla freccia


p1G#127:  s�


p2F#128:  a questo punto ?


p1G#129:  gli giri intorno / ecco a questo punto {[voiceless-palatal-fricative] sc+} scendi gi� <inspiration>
          <sp> d+ / ritornando un pochino sulla sinistra , appena appena


p2F#130:  #<G#131> s�#


p1G#131:  #<F#130> cio� devi# fare come una<aa>


p2F#132:  io ho qui una barchetta qui


p1G#133:  s� <sp> s� <sp> simile


p2F#134:  quindi devo an+ <sp> devo<oo> attrav+ <ehm> devo girare intorno alla
          barchetta da destra o da sinistra ?


p1G#135:  te la barchetta dove ce l'hai ? <breath> {[dialect] a i'} posto della             freccia ?


p2F#136:  <nn>no , la freccia l+ la la barchetta ce l'ho in basso <sp> a destra             della freccia


p1G#137:  {[whispering] in basso a destra} <inspiration> bene , io c'ho i'
          [dialect] cuore l� / ecco te passa a / dalla<aa> <vocal> {[dialect] su 
          i'} fianco<oo> sinistro della<aa> della #<F#138> barchetta#


p2F#138:  #<G#137> s�#


p1G#139:  <tongue-click> bene , scendi gi�


p2F#140:  s�


p1G#141:  per� <sp> non andare to+ totalmente {<NOISE> dritta} / ri+ <sp> cio�              passagli per� s+ <eeh> passando<oo> da / andando d+ / ritornando                  verso<oo> verso destra <inspiration> perch�<ee> se no si becca<aa> un             altro simbolo


<lp>


p2F#142:  s� <sp> <tongue-click> e la sedia ?


p1G#143:  <tongue-click> la s+ / ecco , la sedia � sotto<oo> <inspiration> 
          subito sotto la la / il mio cuore quindi la tua barchetta                         <inspiration> <NOISE> spostata verso destra


p2F#144:  io devo ul+ / raggiunger la sedia #<G#145> ora ?#


p1G#145:  #<F#144> s�# la devi<ii> <vocal> <sp> gi+ / ci giri intorno e                     {[dialect] s'� finito}


p2F#146:  {<NOISE> <ah> ! ci giro intorno ?}


p1G#147:  s� 


p2F#148:  #<G#149> <ah> ! ecco#


p1G#149:  #<F#148> e torni leggermente# in {<NOISE> su} <lp> <tongue-click>                 appena appena {[whispering] proprio}


p2F#150:  s� <sp> <tongue-click> <sp> dove m'hai fatto partire {<NOISE> prima}


p1G#151:  s� <eh> ! {<laugh> infatti} #<F#152> <laugh>#


p2F#152:  #<G#151> va bene# <breath> <vocal> allora <breath> boh <breath>                   <vocal> allora , ti dico<oo> il<ll> <ehm> <sp> e lo unisco al mio                 punto di partenza <sp> <tongue-click> <sp> <vocal> che � <sp>                     praticamente lo stesso punto tuo di <sp> <inspiration> <sp> <vocal> di            arrivo


p1G#153:  <mhmh> <sp> ma <eeh> te l'hai disegnato<oo> sulla mappa la<aa> 


p2F#154:  s� <breath>


p1G#155:  <mh>


[role exchange (F>G)]


p2G#156:  <NOISE> allora <sp> <tongue-click> cio� no , ce l'ho <vocal> 
          ce l'ho<oo> gi� il p+ / il mio punto di partenza che <sp> che coincide
          col tuo , no , di arrivo <lp> <tongue-click> allora <inspiration> <sp>
          <ehm> {<hypo-articulated> praticamente} il<ll> <ehm> devi fare due tratti <vocal> verticali            <sp> verso <sp> la {<NOISE> pipa}


p1F#157:  {<NOISE> allora} <breath> <vocal> qui ins+ <sp> la <vocal> <sp> la                pipa io ce l'ho sopra , #<G#158> <eh> !#


p2G#158:  #<F#157> s�# <sp> <inspiration> <sp> devi fare <sp> <vocal> dal                   {<NOISE> punto} <sp> l'al+ <vocal> dal tuo punto di arrivo


p1F#159:  s�


p2G#160:  devi <sp> <eeh> f+ <sp> <ehm> tratteggiare due trattini in verticale


p1F#161:  s� <breath>


p2G#162:  va bene ?


p1F#163:  #<G#164> {[whispering] due}#


p2G#164:  #<F#163> dopodich�# <sp> <eeh> io t'ho detto che avevo una barchetta ,
          no ?


p1F#165:  s�


p2G#166:  <eeh> fra la {<NOISE> pipa} <breath> <sp> <NOISE> #<F#167> e questo#              simbolo che io avevo


p1F#167:  #<G#166> s�# <lp> #<G#168> s�#


p2G#168:  #<F#167> te non# so se <sp> <tongue-click> se hai il 
          cuore , #<F#169> che cos'hai#


p1F#169:  #<G#168> io c'ho il cuore# s� , e #<G#170> pos+ <unclear>#


p2G#170:  #<F#169> ecco , te# devi <sp> tratteggiare <inspiration> <sp> <ehm>               <vocal> devi fare <sp> <ehm> un {<NOISE> tratto} <sp> attaccato <sp>              alla<aa> / tratteggio che hai appena fatto


p1F#171:  <mh>


p2G#172:  <ehm> un po'<oo> curvo


p1F#173:  s�


p2G#174:  <vocal> <sp> <ehm> per� che <eeh> <sp> passa in mezzo ai due simboli ,            no ?


p1F#175:  s� <NOISE> {<NOISE> <inspiration>} ho capito ho capito ho capito


p2G#176:  poi un altro trattino sempre curvo #<F#177> sempre#


p1F#177:  #<G#176> s� , questi eran# due , hai detto <eh> ? 


p2G#178:  s�


p1F#179:  bene , poi un altro ?


p2G#180:  {<hypo-articulated> allora}, due erano quelli verticali


p1F#181:  s� <sp> s� #<G#182> s� s�#


p2G#182:  #<F#181> poi uno# <sp> {<NOISE> attaccato} <sp> <inspiration> <sp>                <eeh> un po' obliquo <sp> <tongue-click> che t'ho detto che <sp> passa            fra i d+ fra i due simboli


p1F#183:  s�


p2G#184:  e poi continui


p1F#185:  s�


p2G#186:  per� devono essere piuttosto irregolari questi<ii> / un {<NOISE> po'}             mossi #<F#187> perch� non so+# non sono<oo> #<F#187> rettilinei#


p1F#187:  #<G#186> <ah> !# <lp> #<G#186> {<hypo-articulated> aspetta}# , s� s�


p2G#188:  <eeh> ne fai un altro <lp> <tongue-click> un pochino storto un po'<oo>            / quasi orizzontale <lp> per� che sta salendo , fa / indica il                    movimento del salire , no ?


p1F#189:  {<NOISE> bene}


p2G#190:  e poi <sp> un altro , sempre a<aa> {<NOISE> <ehm>} due {<hypo-articulated> centimetri} di              distanza , che inizia invece a raddrizzarsi , sempre per� <sp> <ehm>              <tongue-click> <vocal> diciamo<oo> <sp> <uu>un po' in diagonale


p1F#191:  s�


p2G#192:  � sempre irregolare <sp> <inspiration> finch� devi farne <sp>
          <NOISE> <sp> altri due


p1F#193:  #<G#194> <mhmh>#


p2G#194:  #<F#193> io c'ho# il bar qui


p1F#195:  e dov+ <vocal> e ce l'ho anch'io {[dialect] i bare} !


p2G#196:  #<F#197> ecco#


p1F#197:  #<G#196> i' [dialect] bar# ce l'ho circa<aa> <sp> fai
          {<NOISE> conto} i' [dialect] centro {[dialect] de i'} foglio 
          un po' sopra 


p2G#198:  ecco , #<F#199> s�#


p1F#199:  #<G#198> {<NOISE> s+# <sp> leggermente} sulla #<G#200> destra <sp>                leggerm+#


p2G#200:  #<F#199> {<NOISE> allora , te} devi costeggiare# ora il bar <vocal>               #<F#201> con due# segni sulla #<F#201> destra# 


p1F#201:  #<G#200> sulla destra# <lp> #<G#200> s�#


p2G#202:  due segni<ii> <sp> sempre irregolari , verticali


p1F#203:  s� <sp> bene , ci #<G#204> sono <sp> i+ {[voiceless-velar-stop] acc+}# accanto {[dialect] a i'}            bar io c'ho una {<NOISE> forchetta} che te non #<G#204> hai#


p2G#204:  #<F#203> va bene ?# <sp> <inspiration> <lp> #<F#203> ecco# , io non ho            la forchetta , devi <sp> {<NOISE> <ehm>} cercare di evitarla la                   #<F#205> forchetta#


p1F#205:  #<G#204> s� s�# l'ho <vocal> ci son passato ni' [dialect] {<NOISE>                mezzo}


p2G#206:  ecco , devi salire poi verso la mano


p1F#207:  s� <vocal> ecco #<G#208> {<hypo-articulated> brava}, c'ho anch'io la mano#


p2G#208:  #<F#207> e devi <sp> quindi sempre# tratteggiatamente girare intorno
          alla mano <sp> <inspiration> <sp> <vocal> #<F#209> da<aa> da sopra#


p1F#209:  #<G#208> ma dalla {<NOISE> parte}# ?


p2G#210:  #<F#211> da sopra#


p1F#211:  #<G#210> s� , ma <vocal> dalla parte# sini+ <sp> ci passo dalla parte 
          {<hypo-articulated> sinistra o dalla} parte #<G#212> {<NOISE> destra ?}#


p2G#212:  #<F#211> da destra#


p1F#213:  da #<G#214> destra ?#


p2G#214:  #<F#213> s�#


p1F#215:  va bene <inspiration> quanti tratteggi ?


p2G#216:  sono <sp> rispetto a quei due che <sp> gli {<NOISE> ultimi} t'ho detto            sono uno due tre <sp> quattro <inspiration> <sp> quattro <vocal> fino a           che arrivi <sp> al <sp> margine<ee> <ehm> del foglio , no ?


p1F#217:  <mh>


p2G#218:  <inspiration> cio� , non proprio al {<NOISE> margine} , devi arrivare             un po' sopra la mano <sp> <tongue-click> <sp> <inspiration> e poi <sp>            che tendono a riscendere sono <sp> tre


<sp>


p1F#219:  aspetta che qui ce n'ho altre che


<lp>


p2G#220:  sono quattro fino <sp> #<F#221> al<ll>#


p1F#221:  #<G#220> appena# hai girato {<NOISE> intorno} alla mano


p2G#222:  s� , devi farne altri #<F#223> tre <sp> e t+ e inizi a scendere#


p1F#223:  #<G#222> s� e riscendo da <sp> verso<oo># <sp> verso che zo+


p2G#224:  {<NOISE> devi} a+ <vocal> <sp> passare fra i due cuori , io c'ho due              cuori , no ? {<NOISE> quello} giallo e quello rosso , te , no , il                cuore ce l'hai #<F#225> spostato ?#


p1F#225:  #<G#224> io c'ho la# barchetta , ora ho capito tutto vai vai 
          #<G#226> devo pass�' in me+#


p2G#226:  #<F#225> devi passare# in mezzo ai due #<F#227> simboli#


p1F#227:  #<G#226> va bene# , ho capito ho #<G#228> capito#


p2G#228:  #<F#227> va bene ?#


p1F#229:  {<NOISE> s� s� s�}


p2G#230:  quindi passi in mezzo sempre in modo curvo<oo> #<F#231> e non                     rettilineo#


p1F#231:  #<G#230> s�# <lp> s�


p2G#232:  e poi <sp> <ehm> sempre giri <sp> <ehm> intorno al gelato


p1F#233:  <eeh> il gelato <sp> <NOISE> te ce l'hai si , io ce l'ho sulla sinistra
          {[dialect] de i'} cu+ #<G#234> {[dialect] de i'} cuore#


p2G#234:  #<F#233> anch'io#


p1F#235:  intorno {<NOISE> passandogli} di sopra ?


p2G#236:  passando da sotto dal cono


p1F#237:  {<NOISE> da} #<G#238> sotto#


p2G#238:  #<F#237> e poi# girare s+ / <ii>il / girargli intorno


p1F#239:  <eh>


p2G#240:  con<nn> <sp> sono / ti dico quanti<ii> quanti devono essere i
          tratteggi io <sp> prima non li ho contati #<F#241> i tuoi#


p1F#241:  #<G#240> <eh> no ! e lo# so <eeh> #<G#242> neanch'io#


p2G#242:  #<F#241> comunque# sono <sp> <ehm> allora , {<NOISE> arrivi} <sp> t'ho           detto dalla mano


p1F#243:  s�


p2G#244:  e quelli l+ / quelli che iniziano a essere orizzontali sono uno due tre           <sp> quattro <sp> cinque sei sette otto nove dieci <sp> undici<ii>                <NOISE> {<NOISE> undici  #<F#245>  sono}#


p1F#245:  #<G#244> {<NOISE> considerando}# anche quelli sopra #<G#246>                     {[whispering] per�}#


p2G#246:  #<F#245> {<NOISE> quelli}# sopra al gelato


p1F#247:  {<NOISE> <mhmh> va bene} , allora all'incirca ci sono anch'io ,                   #<G#248> vai#


p2G#248:  #<F#247> {<NOISE> allora} , il# gelato / devi <sp> an<nn>+ devi<ii>               <ehm> curvare


p1F#249:  s�


p2G#250:  e<ee> <ehm> e l'ultimo segmento � proprio sopra il gelato e l� c'� il             mio {<NOISE> punto d'arrivo}


p1F#251:  <tongue-click> aspetta un attimo , io tor+ <vocal> giro intorno al                gelato ? <inspiration> #<G#252> e il#


p2G#252:  #<F#251> giri intorno# al {<NOISE> gelato} e arrivi fino a<aa> <mh>               <unclear> a un punto d'arrivo <ii>in alto <inspiration> <sp> <eeh> devi           girare <sp> poi s+ <vocal> <eeh> tor_nare verso destra


<sp>


p1F#253:  {<hypo-articulated> allora} aspetta <NOISE> <lp> aspetta un attimo , <eh> ! <sp> <NOISE>                <inspiration> allora  per� <sp> se / scusami , se giro {<hypo-articulated> intorno}                    {[dialect] {<hypo-articulated> a} i'} gelato poi <NOISE> <eeh> come f� [dialect] a ritorn�'            ve+ <sp> cio� rit+ <sp> non ritorno in gi� ?

Il turno � particolarmente disturbato.


p2G#254:  non torni in gi� ma <sp> #<F#255> <ehm> <tongue-click> si chiude in               alto verso destra#


p1F#255:  #<G#254> vado direttamente <sp> s�# <sp> va bene <inspiration> <sp>
          <tongue-click> <ehm> <tongue-click> questo punto rispetto alla alla               pallina rossa {[dialect] su i'} gelato , no , la #<G#256> ciliegina#


p2G#256:  #<F#255> s�#


p1F#257:  � verso sinistra o verso destra ?


p2G#258:  verso <sp> <eeh> verso destra


p1F#259:  bene , l'ho beccato allora <sp> {<laugh> spero !} <NOISE> <lp> <NOISE>            io penso<oo>


p2G#260:  s� , abbiamo esaurito poi<ii> , ti dico , non ho non ho contato<oo>
          <inspiration> <sp> <ehm> quanti segmenti avevi te<ee> gi�


p1F#261:  no , io <NOISE> <sp> cercavo di seguirti <ehm> per� <sp> <inspiration>            {<hypo-articulated> pr+} / io non<nn> {<NOISE> c'ho neanche} pensato a dirti quanti erano              <breath> {<hypo-articulated> figu+}!


p2G#262:  <tongue-click> va bene , abbiamo concluso
