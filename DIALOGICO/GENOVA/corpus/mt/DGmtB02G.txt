TEXT_inf.

MAT:            mt
MAP:            B
TRM:
Ndl:            02
Ntr:		
Nls:		
REG:            G


SPEAKERS_inf.

INp1:		J. P., F, 22, Genova, spontaneo, fluente, G>F
INp2:		L. G., F, 23, Genova, spontaneo, fluente, F>G		


RECORDING_inf.

TYP:		DAT
LOC:		Genova/Universit�
DAT:		13/03/01
DUR:		5.09,155
CON: 		buone


TRANSCRIPTION_inf.

DAT:		13/05/01
CMT:		
Nst:		113


p1G#1:  dunque Laura , la partenza sta <sp> sotto il gelato <sp> abbastanza sotto quindi
        <inspiration> <sp> <eeh> diciamo in linea d'aria pure sotto {<NOISE> la<aa>} la barca <sp>
        in<nn> / di fronte al bar per� esterna rispetto ai limoni <lp> {<laugh> pi� o meno hai capito} ?


p2F#2:  {<laugh> no}

<sp>


p1G#3:  allora


p2F#4:  esterna ai limoni , anche sotto la barca


p1G#5:  <vocal> allo+ / tu scendi #<F#6> dal<ll> dal gelato#


p2F#6:  #<G#5> dal gelato <sp># s� #<G#7> <sp> supero i limoni#


p1G#7:  #<F#6> e vai <sp> allo stesso# livello del bar / no , non superi i #<F#8> limoni#


p2F#8:  #<G#7> <ah> !# <sp> allo stesso #<G#9> livello del bar#


p1G#9:  #<F#8> vai allo stesso# livello del bar prima dei limo+ / subito prima dei limoni


p2F#10:  s�


p1G#11:  e rimani esattamente sotto il gelato , per�


<sp>


p2F#12:  okay <sp> per� la barca non c'entra niente


p1G#13:  si vabb� no era per dire che superi la barca anche in verticale


<sp>


p2F#14:  #<G#15> ma come faccio a superare la barca ?#


p1G#15:  #<F#14> comunque la partenza sta sotto il gelato# all'incrocio <inspiration> delle
         direttive tra gelato e bar


<lp>


p2F#16:  gelato <lp> e bar


p1G#17:  ci siam capiti ? <lp> se non hai capito me lo dici ?


<sp>


p2F#18:  ma <eeh> rispetto al cuore ?


p1G#19:  tira due li+ / du+ / traccia due linee verticali <sp> c+ t+ che cuore ? <sp> <laugh> tu
         <eeh> traccia una linea verticale verso il basso <sp> dal gelato e una linea orizzontale <sp>
         dal bar {<NOISE> <lp> verso sinistra}


p2F#20:  {<NOISE> s�}


<sp>


p1G#21:  ecco , all'incrocio trovi {<NOISE> la partenza}


p2F#22:  okay quindi � un pochino sopra i gelati


<sp>


p1G#23:  <ss>sopra i limoni


p2F#24:  s� {<laugh> #<G#25> scusami , sopra i limoni}#


p1G#25:  #<F#24> {<laugh> s�} <inspiration># poi parti #<F#26> verso destra#


p2F#26:  #<G#25> okay# <sp> poi parto verso #<G#27> destra#


p1G#27:  #<F#26> cominci# a fare / tratteggio a giri i limoni


p2F#28:  s�


p1G#29:  scendi


p2F#30:  s�


p1G#31:  scendi scendi scendi {<NOISE> <sp> aggirando sempre} i limoni


p2F#32:  s�


p1G#33:  <inspiration> e vai in verticale dopo che hai aggirato i limoni <sp> scendendo all'esterno
         <sp> del <sp> gatto <lp> quindi vai gi� verticale vai all'esterno del gatto


<sp>


p2F#34:  #<G#35> okay#


p1G#35:  #<F#34> ci siamo ?#


<sp>


p2F#36:  quindi a sinistra del frigo


<lp>


p1G#37:  che che frigo ?


<sp>


p2F#38:  <laugh> {<laugh> no , vabb� #<G#39> niente okay}#


p1G#39:  #<F#38>{<laugh> io non ne ho# frighi}


p2F#40:  <laugh> {<laugh> va bene cos�}


p1G#41:  {<laugh> tu hai un gatto da #<F#42> qualche par+}# ?


p2F#42:  #<G#41> s�# #<G#43> ce l'ho il# gatto


p1G#43:  #<F#42> ecc+# <sp> okay


p2F#44:  <inspiration> a sinistra del gatto


<sp>


p1G#45:  s�


<sp>


p2F#46:  bene


p1G#47:  <inspiration> poi aggiri anche il #<F#48> gatto da sotto <sp> truc# truc e truc truc e truc
         <inspiration> passi {<laugh> sulla destra} del gatto e lo aggiri ancora , quindi risali


p2F#48:  #<G#47> s� <sp> s�# <lp> s�


<sp>


p1G#49:  <inspiration> sali sali sali <sp> e aggiri pure la freccia che c'� <sp> quindi <sp> sali sali
         #<F#50> sali vai#


p2F#50:  #<G#49> la freccia# la aggiro da<aa> #<G#51> sinistra ?#


p1G#51:  #<F#50> aspetta aspetta# s� <sp> tu vai verso<oo> la freccia <sp> #<F#52> salendo#
         <inspiration> la aggiri passando <sp> andando dalla sinistra della freccia <sp> verso #<F#52>
         destra# <sp> quindi quando sei in cima alla freccia #<F#52> riscendi#


p2F#52:  #<G#51> s�# <lp> #<G#51> s�# <lp> #<G#51> s�# <sp> s�


p1G#53:  e passi <sp> di fianco al cuore a sinistra scendendo


p2F#54:  che non ho <sp> #<G#55> <mhmh>#


p1G#55:  #<F#54> <ah> ! non {<NOISE> hai# un} cuore ?


p2F#56:  no


<sp>


p1G#57:  bene ! <sp> <inspiration> quindi <sp> allora tu dalla freccia scendi in verticale #<F#58>
         <sp> hai# una mano in fondo ?


p2F#58:  #<G#57> s�# <lp> no


p1G#59:  sotto la freccia ? non hai una mano <inspiration> vabb� <sp> {<laugh> continua a
         scendere} in verticale , va'


p2F#60:  {<laugh> per quanto ?}


p1G#61:  {<laugh> <eh> ! fino <inspiration> pi� o meno} <lp> a / verticale verso tendente a destra


<sp>


p2F#62:  <ah> !


<sp>


p1G#63:  okay hai una sedia da qualche #<F#64> parte ?#


p2F#64:  #<G#63> s�#


<sp>


p1G#65:  ecco , tu devi andare <NOISE> <ehm> passando un po' larga perch� io ho un cuore l� da
         qualche {<laugh> parte} <inspiration> <sp> devi aggirare la sedia <sp> da sotto <sp>


p2F#66:  #<G#67> <mh>#


p1G#67:  #<F#66> quindi# <sp> tu <vocal> <sp> scendi dalla freccia girando <lp> #<F#68> la
         sedia# <sp> {<laugh> cio�} per dire <sp> vai un po' verticale poi <inspiration> vai
         leggermente verso #<F#68> destra , poi#


p2F#68:  #<G#67> {[whispering] <ehm> magari � cos�} <lp> #<G#67> aggiro# tutta la sedia ?


<sp>


p1G#69:  <tongue-click> aggiri la sedia salendo e #<F#71> arrivi al punto#


p2F#70:  #<G#70> salgo a destra# <sp> #<G#72> e arrivo# al #<G#72> punto , ci sono arrivata#


p1G#71:  #<F#70> arrivi a+# <sp> #<F#70> e arrivi al pu+# <sp> ottimo


INVERSIONE DEI RUOLI


p2G#72:  ora


<sp>


p1F#73:  ora


<sp>


p2G#74:  dal punto <breath> sali su <sp> di pochino <sp> <inspiration> #<F#75> <sp> e# <eeh>
         passi in mezzo a / io ho una barchetta e una pipa un pochino #<F#75> sopra#


p1F#75:  #<G#74> s�# <lp> #<G#74> s� , la tua# barchetta era il mio cuore tanto per {[whispering]
         la crona+}


p2G#76:  <ah> ! va bene


p1F#77:  s�


p2G#78:  vabb�


p1F79#:  #<G#80> #e


p2G#80:  #<F#79> passi# su in mezzo <lp> alla pipa e alla barchet+ o il cuore , quello che hai


p1F#81:  s�


<lp>


p2G#82:  <clear-throat> vai su quindi la pipa ce l'hai ?


p1F#83:  s�


p2G#84:  ecco , ci aggi+ / la<aa> ci vai a<aa> sinistra della pipa <inspiration> s+ / dalla pipa sali su
         in<nn> verticale


p1F#85:  s�


<sp>


p2G#86:  e arrivi fino al bar <sp> <inspiration> alla sini+ alla destra del bar


<sp>


p1F#87:  io <sp> alla sinistra <sp> alla destra del bar ho anche una forchetta , ci passo in #<G#88>
         mezzo ?#


p2G#88:  #<F#87> non# ce l'ho io la forchetta #<F#89> per� credo# che tu ci debba passare in
         #<F#89> mezzo#


p1F#89:  #<G#88> vabb�# <lp> #<G#88> okay#


p2G#90:  <inspiration> vai su , appena sei arrivata al bar inizi a andare sempre su per� verso destra
         <inspiration> <sp> la mano lass� in cima ce l'#<F#91> hai ?#


p1F#91:  #<G#90> s�#


<sp>


p2G#92:  #<F#93> okay devi#


p1F#93:  #<G#92> aggiro la# mano ?


p2G#94:  devi aggirare tutta la mano <sp> <inspiration> <lp> aggiri tutta la mano <sp> e quando
         sei <sp> <ehm> diciamo {<laugh> al polso} #<F#95> insomma quando finisce# la mano
         <inspiration> <eeh> io ho <sp> <eeh> a<aa> sinistra due cuori , uno rosso in basso e uno sul
         giallino in alto , #<F#95> tu li hai ?#


p1F#95:  #<G#94> s�# <lp> #<G#94> s� io ho quello# giallino


p2G#96:  <inspiration> ecco allora f+ / passi sotto a quello giallino


<lp>


p1F#97:  sotto<oo> #<G#98> sotto#


p2G#98:  #<F#97> sotto al cuore# <ehm> s� #<F#99> insomma i+ {[screaming] in orizzontale}#


p1F#99:  #<G#98> siccome tu hai un'al+# <lp> #<G#100> eh ?#


p2G#100:  #<F#99> dalla# mano <lp> tiri verso sinistra e arrivi fino a sotto al cuore , alla punta del
          cuore


p1F#101:  s�


p2G#102:  <inspiration> <sp> continui


p1F#103:  s�


p2G#104:  <inspiration> poi c'� il gelato laggi� in#<F#105> fondo ?# <sp> devi aggirarlo dal
          basso


p1F#105:  #<G#104> s�# <lp> s�


<sp>


p2G#106:  lo aggiri , lo aggiri tutto , sali su dalla sinistra del gelato


<sp>


p1F#107:  s�


p2G#108:  l+ / dopo che l'hai girato <sp> arrivi lass� in cima <inspiration> <lp> #<F#109> <eeh>#


p1F#109:  #<G#108> in cima# sopra / esattamente sopra al gelato ?


p2G#110:  sopra al rosso fai ancora un trattino <sp> verso<oo> il cuore <sp> giallo <sp> e l� c'�
          l'arrivo


<lp>


p1F#111:  speriamo


p2G#112:  ci sei ?


p1F#113:  <inspiration> e non lo so , Laura , lo {<laugh> spero}

