TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            02
Ntr:		
Nls:		
REG:            G


SPEAKERS_inf.

INp1:		L. C., M, 22, Genova, spontaneo, fluente 
INp2:		S. P., M, 21, Genova, spontaneo, fluente 		


RECORDING_inf.

TYP:		DAT
LOC:		Genova
DAT:		13/03/01
DUR:		13.04,989
CON:		buone


TRANSCRIPTION_inf.

DAT:		02/05/01
CMT:		
Nst:		355


p1#1:  casa? 

non sono sicura se dice casa o cosa 


<lp>


p2#2:  <unclear> 

sembra dire "ot"


<sp> 


p1#3:  okay


p2#4:  l'antenna ce l'hai ?


<sp> 


p1#5:  l'antenna una s� con due sbarre #<p2#6> <sp> perpendicolari# <sp> 


p2#6:  #<p1#5> s� <sp> camini due# 


p1#7:  camini due <sp> il cavallo la faccia verso sinistra 


<sp> 


p2#8:  c'ha due orecchie #<p1#9> <unclear>#


p1#9:  #<p2#8> e il# tipo ha una spada 


<sp> 


p2#10:  col berretto in #<p1#11> testa lui#


p1#11:  #<p2#10> s�# <sp> la macchina gi�


<sp> 


p2#12:  #<p1#13> s� <sp> <eeh>#


p1#13:  #<p2#12> cos'� <sp> spe+ / uno# spec+ due specchietti uno<oo> s+ <ehm>
        retrovisore l'altro laterale


p2#14:  no ! <lp> #<p1#15> quello retrovisore# il mio non ce l'ha


p1#15:  #<p2#14> non c'� <lp> quello da parabrezza non ce #<p2#16> l'ha ?#


p2#16:  #<p1#15> no#


<sp> 


p1#17:  {[whispearing] vabb� allora lo cerchio}


<sp> 


p2#18:  praticamente poi c'� il tipo che gua+ guarda in tiv� 


p1#19:  cosa c'ha la tiv� ? 


p2#20:  un cane 


<sp> 


p1#21:  <ehm> okay ce l'ha l'antenna la #<p2#22> tiv� ?#


<sp> 


p2#22:  #<p1#21> s� #


p1#23:  il cane ce l'ha il guinzaglio ?


p2#24:  s� , per�


<sp> 


p1#25:  il guinzaglio dove arriva ?


<sp> 


p2#26:  gli scende dalla manica


<sp> 


p1#27:  s� giusto okay


<sp> 


p2#28:  questo qua che guarda la tiv� ce l'ha l'antenna la tiv� ?


<sp> 


p1#29:  in basso a destra della figura c'� un tombino cosa c'� ?


p2#30:  no, non s+ c'� s� un #<p1#31> tombino con dei buchi s�#


p1#31:  de+ dei buchi <sp> s� la scarpa com'� bianca ? ce l'ha i lacci #<p1#31>
        de+#


p2#32:  s� <sp>  bianca coi lacci il cane c'ha il naso nero tu ce l'ha ?


<sp> 


p1#33:  s�<ii> due orecchie l'albero ? a sinistra della #<p2#34> figura c+ <sp> 
        ce l'hai ?#


p2#34:  #<p1#33> <eeh> <sp># s� , che cosa c'ho ?


<sp> 


p1#35:  okay niente #<p2#36> <laugh> <breath> okay# il tipo che guarda la tiv� 
        ha i capelli neri un orecchio il nasone e il mentone


p2#36:  #<p1#35> <laugh> <sp> <eeh># <pl> s�


<sp> 


p1#37:  ma cos'� ce #<p2#38> l'ha<aa>#


p2#38:  #<p1#37> sorride# il mio


<sp> 


p1#39:  ma ce l'ha i denti ?


<sp> 


p2#40:  no !


<sp> 


p1#41:  <mh> okay


p2#42:  il tuo ?


<sp> 


p1#43:  non , non ce l'ha <inspiration> la statua allora la statua <eeh> ha la 
        base, #<p2#44> giusto ?#


p2#44:  #<p1#43> la base# per� non c'� scritto niente c'� il #<p1#45> triangolo#


p1#45:  #<p2#44> no ! <sp> c'�# un rettangolo okay <inspiration> bene poi nella 
        casa altri particolari mi sembra #<p2#46> nie+ / il tetto ?#


p2#46:  #<p1#45> il tetto con le# strisce #<p1#47> nere# 


p1#47:  #<p2#46> s�# okay 


<sp> 


p2#48:  il tetto com'� il #<p1#49> tuo# ?


p1#49:  #<p2#48> il# tetto <eeh> righe<ee> orizzontali parallele <sp> #<p2#50> tranqui+#


p2#50:  #<p1#49> spiovente# no ?


<sp> 


p1#51:  s� s�




<lp>


p2#52:  due camini hai detto la macchina scusa un attimo #<p1#53> ce l'ha#


p1#53:  #<p2#52> due camini# <sp> allora la macchina allora vediamo un po' due
        fanali <ehm> il paraurti #<p2#54> <sp> le due ruote spunta per met�#


p2#54:  #<p1#53> s� le ruote <unclear> <sp> si si vede# soltanto la met� davanti


p1#55:  s� e la freccia quella d+ / laterale la vedi nera piccina #<p2#56> sopra 
        la ruota# okay 


p2#56:  #<p1#55> s�># 


<sp> 


p1#57:  <breath> l'erbetta ba+ / alla base della statua<aa> ce #<p2#58> l'hai ?#


p2#58:  #<p1#57> s�># #<p1#59> <sp> e ce l'ho rotonda# fa' il giro


p1#59:  #<p2#58> <vocal> <unclear># <lp> s� fa' il giro okay la panchina ce 
        l'ha<aa>? le vedi le due gambe della #<p2#60> panchina quelle dava+#


p2#60:  #<p1#59> solo due#


<sp> 


p1#61:  s� #<p2#62> okay#


p2#62:  #<p1#61> con l'ombra# in fondo 


p1#63:  s� <eeh> <ehm> lo schienale della panchina le vedi le righe #<p2#64> 
        verticali ?#


p2#64:  #<p1#63> verticali# s�


<sp> 


p1#65:  s� <lp> #<p2#66> s�# 


p2#66:  #<p1#65> svoltante# la panchina su in cima ?


<sp> 


p1#67:  okay


<sp> 


p2#68:  il cane c'ha<aa> dei baffi diciamo c'ha dei #<p1#69> puntini# 


p1#69:  #<p2#68> c'ha dei puntini c'ha un #<p2#70> occhietto pu+#


p2#70:  #<p1#69> le orecchie come ce l'ha ?


<sp> 


p1#71:  ce l'ha appuntite due


<sp> 


p2#72:  un l+ /un l'occhio ?


<sp> 


p1#73:  l'occhio � un pallino nero


<sp> 


p2#74:  s�


<sp> 


p1#75:  okay la coda il cane come ce l'ha ? #<p2#76> <sp> n+ /dritta no ?#


p2#76:  #<p1#75> <vocal> all'ins�# corta all'ins�


<sp> 


p1#77:  all'ins� in che senso ?


<sp> 


p2#78:  con la punta verso l'alto 


<sp> 


p1#79:  cio� parallela tipo all'albero al #<p2#80> tronco ?#


p2#80:  #<p1#79> parallela #<p1#81> praticamente alla gamba della panchina#


p1#81:  #<p2#80> alla panchina ? <sp> okay allora no# io ce l'ho <sp> in gi�


<sp> 


p2#82:  #<p1#83> okay#


p1#83:  #<p2#82> <inspiration># poi <ehm> vediamo <sp> #<p2#84> la casa niente#


p2#84:  #<p1#83> si vede l+ /# la <eeh> <sp> il piede dell'uomo 


<sp> 


p1#85:  bianco no , #<p2#86> con i lacci#


p2#86:  #<p1#85> che scarpa# c'ha ? 


<sp> 


p1#87:  ha una scarpa tipo mocassino coi lacci <lp> #<p2#88> di' un po' , il 
        cavallo ce l'ha l'o+#


p2#88:  s� <sp> #<p1#87> il <sp> ma i lacci# sono <sp> cio� i mie+ / i miei 
        #<p1#89> non � proprio#


p1#89:  #<p2#88> ha tre# lacci <ehm> in un verso e uno pi� lungo b+ <sp> 
        perpendicolare agli #<p2#90> altri#


p2#90:  #<p1#89> s�#


<sp> 


p1#91:  il cavallo della statua ce l'ha l'occhio ?


<sp> 


p2#92:  no ! 

<sp> 


p1#93:  #<p2#94> okay i+ <ehm># 


p2#94:  #<p1#93> tuo ?#


<sp> 


p1#95:  no , #<p2#96> neanche il tipo <unclear># 


p2#96:  #<p1#95> {[dialect] i zoccoli}# sono delimitati o no i #<p1#97> tuoi ?#


p1#97:  #<p2#96> no !#


<sp> 


p2#98:  neanche i miei 


p1#99:  okay il tipo a cavallo non c'ha #<p2#100> l'occhio vero ?#


p2#100:  #<p1#99> c'ha l'elmetto# per� in testa <sp> senza occhi co+ / senza 
         bocca il mio


<sp> 


p1#101:  s� , il naso � grosso #<p2#102> no ?#


p2#102:  #<p1#101> grosso# #<p1#103> la spada ce l'ha in mano ?#


p1#103:  #<p2#102> <mh> <sp> la spada# ce l'ha per� non si vede dove inizia e 
         dove finisce


<sp> 


p2#104:  l'impugnatura della spada la #<p1#105> vedi ?#


p1#105:  #<p2#104> no !#


<sp> 


p2#106:  #<p1#107> le scarpe <ehm> quello a cavallo ?#


p1#107:  #<p2#106> la se+ <sp> le s+# <lp> ce l'ha ma non #<p2#108> <ehm># 


p2#108:  #<p1#107> la sella ?#


p1#109:  la sella ce l'ha passa anche sotto il cavallo , #<p2#110> no ?#


p2#110:  #<p1#109> s�#


p1#111:  la cintu+ <sp> <vocal> <sp> quanti bottoni ha la tiv� neri ?


<sp> 


p2#112:  uno due tre quattro cinque sei se+ / otto neri


p1#113:  e sotto #<p2#114> bianchi ?#


p2#114:  #<p1#113> tre#


<sp> 


p1#115:  okay <sp> � grossa la televisione s� ?


<sp> 


p2#116:  s� rot+ <sp> <eeh> rettang+ come un libro #<p1#117> praticamente 
         rettangolare#


p1#117:  #<p2#116> s� aperta <sp># <mhmh>


p2#118:  coll'antenna


<sp> 


p1#119:  <vocal> nel cielo non c'� niente , no ?


<sp> 


p2#120:  no


<sp> 


p1#121:  okay


<sp> 


p2#122:  dove gli arriva il guinzaglio al polso <eeh> al tipo ?


p1#123:  scende gi� #<p2#124> <eeh> verticalmen+ / s�# scende gi� dal polso


p2#124:  #<p1#123> s� al polso � legato per�#


<sp>


p1#125:  scende gi� verticalmente poi #<p2#126> s'attacca#


p2#126:  #<p1#125> c'ha le gambe# accavallate , no ?


<sp> 


p1#127:  s�




<lp> 


p2#128:  quanti sono i buchi del tombino ?


<lp>


p1#129:  sette


<sp> 


p2#130:  s�


<sp> 


p1#131:  #<p2#132> okay#


p2#132:  #<p1#131> <inspiration> <eeh># non c'� <unclear> <sp> la portiera della 
         macchina c'ha una riga trasversa+ di traverso ce #<p1#133> l'ha ?#


p1#133:  #<p2#132> s�#


<lp>


p2#134:  le ruote c'hanno il cerchio<oo> #<p1#135> <eeh># 


p1#135:  #<p2#134> s�# <sp> tutte per�<oo> <sp> s� <sp> un cerchietto pi� 
         piccolo dentro <sp> <inspiration> #<p2#136> <eeh># 


p2#136:  #<p1#135> sotto i# fanali davanti c'� una riga 


<sp> 


p1#137:  s� nera


<sp> 


p2#138:  #<p1#139> parabrezza#


p1#139:  #<p2#138> poi c+# <sp> c'� il parabrezza <unclear> io avevo solo lo 
         specchietto tu non avrai niente


<lp>


p2#140:  c'� lo specchietto retrovisore davanti ?


<sp> 


p1#141:  s� quello del para+ / #<p2#142> quello<oo>#


p2#142:  #<p1#141> e anche# quello di lato #<p1#143> c'hai# 


p1#143:  #<p2#142> s�#


p2#144:  <mh>


p1#145:  solo quello di sinistra


<sp> 


p2#146:  s�


<sp> 


p1#147:  <mh> <sp> tu vedi il lato destro #<p2#148> il lato# sinistro , no ,
         della macchina ? 


p2#148:  #<p1#147> lo vedi# <lp> s�


<sp> 


p1#149:  <mh> <sp> okay


p2#150:  il finestrino praticamente #<p1#151> tut+ / comprende tutto comprende 
         cio�# 


p1#151:  #<p2#150> non c'� <sp> <vocal> tu+ / tipo tutto vetro# okay 


p2#152:  il tetto della macchina non si vede


p1#153:  s� il<ll> cavallo le zanpe ce l'ha tutt'e due alzate ?


p2#154:  una alzata


<sp> 


p1#155:  quella <lp> #<p2#156> destra# sarebbe ? 


p2#156:  #<p1#155> diciamo<oo># <lp> <eeh> mi s+ <sp> mi sembra <sp> forse 
         sinist+ ce n'� una poggiata 


<lp>


p1#157:  mi sembra sinistra poggiata 


<sp> 


p2#158:  s� 


<sp> 


p1#159:  <mh> <vocal> okay 


<sp> 


p2#160:  piegata na+ <sp> come


<sp> 


p1#161:  s� in #<p2#162> avanti#


p2#162:  #<p1#161> in avanti#


<sp> 


p1#163:  l'albero � ben #<p2#164> delimitato<oo># <sp> ha frutti ?


p2#164:  #<p1#163> no# <sp> in alto non si in alto non si vede non #<p1#165> 
         c'ha frutti#


p1#165:  #<p2#164> <mh> <sp> okay# fiori niente il tronco ? � liscio no ? non 
         #<p2#166> ha<aa>#


p2#166:  #<p1#165> s� per� un# po' storto #<p1#167> <sp> un po' piegato#


p1#167:  #<p2#166> s�# <lp> okay <sp> le orecchie del tipo come sono ?


<sp> 


p2#168:  tonde #<p1#169> col#


p1#169:  #<p2#168> normali#


<sp> 


p2#170:  sopracciglia ce l'hai<ii>


p1#171:  s� c'ha un occhietto e la fo / la bocca<aa> �<ee> 


p2#172:  il collo com'� dietro ?


<sp> 


p1#173:  <eeh> non ce l'ha quasi il collo


p2#174:  segmentato #<p1#175> il mio#


p1#175:  #<p2#174> s�# <sp> s� s� s� s� <inspiration> il cane nella tiv� c'ha il 
         naso ?


<lp>


p2#176:  s� � bianco due occhietti <sp> #<p1#177> manc+ /# c'ha un'orecchio pi� 
         lungo e uno pi� #<p1#177> corto#


p1#177:  #<p2#176> oc+ <lp> #<p2#176> quale# pi� lungo ?


<lp>


p2#178:  quello<oo> a sinistra a sinistra guardandosi <sp> #<p1#179> a destra 
         guardando a destra#


p1#179:  #<p2#178> guardandolo ? <sp> okay giusto# <inspiration> il tipo tiene 
         con che mano la televisione ?


p2#180:  con la destra


<sp> 


p1#181:  c'ha il pollice alzato ?


p2#182:  s�


<sp> 


p1#183:  #<p2#184> okay# 


p2#184:  #<p1#183> tre# dita <sp> <eeh> si vedono quattro dita


<sp> 


p1#185:  tre dita e il pollice #<p2#186> okay# <sp> <eeh> <unclear>


p2#186:  #<p1#185> <mh># <P> le zampe del cane come le vedi te?


<sp> 


p1#187:  allora sono<oo> diciamo<oo> <sp> parallele #<p2#188> cio�#


p2#188:  #<p1#187> no alla# punta diciamo


<sp> 


p1#189:  sono tipo mozzate a+ / si vedono anche divise in due <sp> per� 
         #<p2#190> non<nn> senza unghie#


p2#190:  #<p1#189> tonde <sp> tonde# con una trattina in mezzo #<p1#191> giusto 
         ?#


p1#191:  #<p2#190> s�# giusto


p2#192:  tutt'e due


<sp> 


p1#193:  <mhmh> 


<lp>


p2#194:  ce l'ha <sp> <eeh> <sp> la bocca il cane com'�?


p1#195:  la bocca � <ehm> #<p2#196> inesp+ / inespressiva s�#


p2#196:  #<p1#195> una riga poi verso l'alto alla fine#


<lp>


p1#197:  poi la #<p2#198> base della# <lp> s� vabb� quella


p2#198:  #<p1#197> dentro il# naso c'� un p+ / trattino bianco il #<p1#199> 
         cane#


p1#199:  #<p2#198> s�# vabb� quella <lp> e la base della statua dove c'� 
         l'erbetta com'� ha una riga che la<aa>


<sp> 


p2#200:  c'� una riga


<sp> 


p1#201:  pi� una sotto #<p2#202> no ?#


p2#202:  #<p1#201> una# riga che comprende l'erba all'interno e #<p1#203> poi 
         un'altra# verso il cane pi� in basso


p1#203:  #<p2#202> s�# <lp> che si ferma all'altezza #<p2#204> dell'orecchio , 
         no ?#


p2#204:  #<p1#203> s�#


<sp> 


p1#205:  <mh> okay


<sp> 


p2#206:  e al basamento praticamente � <inspiration> #<p1#207> <eeh> c'ha#


p1#207:  #<p2#206> un trapezio ?#


<sp> 


p2#208:  #<p1#209> s� un <sp> un trapezio poi sale su e# comprende un quadrato 
         #<p1#209> dentro#


p1#209:  #<p2#208> c'ha il trapezio poi un al+ <ehm> <sp> s�# <lp> #<p2#208> 
         okay# okay <inspiration> <sp> <ehm> #<p2#210> vediamo un# 


p2#210:  #<p1#209> la sella ?#


<sp> 


p1#211:  la sella abbian gi� detto no niente nessuna particolarit� mi sembra 
         <sp> e le finestre della casa sono<oo> uguali come #<p2#212> grandezza 
         ?#


p2#212:  #<p1#211> sono<oo># rettangola+ <sp> <eeh> quadrate cio�


<sp> 


p1#213:  ci #<p2#214> sono<oo> <sp> tre <sp> due# <sp> uno uno uno


<sp> 


p2#214:  #<p1#213> tre <sp> una fila di tre una fila di due# uno uno uno 


<sp> 


p1#215:  okay


<lp>


p2#216:  partendo da destra <eh> ?


<sp> 


p1#217:  s� <lp> poi , vediamo un po' , la gamba del tipo � quella destra 
         accavallata sulla sinistra #<p2#218> vero ?#


p2#218:  #<p1#217> s�#


<sp> 


p1#219:  okay


<sp> 


p2#220:  ce l'hai nel braccio quel trattino circolare ?


<sp> 


p1#221:  s�


<lp>


p2#222:  la pieghetta #<p1#223> vicino#


p1#223:  #<p2#222> s� s�# <sp> <inspiration> <sp> <eeh> <breath> la casa poi<ii> 
         <sp> basta non si vede no dove finisce la casa <sp> � sfumata


<sp> 


p2#224:  s� <sp> dall'albero � #<p1#225> sfumata#


p1#225:  #<p2#224> s�# 


<lp>


p2#226:  due camini hai detto


<sp> 


p1#227:  due camini l'antenna che � tipo una -Ti- pi� un trattino pi� grosso 
         sotto no ? <sp> pi� lungo ?


<sp> 


p2#228:  s+ / un po' pi� lungo


p1#229:  s� un po' pi� lungo


<sp> 


p2#230:  il muso del cavallo ce l'hai davanti <sp> #<p1#231> e+#


p1#231:  #<p2#230> il muso# del cavallo � un po' tondo con una striscetta<aa> in 
         mezzo


<sp> 


p2#232:  s� ma non c'ha<aa> <sp> le narici #<p1#233> gli occhi la bocca s+ la 
         bocca si vede un p+#


p1#233:  #<p2#232> no non ha niente <eeh> ha solo una bo+ <sp> accennata# la 
         bocca s�


<sp> 


p2#234:  il collo ?


<sp> 


p1#235:  il collo<oo> � <eeh> 


p2#236:  scende gi� colla piega davanti


<sp> 


p1#237:  che piega ?


<sp> 


p2#238:  cio� scende il collo verso il #<p1#239> basso#


p1#239:  #<p2#238> poi c'�# il petto , no ?


p2#240:  il petto c'ha una specie di <sp> di piega


<sp> 


p1#241:  dove ?


<sp> 


p2#242:  cio� v+ / <eeh> scende il collo giusto ?


<sp> 


p1#243:  s� per� non<nn> � tutta una stessa linea no ? non � che si #<p2#244> 
         interrompe o niente#


p2#244:  #<p1#243> s� stessa linea# per� il petto diciamo 


p1#245:  s� � un po' pi� grosso #<p2#246> gonfio#


p2#246:  #<p1#245> c'ha una striscia# davanti poi rientra <sp> non � dritto


p1#247:  no no no no <lp> e poi a te inizia la zampa #<p2#248> no ?#


p2#248:  #<p1#247> s�#


<sp> 


p1#249:  <vocal> okay <lp> allora il tipo abbian detto mi sembra che non abbia 
         pi�<uu> 


p2#250:  la spada com'� , scusa ?


<sp> 


p1#251:  la spada � rivolta verso l'alto


<sp> 


p2#252:  ma � a forma � tipo una cosa unica regolare o<oo> 


<sp> 


p1#253:  � tagliata #<p2#254> verso sinistra#


p2#254:  #<p1#253> tipo coltello# <sp> tipo #<p1#255> coltello ?# 


p1#255:  #<p2#254> � tipo# coltello verso sinistra � pi� tagliata


<sp> 


p2#256:  s� <sp> #<p1#257> il naso# della statua c'ha il nasone , no? 


p1#257:  #<p2#256> <eeh># <lp> c'ha il nasone come anche il tipo che guarda 
         la<aa> televisione <sp> l'ombra della panchina � quella sulla gamba 
         davanti a destra , no ?


<lp>


p2#258:  s�


p1#259:  � in ombra quella gamba l� <NOISE> <sp> <mhmh> l'altra gamba invece � 
         chiara , vero ?


<sp> 


p2#260:  pi� chi+ /s� #<p1#261> chia+#


p1#261:  #<p2#260> s�# <sp> con<nn> <sp> rivolta verso <sp> il di dietro , no ?


<sp> 


p2#262:  <mh> <sp> <vocal> #<p1#263> invece l'albero# no c'ha una frasca 
         praticamente


p1#263:  #<p2#262> okay# <lp> #<p2#264> pi� piccola#


p2#264:  #<p1#263> di sotto# di so+ par+ parti dal basso dopo il tronco , no ?


p1#265:  s�


<sp> 


p2#266:  c'ha vabb� le frasche normali poi ne ha una all'interno


p1#267:  s� pic+ <sp> pi� piccola #<p2#268> diciamo#


p2#268:  #<p1#267> che arriva# per� fino in cima


<sp> 


p1#269:  s� per� finisce a sinistra della figura , no ?


<sp> 


p2#270:  tut+ tutt'e due le frasche cominciano <sp> all'inizio della figura 
         <sp> sulla sinistra <sp> quella di sotto 


<sp> 


p1#271:  s� <sp> #<p2#272> poi c+#


p2#272:  #<p1#271> che � pi�# <sp> che � pi� corta #<p1#273> per� di no+#


p1#273:  #<p2#272> sono praticamente# tre no ?


<sp> 


p2#274:  tre <sp> #<p1#275> una# all'interno che � corta


<sp> 


p1#275:  #<p2#274> s�# <lp> s� e finisce a sinistra della figura


<sp> 


p2#276:  e per� <sp> arriva e+ sul fondo <lp> l'altra <sp> #<p1#277> dopo 
         questa#


p1#277:  #<p2#276> l+ <sp># l'altra che � pi� #<p2#278> grossa# 


p2#278:  #<p1#277> � la# pi� lunga perch� arriva fino in cima <unclear> poi non 
         si vede pi� l'albero #<p1#279> diciamo#


p1#279:  #<p2#278> s�# <sp> e poi l'altra ancora che #<p2#280> parte<ee>#


p2#280:  #<p1#279> che# <NOISE> parte dal c+ / dalla f+ / inizio <eeh> figure 
         per� finisce prima #<p1#281> dell'altra#


p1#281:  #<p2#280> s�# al quarto<oo> 


<sp> 


p2#282:  un po' dopo il tetto diciamo 


p1#283:  s� , conti i quattro cosi dove inizia ?


<sp> 


p2#284:  #<p1#285> dove ?#


p1#285:  #<p2#284> qua+ quattro# foglie sarebbero tipo ?


<sp> 


p2#286:  dopo il tetto ?


p1#287:  <mhmh> 


p2#288:  la prima dici ?


p1#289:  s� <sp> #<p2#290> attacca# praticamente dopo


p2#290:  #<p1#289> en+# <lp> io ne conto due dopo il tetto 


<lp>


p1#291:  s� due dopo il tetto e poi<ii> <eeh> finisce e ce ne sono altre quattro 
         della frasca in mezzo , no ?


p2#292:  s� 


<sp> 


p1#293:  <mh> <sp> <vocal> <inspiration> #<p2#294> okay# 


p2#294:  #<p1#293> e l'altra# all'interno #<p1#295> invece#


p1#295:  #<p2#294> e l'altra# all'interno un po' pi� di <ehm> #<p2#296> 
         irregolare#


p2#296:  #<p1#295> c+ / la pi� corta di# tutte


<sp> 


p1#297:  s� <lp> okay <sp> allora il tipo l'abbian visto la #<p2#298> 
         televisione#


p2#298:  #<p1#297> l'erbetta# nera la vedi , #<p1#299> no?#


<sp> 


p1#299:  #<p2#298> l'erbetta# � ne+ / l'antenna �<ee> bella lunga , no ?


<sp> 


p2#300:  tre cose ha <sp> #<p1#301> una due tre#


p1#301:  #<p2#300> uno <unclear> s�# arriva s� alla #<p2#302>seconda#


p2#302:  #<p1#301> colla punta# in alto


p1#303:  okay <sp> <inspiration> gambe accavallate <sp> <inspiration> il cane 
         <sp> tranquillo � accucciato vero il cane ?


<sp> 


p2#304:  sdraiato gi�


<sp> 


p1#305:  s� guarda verso destra


<sp> 


p2#306:  s�


<sp> 


p1#307:  <mh> la statua invece guarda verso sinistra


<sp> 


p2#308:  il tipo guarda la tiv�


p1#309:  il tipo guarda la tiv� la macchina guarda in l�


<lp>


p2#310:  #<p1#311> il muso verso# sinistra , no ?


p1#311:  #<p2#310> <mh> # <lp> il muso della statua s�


<sp> 


p2#312:  anche della macchina <eeh> 


<sp> 


p1#313:  anche della macchina <lp> poi vediamo un #<p2#314> po'#


p2#314:  #<p1#313> nuvole# <sp> #<p1#315>sole ?#


p1#315:  #<p2#314> nuvole# non ne abbiamo <sp> sole nemmeno


<sp> 


p2#316:  <ehm> 


<sp> 


p1#317:  <ehm> la panchina abbiamo detto che ha quelle righe <lp> <inspiration>
         <sp> <mh> <sp> <inspiration> <sp> poi la casa <sp> <clear-throat>
         la casa inizia <lp> la parte destra <sp> parte dal tetto , no , della 
         macchina ?


<sp> 


p2#318:  non proprio<oo> #<p1#319> l'inizio , no ?#


p1#319:  #<p2#318> s� dal parabrezza# , no ?


p2#320:  s' un po' dopo il parabrezza


p1#321:  okay <sp> e l'antenna arriva pi� o meno a altezza del muso ?


<sp> 


p2#322:  s�


p1#323:  del cavallo <sp> okay 


<sp> 


p2#324:  il cavallo ha le orecchie a punta , no ?


<sp> 


p1#325:  <eeh> il tipo al cavallo ce l'ha la mano sinistra ?


<sp> 


p2#326:  s� , tiene il collo del cavallo


<lp>


p1#327:  in che modo ?


<sp> 


p2#328:  come se fosse agganciato


<sp> 


p1#329:  <inspiration> perch� io <sp> <eeh> la manica che finisce


<sp> 


p2#330:  s� la vedi la mano ? 


<sp> 

p1#331:  <ehm> no ! <sp> vedo praticamente il pezzo della<aa> della sella sai 
         quello davanti <lp> che � tipo un ton+ / una cosa tonda , no , hai 
         #<p2#332> presen+#


p2#332:  #<p1#331> secondo me �# mano quella <eh> non � #<p1#333> che#


p1#333:  #<p2#332> io non# credo <lp> non perch� vedi che � tagliata se no 
         sarebbe come la spada che non ha<aa> non � delimitata la mano , vedi ?


<sp> 


p2#334:  <mh> 


<sp> 


p1#335:  tu hai il braccio della statua <lp> no ? giusto ?


p2#336:  s�


<sp> 


p1#337:  e poi finisce tipo come se ci fosse una manica e poi dovrebbe esserci 
         la mano


<sp> 


p2#338:  s� per� invece � la sella dici


<sp> 


p1#339:  s� secondo non c'�<ee> non c'� la mano <sp> <mb�> per� se non ce 
         #<p2#340> l'hai nemmeno te#


p2#340:  #<p1#339> lo stiv+ <sp># lo stivale ? <sp> #<p1#341> ce l'ha#


p1#341:  #<p2#340> lo stivale# c'� il<ll> pantalone delimita+ / che finisce poi 
         inizia lo stivale la scarpa , no ?


p2#342:  <mh> 


<sp> 


p1#343:  <inspiration> #<p2#344> okay# 


p2#344:  #<p1#343> e in fondo# com'� c'ha come <sp> #<p1#345> lo sperone# un po' 
         una specie di prolungamento in #<p1#345> fondo#


p1#345:  #<p2#344> in fon+# <lp> #<p2#344> dove ?#


<sp> 


p2#346:  lo stivale


<sp> 


p1#347:  verso destra ?


<sp> 


p2#348:  <eeh> s�


<sp> 


p1#349:  s� ? <sp> <inspiration> allora segnalo io non ce l'ho


<sp> 


p2#350:  c'� un prolungamento in fondo al dal tallone , no?


<sp> 


p1#351:  <mh> verso destra mi hai detto ? 


<sp> 


p2#352:  s� #<p1#353> verso destra#


p1#353:  #<p2#352> no no# non ce l'ho non ce l'ho <lp> e poi sotto lo stivale 
         c'� #<p2#354> la<aa># 


p2#354:  #<p1#353> la# riga della #<p1#355> sella#


p1#355:  #<p2#354> la# riga della sella okay 

