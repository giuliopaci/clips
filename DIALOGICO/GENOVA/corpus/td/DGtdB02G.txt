TEXT_inf.

MAT:            td
MAP:            B
TRM:
Ndl:            02
Ntr:		
Nls:		
REG:            G


SPEAKERS_inf.

INp1:		J. P., F, 22, Genova, spontaneo, fluente
INp2:		L. G., F, 23, Genova, spontaneo, fluente


RECORDING_inf.

TYP:		DAT
LOC:		Genova/Universit�
DAT:		13/03/01
DUR:		11.18,045
CON: 		buone


TRANSCRIPTION_inf.

DAT:		15/05/01
CMT:		
Nst:		290


p1G#1:  Lauretta


p2F#2:  dimmi


p1G#3:  ti faccio l'elenco delle cose che ho ? <sp> #<F#4> e mi dici# cosa ti manca ?


p2F#4:  #<G#3> s�# <lp> <mh>


p1G#5:  <inspiration> allora , io ho un bambino in piedi


p2F#6:  s�


<sp>


p1G#7:  con una #<F#8> paperella<aa> salvagente# , la paperella guarda a sinistra #<F#8> mentre#
        il bambino che in teoria � col corpo girato a sinistra sta guardando <sp> in basso a #<F#8>
        destra#


p2F#8:  #<G#7> s�# <lp> #<G#7> s�# <lp> #<G#7> s�#


p1G#9:  <NOISE> {[whispering] <eh> fino qua ci siamo} <inspiration> la paperella 
        � a pois <laugh>


p2F#10:  s�


p1G#11:  {<laugh> ed � alquanto grossa direi}


p2F#12:  {<laugh> s�}


p1G#13:  {<laugh> nel senso che gli arriva} la testa della paperella al collo


<sp>


p2F#14:  s� s+ / un #<G#15> po' sopra#


p1G#15:  #<F#14> <mh># <lp> s� un po' sopra al #<F#16> collo#


p2F#16:  #<G#15> okay#


<lp>


p1G#17:  <inspiration> #<F#18> allora#


p2F#18:  #<G#17> la paperella# ha il coso<oo> che<ee> si gonfia


p1G#19:  s�


p2F#20:  la / tipo coda


p1G#21:  s� <sp> <inspiration> #<F#22> allora# <sp> il bambino ha il costume {<laugh> sotto}


p2F#22:  #<G#21> <mh># <sp> <inspiration> <lp> s�


p1G#23:  {<laugh> magari � importante #<F#24> <inspiration># allora il bambino sta guardando
         per terra <inspiration> perch� c'� un uovo} a pois


p2F#24:  #<G#23> s� s�# <lp> s�


<sp>


p1G#25:  grosso


p2F#26:  s� , con tre pois


p1G#27:  con tre pois


p2F#28:  okay <sp> #<G#29> <inspiration> aspetta questi tre#


p1G#29:  #<F#28> ora <vocal> <sp> i {<laugh> pois# <sp> sono neri , #<F#30> no ?}#


p2F#30:  #<G#29> <G#31> s�#


p1G#31:  #<F#30> ecco# okay


p2F#32:  <inspiration> anche la paperella ha tutti i pois #<G#33> neri#


p1G#33:  #<F#32> esattamen+# <sp> #<F#34> tutti , s�#


p2F#34:  #<G#33> okay <inspiration># <sp> l'uovo , a pois , ha anche due righe


p1G#35:  s� <sp> <vocal> #<F#36> ora l'uovo#


p2F#36:  #<G#35> <vocal> i+ <sp> in# due parti diverse ha due righe


<sp>


p1G#37:  <ehm> diciamo iniziano<oo> in basso e continuano su un lato


p2F#38:  s�


p1G#39:  uniscono i due pois <sp> <vocal> per� l'uovo mio pende verso destra


<sp>


p2F#40:  s� , anche il mio


<sp>


p1G#41:  cio� la punta


<sp>


p2F#42:  #<G#43> � verso destra#


p1G#43:  #<F#42> <mh> <sp> � verso# #<F#44> destra#


p2F#44:  #<G#43> okay#


p1G#45:  va bene <sp> #<F#46> allora segn+#


p2F#46:  #<G#45> poi c'�# questa barchetta qua in <ehm> mare


p1G#47:  miseranda , direi


p2F#48:  s�<ii>


p1G#49:  � #<F#50> piccolella#


p2F#50:  #<G#49> allora# con la bandierina


<sp>


p1G#51:  s�


p2F#52:  verso <sp> destra


p1G#53:  ecco , no , io ce l'ho verso #<F#54> sinistra#


p2F#54:  #<G#53> okay# <sp> <inspiration> di fianco alla barchetta che � a vela ha la sua vela
         <sp> triangolare con vabb� dei puntini #<G#55> dentro <sp># ha anche poi una specie di
         corda a destra della #<G#55> vela#


p1G#55:  #<F#54> s�# <lp> #<F#54> s�#


<sp>


p2F#56:  okay e poi c'� ovviamente la la barchetta


p1G#57:  s� , posso<oo> dirti che si chiam+ vabb� , no , {<NOISE> niente <laugh> #<F#58>
         <unclear>} <inspiration> s� <sp> poi <sp> la# punta della barchetta verso sinistra


p2F#58:  #<G#57> {<laugh> no <sp> di fianco}# <lp> s�


p1G#59:  okay


p2F#60:  <inspiration> e <eeh> di fianco alla barchetta a destra c'� una nuvoletta


<sp>


p1G#61:  <eeh> s� <sp> che finisce nel foglio cio� #<F#62> finisc+ <sp> s�#


p2F#62:  #<G#61> s� <sp># non finisce #<G#63> insomma#


p1G#63:  #<F#62> s�#


p2F#64:  <inspiration> e invece lass� in cima a sinistra #<G#65> c'� un'altra nuvoletta#


p1G#65:  #<F#64> c'� un'altra nuvola <sp># s�


p2F#66:  okay


p1G#67:  che � un po' lunga e arriva fin sopra la testa del bambino ?


<sp>


p2F#68:  fino a sopra l'orecchio ?


p1G#69:  s�


p2F#70:  okay


<sp>


p1G#71:  poi <sp> per terra di fianco<oo> all'uovo in alto #<F#72> sulla destra c'� una pietra#


p2F#72:  #<G#71> s� <sp> s� <sp># s�


<sp>


p1G#73:  s� <sp> <inspiration> e poi <sp> <eeh> sempre per terra davanti al bambino sotto la testa
         della paperella <sp> ci son delle montagnette di sabbia ?


p2F#74:  s� <sp> #<G#75> con# tipo<oo> boh <sp> vabb� {[whispering] niente #<G#75> 'ste
         cose# qua}


p1G#75:  #<F#74> poi# <lp> #<F#74> s�# insomma un po' s+


<sp>


p2F#76:  dall'uovo <sp> a sinistra <sp> parte questa riga di sabbia


p1G#77:  s�


<sp>


p2F#78:  con due anche <G#79> affaret+ <sp> virgolette#


p1G#79:  #<F#78> 'petta un {<laugh> secondo i piedi}# del tuo bambino sono normali ?


<sp>


p2F#80:  {<laugh> <eeh>} <sp> <laugh> diciamo di s�


p1G#81:  bene , le mani se ne vede una


<sp>


p2F#82:  s�


<sp>


p1G#83:  c'ha l'ombelico


<sp>


p2F#84:  s� <sp> e anche una<aa> virgoletta sopra l'#<G#85> *ormelico# , la #<G#85> costoletta#


p1G#85:  #<F#84> s�<ii># <sp> #<F#84> poi c'ha# due occhi , un sopracciglio solo


p2F#86:  s� #<G#87> <sp> il nasino#


p1G#87:  #<F#86> uno stracce+# <sp> nasino , vabb� , uno straccio di riga per naso


<sp>


p2F#88:  questa bocca verso / <unclear> una smorfia che lo fa quasi triste


p1G#89:  s� e i capelli <sp> <clear-throat> a<aa> stile<ee>


<sp>


p2F#90:  Elvis


p1G#91:  {<laugh> Elvis the Pelvis} <sp> #<F#92> neri#


p2F#92:  #<G#91> neri#


<sp>


p1G#93:  bene


p2F#94:  s�


p1G#95:  un orecchio <laugh>


p2F#96:  okay


<sp>


p1G#97:  � un most+ vabb� <inspiration> #<F#98> <eeh>#


p2F#98:  #<G#97> allora# , questo coso qua che gonfia la paperella


<sp>


p1G#99:  s�


<sp>


p2F#100:  descriviamolo ? <sp> #<G#101> com'� ? <sp> � normale ?# <sp> ha il<ll> <ehm> il
          cosetto diciamo che c+ collega le due parti #<G#101> quando tu apri la cosa# <sp> a destra


p1G#101:  #<F#100> e vabb� come<ee> � un# <lp> #<F#100> <ss>s�# <lp> s�


<sp>


p2F#102:  <mh> e allora � uguale


<sp>


p1G#103:  ed ha una specie di <bb>bottiglia<aa> dell'olio<oo> dell'aceto {<laugh> balsamico}
          sembra <laugh> <sp> {<laugh> la mia #<F#104> bottiglia dell'aceto balsamico# � cos�}


p2F#104:  #<G#103> {<laugh> <eh> un po' s�}# <sp> <inspiration>


p1G#105:  poi <sp> mare {<NOISE> �<ee> mare <sp>} #<F#106> c'� un#


p2F#106:  #<G#105> <eh> ha un po' di# onde


p1G#107:  c'ha un po' di onde


<sp>


p2F#108:  #<G#109> vai a sapere#


p1G#109:  #<F#108> ce l'abbiamo uguale<ee># {[whispering] Laura}


<sp>


p2F#110:  <ehm>


<sp>


p1G#111:  <eeh> il mare in fondo ha una linea talmente dritta e perfetta <lp> che non sembra quasi
          un orizzonte


<sp>


p2F#112:  per� � l'orizzonte


p1G#113:  per� � l'orizzonte


<sp>


p2F#114:  <ehm>


<sp>


p1G#115:  <eeh>


<sp>


p2F#116:  {<NOISE> fammi} #<G#117> pensare#


p1G#117:  #<F#116> la paperella# ha un occhio <breath>


p2F#118:  ha un occhio che guarda verso il basso


p1G#119:  s�


p2F#120:  insomma davanti #<G#121> in basso#


p1G#121:  #<F#120> ha un# becco <sp> {<NOISE> #<F#122> chiuso}#


p2F#122:  #<G#121> ha un becco# <sp> quanto lungo ? <sp> un pochino <sp> 
          #<G#123> normale# #<G#123> <sp># che fa quasi un sorriso lei ?


p1G#123:  #<F#122> <eh> [whispering]# <sp> #<F#122> vabb� [whispering]# <lp> <ss>s�


<sp>


p2F#124:  <mh> <inspiration> #<G#125> queste# macchie ne ha una l� sulla testa , 
          #<G#125> una sul# collo a sinistra


p1G#125:  #<F#124> e+ [whispering]# <lp> #<F#124> <ss>s+# <lp> s�


p2F#126:  una sotto quasi rotonda


p1G#127:  #<F#128> s�#


p2F#128:  #<G#127>una# che d� verso il costume del bambino <NOISE> <sp> <inspiration> <lp>
          <vocal> e una <sp> <eeh>


p1G#129:  dietro la mano


p2F#130:  s� <sp>  #<G#131> dietro la ma+ [whispering]#


p1G#131:  #<F#130> tra la mano# <sp> e #<F#132> il<ll>#


p2F#132:  #<G#131> e il picciolo#


p1G#133:  s� <lp> <inspiration> e poi cosa ci potrebbe essere ? {<NOISE> <lp>} che manca ?


<sp>


p2F#134:  le caviglie il tuo {<laugh> bambino le ha segnate} ?


p1G#135:  {<laugh> s�}


p2F#136:  <laugh> {<laugh> e le ginocchia} ?


p1G#137:  pure


p2F#138:  <laugh> {<laugh> pure al mio} <sp> <inspiration>


<sp>


p1G#139:  e poi ? <sp> e #<F#140> poi ?#


p2F#140:  #<G#139> <eeh># <breath>


p1G#141:  non lo so io ci aggiungerei qualcosa ma non c'� {[whispering] niente da aggiungere}
          l'unica cosa di diverso � la bandierina


p2F#142:  e s�


<sp>


p1G#143:  la bandierina ecco


<sp>


p2F#144:  #<G#145> <eh>#


p1G#145:  #<F#144> � proprio# subito sopra la<aa> la<aa> vela , #<F#146> oppure#


p2F#146:  #<G#145> no � un# pochino in su


p1G#147:  s� <sp> vabb� � proprio #<F#148> uguale#


p2F#148:  #<G#147> � bianca#


<sp>


p1G#149:  � bianca , s�


p2F#150:  <mh> la vela � bianca ?


p1G#151:  s�


<lp>


p2F#152:  la barchetta � #<G#153> bianca ?#


p1G#153:  #<F#152> {<laugh> s�# dai} <sp> <laugh> {<laugh> le nuvole} sono bianche il
          #<F#154> cielo �# bianco il mare � bianco


p2F#154:  #<G#153> {<laugh> s�}# <lp>s�


p1G#155:  <unclear> a parte i pois <sp> #<F#156> e i capelli#


p2F#156:  #<G#155> il<ll> costume# del bambino <sp> � bianco ?


p1G#157:  s�

<sp>


p2F#158:  <mh> <sp> che ha la cut+ / ha la cucitura segnata ?


p1G#159:  secondo me � <uu>n boxer


<sp>


p2F#160:  s�


<sp>


p1G#161:  <mh>


<sp>


p2F#162:  col con con un po' di cucitura l� in #<G#163> vita# insomma


p1G#163:  #<F#162> s�#


p2F#164:  <mh>


<sp>


p1G#165:  e poi ?


<sp>


p2F#166:  <inspiration> la mano del bambino tu come la vedi ?


<lp>


p1G#167:  #<F#168> in in che in che senso ?#


p2F#168:  #<G#167> c'ha il pollice# <lp> staccato che che finisce praticamente sulla<aa> macchia
          della #<G#169> paperella# in alto <inspiration> e tutto il resto della mano in basso di cui si
          vede bene il / l'indice


p1G#169:  #<F#168> s�# <lp> s�


p2F#170:  <mh> e allora � uguale <lp> <inspiration> <sp> #<G#171> <eeh>#


p1G#171:  #<F#170> <eeh> non lo so <breath> secondo me<ee> ce l'abbiamo uguale uguale


<sp>


p2F#172:  anche perch� le onde del mare possiamo iniziare a descriverle ma


<sp>


p1G#173:  vogliamo parlare di tutte le righe che ci sono sul mare ?


p2F#174:  s�


<sp>


p1G#175:  parliamone


<sp>


p2F#176:  partiamo dalla sinistra della paperella


p1G#177:  s� , anch'io guardavo l�


p2F#178:  <inspiration> s� <sp> ce n'� una


p1G#179:  lunga e una <sp> corta


p2F#180:  s� , quella lunga inizia da<aa> fuori dal #<G#181> foglio <sp># quella corta invece no


p1G#181:  #<F#180> s�# <lp> s�


<sp>


p2F#182:  poi ce n'� una <sp> a -Zeta- tipo #<G#183> curva pericolosa# <inspiration> e un'altra
          anche #<G#183> subito sotto <inspiration>#


p1G#183:  #<F#182> s�# <lp> #<F#182> sotto <sp> s�#


p2F#184:  altre due a curva pericolosa per� <vocal> rivolte dall'altra parte


p1G#185:  s�


p2F#186:  subito sotto e altre due ancora <ehm> sotto


p1G#187:  s�


<lp>


p2F#188:  <breath> <clear-throat> poi sotto al<ll> sederino della paperella ce n'� un'altra ti+ a -
          Esse-


<sp>


p1G#189:  #<F#190> s�#


p2F#190:  #<G#189> con la<aa># gamba lunga


<sp>


p1G#191:  'petta il<ll> tuo<oo> mare quante onde fa sulla riva ? <lp> cio� quante<ee>


p2F#192:  <eeh> quante gobbe ?


<sp>


p1G#193:  vabb� se fa il mare se il mare fa le gobbe <sp> quante gobbe fa {<laugh> il tuo mare}


p2F#194:  {<laugh> no , nel senso <sp> soltanto per� come c+ / la striscia che tocca la spiaggia ?


p1G#195:  s�


p2F#196:  <inspiration> <sp> una <lp> e<ee> <sp> una appuntita


<sp>


p1G#197:  s�


<sp>


p2F#198:  e<ee> poi una leggerissima onda fino al ginocchio del bambino


<lp>


p1G#199:  scusa ?


<lp>


p2F#200:  cio� continua ma � quasi dritta


<sp>


p1G#201:  s�


<lp>


p2F#202:  e arriva pi� o meno al livello del ginocchio del bambino ?


p1G#203:  s� <lp> tu li vedi tutti i pesci in fondo al mare ?


<lp>


p2F#204:  <eeh> no


<sp>


p1G#205:  <eeh> neanch'io


p2F#206:  <eh> ! {<laugh> immaginavo} <sp> <inspiration> <sp> <clear-throat> <sp> quant'�
         grosso l'uovo ?


<sp>


p1G#207:  ecco [whispering] l'uovo sar� grosso da terra al ginocchio circa<aa> del bambino ?


p2F#208:  <eh> ! s�


<lp>


p1G#209:  e le palle sono larghe larghe


<lp>


p2F#210:  le palle dell'uovo ?


p1G#211:  s�


p2F#212:  <inspiration> <sp> <eh> ! s�


<sp>


p1G#213:  cio� sono #<F#214> inso+#


p2F#214:  #<G#213> cio� sono# grosse ma sono / saranno pi� o meno come le<ee> <eeh> i pois
          della paperella


p1G#215:  s� <NOISE> <lp> non mi viene in mente #<F#216> altro [whispering]#


p2F#216:  #<G#215> a Je'#

<lp>


p1G#217:  che hai detto ?


<sp>


p2F#218:  dico , a Je' , {<laugh> che #<G#219> facciamo qua ?}#


p1G#219:  #<F#218> <eh> ! a Je' a Lauri'# <lp> <eeh> non lo so


<sp>


p2F#220:  dall'altra parte le onde del mare <NOISE> <inspiration> <sp> sono sotto alla barchetta
          ce n'� una <sp> linea un po'<oo> #<G#221> frastagliata <sp> a sinistra#


p1G#221:  #<F#220> <ss>s� che proprio / cio� giusto perch�# me l'hai fatta notare <eh> !


p2F#222:  a sinistra e una a destra <sp> #<G#223> sotto <sp> tra mezzo# alle due <sp> ce n'�
          un'altra


p1G#223:  #<F#222> <ss>s� <lp> s�


p2F#224:  <inspiration> poi ce n'� <sp> una due tre quasi in serie di<ii> -Esse-


p1G#225:  s�


p2F#226:  <inspiration> e poi <ah> ! no quella sotto alla p+ <eeh> il sederino della paperella
          l'#<G#227> abbiam detta# <sp> e basta


p1G#227:  #<F#226> <ss>s� ho# <lp> � tutto , Laura


<lp>


p2F#228:  tra <sp> l'uovo a destra <sp> e la pietra <sp> <inspiration> cio� no <sp> <ehm> di
          fianco all'uovo <sp> e sotto la pietra


p1G#229:  s�


p2F#230:  ci sono tre righettine


p1G#231:  #<F#232> s�#


p2F#232:  #<G#231> di# sabbia


<sp>


p1G#233:  s�


<sp>


p2F#234:  okay <lp> il<ll> il tuo bam+ al tuo bambino spuntano i capelli sotto l'orecchia ?


p1G#235:  s�


p2F#236:  <inspiration> <eh> ! anche al mio


<sp>


p1G#237:  {<laugh> stiamo inventando , Laura , a 'sto punto direi che <P> non lo so , #<F#238>
          vogliamo#


p2F#238:  #<G#237> nella tua# nuvola grande , quella<aa> su in alto a sinistra del foglio


<sp>


p1G#239:  <mhmh> ?


p2F#240:  ci sono all'interno #<G#241> de<ee> / quattro# virgolette


p1G#241:  #<F#240> s� <sp> <breath># <sp> s�


p2F#242:  <mh> <sp> pure a nella mia


<lp>


p1G#243:  che dire ?


p2F#244:  {<NOISE> e dentro alla tua vela} quante ce ne sono ? <sp> cinque ?


p1G#245:  cinque


p2F#246:  {<laugh> okay} <sp> <inspiration>

<lp>


p1G#247:  questa<aa> barca a vela alla deriva , perch� non c'� nessun omino sopra giusto ?


<sp>


p2F#248:  s�


<sp>


p1G#249:  e basta


<lp>


p2F#250:  <inspiration> {<laugh> dentro} al tuo foglio c'� scritto -Bi- #<G#251> <sp> {<laugh>
          trattino} -Pi- due ? <sp> -Pi- due ? <sp> {<laugh> c'� scritto} ?#


p1G#251:  #<F#250> <laugh> {<laugh> Laura} ! <sp> <inspiration> alla disperazione sei <sp>#
          <inspiration> no , non c'� -Pi- due , c'� -Pi- #<F#252> uno <unclear>#


p2F#252:  #<G#251> {<laugh> allora � un'altra differenza}#


p1G#253:  {<laugh> sei} proprio alla disperazione, sai pi� che inventarti


<sp>


p2F#254:  <inspiration> <ah> ! aspetta il bambino ha una {<laugh> specie di tatuaggio nel
          braccio} <sp> <inspiration> #<G#255> nel braccio#


p1G#255:  #<F#254> <ah> ! <unclear> ora# ora ora questa l'hai sparata #<F#256> grossa , c'ha
          <sp> {<laugh> un due puntini}# sul braccio #<F#256> <sp> sinistro#


p2F#256:  #<G#255> no , no , aspetta nel {<laugh> braccio}# <lp>  #<G#255> <inspiration>
          sono# un tatuaggio , quelli che si fanno i bambini #<G#257> piccoli , mio fratello se li faceva
          un sacco#


p1G#257:  #<F#256> <ah> ! sono un tatuaggio <sp> ho capito# <sp> mi vuoi anche dire che l�
          dentro magari c'� tipo tutto il mondo disegnato ?


p2F#258:  <eh> ! <mb�> da qua non lo vedo per� potrebbe esse+


p1G#259:  <ah> ! capito [whispering]


<sp>


p2F#260:  <clear-throat> <sp> <ehm> <lp> {<laugh> delle righette} nella paperella ne abbiam
          parlato ?


p1G#261:  #<F#262> che [whispering]#


p2F#262:  #<G#261> nella# paperella


p1G#263:  <inspiration> s�


<sp>


p2F#264:  ci sono della righette #<G#265> ce n'� una# che spunta <sp> sia a destra che a sinistra di
          un della macchia<aa> #<G#265> <sp> sotto# al collo <sp> <inspiration> <sp> a #<G#265>
          destra#


p1G#265:  #<F#264> <ss>s�# <lp> #<F#264> s�# <lp> #<F#264> sotto# al collo ?


p2F#266:  <ehm> s+ / la terza <sp> a #<G#267> partire dalla# testa la terza #<G#267>
          <inspiration># cio� spunta sia a destra che a #<G#267> sinistra <sp> a dest+#


p1G#267:  #<F#266> s�# <lp> #<F#266> s�# <lp> #<F#266> s� <sp> sopra a quella# di destra
          #<F#268> ce ne sono# altre due


p2F#268:  #<G#267> <eh> sopra# <sp> s�


p1G#269:  in scala


p2F#270:  <mh> <sp> <inspiration> #<G#271> e poi# invece in quell'altra sempre in basso dal
          sederino della paperella #<G#271> <sp> ce n'�# una che va dalla mano al pois <sp> e dal
          pois al sederino della paperella


p1G#271:  #<F#270> poi# <lp> #<F#270> s�# <lp> s�


<sp>


p2F#272:  tutto hai <eh> ?


p1G#273:  s� <sp> secondo me<ee> non lo so vogliamo anche vedere<ee>


p2F#274:  <inspiration> il picciolo per gonfiar la paperella <sp> � tutto d'un pezzo , no ? <sp> cio�
          <sp> il<ll>


<sp>


p1G#275:  in che senso ?


p2F#276:  <inspiration> <clear-throat> <sp> cio� <ehm> aspetta allo+ ce n'� <sp> un+ un
          cerchiettino sotto <lp> <inspiration> il / la base del picciolo <lp> � tutta collegata cio�
          #<G#277> non � senza righe#


p1G#277:  #<F#276> no <sp> <inspiration># no no no


p2F#278:  ci son le righe <sp> #<G#279> che dividono <sp> okay#


p1G#279:  #<F#278> s� <sp> c'�# un<nn> mattoncino <lp> cio� tipo<oo> <lp> que_sto co+
          <laugh> come definirlo , non lo so


<sp>


p2F#280:  <mh>


<sp>


p1G#281:  cio� u+ un rettangolo<oo> <sp> leggermente panciuto


<sp>


p2F#282:  #<G#283> <mhmh>#


p1G#283:  #<F#282> che sta attaccato# alla #<F#284> paperella <sp>#


p2F#284:  #<G#283> s�#


p1G#285:  poi sopra in verticale c'� un altro rettangolino


p2F#286:  s�


p1G#287:  e poi sopra ancora c'� una mezza luna


p2F#288:  s� che � collegata dal<ll> dal cosetto #<G#289> <eh>#


p1G#289:  #<F#288> s�#


<sp>


p2F#290:  e tutto questo ce l'ho anch'io

