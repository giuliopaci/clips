TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            01
Ntr:  
Nls:  
REG:            L


SPEAKERS_inf.

INp1:  P. O., F, 24, S. Cesario (LE), spontaneo, fluente
INp2:  M. P., M, 25, Lecce, spontaneo, fluente


RECORDING_inf.

TYP:  DAT
LOC:  Lecce/Universit�
DAT:  09/05/01
DUR:  10.02,551
CON:  parzialmente buone


TRANSCRIPTION_inf.

DAT:  29/11/01
CMT:  Nella registrazione si sente la voce dell'interlocutore (e anche, di tanto in tanto, un leggero rumore di fondo dell'ambiente).
Nst:  229





p1#1:  <tongue-click> <inspiration> <eeh> Marco andiamo<oo> di figura in figura 
       <sp> #<p2#2> <eeh> io parto col cane <lp> ci sei ?#


p2#2:  #<p1#1> <mh> <lp> <tongue-click> il cane# <sp> s�


p1#3:  <inspiration> allora <clear-throat> <sp> <tongue-click> <inspiration> 
       allora il<ll> <eeh> / la sua coda � dritta ?


p2#4:  <tongue-click> no <sp> �<ee> a uncino verso #<p1#5> l'alto#


p1#5:  #<p2#4> <tongue-click> okay# quindi l+ la prima differenza � questa


p2#6:  <tongue-click> s�


p1#7:  <inspiration> allora le due gambe / le due zampe sono tutt'e du+ / � 
       steso ?


<sp>


p2#8:  s� , il cane sta steso , e le zampe stanno<oo> diciamo in parallelo 
       #<p1#9> quasi#


p1#9:  #<p2#8> <tongue-click> esattamente# quindi � uguale <inspiration> il naso 
       ha un'ombra bianca ?


p2#10:  s� s� #<p1#11> s�#


p1#11:  #<p2#10> s�# , <inspiration> e quanti puntini si trova sul muso ?


p2#12:  <inspiration> uno due tre quattro cinque sei e #<p1#13> sette#


p1#13:  #<p2#12> okay# � uguale <inspiration> #<p2#14> sta sorriden+#


p2#:14  #<p1#13> ha un collare [screaming]#


p1#15:  ha un collare s� <inspiration> ma al collare <inspiration> <eeh> ha 
        tutti q+ / tutt'e quattro i lati ? <lp> #<p2#16> capito ?#

p2#16:  #<p1#15> s�# s� s� s� s� ha il c+ / #<p1#17> il# collare<ee> s�


p1#17:  #<p2#16> <eh># <lp> <tongue-click> <sp> e io invece no perch� 
        <inspiration> dalla parte del guinzaglio <sp> a sinistra #<p2#18> a me 
        manca# proprio la striscia


p2#18:  #<p1#17> s�# <lp> s� <sp> a <eeh> me s+ / <mh> #<p1#19> c'�#


p1#19:  #<p2#18> a te# c'� #<p2#20> vero ?# 


p2#20:  #<p1#19> c'�# #<p1#21> <inspiration>#


p1#21:  #<p1#20> quindi# stiamo al secondo


p2#22:  s�


p1#23:  okay <inspiration> <eeh> sta sorridendo il cane ?


<sp> <NOISE>


p2#24:  s� s� s� sta #<p1#25> sorridendo#


p1#25:  #<p2#24> s�# <sp> <eeh> l'occhio ce l'ha ? <laugh>


p2#26:  s� #<p1#27> ce l'ha l'occhio <inspiration> s+#


p1#27:  #<p2#26> <laugh> e le due<ee># orecchie sono #<p2#28> appuntite<ee> in 
        alto ?#


p2#28:  #<p1#27> <ss>sono appuntite# sono appuntite #<p1#29> verso<oo> verso 
        l'alto#


p1#29:  #<p2#28> <tongue-click> <lp> okay# #<p2#30> un'altra<aa> cosa <lp> s� 
        <sp> infatti#


p2#30:  #<p1#29> <inspiration> in basso a destra ci stanno<oo> <vocal> quei<ii># 
        sette<ee> puntini non so cosa sono <eh> ?


p1#31:  s� sono sette <sp> #<p2#32> ma come si trovano ?#


p2#32:  #<p1#31> s� <lp> <inspiration># {<NOISE>
<lp>} come #<p1#33> si trovano
        ?#


p1#33:  #<p2#32> <tongue-click> <inspiration> no# vabb� sono sette #<p2#34> 
        quindi punto#


p2#34:  #<p1#33> sono sette#


p1#35:  <mh> <lp> <inspiration> poi <sp> <NOISE> il<ll> #<p2#36> che so#


p2#36:  #<p1#35> la scarpa# vediamo la scarpa #<p1#37> <unclear>#


p1#37:  #<p2#36> <tongue-click> s�# s� s�


<sp>


p2#38:  tiene diciamo uno due tre quattro #<p1#39> buchi#


p1#39:  #<p2#38> <NOISE> <tongue-click># s� <lp> <tongue-click> #<p2#40> anche a 
        me#


p2#40:  #<p1#39> <tongue-click> <vocal> un# tacco


<sp>


p1#41:  tacco per� questo tacco a me<ee> <ehm> <inspiration> � in+ / cio� voglio 
        dire <ehm> c'� soltanto disegnata la {<NOISE> scarpa} il<ll> #<p2#42> 
        <inspiration>#


p2#42:  #<p1#41> s� il tacco# non sta disegnato #<p1#43> no no no no no si vede 
        solamente <inspiration> diciamo<oo> il contorno , s�#


p1#43:  #<p2#42> non sta disegnato <sp> <mh> <lp> <inspiration> che c'�# <lp> s�


p2#44:  <tongue-click> <inspiration> poi abbiamo una<aa> cosa / dove andiamo <sp>
        #<p1#45> la panchina#


p1#45:  #<p2#44> un / il guinzaglio# com'� ?


<sp>


p2#46:  <inspiration> il guinzaglio scende dalla <lp> dall+ <eeh> <sp> 
        dall'avambraccio destro del signore che sta seduto l�


p1#47:  s�


<sp>


p2#48:  e va <ehm> <sp> #<p1#49> quasi dietro al cane , dalla parte di dietro 
        per�#


p1#49:  #<p2#48> quindi di+ <lp> <inspiration> quindi# novanta gradi #<p2#50> 
        circa <sp> s�#


p2#50:  #<p1#49> s� novanta gradi#


p1#51:  #<p2#52> va bene#


p2#52:  #<p1#51> <inspiration># la panchina invece vediamo i piedi della 
        panchina <sp> #<p1#53> <vocal># il piede<ee> <eeh> di dietro

p1#53:  #<p2#52> s�# <lp> s� <lp> #<p2#54> <NOISE> <sp> � verso l'esterno#


p2#54:  #<p1#53> <ehm> ha la pu+ <sp> ha la punta# verso si+ verso <eeh> verso 
        #<p1#55> dietro#


p1#55:  #<p2#54> verso# sinistra #<p2#56> dietro s�#


p2#56:  #<p1#55> verso s� , verso# sinistra


p1#57:  mentre il resto � in ombra


<sp>


p2#58:  il resto<oo> s� #<p1#59> � in o+#


p1#59:  #<p2#58> <tongue-click> diciamo<oo># la parte inferiore <lp> vero ?


<lp>


p2#60:  la parte inferiore � in ombra ?


<sp>


p1#61:  � in ombra dell'altro<oo> dell'altro piede


<lp>


p2#62:  <ah> s� s� s� s� s� #<p1#63> non si vede perch� c'� il cane davanti 
        <inspiration> <mh>#


p1#63:  #<p2#62> <mh> <sp> <vocal> <sp> s� s� <inspiration># poi <sp> <ehm> 
        {<inspiration> <NOISE>} {<ehm> <breath>} il <ehm> il televisore


<lp>


p2#64:  il televisore


p1#65:  s�


p2#66:  c'� una ragazza che sta ridendo


p1#67:  che sta ridendo s� , quanti pulsanti neri ha ?


p2#68:  <vocal> otto neri <sp> #<p1#69> e tre# <eeh> bianchi


p1#69:  #<p2#68> s�# <lp> tre quindi quelli di sotto piccoli , #<p2#70> quelli 
        di sopra bia+ / quello di sopra <mh> <inspiration> l'antenna# quanti<ii> 
        <lp> #<p2#70> ne ha# tre di<ii> colonne


p2#70:  #<p1#69> s� [whispering] <sp> s� s� <lp> poi# <lp> #<p1#69> <vocal># <P> 
        <breath> <sp> s� , ne ha tre di #<p1#71> colonne#


p1#71:  #<p2#70> e ha il# pallino sopra ?


<sp>


p2#72:  <tongue-click> <sp> no


p1#73:  ecco io ce l'ho


<lp>


p2#74:  un pallino<oo> <vocal> bello visibile , no , � ?


<sp>


p1#75:  <eh> a me s�


p2#76:  s� , se � visibile s� perch� io<oo> <nn>no , non distinguo nessuna 
        pallina


p1#77:  <mh> <sp> <inspiration> poi l'uomo<oo>


p2#78:  poi<ii> l'uomo #<p1#79> l'uomo che# cosa <sp> #<p1#79> <vocal>#


p1#79:  #<p2#78> s�# <lp> #<p2#78> <tongue-click> <ehm># l'orecchio ce l'ha ?


p2#80:  s� , ce l'ha l'orecchio


p1#81:  il naso <lp> il naso � lungo ?


p2#82:  s� il naso � lungo verso<oo>


p1#83:  verso la #<p2#84> donna# che ride


p2#84:  #<p1#83> es+# <lp> s�


p1#85:  <inspiration> il mento ce #<p2#86> l'ha<aa>#


p2#86:  #<p1#85> <tongue-click># #<p1#87> sporgente#


p1#87:  #<p2#86> s�# <lp> #p2#88> <inspiration> e<ee>#


p2#88:  #<p1#87> sporge+[whispering] poi per� dietro al# collo


p1#89:  s� ha una specie di<ii> <sp> #<p2#90> angolo#


p2#90:  #<p1#89> angolo# s�


p1#91:  <mh>


p2#92:  <inspiration> poi scusami Paola se #<p1#93> ti interrompo#


p1#93:  #<p2#92> s+ <sp> s� s�#


p2#94:  <inspiration> sulla sinistra c'� un tronco , #<p1#95>
giusto ?#


p1#95:  #<p2#94> <tongue-click># giustissimo


p2#96:  questo tronco non � <sp> <vocal>


p1#97:  #<p2#98> normale#


p2#98:  #<p1#97> normale# <sp> #<p1#99> � un poco<oo> incurvato <ehm>#


p1#99:  #<p2#98> � un po'<oo> <inspiration> verso sinistra# <lp> cio� <sp> <eeh> 
        dalla parte di sinistra , dalla parte #<p2#100> sinistra <sp> <mh> ?#


p2#100:  #<p1#99> s� <sp> s�# <sp> guardandolo la parte<ee> la parte sinistra � 
         pu+ <sp> un po+ un poco incurvata


p1#101:  s� esatto anche io ce l'ho cos� <sp> <inspiration> <eeh> l'albero quante<ee> 
         quante strisce ha di<ii> <lp> di erba diciamo di foglie perch� ne ha / 
         io ce ne ho uno due tre <inspiration> una all'interno divisa in due , 
         quasi


<lp>


p2#102:  s� <sp> <vocal>


<sp>


p1#103:  ci #<p2#104> sei ?#


p2#104:  #<p1#103> s�# s� s� #<p1#105> <ss>s� poi ne ha<aa># una s+ / una 
         #<p1#105> <ehm>#


p1#105:  #<p2#104> <mhmh> <inspiration> pi� o meno# unita


p2#106:  s� <sp> #<p1#107> e poi una esterna#


p1#107:  #<p2#106> <mh> <inspiration># <sp> una esterna che <sp> � divisa 
         diciamo #<p2#108> pi� o meno <sp> � uguale#


p2#108:  #<p1#107> s� s� s� � divisa � divisa# <inspiration> senti 'na<aa> 
         [dialect] #<p1#109> un'osservazione il tronco#


p1#109:  #<p2#108> s�# <lp> s�


p2#110:  <tongue-click> quando scende <inspiration> #<p1#111> che si# nasconde 
         dietro la panchina <lp> <inspiration> noti delle imperfezioni te ?


p1#111:  #<p2#110> s�# <P> <inspiration> <lp> s�<ii> <eeh> la parte inferiore va
         direttamente sul vestito #<p2#112> del<ll>#


p2#112:  #<p1#111> brava# <sp> #<p1#113> brava <lp> <ah> io pensavo *invose
         invece che qui#


p1#113:  #<p2#112> <mh> <NOISE># <lp> <inspiration>  #<p2#112> s� infatti# 
         fosse una #<p#114> caratteristica#


p2#114:  #<p1#113> <mh> <sp> vediamo# di fronte la casa <sp> #<p1#115> che 
         abbiamo# di fronte , abbiamo un'antenna con due <eeh> diciamo


p1#115:  #<p2#114> <tongue-click> s�# <lp> camini <lp> #<p2#116> due<ee>#


p2#116:  #<p1#115> con due camini# #<p1#117> bravi# , con due camini e<ee> con la 
         testa diciamo triangolare


p1#117:  #<p2#116> s�# <P> s� <sp> #<p2#118> s�#


p2#118:  #<p1#117> <inspiration># poi accanto c'� #<p1#119> un'antenna con 
         due<ee> <vocal>#


p1#119:  #<p2#118> <inspiration> <vocal> <sp> <vocal> <sp> con due#


p2#120:  <vocal> cose #<p1#121> orizzontali#


p1#121:  #<p2#120> s�# anche io #<p2#122> <inspiration>#


p2#122:  #<p1#121> dopodich�# abbiamo un #<p1#123> tetto spiovente#


p1#123:  #<p2#122> il tetto# spiovente naturalmente <vocal> <sp> <ehm> ci sono 
         delle strisce anche a te ?


<sp>


p2#124:  s� s� s� #<p1#125> s� s� <inspiration> le finestre# sono<oo> s+ / no le 
         strisce non si possono #<p1#125> contare<ee>#


p1#125:  #<p2#124> s� <sp> quante<ee># <P> #<p2#124> <mhmh># <sp> <tongue-click> s+ 
         <sp> s� , ne sono# #<p2#126> tante#


p2#126:  #<p1#125> sono# tante <eeh> le finestre invece di fronte sono una due 
         tre quattro cinque sei sette e #<p1#127> otto#


p1#127:  #<p2#126> e# otto <vocal> #<p2#128> come sono<oo># <inspiration> uno 
         due tre #<p2#128> quattro cinque sopra#


p2#128:  #<p1#127> perfetto <inspiration> <lp> allora hai cinque cinque# cinque 
         sopra <sp> #<p1#129> tre<ee># nella colonna esterna e due nella colonna 
         interna


p1#129:  #<p2#128> due# <P> <ah> s� s� <sp> #<p2#130> esatto s�#


p2#130:  #<p1#129> perfetto <inspiration># poi un'altra cosa <sp> il tetto 
         spiovente <lp> <tongue-click> <sp> sulla destra tiene un angolo


<sp>


p1#131:  s�


p2#132:  che esce fuori d+ <sp> dalla<aa> #<p1#133> <ehm> dalla perpendicolare 
         de+#


p1#133:  #<p2#132> s� s� anche io#


p2#134:  s� <inspiration> #<p1#135> benissimo# andiamo


p1#135:  #<p2#134> c'�# <lp> si vede una<aa> finestra <sp> sul<ll> profilo della casa 
         ?


<lp>


p2#136:  <nn>no


p1#137:  <inspiration> ecco esattamente<ee> <sp> <ehm> allora <sp> 
         parallelamente <inspiration> al<ll> <eeh> / alla<aa> / all'ultima colonna che 
         hai detto tu


<sp>


p2#138:  s�


<sp>


p1#139:  di quelle tre finestre <sp> esattamente sulla<aa> <inspiration> 
         sul<ll> profilo della casa


p2#140:  <mhmh>


p1#141:  si vede all'altezza dello zoccolo del cavallo una finestra diciamo

         un<nn> / una striscia #<p2#142> nera#


p2#142:  #<p1#141> che per� non# sta su questa facciata frontale <sp> #<p1#143> 
         <ss>sta sulla facciata laterale#


p1#143:  #<p2#142> <tongue-click> no no no sta sull'altra# facciata sulla destra 
         diciamo


p2#144:  quella � una differenza<aa> #<p1#145> Paola non ce l'ho#


p1#145:  #<p2#144> okay perfetto# <lp> <inspiration> <tongue-click> andiamo<oo> 
         alla macchina ?


p2#146:  <tongue-click> andiamo alla macchina


p1#147:  <mh> <lp> ce l'ha lo specchietto ?


p2#148:  s� <sp> lo specchietto retrovisore ce #<p1#149> l'ha#


p1#149:  #<p2#148> retrovisore# ce l'ha , quello<oo> di lato ?


<lp>


p2#150:  no <eeh> lo specchietto retrovisore<ee> esterno #<p1#151> tiene non 
         quello interno#


p1#151:  #<p2#150> <ah> esterno ce l'ha# <sp> #<p2#152> e<ee> io ne ho anche un 
         altro<oo>#


p2#152:  #<p1#151> s� <lp> quello# #<p1#153> interno#


p1#153:  #<p2#152> <inspiration> quello# interno


p2#154:  <mh> quello non ce l'ho io


p1#155:  non ce l'hai tu ? <sp> okay � un'altra <inspiration> poi <sp> <ehm> 
         <breath> <tongue-click> questa macchina <inspiration> vediamo un poco 
         sulla / tu la vedi la ruota ?


p2#156:  s�


p1#157:  sulla ruota c'� una striscia vero ? <P> sulla ruota diciamo<oo> <ehm> � 
         un disegno della #<p2#158> macchina#


p2#158:  #<p1#157> innanzitutto# io vedo #<p1#159> la ruota# anteriore sinistra 
         e parte della ruota anteriore destra


p1#159:  #<p2#158> s�# <P> <inspiration> e parte s� anche #<p2#160> io#


p2#160 :  #<p1#159> s�# , della ruota anteriore sinistra


p1#161:  s�


p2#162:  <tongue-click> io vedo il cerchione , #<p1#163> cio� il cerchio <sp> e 
         la ruota#


p1#163:  #<p2#162> s� <sp> s� s� c'�# <sp> #<p2#164> <inspiration> s�#


p2#164:  #<p1#163> mentre# di quella anteriore destra io #<p1#165> vedo<oo>#


p1#165:  #<p2#164> <tongue-click> <inspiration># soltanto la parte interna 
         #<p2#166> e poco dell'esterno#



p2#166:  #<p1#165> <vocal> {[screaming] due archi} sono# <unclear> #<p1#167> � 
         un arco e poi una<aa> una spezzata diciamo#


p1#167:  #<p2#166> s� <lp> um+ <sp> s� esatto# #<p2#168> s� anche io#


p2#168:  #<p1#167> s� s� <inspiration> poi# c'abbiamo un paraurti , un radiatore


p1#169:  s�


p2#170:  un paraurti #<p1#171> squadrato# , il paraurti � squadrato #<p1#171> 
         giusto ?#


p1#171:  #<p2#170> due<ee># <P> #<p2#170> squadrato# #<p2#172> s�#


p2#172:  #<p1#171> s�# <inspiration> poi abbiamo #<p1#173> due fa+#


p1#173:  #<p2#172> per� tra# il paraurti e le due , diciamo , i due fanali


p2#174:  c'� una striscia #<p1#175> nera#


p1#175:  #<p2#174> c'� una# striscia #<p2#176> nera s�#


p2#176:  #<p1#175> perfetto# #<p1#177> <inspiration> poi c'� l+#


p1#177:  #<p2#176> <inspiration> senti# sulla ma+ / sulla ruota<aa> sinistra 
         <sp> #<p2#178> anteriore#


p2#178:  #<p1#177> <mh># <lp> <mhmh>


<sp>


p1#179:  s+ <sp> esattamente tra la ruota an+ / sinistra anteriore <sp> e lo 
         specchietto retrovisore esteriore diciamo , #<p2#180> esterno# , 
         esteriore <inspiration> si trova una striscia


p2#180:  #<p1#179> s�<ii># <P> s�


p1#181:  e <sp> esattamente alla sinistra <sp> esterna di questa striscia si 
         trova un puntino nero


p2#182:  sono le frecce<ee> laterali


p1#183:  #<p2#184> esatto#


p2#184:  #<p1#183> tra la# ruota <sp> e il fanalino sinistro anteriore


p1#185:  perfetto s� #<p2#186> ce l'hai anche tu allora#


p2#186:  #<p1#185> <mh> <lp> s�#


p1#187:  <mh>


p2#188:  no la macchina penso #<p1#189> che#


p1#189:  #<p2#188> s�# non<nn> / <tongue-click> <eeh> e il finestrini i finestrini non sono 
         disegnati


<lp>


p2#190:  <tongue-click> i finestrini ?


p1#191:  #<p2#192> diciamo non<nn> non c'� la<aa> / lo scheletro dei<ii> 
         finestrini n� di quel+# <sp> va bene allora


p2#192:  #<p1#191> <nn>no non sono <sp> no no no no no <lp> no no no no#


p1#193:  il cavallo ?


<lp>


p2#194:  il cavallo sopra , allora il cavallo sta su una zampa


p1#195:  s� una #<p2#194> sola#


p2#196:  #<p1#195> l'anteriore# sinistra credo , l'anteriore des+ <sp> bo
         #<p1#197> non lo so dire <unclear>#


p1#197:  #<p2#196> s� vabb� non si capisce# #<p2#198> <mhmh>#


p2#198:  #<p1#197> va be' una sta# dritta l'altra invece sta<aa> <ehm>


p1#199:  #<p2#200> s� , piegate <sp> s� <sp> s� s�#


p2#200:  #<p1#199> piegata a novanta gradi <sp> a novanta gradi come uno zoccolo<oo># 
         <inspiration> #<p1#201> <eeh>#


p1#201:  #<p2#200> s� sporgente# vabb� #<p2#202> s�#


p2#202:  #<p1#201> s�# #<p1#203> sporgente#


p1#203:  #<p2#202> <inspiration> si# vede soltanto il profilo vero ? #<p2#204> 
         non c'� nie+ a parte diciamo<oo># la bocca<aa> del<ll> cavallo


p2#204:  #<p1#203> s� s� il profilo il profi+# <P> s� la bocca c'� non c'ha n+ / 
         gli occhi per�


p1#205:  <vocal> s�


p2#206:  non ha gli #<p1#207> occhi#


p1#207:  #<p1#206> s�# <sp> e le orecchie ?


p2#208:  le orecchie s� , ce l'ha verso l'alto


p1#209:  s� anche io <lp> e l'uomo ?


p2#210:  l'uomo<oo> , l'occhio ce l'ha l'uomo ?


p1#211:  no


<lp>


p2#212:  io ce l'ho


<sp>


p1#213:  okay � un'altra <P> <inspiration> il cappello ce l'ha ?


<lp>


p2#214:  il cappello s�


p1#215:  e<ee> i pantaloni ?


<sp>


p2#216:  ha i #<p1#217> pantaloni e gli stivali forse#


p1#217:  #<p2#216> s� <lp> <inspiration># #<p2#218> forse , perch� non si<ii>#


p2#218:  #<p1#217> perch�<ee> il pantalone si# ferma a mezzo #<p1#219> ginocchio 
         , cio�#


p1#219:  #<p2#218> s� s� s�# <sp> #<p2#220> diciamo <sp> vicino la caviglia#


p2#220:  #<p1#219> s�# <inspiration> <lp> s� s� s� s� #<p2#221> s�#


p1#221:  #<p2#220> s�# <sp> <eeh> <sp> poi la spada ce l'ha ?


<sp>


p2#222:  ha una spada rivolta verso<oo> <sp> #<p1#223> l'alto <sp> s�#


p1#223:  #<p2#222> verso l'alto <inspiration># per� tra la spada e la mano non 
         si vede diciamo la differenza vero ?


p2#224:  no


p1#225:  okay <sp> quindi � soltanto<oo> <inspiration> <sp> <eeh> sotto al 
         cavallo<oo> <sp> c'� una scritta per caso ?


<lp>


p2#226:  sotto al cavallo non c'� nessuna scritta


p1#227:  <eh> per� c'� il quadrato vero ?


p2#228:  <eeh> credo che sia<aa> <ehm>  <lp> <tongue-click> l'im+ l'imbracatura 
         del cavallo <lp> quella striscia che si vede sotto al #<p1#229> piede#


p1#229:  #<p2#228> <tongue-click># <inspiration> <ah> quella dici , s� s� s�



