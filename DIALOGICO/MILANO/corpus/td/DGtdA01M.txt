TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            01
Ntr:          
Nls:
REG:            M


SPEAKERS_inf.

INp1:          	P. Z., M, 22, Milano, spontaneo, fluente                   
INp2:          	E. B., F, 23, Milano, spontaneo, fluente


RECORDING_inf.

TYP:           	DAT        
LOC:           	Milano/aula universitaria
DAT:           	28/11/00
DUR:           	11.15,487
CON:           	buone


TRANSCRIPTION_inf.

DAT:           	09/04/02
CMT:           
Nst:           	261



p1#1:  okay <NOISE> <sp> <tongue-click> sono Paolo e sono Pi uno


p2#2:  sono Elena <sp> e sono Pi due <breath> <lp> allora , chi inizia ? 


p1#3:  vai vai Elena


p2#4:  tento di descriverti<ii> <tongue-click> allora partiamo col cane ?


p1#5:  partiamo con il cane che starebbe qui sotto , s�


p2#6:  <mh> <sp> <tongue-click> allora a me sembra che ci sia una cosa strana
       <sp> a livello del guinzaglio <sp> {<NOISE> <sp> alla gola <sp> nel senso 
       che} non � terminata #<p1#7> la<aa>#


p1#7:  #<p2#6> <tongue-click> anche# la mia 


<lp>


p2#8:  {<NOISE> segnamo} quello , sar� quello l'errore 


p1#9:  e no #<p2#10> perch� se<ee>#


p2#10:  #<p1#9> e no , {<NOISE> infatti}# <lp> #<p1#11> hai ragione#


p1#11:  #<p2#10> se fosse# <sp> se fosse terminata sarebbe <inspiration> no no 
        neanche la mia <sp> � terminata , quindi<ii> 


p2#12:  allora <breath>


p1#13:  la bocca del cane cos'� ? com'� che � ?


p2#14:  sta #<p1#15> {<laugh> sorridendo}#


p1#15:  #<p2#14> okay# , allora anche il mio


p2#16:  il naso � molto grande <sp> e nero


p1#17:  s� , grande<ee> , #<p2#18> nero , con<nn># <sp> una specie di #<p2#18>
        mezzaluna<aa># , s�


p2#18:  #<p1#17> <mh># <lp> #<p1#17> s�# <lp> <inspiration> <sp> le orecchie
        sono<oo> in su , a #<p2#19> punta#


p1#19:  #<p2#18> a punta# in su <sp> s�


p2#20:  forse la coda <sp> #<p1#21> �#


p1#21:  #<p2#20> la# coda �<ee> appuntita 


p2#22:  in su ?


p1#23:  no in su , in gi�


p2#24:  okay , ne abbiamo trovato uno perch� la mia � in su <sp> #<p1#25> <mh>#


p1#25:  #<p2#24> okay#


p2#26:  le {<NOISE> zampe} ?


p1#27:  le zampe <ehm> <sp> <tongue-click> <sp> ma <eh> a+ / {<laugh> sono<oo>}
        <laugh> s+ <sp> #<p2#28> sono# tutte e due<ee> gi� <sp> per <sp> per
        terra sdraiate# per terra


p2#28:  #<p1#27> <laugh> <sp> {<hypo-articulated> terra}# <lp> #<p1#29> s�#


p1#29:  #<p2#28> e<ee># in orizzontale <inspiration> con<nn> tutte e due nella
        stessa direzione , sono identiche


p2#30:  hanno #<p1#31> {<NOISE> due<ee>}#


p1#31:  #<p2#30> con# due / s� #<p2#32> s� , esatto <sp> s�#


p2#32:  #<p1#31> s� , due co+ / ok+ okay allora# l� niente <inspiration>
        passiamo a #<p1#33> qualcos'altro ?#


p1#33:  #<p2#32> s�# <lp> s� passiamo<oo> 


p2#34:  dimmi tu


p1#35:  passiamo al monumento #<p2#36> al<ll> <sp> al monumento#


p2#36:  #<p1#35> al monumento <sp> s� <sp> ecco# <sp> dimmi


p1#37:  qui il<ll> il cavallo ha la la la<aa> la zampa destra alzata ? s�


p2#38:  la zampa <sp> la sua #<p2#39> zampa destra ?#


p1#39:  #<p2#38> s� , la sua# #<p2#40> zampa dalla zampa del cavallo#


p2#40:  #<p1#39> s� <lp> s�#


p1#41:  s� <inspiration> okay <inspiration> <eeh> quindi sono uguali <lp>
        poi la la spada del cavaliere


p2#42:  � in su


p1#43:  � in su ?


p2#44:  s�


p1#45:  con la parte arrotondata verso sinistra o verso destra ? {[whispering]
        verso sinistra <sp> s� <sp> sar�}


<lp>


p2#46:  l'� verso #<p1#47> sinistra#


p1#47:  #<p2#46> s�# <lp> #<p2#48> e<ee>#


p2#48:  #<p1#47> la parte# arrotondata ?


p1#49:  s� , va ben la #<p2#50> la<aa>#


p2#50:  #<p1#49> la punta ?#


p2#51:  s� , la #<p2#52> punta la<aa># {<laugh> sai come #<p2#52> no ?}
        <inspiration> infatti# <sp> non in linea retta {<laugh> volevo dire}
        #<p2#52> <laugh>#


p2#52:  #<p1#51> {<laugh> s� , okay}# <lp> #<p1#51> {<laugh> s� s�} <laugh>#
        <lp> #<p1#51> va bene#


p1#53:  e<ee> il<ll> il cavaliere ha un mento molto sporgente


p2#54:  s�


<lp>


p1#55:  e anche un naso #<p2#56> molto s+#


p2#56:  #<p1#55> anche# un nasino , #<p1#57> s�#


p1#57:  #<p2#56> {<laugh> s�}# #<p2#58> <laugh>#


p2#58:  #<p1#57> <laugh># <sp> <ehm> <tongue-click> le orecchie del cavallo ?


p1#59:  sono in su , a punta


p2#60:  <mh> <sp> anche le mie


p1#61:  <mh>


p2#62:  <mh> <sp> <mhmh> <lp> #<p1#63> non vedo nient'altro che possa#


p1#63:  #<p2#62> t+ <sp> <inspiration> proviamo# sul tetto che ci sono<oo>
        #<p2#64> delle<ee> <sp> s+ <sp> s+#


p2#64:  #<p1#63> <tongue-click> delle an+ <sp> s� , ho visto#


p1#65:  <eh> c'�<ee> sulla destra una<aa> linea verticale con due orizzontali in
        alto <sp> la prima , quella pi� in alto , un po' pi� piccola della
        seconda , pi� in basso , sto parlando di quelle orizzontali


p2#66:  st+ <sp> ma stai descrivendo #<p1#67> l'antenna ?#


p1#67:  #<p2#66> s�# , {<hypo-articulated> descrivendo} #<p2#68> l'antenna# , s� 


p2#68:  #<p1#67> <eh> !#


p1#69:  � cos� anche la tua ?


p2#70:  dunque quella so+ <sp> <mh> quella ve+ / orizzontale � pi� piccola di
        quella che sta


<lp>


p1#71:  no , io ne ho una verticale e #<p1#72> due orizzontali#


p2#72:  #<p2#71> e due orizzontali# <sp> <tongue-click> la prima orizzontale �
        pi� piccola di quella #<p1#73> che sta#


p1#73:  #<p2#72> s�#


p2#74:  s� <sp> allora � uguale


p1#75:  uguale


p2#76:  quelle *speci di<ii> <sp> <tongue-click> non so #<p1#77> come desc+#


p1#77:  #<p2#76> comignoli#


p2#78:  {<NOISE> esatto}


p1#79:  sono<oo> gemelli


p2#80:  #<p1#81> uguali#


p1#81:  #<p2#80> s�#


<sp>


p2#82:  anche i miei


p1#83:  {[whispering] pi� o meno <sp> s�}


p2#84:  <ah> ! forse il numero delle finestre


p1#85:  otto 


p2#86:  s� <sp> come sono #<p1#87> disposte ?#


p1#87:  #<p2#86> sono<oo># sono cinque<ee> <sp> <eeh> a mezzo {[postalveolar-affricate] c+} centimetro dal
        tetto


p2#88:  s�


p1#89:  <eeh> in orizzontale , poi tre in verticale sulla destra <sp> contando
        #<p2#90> anche quella<aa> , le cinque#


p2#90:  #<p1#89> s� <sp> s�# <sp> <tongue-click> e #<p1#91> un'altra in 
        {<NOISE> me+}#


p1#91:  #<p2#90> e poi# un'altra<aa> subito<oo>


p2#92:  okay , e quello {<NOISE> �} {<laugh> uguale}


p1#93:  s�


p2#94:  <unclear> proviamo con la #<p1#95> macchina <breath> ?#


p1#95:  #<p2#94> s�#


p2#96:  <tongue-click> allora <eeh> non c'� niente di strano in questa macchina
        #<p1#97> <inspiration> <sp> <laugh> cio� non riesco a<aa>#


p1#97:  #<p2#96> <laugh># allora {[whispering] allora}


<lp>


p2#98:  <vocal> <tongue-click> � assolutamente normale quindi non riesco a dirti
        cosa


p1#99:  proviamo con l'antenna <tongue-click> con l'antenna del / della specie
        di computer #<p2#100> non so che# cosa / che #<p2#100> sta<aa> / 
        dell'uomo#


p2#100:  #<p1#99> s�# <lp> #<p1#99> s� , ho visto#


p1#101:  <eeh> <vocal> � divisa in tre #<p2#102> parti#


p2#102:  #<p1#101> s�#


p1#103:  #<p2#104> cio�<ee>#


p2#104:  #<p1#103> verso# sinistra


p1#105:  verso #<p2#106> sinistra ?#


p2#106:  #<p1#105> s�#


<lp>


p1#107:  cio� ? in che senso , perch� la mia non � verso sinistra


p2#108:  va verso il cavallo


<lp>


p1#109:  va verso il cavallo


p2#110:  del monumento


<sp>


p1#111:  s� <sp> ma , decisamente verso il cavallo oppure va verso la casa ? va
         verso l'antenna che abbiamo descritto #<p2#112> prima ?#


p2#112:  #<p1#111> va# verso l'antenna


p1#113:  <ah> ! va verso l'antenna <inspiration> ed � / praticamente ogni pezzo
         sono un centimetro pi� o meno <lp> cio� , ogni #<p2#114> pezzo in cui �
         divisa<aa>#


p2#114:  #<p1#113> s� , � {<NOISE> un} centimetro e m+# <sp> s� , � un #<p1#115>
         centimetro#


p1#115:  #<p2#114> s�# , circa un #<p2#116> centimetro#


p2#116:  #<p1#115> quindi# quello non {<laugh> �} #<p1#117> <laugh>#


p1#117:  #<p2#116> no , quello non �#


p2#118:  <tongue-click> proviamo con l'u+ / beh <eeh> <sp> l'uomo


p1#119:  l'uomo sta vedendo nel computerino un cane 


p2#120:  #<p1#121> s�#


p1#121:  #<p2#120> s� ?# <lp> con due pallini come occhi e un naso grande


p2#122:  s� <lp> <tongue-click> quello


p1#123:  ci sono<oo> uno due tre qua+ / otto<oo> #<p2#124> quadratini come
         pulsanti#


p2#124:  #<p1#123> bo+ <sp> s� <sp> neri#


p1#125:  s�


p2#126:  e tre bianchi , diciamo


p1#127:  s� , tre bianchi uno grande pi�<uu> sopra #<p2#128> e<ee> <sp> e poi#
         due sotto


p2#128:  #<p1#127> <mh> <sp> <mh># <lp> <mh> <lp> l'uomo ha qualche espressione
         particolare ?


<sp>


p1#129:  no , ha la #<p2#130> bocca<aa>#


p2#130:  #<p1#129> dritta#


p1#131:  s�


p2#132:  <mh>


p1#133:  cio� , c'� una lineetta


p2#134:  s� , anche a me <sp> <breath> <lp> anche per me <inspiration> <eeh>


p1#135:  {<laugh> ci stiamo #<p2#136> scoraggiando} <laugh>#


p2#136:  #<p1#135> {<laugh> le le scarpe dell'uomo ?# come sono ?}


p1#137:  {<laugh> allora} , le scarpe dell'uomo <ehm> i lacci


<sp>


p2#138:  s�


p1#139:  c'� una<aa> / sono sono tre <sp> in<nn> / adesso s� , pi� o meno in 
         orizzontale


p2#140:  s�


p1#141:  e<ee> poi una una<aa> <uu>una cosa che taglia in<nn> verti+ / in
         obliquo chiaramente per� sta facendo<oo> #<p2#142> orizzontale
         verticale#


p2#142:  #<p1#141> <tongue-click> s� {<NOISE> s�} ho capito , # no , quindi sono
         uguali a quelle #<p1#143> del mio# 


p1#143:  #<p2#142> s�#


p2#144:  {[whisprering] <oh> mamma !} <breath> <sp> <mh> <sp> <NOISE> 
         <tongue-click> io ho delle {<NOISE> specie} di impronte


p1#145:  s�, sulla<aa> #<p2#146> sinistr+ / sulla destra ?# 


p2#146:  #<p1#145> in basso # <sp> a destra , #<p1#147> s�#


p1#147:  #<p2#146> su# #<p2#146> <p2#148> nell'angolo ?#


p2#148:  #<p1#147> s�#


p1#149:  dai , speriamo {<laugh> qua ! aspetta , quante sono ?}


p2#150:  <eeh> qua+ / sette


p1#151:  {<laugh> anche le mie}


p2#152:  {<laugh> dai} <laugh> <vocal> senti #<p1#153> <ehm>#


p1#153:  #<p2#152> ma# magari sono disposti in modo diverso #<p2#154> boh , non
         {[whispering] lo so}#


p2#154:  #<p1#153> pu� essere# 


<sp>


p1#155:  cio� , c'� / ce n'� praticamente <inspiration> una in mezzo e poi due 
         che<ee> / le due laterali #<p2#156> che che formano# due triangoli ,
         #<p2#156> cio� , pi�# o meno 


p2#156:  #<p1#155> s�# <lp> #<p1#155> s�# <sp> � uguale


p1#157:  s� <sp> mamma {<NOISE> mia !}


p2#158:  <ehm> <sp> <tongue-click> senti la forma del piedistallo del monumento


<sp>


p1#159:  s�


p2#160:  io ho un basamento che � tipo<oo> <breath> <lp> <tongue-click> oddio
         adesso con le figure {<laugh> geometriche} <inspiration> <sp> un
         #<p1#161> trapezio#


p1#161:  #<p2#160> {<laugh> s�# , un #<p2#162> trapezio} <laugh> <sp> s�#


p2#162:  #<p1#161> <laugh># <sp> poi<ii> un<nn> quadrato


<lp>


p1#163:  s� un<nn> #<p2#164> quadrato<oo> tagliato , s�#


p2#164:  #<p1#163> che co+ <lp> s� , tagliato# che contiene un altro 
         #<p1#165> quadrato#


p1#165:  #<p2#164> s�# , esatto


p2#166:  e niente , poi un rettangolo


<sp>


p1#167:  s� , #<p2#168> anch'io#


p2#168:  #<p1#167> niente# , � uguale � {<NOISE> uguale} <inspiration> <sp> io
         non riesco a trovare <ehm>


<lp>


p1#169:  <breath> #<p2#170> {[whispering] allora}#


p2#170:  #<p1#169> <eh> <sp> <eh># <breath> <sp> <NOISE> <lp> <tongue-click>
         <inspiration> la {<NOISE> ruota} della #<p1#171> macchina#


p1#171:  #<p2#170> <mh>#


<lp>


p2#172:  quella<aa> di <lp> alla nostra sinistra


<lp>


p1#173:  s�


<sp>


p2#174:  non � stata completamente <ehm>


p1#175:  s� , � <sp> � #<p2#176> vero#


p2#176:  #<p1#175> anche la#


<sp>


p1#177:  neanche la #<p2#178> mia# 


p2#178:  #<p1#177> neanche# la #<p1#179> tua#


p1#179:  #<p2#178> neanche# la {<laugh> mia} <laugh> 


p2#180:  <vocal> #<p1#181> <ehm>#


p1#181:  {[whispering] madonna ! <lp> #<p2#180> s+ / troviamone# un {<laugh>
          altro} <laugh> allora <sp> allora [whispering]}


p2#182:  {<NOISE> l'albero} non l'abbiamo visto <sp> per�


p1#183:  l'albero anche questo non #<p2#184> � stato<oo>#


p2#184:  #<p1#183> <ah> !# <sp> #<p1#185> aspetta# 


p1#185:  #<p2#184> dimmi#


p2#186:  ha una rientranza il mio <lp> <ehm> <sp> dalla parte #<p1#187>
         sinistra#


p1#187:  #<p2#186> s� , anche il# mio


p2#188:  <eh> ! <sp> #<p1#189> <laugh>#


p1#189:  #<p2#188> s� s� , anche il {<laugh> mio}#


p2#190:  non #<p1#191> mi#


p1#191:  #<p2#190> <eeh># <ss>s+ / la panchina <inspiration> <lp> <tongue-click>
         la panchina � lievemente<ee> arricciata in cima


p2#192:  #<p1#193> <tongue-click> s�#


p1#193:  #<p2#193> s�#


p2#194:  <laugh> <lp> <tongue-click> � in ombra la parte #<p1#195> {<NOISE>
         sotto}#


p1#195:  #<p2#194> s�#


p2#196:  #<p1#197> e#


p1#197:  #<p2#196> la# parte sotto nel senso la #<p2#198> parte<ee>#


p2#198:  #<p1#197> quella# dove � a+ <sp> appoggia+ / dove ci sono le gambe ,
         diciamo


<lp>


p1#199:  dove ci sono le #<p2#200> gambe , <ah> no <unclear>#

l'intero turno � disturbato


p2#200:  #<p1#199> le gambe del personaggio#


<lp>


p1#201:  <ah> ! c'� l'ombra ?


<sp>


p2#202:  s�


<sp>


p1#203:  no , nella mia non c'� l'ombra <sp> <inspiration> dove ci sono le gambe
         del personaggio mi stai dicendo che<ee> <inspiration> #<p2#204> <eeh>
         cio� , dove# c'� il cane dove ci sono le #<p2#204> gambe del ca+#


p2#204:  #<p1#203> s�# <lp> #<p1#203> <tongue-click> dove c'�# s� <vocal> la
         gamba {<NOISE> che} <sp> � sul<ll> <ehm> <tongue-click> va verso il
         sedere {<laugh> del #<p1#205> cane#}


p1#205:  #<p2#204> s�# <NOISE> l� c'� #<p2#206> l'ombra ?#


p2#206:  #<p1#205> c'�# l'ombra


<sp>


p1#207:  <eh> ! allora #<p2#208> segnalo perch� io# non ce l'ho #<p2#208>
         l'ombra l�#


p2#208:  #<p1#207> tu non ce l'hai ?# <lp> #<p1#207> <ah># vedi ! <sp> 
         <inspiration> <NOISE> <lp> <tongue-click> <sp> per caso hai le<ee>
         strisce verticali <inspiration> sul<ll> {<NOISE> <ehm>} <tongue-click>
         sullo #<p1#209> schienale ?#


p1#209:  #<p2#208> sul retro# della / s� , sullo #<p2#210> schienale# , s� s�


p2#210:  #<p1#209> s�# <lp> quelle ce le #<p1#211> hai#


p1#211:  #<p2#210> s�# s�


p2#212:  <mh> <tongue-click> <sp> l'erba<aa> vi+ <sp> sotto il monumento ?


p1#213:  <mh> {<NOISE> � a / praticamente} fatta a cerchio


p2#214:  s�


p1#215:  puntini , cio� trattini<ii> <inspiration> che circondano tutto il /l+ 
         l+ / il monumento


p2#216:  s�


p1#217:  e <NOISE> per� non sono in / su tutta la la la<aa> la superficie <sp>
         #<p2#218> sono soltanto<oo>#


p2#218:  #<p1#217> <ah> ! no ? <sp> solo<oo>#


p1#219:  <tongue-click> solo un po' #<p2#220> esternamente <sp> all'interno# �
         bianco


p2#220:  #<p1#219> s� <NOISE> s�# <lp> <tongue-click> okay


p1#221:  #<p2#222> <mh>#


p2#222:  #<p1#221> � uguale# <sp> <tongue-click> <inspiration> <tongue-click>
         <sp> il mento del personaggio ? <breath>


p1#223:  molto pronunciato


p2#224:  <mh> <tongue-click> anche il #<p1#225> mio#


p1#225:  #<p2#224> pi� o# meno come il #<p2#226> naso#


p2#226:  #<p1#225> anche# il naso , le ore+ <sp> l'orecchio <sp> {[whispering]
         solo ce n'� solo uno}


p1#227:  e poi ha una rientranza questo #<p2#228> {<laugh> personaggio}#


p2#228:  #<p1#227> {<laugh> s�# , anche il #<p1#229> mio} <laugh>#


p1#229:  #<p2#228> sulla# nuca <sp> #<p2#228> anche il tuo ?# <lp> un angolo


p2#230:  la panchina ?


p1#231:  <mh>


p2#232:  la <ehm> <lp> {<NOISE> [whispering] alla gamba , non so come si chiama}
         <sp> che non � stata / che ti ho detto che <ehm> <lp> ce n'� una in
         ombra 


<sp>


p1#233:  s�


p2#234:  l'altra � � <sp> la tua � in ombra ? <lp> no ?


<lp>


p1#235:  <ah> ma s� ! ma � la mia <sp> <tongue-click> <inspiration> <NOISE>
         s� , ma <vocal> allora , no no , e avevo visto male io , alla fine
         anche la mia<aa> / la gamba di cui stavi parlando prima � in ombra , 
         la #<p2#236> mia#


p2#236:  #<p1#235> <tongue-click># <inspiration> <ah> ! #<p1#237> {<NOISE> ho
         capito}#


p1#237:  #<p2#236> cio� tutta la gam+ / io credevo# che fosse proprio il prato
         cio� , *ce fosse il il / l+ l+ <inspiration> il suolo in ombra


p2#238:  ho #<p1#239> capito#


p1#239:  #<p2#238> invece# no , � tutto bianco il suolo , cio� #<p2#240> il#


p2#240: #<p2#239> s�#


p1#241:  okay niente , allora {<laugh> toglilo #<p2#242> perch� non �}
         <inspiration> no , l'altra non �# in ombra 


p2#242:  #<p1#241> <laugh># <lp> <tongue-click> e no ! neanche il mio <sp> � una
         tragedia {<laugh> praticamente} <lp> <laugh>


p1#243:  s� , perch� c'� solo la cod+ <sp> ma senti , della macchina ?


p2#244:  #<p1#245> dimmi <ehm>#


p1#245:  #<p2#244> <eeh># vediamo un po' <sp> <NOISE> l'altezza del<ll> <sp> 
         cio� <sp> il l+ le <vocal> i finestrini ? #<p2#246> {<NOISE> il 
         parabrezza}# / i finestrini <inspiration> sono<oo> delimitati da linee
         non / sono semplici #<p2#246> proprio#


p2#246:  #<p1#245> <mh># <P> #<p1#245> s�#


p1#247:  finite tre linee c+ c'� il / lo specchietto retrovisore , vero ?

'c+' � una affricata postalveolare 


p2#248:  s� 


p1#249:  c'� <inspiration> <lp> <ehm> <lp> <breath> i fari sono<oo> quadrati
         sono<oo> #<p2#250> rettangolari ?#


p2#250:  #<p1#249> s+ <ss>s�#


p1#251:  s� <inspiration> <lp> per il resto � semplicissimo non � che ci sono
         altri #<p2#252> segni<ii>#


p2#252:  #<p1#251> niente#


p1#253:  #<p2#254> niente#


p2#254:  #<p1#253> �<ee># {<laugh> una macchina normalissima}


p1#255:  s� , c'� {<NOISE> una} linea che passa in orizzontale<ee> sulla
         fiancata della macchina ?


p2#256:  s�


p1#257:  okay <inspiration> <lp> #< p2#258> che tragedia !# va be' <NOISE>


p2#258:  #<p1#257> <mhmh> <sp> {<laugh> veramente}# <lp> <NOISE> <tongue-click>
         <sp> non {<NOISE> �} che ce n'� solo uno ? <breath> <sp> e {<NOISE> 
         non} ce lo vogliono dire ? 


<lp>


p1#259:  <laugh>


p2#260:  <laugh> ma dai ! la coda del cane ?


p1#261:  spero <inspiration> <sp> <eh> ! non so , no non<nn>



