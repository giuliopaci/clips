TEXT_inf.

MAT:            mt
MAP:            A
TRM:
Ndl:            04
Ntr:  
Nls:
REG:            P


SPEAKERS_inf.

INp1:		T. L., F, 27, Erice, spontaneo, fluente, G>F
INp2:		C. B., F, 22, Palermo, spontaneo, fluente, F>G


RECORDING_inf.

TYP:		DAT
LOC:		Palermo/Centro studi filologici e linguistici siciliani
DAT:		16/07/01
DUR:		17.55,347
CON:		buone


TRANSCRIPTION_inf.

DAT:		22/10/01 
CMT:
Nst:		426
              

p2F#1:  allora


p1G#2:  possiamo ?


p2F#3:  vai


p1G#4:  {[whispering] okay} <inspiration> , allora Cinzia ?


p2F#5:  <mh>


p1G#6:  ascoltami , il tracciato che<ee> ti indico<oo> vede praticamente 
        interessati <inspiration> il televisore in basso


p2F#7:  s�


p1G#8:  la torta , le due macchine rosse e la macchina azzurra


p2F#9:  <mhmh>


p1G#10:  ci siamo ? <inspiration> #<F#11> quindi#
 

p2F#11:  #<G#10> le due  macchine# rosse ? #<G#12> ce n'�# solo una 
         {<laugh> macchina rossa} c'� il camion casomai rosso


p1G#12:  #<F#11> le d+# <lp> no , ci sono due vetture rosse


<sp>


p2F#13:  no , qua c'� una vettura<aa> azzurra  due maggioloni  uno azzurro 
         uno rosso e un camion rosso


<sp>


p1G#14:  <tongue-click> e allora evidentemente abbiamo 
         {<laugh> qualcosa di diverso} #<F#15> <laugh>#


p2F#15:  #<G#14> {<laugh> e s�}#


p1G#16:  e allora partiamo<oo> <vocal> <ehm> la partenza 
         praticamente � <sp> <inspiration> segna una croce <sp> 
         poco<oo> sopra<aa> <lp> 
         <vocal> allora <eeh> poco sopra il televisore


p2F#17:  #<G#18> s�#


p1G#18:  #<F#17> sul# lato per�<oo> leggermente pi� a sinistra del foglio


p2F#19:  s�


p1G#20:  <inspiration> #<F#21> okay ?#


p2F#21:  #<G#20> s�# s�


p1G#22:  da qui <sp> partiamo <inspiration> segna praticamente sc+ / 
         una curva #<F#23> discendente#


p2F#23:  #<G#22> s�#


p1G#24:  <inspiration> <ehm> con de+ con<nn> dei tratti #<F#25> 
         {[whispering] proprio}# con una linea #<F#25> tratteggiata#


p2F#25:  #<G#24> s�# <lp> #<G#24> s�#


p1G#26:  <inspiration> che va<aa> sotto il<ll> televisore


<sp>


p2F#27:  s�


<lp>


p1G#28:  okay ?


p2F#29:  s� sotto <sp> #<G#30> perfett+#


p1G#30:  #<F#29> perfetto# <eeh> diciamo che la distanza tra il televisore
         e la<aa> e la linea tratteggiata sar� orientativamente di un dito


p2F#31:  <inspiration> {[whispering] <ah> va bene allora un poco pi� largo} 
         <sp> <inspiration> <eh>
             

p1G#32:  okay ?


p2F#33:  #<G#34> va bene#


p1G#34:  #<F#33> <inspiration> mantieni# sempre questa distanza di un 
         {<NOISE> dito tutt'attorno al} televisore
         no <nn>no non girare per�<oo> in modo circolare


p2F#35:  s�


p1G#36:  arriva fino dove c'� il #<F#37> filo#


p2F#37:  #<G#36> s�#


p1G#38:  okay ? <inspiration> adesso da questo punto invece fai <eeh> 
         una<aa> <ehm> leggera curva 


p2F#39:  <eh>


p1G#40:  a destra come se dovessi disegnare u_na<aa> <inspiration> 
         <ehm> un colle <sp> no ?


p2F#41:  s� <sp> a / quindi a destra va bene <eh>


p1G#42:  #<F#43> s�#


p2F#43:  #<G#42> una legge+ /# cio� a salire di #<G#44> nuovo no ?#


p1G#44:  #<F#43> a salire# s� #<F#45> verso destra per�#


p2F#45:  #<G#44> {[whispering] s�} <sp> s�# #<G#46> okay sto salendo#


p1G#46:  #<F#45> fai<ii> all'incirca# appunto <ehm> un<nn> fai un colle 
         e non lo fare molto alto <lp> ci sei ?


p2F#47:  s�


p1G#48:  e vai scendendo verso la torta


<sp>


p2F#49:  <inspiration> <eeh> <eeh> cio� praticamente d+ / � come se 
         dovessi fare una<aa> una linea di lunghezza d'onda ? che ne 
         #<G#50> so una cosa del genere ?#


p1G#50:  #<F#49> s� perfetto# come se #<F#51> dovessi <unclear>#


p2F#51:  #<G#50> cio� salire# e ridiscendere


p1G#52:  perfetto fai praticamente <unclear> <eeh> tra l+ il televisore 
         #<F#53> e la# torta disegna  praticamente un colle


p2F#53:  #<G#52> s�# <lp> s� <sp> #<G#54> in mezzo# in mezzo tu gli sci 
         ce l'hai ?


p1G#54:  #<F#53> ci sei ?# <lp> no


p2F#55:  <ah> ! io c'ho gli sci #<G#56> vabb� comunque<ee>#


p1G#56:  #<F#55> <NOISE> <sp> allora# quindi evidentemente sali sugli sci<ii>
         #<F#57> praticamente# fai andare questa c+ <sp> <inspiration> 
         fai andare #<F#57> questa curva# <eeh> sopra gli sci <sp> no ?


p2F#57:  #<G#56> s�<ii># <lp> #<G#56> <breath># <lp> s�


<lp>


p1G#58:  quest'#<F#59> onda sale sugli sci#


p2F#59:  #<G#58> s� e poi riscende# sotto<oo> <eeh> #<G#60> in basso#


p1G#60:  #<F#59> allora# <sp> perfetto allora ri<ii>+ / fermati #<F#61> a<aa>  
         <ehm># <tongue-click> sei arrivata praticamente #<F#61> all'altezza# della torta ?


p2F#61:  #<G#60> <laugh># <lp> #<G#60> <inspiration># <eeh> si sono gi� sotto la torta 


p1G#62:  <ah> ! <sp> <eeh> sotto la torta per� considera che deve rimanere poco spazio tra
         la torta e<ee> / cio� tra il vassoio e la linea tratteggiata che stai
         com+ / facendo


p2F#63:  quanto deve rimanere ?


p1G#64:  <inspiration> <ehm> <breath> <lp> mezzo centimetro un #<F#65> centimetro al massimo#


p2F#65:  #<G#64> <inspiration> cio� devo andare# quasi si<ii> dove c'� il vassoio insomma
         #<G#66> colla<aa> coi tratti# 


p1G#66:  #<F#65> un po' pi� dista+# #<F#67> un po' pi�# distante fai<ii> conto {[screaming] 
         la<aa>} <inspiration> <tongue-click> considera l'altezza della torta


p2F#67:  #<G#66> un po' pi+# <lp> {<NOISE> s�}


p1G#68:  okay ? proprio la torta sola


p2F#69:  s�


<sp>


p1G#70:  okay <sp> lo spessore come #<F#71> quando#


p2F#71:  #<G#70> lo spessore# come #<G#72> distanza#


p1G#72:  #<F#71> del pan di Spagna# perfetto #<F#73> <inspiration> fai# la stesso
         lo<oo> <tongue-click> <vocal> praticamente continua questa curva
        

p2F#73:  #<G#72> <laugh># <lp> sotto la torta


p1G#74:  sotto la torta


p2F#75:  #<G#76> <eh>#


p1G#76:  #<F#75> e# vai praticamente <sp> salendo


p2F#77:  s�


p1G#78:  verso l'altro lato della torta quindi #<F#79> circonda praticamente#
         questa torta #<F#79> no ?#


p2F#79:  #<G#78> s� s� s�# <lp> #<G#78> �# come se ci fosse una specie di esse a<aa> / 
         sulla torta {[whispering] quasi} cio� da+ vabb� comunque {<laugh> non ti preoccupare}
         vai <vocal>


p1G#80:  #<F#81> no# <ehm> perch� una -Esse- #<F#81> Cinzia aspetta#


p2F#81:  #<G#80> <vocal># <lp> #<G#80> no no <unclear># una -Esse- coricata diciamo
         che mi va dal televisore alla torta


p1G#82:  {[screaming] s�}


<sp>


p2F#83:  #<G#84> ecco#


p1G#84:  #<F#83> <eh># <sp> diciamo #<F#85> di s�#


p2F#85:  #<G#84> come se# fosse <sp> #<G#86> vabb� dai#  


p1G#86:  #<F#85> okay# <inspiration> arriva praticamente / dove sei arrivata ?


p2F#87:  <aa>all'altezza della torta dall'altro lato sulla<aa> sulla destra


p1G#88:  #<F#89> sulla# destra


p2F#89:  #<G#88> <vocal> sono# #<G#90> salita#


p1G#90:  #<F#89> allora<aa># <eeh> fermati praticamente alla<aa> la parte centrale 
         della torta in alto


<lp>


p2F#91:  quindi devo circolare #<G#92> un po' intorno alla torta#


p1G#92:  #<F#91> <tongue-click> s� perfetto# circ+ fai proprio questo<oo> #<F#93> questo segn+ 
         <ehm> questo segno attorno<oo> alla# torta


p2F#93:  #<G#92> s� vabb� girare intorno alla tor+ <sp> s�#


p1G#94:  #<F#95> <inspiration> okay#


p2F#95:  #<G#94> e sono arrivata# al centro


<sp>


p1G#96:  #<F#97> perfetto# <inspiration> #<F#97> adesso# fai delle linee proprio rette


p2F#97:  #<G#96> <breath># <sp> #<G#96> <laugh># <lp> s�


p1G#98:  <inspiration> quasi praticamente <eeh> verso sinistra


<lp>


p2F#99:  s�


p1G#100:  ci sia+ / ci sei ?


p2F#101:  s�


p1G#102:  <tongue-click> okay <inspiration> fai del+ / sempre delle linee tratteggiate 
          <inspiration> lasciando praticamente la distanza sempre pi� o meno di un 
          centimetro tra <eeh> il<ll> <inspiration> tra praticamente la vetta di quel 
          mon+ / di quel colle che hai fatto gi�


<sp>


p2F#103:  <vocal> <ah> ! s� ? quindi praticamente devo andare dritto continuare<ee> <ehm>
          #<G#104> ho capi+#


p1G#104:  #<F#103> a sinistra# #<F#105> <sp> ci sei ? come se tornassi indietro#


p2F#105:  #<G#104> <inspiration> s� continua+/ andare dri+ cio� rispetto# al
          centro della torta andare dritto e tornare indietro cio� <ehm> s�


p1G#106:  #<F#107> fare# una linea retta


p2F#107:  #<G#106> <eh># <lp> s� s� a tornare<ee> sulla cima del monte s�


p1G#108:  perfetto lascia <eeh> un<nn>  all'incirca un centimetro tra la vetta del colle
          <sp> e l'altr+ e la linea {<NOISE> praticamente che} stai<ii> ef+ 
          #<F#109> facendo#
 

p2F#109:  #<G#108> s� forse# c'� pi� di un centimetro qua #<G#110> {<NOISE> comunque aspetta}#


p1G#110:  #<F#109> <vocal> vabb�# orientativamente


<lp>


p2F#111:  <laugh> <inspiration> allora


<sp> 

Dopo <sp> vi � un evento non vocale


p1G#112:  <vocal> quando arrivi alla al<ll> <ehm> alla vetta del monte


<lp>


p2F#113:  s�


p1G#114:  cont+ <eeh> fermati


p2F#115:  <eh> #<G#116> s�# so+ / mi sono {<NOISE> fermata}


<sp>


p1G#116:  #<F#115> <vocal># <lp> okay sei proprio dove c'� l<ll>+ <eeh> dove c'� la vetta
          #<F#117> praticamente ?#


p2F#117:  #<G#116> s�#


p1G#118:  okay #<F#119> <inspiration># adesso man mano


p2F#119:  #<G#118> <breath># <eh>


p1G#120:  <vocal> fai c+ <ehm> diciamo <sp> comincia a tondeggiare nuovamente <sp>


p2F#121:  #<G#122> s�#


p1G#122:  #<F#121> <inspiration># fino a percorrere tutto il foglio
          sal+ / cercando di salire per� man #<F#123> mano#


p2F#123:  #<G#122> dove# devo arrivare #<G#124> intanto ?#


p1G#124:  #<F#123> devi# arrivare alla macchina rossa


<sp>


p2F#125:  <ah> !


p1G#126:  che c'+ / che � a sinistra del foglio


p2F#127:  s� #<G#128> quella<aa> in basso# a sinistra un po' in ba+ /#<G#128> cio�
          poco# sotto il #<G#128> centro# a #<G#128> sinistra#


p1G#128:  #<F#127> perch� io ho# #<F#127> s�# <sp> #<F#127> <vocal># <sp> #<F#127> sotto# 
          il pettine

    
<sp>


p2F#129:  sotto il pe+ ? io ce l'ho sopra il pettine <lp> vabb� <lp> {<laugh> comunque}
          <inspiration> {[screaming] allora} devo arrivare alla macchina rossa , hai detto 
          #<G#130> come ?#


p1G#130:  #<F#129> �# guarda � una macchina rossa a sinistra del #<F#131>foglio#


p2F#131:  #<G#130> s�# e quindi #<G#132> ci devo arrivare come <sp> hai detto#


p1G#132:  #<F#131> <inspiration> ci devi# arrivare <sp> allora la devi praticamente
          circondare , vai prima da sotto <sp>


<sp>


p2F#133:  s�


<sp>


p1G#134:  okay ?


<sp>


p2F#135:  sempre con la linea retta ?


<sp>


p1G#136:  con la linea / la devi fare sempre tratteggiata #<F#137> questa no ? <sp> okay#


p2F#137:  #<G#136> <breath> s� s� dico questa linea# tratteggiata dopo
          il monte diciamo sulla #<G#138>cima# 


p1G#138:  #<F#137> s� no# comincia a curvare verso l'alto


<sp>


p2F#139:  s� <lp> okay <NOISE>


p1G#140:  adesso io in geometria non sono molto brava dovrebbe {<laugh> essere una 
          specie di iper+ {[whispering] iperbole [whispering]} non lo so <laugh>} #<F#141> 
          <inspiration>#


p2F#141:  #<G#140> inizia a# curvare verso #<G#142> l'alto#


p1G#142:  #<F#141> <vocal> okay# <sp> <inspiration> {<laugh> l'iperbole / #<F#143> ho detto 
          una fesseria} fai conto <unclear>#


p2F#143:  #<G#142> <inspiration> <sp> io alla macchina# #<G#144> Tiziana ?#


p1G#144:  #<F#143> s�#


p2F#145:  alla macchina devo arrivare alla ruota davanti posteriore<ee> 
          #<G#146> oppure#


p1G#146:  #<F#145> <inspiration> {[screaming] allora}# tu devi arrivare allora #<F#147> 
          <aa>alla ma+# <sp> alla cu+ / scusami alla ruota destra anteriore


p2F#147:  #<G#146> cos� inclino# la curva <P> destra #<G#148> anteriore#


p1G#148:  #<F#147> che non vedi#


p2F#149:  s� che #<G#150> non vedo#


p1G#150:  #<F#149> ci sei ?#


p2F#151:  s� #<G#152> <inspiration> ho capito#


p1G#152:  #<F#151> perfetto# <inspiration> fai proprio un segno dove c'� il cofano
          praticamente


<lp>


p2F#153:  #<G#154> s�#


p1G#154:  #<F#153> dove# c'�<ee> la parte davanti okay ? <inspiration>


p2F#155:  {[whispering] aspetta che faccio} la #<G#156> curva#


p1G#156:  #<F#155> dove c'�# il faro


p2F#157:  s�


<sp>


p1G#158:  okay <inspiration> poi fai praticamente una linea


<lp>


p2F#159:  s�


<lp>


p1G#160:  proprio<oo> <inspiration> <vocal> allo+ hai prese+ <vocal> una linea praticamente
          proprio quasi a<aa> toccare il tetto dell'auto <lp> #<F#161> in obliquo#


p2F#161:  #<G#160> una linea ?# <vocal> <ah> ! a toccar il tetto #<G#162> dell'auto ?#


p1G#162:  #<F#161> <vocal> s�# <inspiration> hai presente quando si dice che una retta 
          passa<aa> per un punto ?


p2F#163:  <eh> <laugh>


p1G#164:  deve toccare {<NOISE> un punto ?}


p2F#165:  #<G#166> {<laugh> <eh>}#


p1G#166:  #<F#165> okay


p2F#167:  {<laugh> okay #<G#168> ci siamo va be+#}


p1G#168:  #<F#167> fai <sp> fai una# okay <inspiration> <ehm> vai invece adesso 
          segna una linea nella ruota posteriore sinistra


<lp> 

Dopo <lp> vi � un evento non vocale


p2F#169:  posteriore sinistra #<G#170> che � questa#


p1G#170:  #<F#169> posteriore# <sp> #<F#171> sinistra#


p2F#171:  #<G#170> s� <sp> <eh>#


p1G#172:  <inspiration> dell'auto scendendo verso il gi�


<sp>


p2F#173:  #<G#174> una / quindi devo ritor+ / devo come<ee> {[screaming] circondare la} macchina#


p1G#174:  #<F#173> vai verso il basso <sp> <tongue-click> s� {[screaming] come tornare} indietro#


p2F#175:  s�


p1G#176:  okay <inspiration>


p2F#177:  <eh> all'incirca #<G#178> <eh> s�#


p1G#178:  #<F#177> <vocal> ora fai# tutta una linea tratteggiata


<sp>


p2F#179:  s�


<sp>


p1G#180:  <tongue-click> scendendo verso la torta <lp> praticamente


p2F#181:  s�


<sp>


p1G#182:  <tongue-click> #<F#183> okay ?#


p2F#183:  #<G#182> cio�# � come<ee> fare il percorso #<G#184> inverso# un po' 
          #<G#184> pi� stretto ?# <sp> cio�<ee> <vocal> #<G#184> s�#


p1G#184:  #<F#183> <tongue-click># <sp> #<F#183> pe+# <lp> #<F#183> no# <sp> no <vocal> 
          non<nn> non la devi toccare la torta quindi no+ / praticamente la linea che 
          #<F#185> stai<ii>#


p2F#185:  #<G#184> deve essere# parallela a quello che ho tracciato prima all'incirca


p1G#186:  all'incirca s�


<sp>


p2F#187:  {[whispering] all'incirca {<laugh> chiss� che stiamo facendo <laugh>} [whispering]}


<lp>


p1G#188:  quando arrivi #<F#189> sopra la torta all'altezza# della torta


p2F#189:  #<G#188> {<laugh> sai come fraintendersi} <laugh># <inspiration> <eh> <laugh>


p1G#190:  ti fermi


p2F#191:  {<laugh> okay} <inspiration> poi ? 


<sp>


p1G#192:  okay ? <inspiration> <tongue-click> risali nuovamente verso destra


<sp>


p2F#193:  <eh>


<sp>


p1G#194:  <tongue-click> con le curve


p2F#195:  s�


p1G#196:  okay ? <inspiration>


p2F#197:  e c'� la macchina azzura <lp> no


<lp>


p1G#198:  #<F#199> sotto la macchina azzurra non<nn>#


p2F#199:  #<G#198> dove devo andare adesso ? <sp> devo an_dare# sotto la macchina azzurra
          dove faro destro<oo> faro<oo> #<G#200> di destra di sinistra ruo+#


p1G#200:  #<F#199> allora ruota# destra #<F#201> anteriore#


p2F#201:  #<G#200> quindi# quella che si vede


p1G#202:  perfetto
 

p2F#203:  <inspiration> e devo andarci curvando ?


p1G#204:  s�


<sp>


p2F#205:  okay


<lp>


p1G#206:  quindi asseconda tutto questo #<F#207> percorso#


p2F#207:  #<G#206> <ss>s� 


p1G#208: continui f+ / il<ll> percorso fino alla #<F#209> ruota<aa> <sp> destra# posteriore


p2F#209:  #<G#208> <vocal> gi� ci sono arrivata alla ruota# <lp> s�


<sp>


p1G#210:  okay ? <sp> <inspiration> e sali


<lp>



p2F#211:  s�


p1G#212:  sali per un altro po' , ci sono una serie di #<F#213> stelle#


p2F#213:  #<G#212> {[screaming] hai detto}# destra posteriore ?


<lp>


p1G#214:  s�


p2F#215:  {[whispering] <ah> <unclear> <NOISE> vabb� tanto qua #<G#216> ci passa <unclear>#}


p1G#216:  #<F#215> vabb� destra# anteriore e destra posteriore #<F#217> solo quelle 
          si vedono#


p2F#217:  #<G#216> vabb� infatti sono# allineate s� #<G#218> va bene dai#


p1G#218:  #<F#217> okay#


p2F#219:  poi #<G#220> dove devo andare ?#


p1G#220:  #<F#219> <inspiration> <eeh># <sp> hai delle stelle al centro del #<F#221> foglio ?#


p2F#221:  #<G#220> s�#


p1G#222:  okay <vocal> <ehm> praticamente <vocal> sei salita un po' con<nn> il tracciato ?


<sp>


p2F#223:  s� #<G#224> s� vabb� dai#


p1G#224:  #<F#223> okay# fermati perch� � il punto di arrivo


<lp>


p2F#225:  devo salire poco poco sopra la #<G#226> ruota<aa> <vocal>#


p1G#226:  #<F#225> s� un altro po'# #<F#227> <eeh> <uu>un pochino e poi#


p2F#227:  #<G#226> curvare un po' e scendere verso#


<lp>


p1G#228:  no no <sp> non devi pi� scendere devi soltanto salire <eeh> la ruota posteriore


<sp>


p2F#229:  <unclear> #<G#230> salire ?#


p1G#230:  #<F#229> cio� devi# assecondare praticamente <inspiration> la direzione 
          della ruota posteriore


<sp>


p2F#231:  <inspiration> <ah> ! quindi <sp> {[whispering] aspe+ per� il suo punto d'arrivo 
          allora non � uguale al mio nel senso di<ii> / proprio posizionato} <inspiration>


p1G#232:  okay ?


p2F#233:  <inspiration> s� all'#<G#234> incirca#


p1G#234:  #<F#233> fermati# #<F#235> fermati#


p2F#235:  #<G#234> <breath> non# so esattamente #<G#236> <unclear>#


p1G#236:  #<F#235> <eh> <mb�># evidentemente #<F#237> a+#


p2F#237:  #<G#236> cio� aspetta# rispetto alla / l'ultima<aa> <vocal> rispetto alla 
          ruota posteriore ?


p1G#238:  s�


p2F#239:  considera / devo andare come fare una diagonale ? che cosa devo #<G#240> fare ?#


p1G#240:  #<F#239> no no# devi salire verso<oo> verso il centro del<ll> foglio
          praticamente quasi


p2F#241:  s�


p1G#242:  okay ? e ti fermi


<sp>


p2F#243:  {[whispering] il centro sar� circa} #<G#244> qua#


p1G#244:  #<F#243> fai# altri due tra_tti pi+ / orientativamente e #<F#245> ti fermi#


p2F#245:  #<G#244> s� vabb�#


p1G#246:  okay?


p2F#247:  okay <sp> s� Ti<ii> bisogna fermarsi sopra una stella oppure no ?


p1G#248:  #<F#249> orientativamente#


p2F#249:  #<G#248> va bene# #<G#250> s� all'incirca#


p1G#250:  #<F#249> perch� credo che tu le# stelle le abbia diverse dalle mie


<sp>


p2F#251:  vabb� comunque #<G#252> {<laugh> fermiamo}#


p1G#252:  #<F#251> okay ?#


p2F#253:  okay


INVERSIONE DEI RUOLI


p1F#254:  #<G#255> dimmi tu#


p2G#255:  <inspiration> #<F#254> dunque# <tongue-click> allora <tongue-click> il tuo punto 
          di partenza


p1F#256:  s�


p2G#257:  �<ee> la<aa> farfalla <sp> se ce l'hai


p1F#258:  no


p2G#259:  non c'hai una farfalla ?


p1F#260:  no


<sp>


p2G#261:  va bene e quindi<ii> che si #<F#262> fa ?#
 

p1F#262:  #<G#261> dimmi# quali #<G#263> sono<oo> i<ii>#


p2G#263:  #<F#262> <tongue-click> sono# <eeh> allora un percorso che va
          da una farfalla che � circa poca so+ poco sopra il centro del foglio


<sp>


p1F#264:  #<G#265> <mhmh>#


p2G#265:  #<F#264> <inspiration> quindi# un po' pi� spostata da me<ee> sulla 
          #<F#266> destra rispetto<oo># / la mia #<F#266> destra#


p1F#266:  #<G#265> perfetto# <lp> #<G#265> s�# #<G#267> okay #


p2G#267:  #<F#266> <inspiration># po_i <eeh> passa vicino a un camion
          con<nn> un tessuto rosso , � giallo e rosso #<F#268> po+#
 

p1F#268:  #<G#267> che � sulla# destra ? #<G#269> o sulla sin+#


p2G#269:  #<F#268> � sempre# sulla destra #<F#270> anche# pi� a destra
          #<F#270> rispetto la farfalla pi� in alto#


p1F#270:  #<G#269> s�# <lp> #<G#269> s� okay okay okay#


p2G#271:  <inspiration> po_i un poco pi� a sinistra quindi ritorniamo all'incirca 
          *alliniati #<F#272> con la farfalla in alto# un dado


p1F#272:  #<G#271> s� s� <sp> okay# <sp> perfetto #<G#273> poi ?#


p2G#273:  #<F#272> poi# passa da un lupo che � proprio completamente a #<F#274> sinistra#


p1F#274:  #<G#273> s� #<G#275> perfetto#


p2G#275:  #<F#274> e poi# sotto il lupo un po' pi� a destra ci sono degli 
          #<F#276> occhiali da sole e poi ci# sono le stelle


p1F#276:  #<G#275> okay okay okay# <lp> perfetto <sp> #<G#277> allora cominciamo#


p2G#277:  #<F#276> cio� che cosa hai tu# di queste #<F#278> cose ?#


p1F#278:  #<G#277> <inspiration> allora# io ho il camion
          il dado <inspiration> <eeh> il lupo e gli occhiali , in pi� rispetto a te <sp>
          <vocal> ho<oo> un<nn> una bici e un pettine che tu non hai


<sp>


p2G#279:  <mh>


p1F#280:  #<G#281> okay ? dammi il punto di partenza e co+#


p2G#281:  #<F#280> <mh> <sp> s+ <sp> no io il pettine# ce l'ho <sp> 
          #<F#282> comunque non c'entra niente no#


p1F#282:  #<G#281> per� non<nn> non viene intaccato#


p2G#283:  #<F#284> <inspiration># allora fai<ii> / allora considera che la mia farfalla


p1F#284:  #<G#283> okay# <lp> s� #<G#285> vabb� � il mio punto di# partenza #<G#285> okay#


p2G#285:  #<F#284> parte al centro# <lp> #<F#284> s�# parte al p+ / al centro del foglio
          per� un po' pi� sopra rispetto al centro e un poco poco #<F#286> poco#


p1F#286:  #<G#285> po+ pi�# a #<G#287> destra <eeh># 


p2G#287:  #<F#286> poco poco# pi� a #<F#288> destra#


p1F#288:  #<G#287> coincide# con il mio punto di partenza #<G#289> con il# <unclear>
          il mio punto di arrivo


p2G#289:  #<F#288> <ah> !# <lp> <ah> vedi <NOISE> {[whispering] questa � una #<F#290> cosa 
          <unclear>}#


p1F#290:  #<G#289> okay#


p2G#291:  okay allora aspetta che aggiusto la mia e facciamo {[whispering] il punto di arrivo} 
          <inspiration> <sp> <inspiration> allora praticamente da questo tuo punto d'arrivo 
          considera che devi tracciare una<aa> come se fosse una diagonale molto
          morbida ad uscire verso la destra del foglio <sp> #<F#292> ci sei ?#


p1F#292:  #<G#291> s�# <sp> s�


p2G#293:  {[screaming] e} arrivare praticamente alle ruote posteriori del camion 
          <inspiration> lasciando circa meno<oo> circa mezzo centimetro rispetto 
          alla ruota<aa> <unclear> all'ultima ruota del camion di distanza 
          {<NOISE> come spazio bianco}


p1F#294:  <ss>s�


p2G#295:  <inspiration> quindi considera questa #<F#296> diagonale molto morbida# va


p1F#296:  #<G#295> perfetto perfetto#


p2G#297:  <inspiration> poi <eeh> continua a mantenere questa distanza circa di 
          mezzo centimetro lungo tutto l'altezza del #<F#298> camion e sempre#
          con una linea curva


p1F#298:  #<G#297> perfetto# <P> s�


p2G#299:  {[screaming] vai} spostando questa curva la orienti verso sinistra adesso
          <inspiration> ad a orientarti circa sopra lo spigolo il vertice praticamente del<ll>
          del cubo <sp> del cubo cio� del dado #<F#300> qua#


p1F#300:  #<G#299> s�# #<G#301> okay#


p2F#301:  #<F#300> <inspiration> considera# lo spigolo pi� in alto del cubo dove c'� il
          quattro no? #<F#302> dove c'�# il pallino <unclear>


p1F#302:  #<G#301> s+# <sp> #<G#303> s�#


p2G#303:  #<F#302> proprio<oo># il<ll> sopra il #<F#304> vertice#


p1F#304:  #<G#303> okay# #<G#305> <inspiration> quindi# praticamente dalla / dal<ll> tendone
          rosso del camion io posso cominciare a salire


p2G#305:  #<F#304> <vocal> quindi# <lp> {[screaming] s�} dal tendone rosso del camion 
          #<F#306> tu devi salire# girando #<F#306> per�<oo> facendo questa curva#


p1F#306:  #<G#305> okay# <lp> #<G#305> s� s� okay mi interessa la distanza# dal cu+ dal cubo
          un centimetro #<G#307> sempre ?#


p2G#307:  #<F#306> {[screaming] meno}# #<F#308> meno meno di un centime+#


p1F#308:  #<G#307> m+ <sp> mezzo centimetro# come #<G#309> ho fatto con#


p2G#309:  #<F#308> anche di meno# #<F#310> di mezzo centime+# rispetto allo spi+ /
          � #<F#310> una<aa> <ehm> una distanza che si va assottigliando#


p1F#310:  #<G#309> okay# <lp> #<G#309> va bene va bene okay <sp> <unclear># <sp> #<G#311> 
          va bene#


p2G#311:  #<F#310> <inspiration> per� devi# rimanere sempre lo+ <vocal> un po' lon+ <vocal> 
          proprio<oo> #<F#312> <mm>millimetri# lontana dal cubo <eeh> dalla<aa> dal dado 
          {[screaming] poi} dallo spigolo<oo> dallo #<F#312> spigolo<oo># vabb� <sp> 
          #<F#312> superiore del dado#


p1F#312:  #<G#311> s� s� okay# <P> #<G#311> s�# <lp> del #<G#311> quattro okay#


p2G#313:  <inspiration> vai scendendo sempre con una curva
          molto #<F#314> per�<oo> <vocal>#


p1F#314:  #<G#313> morbida#


p2G#315:  <mh> no neanche molto morbida in effetti sarebbe anche quasi una retta per�
          � un una retta un po<oo>'


p1F#316:  #<G#317> va bene okay non �#


p2G#317:  #<F#316> <inspiration> vabb�# comunque � pi� re+ pi� retta {<laugh> rispetto
          all'altra curva}


p1F#318:  #<G#319> s�#


p2G#319:  #<F#318> <inspiration> vai# scendendo <inspiration> <eeh> di circa quattro
          tratti<ii> #<F#320> insomma<aa> grossi# <ee>e vai<ii> scendendo <sp> pi� a sinistra
          ovviamente
        

p1F#320:  #<G#319> <mh># <P> <vocal> quindi molto larghi tutto #<G#321> sommato#


p2G#321:  #<F#320> no# non molto larghi considera che � una distanza<aa> che<ee> in linea
          rispetto allo spigolo del cubo rimane sempre di pochi millimetri quindi devi
          scendere come se fosse una diagonale<ee> <inspiration> una discesa ripida va
          #<F#322> come dirt+#


p1F#322:  #<G#321> <mh> <sp># okay


p2G#323:  <ah> vabb� <inspiration> dopodich� arrivata a una specie<ee> vabb� <ehm> 
          dopodich� #<F#324> arrivata un po' sopra gli occhiali#


p1F#324:  #<G#323> mi devi dire co+ / mi /# scusa mi devi dire come collegare il
          cubo a quale altra #<G#325> figura#


p2G#325:  #<F#324> <tongue-click> devi# collegare il cubo al lupo


<lp>


p1F#326:  al #<G#327> lupo#


p2G#327:  #<F#326> ce# l'hai il lupo ?


p1F#328:  <inspiration> io ce l'ho il lupo per� tra il dado e il<ll> il lupo ho<oo> una<aa> bici


<lp>


p2G#329:  no bici non ce #<F#330> ne sono#


p1F#330:  #<G#329> <eeh> mi# ostacola<aa> un po' il<ll> il #<G#331> percorso#


p2G#331:  #<F#330> {<laugh> oddi+}# <inspiration> allora <eeh> se ostacola il percorso
          allora<aa> la bici com'�<ee> #<F#332> combinata � mes+#


p1F#332:  #<G#331> no mi devi# dire<ee> <eeh> devi tornare un attimo sul cubo


<sp>


p2G#333:  s� <lp># allora sul cubo #<F#334> <inspiration>#


p1F#334:  <vocal> <lp> #<G#333> io sono# sullo spigolo in alto <sp> 
          #<G#335> del cubo sul quattro#


p2G#335:  #<F#334> s� <sp> allora tra+ <vocal># traccia una<aa> discesa<aa> #<F#336> ripida#


p1F#336:  #<G#335> ripida ?# <sp> #<G#337> okay#


p2G#337:  #<F#336> s�# <sp> <inspiration> traccia una discesa ripida #<F#338> verso<oo> verso 
          la <inspiration># 
        

p1F#338:  #<G#337> verso il basso#


<sp>


p2G#339:  verso {<laugh> la bicicletta}


p1F#340:  okay verso #<G#341> il basso quindi#


p2G#341:  #<F#340> <eeh> s�# <inspiration> per� <eeh> <vocal> passa<aa> con la bicicl+ <eeh> 
          diciamo<oo>


p1F#342:  sotto la #<G#343> bici#


p2G#343:  #<F#342> <tongue-click> s�# passa #<F#344> sotto la# bicicletta e fai una curva 
          a rialzare


p1F#344:  #<G#343> okay# <P> s�


<sp>


p2G#345:  cio� passi #<F#346> sotto e rialz+# ti rialzi <inspiration> e f+ / continua a 
          tracciare questa linea curva <inspiration> ad andare intorno praticamente al 
          lupo #<F#346> fino#


p1F#346:  #<G#345> okay okay# <P> #<G#345> devo<oo># <eeh> #<G#347> scusami#


p2G#347:  #<F#346> devi circondare# il lupo con #<F#348> questa linea curva#


p1F#348:  #<G#347> s� <sp> co+# comincio dalla coda ?


<sp>


p2G#349:  <inspiration> cominci dalla coda #<F#350> esattamente e arrivi alla zampa<aa># al+ 
          alla zampa esterna , cosa ?


p1F#350:  #<G#349> <vocal> okay e passo sopra ?# <P> <eeh> <vocal> quindi passo #<G#+> sopra ?#


p2G#351:  #<F#350> s�# passi sopra il lupo e lo cir_condi per met� #<F#352> praticamente#


p1F#352:  #<G#351> okay#


p2G#353:  <inspiration> dopodich� alla zampa<aa> <ehm> questa � la sua zampa de+ no sinistra 
          sare+ vabb� comunque #<F#354> quella pi�<uu> <vocal> esterna#


p1F#354:  #<G#353> okay <lp> pi� evidente#


p2G#355:  #<F#356> s�#


p1F#356:  #<G#355> okay#


p2G#357:  <inspiration> comunque da questa zampa scendi un poco verso destra


<lp>


p1F#358:  s�


<lp>


p2G#359:  {[screaming] e} <sp> tieni una distanza di mezzo centimetro circa dagli occhiali da 
          sole se ce li hai <lp> sempre scendendo


<lp>


p1F#360:  <tongue-click> e gli occhiali da sole io ce li ho accanto al lupo sulla destra


<sp>


p2G#361:  esatto so_no<oo> accanto al lupo sulla destra per� sono a+ circa all'altezza 
          delle zampe del lupo delle #<F#362> zampe anteriori giusto ?#


p1F#362:  #<G#361> s� <lp> s�#


p2G#363:  <inspiration> perfetto allora tu dalla zampa anteriore <inspiration>


p1F#364:  #<G#365> s�#


p2G#365:  #<F#364> vai# tenendo questo stesso bordo scendendo un poco e 
          mantenendo una distanza <inspiration> di circa mezzo centimetro , credo che sia , 
          dalla<aa> <ehm> dal tratteggio cio� il tratteggio deve essere circa mezzo 
          centimetro distante da+ dagli occhia+ dalle lenti degli occhiali


p1F#366:  <mh> <sp> okay devo circondare gli occhiali #<G#367> con l'asta destra?#


p2G#367:  #<F#366> no non devi *circolnare# niente devi *somplicemente 
          passarci passare davanti alle #<F#368> lenti#


p1F#368:  #<G#367> okay#


<sp>


p2G#369:  <inspiration> e passando davanti alle lenti continui ad andare avanti 
          scendendo 


<sp>


p1F#370:  #<G#371> s�#


p2G#371:  #<F#370> poco# <inspiration> sulla destra #<F#372> e <unclear># e orientandoti
          verso adesso il centro del #<F#372> foglio perch�# dobbiamo arrivare alle stelle
 

p1F#372:  #<G#371> okay# <P> #<G#371> okay okay# <P> okay


p2G#373:  okay ? vai scendendo verso le stelle


<sp>


p1F#374:  okay


p2G#375:  <inspiration> e<ee> arrivi<ii> alle stelle io di stelle ne ho una due e tre verso 
          la ri+ / {[whispering] aspetta uno due tre quattro cinque sei} se_tte #<F#376> otto#


p1F#376:  #<G#375> otto#


p2G#377:  otto stelle perfetto allora tu sei <inspiration> <eeh> sulla prima al p+ / quella 
          piccola #<F#378> tonda ?# proprio la #<F#378> prima al centro# <inspiration> e 
          scendi tutto dritto fino #<F#378> alla<aa>#


p1F#378:  #<G#377> s�# <lp> #<G#377> okay# <P> #<G#377> come la prima# al centro , 
          no #<G#379> la prima punto#


p2G#379:  #<F#378> aspetta# io ne ho <sp> #<F#380> la prima#


p1F#380:  #<G#379> la prima# in alto


p2G#381:  s� la #<F#382> prima in# alto � una piccola in mezzo 
          a due #<F#382> grandi#


p1F#382:  #<G#381> okay# <P> #<G#381> s�#


p2G#383:  esatto tu #<F#384> devi passare#


p1F#384:  #<G#383> no <sp> no# no


p2G#385:  no okay non � / le hai uguali alle mie <inspiration> {[screaming] vabb�} comunque 
          scendi verso il centro del foglio


p1F#386:  s� 


p2G#387:  <inspiration> continua a tratteggiare in mezzo alle stelle allineandone tre centrali


<lp>


p1F#388:  okay


p2G#389:  e poi<ii> ti fermi al<ll> all'ultima<aa> al centro del foglio<oo> <vocal> 
          un poco pi� avanti � l'arrivo poco di poco <P> {<laugh> chiss� che #<F#390> 
          abbiamo fatto} <laugh>#


p1F#390:  #<G#389> <inspiration> considera che se le stelle sono in o+# sono otto


p2G#391:  #<F#392> s�#


p1F#392:  #<G#391> <inspiration> il# mio tratteggio va alla parte interna o alla 
          parte #<G#393> esterna ?#


p2G#393:  #<F#392> <unclear> in+# interna devi passare in mezzo alle stelle alla
          fila di quella centrale {[whispering] alle alla fila centrale} <lp> 
          #<F#394> considera ci sono tre stelle interne# no ?


p1F#394:  #<G#393> le devo assecondare tutte ?#


p2G#395: cosa ?


p1F#396:  le devo assecondare #<G#397> tutte <unclear> ?#


p2G#397:  #<F#396> s�# vai dritta<aa> assecondando<oo> vai dritto assecondando<oo>
          come se l'allineassi come se congiungessi queste tre capito ? <lp>
          la pi� <eeh> esterna verso<oo> verso gli occhiali alla pi�<uu> esterna 
          verso<oo> il punto di arrivo che sarebbe il centro del foglio poi
          <inspiration> <P> <NOISE> #<F#398> okay ?#


p1F#398:  #<G#397> <tongue-click> non ho# capito bene le<ee> le #<G#399> stelle<ee> 
          Cinzia#


p2G#399:  #<F#398> allora considera# <vocal> di stelle ce n'� #<F#400> <unclear>#


p1F#400:  #<G#399> la prima# e l'ultima devono essere #<G#401> unite<ee>#


p2G#401:  #<F#400> s�# <sp> si esattamente #<F#402> dai tratteggi devi passare# 
          per una seconda interna <sp> capito ? <sp> ce ne hai u+ / fra {<laugh> la prima 
          e la l'ultima una interna ?}


<sp>


p1F#402:  #<G#401> quindi praticamente da+# <P> <unclear> come faccio ad averne una se sono
          otto ? scusami


<lp>


p2G#403:  #<F#404> come ?#


p1F#404:  #<G#403> io# ce li ho tutte praticamente a -U-


<lp>


p2G#405:  <ah> ! ce li hai ad -U-


<sp>


p1F#406:  <eh> <breath>


p2G#407:  <mh> vabb� {<laugh> comunque} tu considera che devi andare con una linea
          quasi dritta #<F#408> tratteggiata al centro del foglio#


p1F#408:  #<G#407> <unclear> ascoltami Cinzia ascoltami# <inspiration> #<G#409> allora# 
          tra la prima e l'#<G#409> ultima#


p2G#409:  #<F#408> s�# <P> #<F#408> s�#


p1F#410:  giusto ? se i+ <vocal> io le<ee> faccio praticamente <sp> mi bastano due tratteggi 
          per legare la prima all'ultima


p2G#411:  s�


p1F#412:  <inspiration> si crea un vuoto all'interno ?


p2G#413:  no c'� una stella in mezzo a queste due


p1F#414:  no io non ho #<G#415> niente#


p2G#415:  #<F#414> vabb�# comunque tu congiungi queste #<F#416> due<ee> e# basta {<laugh>
          con due tratti} e poi vai poco poco di un centimetro gi� e t+ al<ll> / poco 
          sotto il centro del foglio c'� l'arrivo <inspiration> <mh>


p1F#416:  #<G#415> <mh> okay# <P> <tongue-click> io ho l'impressione #<G#417> che<ee> 
          la tua indicazione *dovrevva essere questa#

 
p2G#417:  #<F#416> {<laugh> che abbiamo sbagliato tutto} <inspiration> vabb� poi 
          #<F#418> lo vedremo#


p1F#418:  #<G#417> per�# <inspiration> okay allora quindi dopo che ho unito le due stelle 
          la prima e l'ultima devo scendere verso<oo> #<G#419> il basso ?#


p2G#419:  #<F#418> di un centimetro# s� solo di un centimetro sempre dritto {[whispering]
          ma<aa> #<F#420> solo di un centime+}#


p1F#420:  #<G#419> <tongue-click> sempre dritto# verso il basso


p2G#421:  s� s� #<F#422> ma solo di# un centimetro <sp> ti devi fermare quasi davanti 
          alle #<F#422> stelle#


p1F#422:  #<G#421> okay# <P> #<G#421> direzione# verso sinistra del foglio


p2G#423:  <eeh> no direzione proprio centrale <laugh>


p1F#424:  proprio #<G#425> centrale ?#


p2G#425:  #<F#424> {<laugh> centrale# s�} <inspiration> <NOISE> centrale s� non c'� 
          equidistanza <lp> #<F#426> <inspiration>#


p1F#426:  #<G#425> okay#
