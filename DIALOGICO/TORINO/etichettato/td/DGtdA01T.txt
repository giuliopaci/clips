TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            01
Ntr:
Nls:
REG:            T
 
 
SPEAKERS_inf.

INp1: 		V. M., M, 23, Torino, spontaneo, fluente
INp2: 		C. D., F, 18, Torino, spontaneo, fluente
 
 
RECORDING_inf.

TYP: 		DAT
LOC: 		Torino/abitazione
DAT: 		04/01/01
DUR: 		8.26,591
CON: 		buone


TRANSCRIPTION_inf.

DAT: 		10/09/01
CMT: 
Nst: 		291




p1#1:  -Pi uno Vittorio


p2#2:  -Pi due Carola
 
 
p1#3:  allora hai<ii> un omino sulla panchina che guarda<aa> tipo un televisorino
 
 
p2#4:  #<p1#5> s�#


p1#5:  #<p2#4> che# / che al guinzaglio c'ha un cane


p2#6:  <tongue-click> s�


p1#7:  <mh> poi su hai un<nn> signore su un cavallo <laugh> <sp> #<p2#8> <inspiration> c'hai una statua#
 

p2#8:  #<p1#7> <inspiration> s� ma aspetta aspetta# partiamo <sp> da un #<p1#9> particolare allora il televisore <inspiration>#


p1#9:  #<p2#8> <tongue-click> <inspiration> s�#


p2#10:  il televisore / c'� il cane che sorride col naso bianco ?


p1#11:  no , non c+ / <sp> <ah> s� c'ha il naso bianco s�


p2#12:  okay ci sono <sp> quattro tasti tutti neri sotto i quattro #<p1#13> tasti ?#


p1#13:  #<p2#12> s�<ii>#


p2#14:  l'omino schiaccia col dito l+ / <sp> il<ll> <sp> secondo tasto #<p1#15> partendo da destra ?#


p1#15:  #<p2#14> s�# <sp> #<p2#16> s�#


p2#16:  #<p1#15> e ci# son tre tasti bianchi ?


p1#17:  s� uno grosso e due #<p2#18> piccoli#


p2#18:  #<p1#17> okay#


p1#19:  okay poi l'antenna ? #<p2#20> <inspiration>#


p2#20:  #<p1#19> antenna# #<p1#21> �#


p1#21:  #<p2#20> � divisa# in tre pezzi ?


p2#22:  s� <sp> <tongue-click> #<p1#23> con un puntino ? <NOISE>#


p1#23:  #<p2#22> <unclear> c'� <sp> s� perfetto <sp> e<ee> <inspiration> questo televisore qua# � aperto tipo libro ?    


p2#24:  s�<ii> #<p1#25> <breath>#


p1#25:  #<p2#24> okay#


p2#26:  {<NOISE> okay}


p1#27:  allora passiamo al tipo


p2#28:  all'<ll>omino ?


p1#29:  #<p2#30> s�#


p2#30:  #<p1#29> che# guarda 'sta #<p1#31> cosa ? s�#


p1#31:  #<p2#30> s� s� <sp> allora# <lp> <eeh> dunque ha tre dita ? <lp> #<p2#32> che tengono<oo> il<ll> / dunque ce n'� uno 

	che schiaccia il pulsante#


p2#32:  #<p1#31> <vocal> <lp> <laugh> <sp># <inspiration> #<p1#33> {<laughing> s� e poi ci son tre dita}#


p1#33:  #<p2#32> okay# fantasti+ #<p2#34> <inspiration> po+ /#


p2#34:  #<p1#33> <inspiration> poi c'�# tipo la camicia <breath> <sp> e la giacca


p1#35:  <breath>


p2#36: <unclear> il polsino ?


p1#37:  s�<ii> 


p2#38:  e poi la gia+ <vocal> #<p1#39> la manica della giacca#


p1#39:  #<p2#38> s� s� s� <inspiration># e il #<p2#40> mento ce l'ha# /


p2#40:  #<p1#39> tutto# #<p1#41> bianco#

 
p1#41:  #<p2#40> il mento# ce l'ha pronunciato ?

 
p2#42:  <NOISE> parecchio
 
 
p1#43:  il naso anche ?
 
 
p2#44:  s�
 
 
p1#45:  ha uno scalino dietro nella testa ?
 
 
p2#46:  s�<ii>
 
 
p1#47:  l'orecchio � abbastanza grosso ?
 
 
p2#48:  <mhmh> 
 
 
p1#49:  capelli / #<p2#50> capelli<ii> ?#
 
 
p2#50:  #<p1#49> colorati# di nero
 
 
p1#51:  s� <sp> <NOISE> <inspiration> poi pantaloni ? <sp> ci sono ? #<p2#52> s�#
 

p2#52:  #<p1#51> s�# <sp> hai due righe ? <sp> su uno dei due pantaloni ?
 
 
p1#53:  quelli verso la / la panca ?
 
 
p2#54:  s�
 
 
p1#55:  s�
 
 
p2#56:  <mhmh> <laugh> #<p1#57> <inspiration>#
 
 
p1#57:  #<p2#56> poi c'� / le# scarpe hanno<oo> <sp> <tongue-click> tre lacci ?
 
 
p2#58:  s� <lp> tre lacci per� poi dopo c'� ancora una #<p1#59> righetta ?#
 
 
p1#59:  #<p2#58> s� in# mezzo , s� 
 
 
p2#60:  non in mezzo
 
 
p1#61:  come non in #<p2#62> mezzo ?#
 
 
p2#62:  #<p1#61> c'�# la righetta in mezzo
 
 
p1#63:  s�<ii>
 
 
p2#64:  tre pallini #<p1#65> <sp> e poi ancora# una righetta
 
 
p1#65:  #<p2#64> s� <lp># s� <sp> s�
 
 
p2#66:  <mhmh> <sp> <tongue-click> <sp> il #<p1#67> cane ?#
 
 
p1#67:  #<p2#66> poi il# guinzaglio / c'� il guinzaglio del cane ?
 
 
p2#68:  il guinzaglio<oo> / <sp> s� , per� #<p1#69> non � collegato direttamente alla coda#
 
 
p1#69:  #<p2#68> s� <NOISE> <sp> s� anche# a #<p2#70> me#              
 
 
p2#70:  #<p1#69> <mh># <tongue-click> va #<p1#71> bene#
 
 
p1#71:  #<p2#70> il# cane<ee> un po' sorride ?
 
 
p2#72:  s�
 
 
p1#73:  {<laughing> okay} <inspiration> #<p2#74> ci sono<oo># /
 
 
p2#74:  #<p1#73> ha la luna# nel naso ?
 
 
p1#75:  s�<ii>
 
 
p2#76:  ha le orecchie a punta ?
 
 
p1#77:  s� 
 
 
p2#78:  il naso ?
 
 
p1#79:  s� <sp> tondo <laugh> 
 
 
p2#80:  vabb� #<p1#81> <laugh>#
 
 
p1#81:  #<p2#80> ci sono# sette #<p2#82> puntini<ii>#  all'altezza dei baffi
 
 
p2#82:  #<p1#81> s�#
 

p1#83: <inspiration> #<p2#84> la coda � a# punta ?
 
 
p2#84:  #<p1#83> <tongue-click> <lp># <lp> <NOISE> s�
 
 
p1#85:  non tanto lunga ?
 
 
p2#86:  no <sp> fa un po' il segno della Nike 
 
 
p1#87:  s� , <unclear> avere un po' di #<p2#88> fantasia#
 
 
p2#88:  #<p1#87> vabb�# i+ <breath> <sp> #<p1#89> tu <inspiration> <sp> poi n+#
 
 
p1#89:  #<p2#88> <inspiration> poi alla# destra del cane<ee> ci sono<oo> <sp> sette pun+ / <sp> sette puntini ? #<p2#90> <breath>#
 
 
p2#90:  #<p1#89> s�# <breath> okay la macchina
 
 
p1#91:  la macchina / la macchina <lp> dunque <breath>
 
 
p2#92:  allora ha le ruote / le {<NOISE> tue} ruote sono colorate o no ?
 
 
p1#93:  no <breath>
 
 
p2#94:  {<NOISE> no <sp> nemmeno le #<p1#95> mie#}
 
 
p1#95:  #<p2#94> il parafango<oo>#
 
 
p2#96:  ha sopra una righetta
 
 
p1#97:  s�<ii>
 
 
p2#98:  i tre fari <NOISE> che non #<p1#99> sono tre e vabb�#
 
 
p1#99:  #<p2#98> s� <sp> due fari# e una cosa #<p2#100> centrale <inspiration>#
 
 
p2#100:  #<p1#99> <mh> <laugh>#
 
 
p1#101:  #<p2#102> poi hai# /
 
 
p2#102:  <inspiration> #<p1#101> alla destra# c'� una riga con sotto un rettangolino nero #<p1#103> scuro#
 
 
p1#103:  #<p2#102> s�<ii># <breath> s�
   
 
p2#104:  lo #<p1#105> specchietto#
 
 
p1#105:  #<p2#104> s�# <sp> la porta � segnata ?
 
 
p2#106:  s�
 
 
p1#107:  hai il retrovisore<ee> / <sp> hai lo specchio retrovisore interno ?
 
 
p2#108:  no
 
 
p1#109:  in alto ? 
 
 
p2#110:  no
 
 
p1#111:  okay allora segnalo <lp> lo segnalo anch'io ? <sp> <boh>
 
 
p2#112:  {<NOISE> no <sp> tu ce l'hai<ii> ?} 
 
 
p1#113:  s� io ce l'ho #<p2#114> <breath>#
 
 
p2#114:  #<p1#113> e allora#
 
 
p1#115:  <inspiration> poi sulla ruota hai un cerchio interno ?
 
 
p2#116:  <tongue-click> ho due / <sp> <tongue-click> ho la ruota e poi il cerchio #<p1#117> dentro#
 
 
p1#117:  #<p2#116> okay <breath># <tongue-click> allora alla macchina siamo a #<p2#118> posto#
 
 
p2#118:  #<p1#117> aspetta# dietro quella ruota l� c'� il s+ / <sp> ci sono altre<ee> / <sp> c'� l'altra #<p1#119> ruota ?#
 
 
p1#119:  #<p2#118> s�# 
 
 
p2#120:  e c'� anche il simbolo del / <sp> del posteriore della #<p1#121> ruota ? , {[whispering] s�}#
 
 
p1#121:  #<p2#120> s�#
 
 
p2#122:  <inspiration> okay <inspiration> <eeh> adesso guardiamo la / <sp> #<p1#123> il# /
 
 
p1#123:  la #<p2#122> casa ?# <breath> 
 
 
p2#124:  no
 
 
p1#125:  non hai la casa ?
 
 
p2#126:  s� che {[laughing] ce #<p1#127> l'ho}# 
 
 
p1#127:  #<p2#126> <ah> <laugh>#
 
 
p2#128:  <laugh> #<p1#129> <NOISE>#
 
 
p1#129:  #<p2#128> <unclear> guardare ? <breath>#
 
 
p2#130:  {[laughing] la #<p1#131> statua} <NOISE>#
 
 
p1#131:  #<p2#130> la statua#
 
 
p2#132:  #<p1#133> allora partendo dal basso#
 
 
p1#133:  #<p2#132> partiamo da<aa> <sp># l'aiuola ?
 
 
p2#134:  <tongue-click> c'� l'aiu+ / <sp> c'� lo scalino <sp> #<p1#135> poi tutte le righette dei <unclear> finti 
	fiorellini#
 
 
p1#135:  #<p2#134> s� <lp> <NOISE> <sp> s� <sp> s� s�#
 
 
p2#136:  <NOISE> poi c'� un trapezio
 
 
p1#137:  s�<ii> <breath>
 
 
p2#138:  poi c'� un rettangolo con dentro un altro<oo> <sp> #<p1#139> coso#
 
 
p1#139:  #<p2#138> s� <breath>#
 
 
p2#140:  <tongue-click> con dentro un'ombra strana che #<p1#141> penso# /
 
 
p1#141:  #<p2#140> no# <sp> <vocal> non ce l'ho <breath>
 
 
p2#142:  ma l'ombra #<p1#143> forse � la foto+# /
 
 
p1#143:  #<p2#142> un'ombra strana <breath>#
 
 
p2#144:  #<p1#145> la fotocopia#
 
 
p1#145:  #<p2#144> forse � la fotocopia# <lp> #<p2#146> <boh>#
 
 
p2#146:  #<p1#145> non lo# so
 
 
p1#147:  <vocal> o hai bevuto #<p2#148> <unclear> <inspiration> <laugh>#
 
 
p2#148:  #<p1#147> queste ombre qua ci sono anche nella casa# <sp> #<p1#149> nella pagina del libro#
 
 
p1#149:  #<p2#148> s� allora / no allora nie+ / <sp> allora# niente
 
 
p2#150:  allora ce l'hai anche tu ?
 
 
p1#151:  s�
 
 
p2#152:  okay poi c'� un #<p1#153> rettangolo sottile#
 
 
p1#153:  #<p2#152> <unclear> <sp> s�<ii># s�<ii>
 
 
p2#154:  lo zoccolo del cavallo della gamba<aa>
 
 
p1#155:  #<p2#156> s�#
 
 
p2#156:  #<p1#155> che# #<p1#157> poggia#
 
 
p1#157:  #<p2#156> �# dritta la #<p2#158> gamba del cavallo ?#
 
 
p2#158:  #<p1#157> s�<ii>#
 
 
p1#159:  poi c'� l'altra quella #<p2#160> storta<aa>#
 
 
p2#160:  #<p1#159> s�<ii>#
 
 
p1#161:  okay #<p2#162> <inspiration>#
 
 
p2#162:  #<p1#161> l'omino ?#
 
 
p1#163:  e<ee> l'omino ha una spada sguainata in #<p2#164> alto#
 
 
p2#164:  #<p1#163> esatto# <sp> s� poi <sp> il tuo ha occhi, bocca
 
 
p1#165:  #<p2#166> no, non ha niente#
 
 
p2#166:  #<p1#165> {[whispering] <unclear>}# niente nemmeno il mio <inspiration> #<p1#167> il# /
 
 
p1#167:  <inspiration> #<p2#166> poi hai# / il cavallo ha le orecchie a punta ?
 
 
p2#168:  s�
 
 
p1#169:  il muso<oo>
 
 
p2#170:  con la bocca con la righetta 
 
 
p1#171:  s�
 
 
p2#172:  il naso <sp> un po' a punta #<p1#173> all'ins�# 
 
 
p1#173:  #<p2#172> s�#
 
 
p2#174:  #<p1#175> e non ho nient'altro di disegnato#
 
 
p1#175:  #<p2#174> <vocal> <sp> no non ha# nessun occhio c+ / <sp> hai la sella disegnata ? <sp> stilizzata diciamo
 
 
p2#176:  s� <sp> pu� {<NOISE> darsi <sp> o � la mano o � la #<p1#177> sella <lp> ma s� quella roba l�}#
 
 
p1#177:  <inspiration> #<p2#176> <lp> s� c'� la mano la sella tutto insieme <sp> e poi sotto# allo stivale del<ll> / del tipo 
	hai<ii> <NOISE> le / la continuazione della sella ?
 
 
p2#178:  #<p1#179> s�#
 
 
p1#179:  #<p2#178> <mh># l� la<aa> <sp> #<p2#180> okay#
 
 
p2#180:  #<p1#179> le due# righette ?
 
 
p1#181:  s�
  

p2#182:  <inspiration> e poi <sp> dove c'� lo stivale del / dell'omino c'� una riga <sp> storta ?

 
p1#183:  <breath> <lp> non c'� nessuna riga #<p2#184> storta#
 
 
p2#184:  #<p1#183> ecco# falla <lp> dove #<p1#185> c'�<ee> lo stivale#
 
 
p1#185:  #<p2#184> dove ? <sp># <eh>
 
 
p2#186:  <inspiration> subito<oo> <mmh> <sp> sopra<aa> <sp> la punta pi� in alto della scarpa tra il piede e la scarpa <sp> se 
	tu ti sposti a #<p1#187> destra#
 
 
p1#187:  #<p2#186> <laugh># tra il piede e la scarpa <breath>
 
 
p2#188:  s�<ii> dove c'� la fine della gamba <sp> e {[laughing] l'inizio del disegno della s+ / <sp> del piede}
 
 
p1#189:  #<p2#190> s�#
 
 
p2#190:  #<p1#189> <inspiration># se tu ti sposti a destra #<p1#191> ci dovrebbe essere una specie di ombra dall'altra parte 
	
	no ? <sp> di righetta <sp> che sarebbe la staffa#
 
 
p1#191:  #<p2#190> s� <lp> <NOISE> <lp># <NOISE> <sp> s� 
 
 
p2#192:  {<NOISE> ecco}
 
 
p1#193:  non riesco a capire
 
 
p2#194:  #<p1#195> <laugh> <inspiration> <NOISE>#
 
 
p1#195:  #<p2#194> no c'� un po'<oo> tipo specie di tallone su questa# scarpa 
 
 
p2#196:  {<NOISE> s�}
 
 
p1#197:  <ah> ! #<p2#198> okay c'� allora#
 
 
p2#198:  #<p1#197> vabb� <sp> sopra#
 
 
p1#199:  sopra ? <sp> s� c'� una stanghetta che copre, nera p+ <sp> una riga nera ?
 
 
p2#200:  #<p1#201> s�#
 
 
p1#201:  #<p2#200> la# continuazione della<aa> / #<p2#202> della s+# /
 
 
p2#202: #<p1#201> esatto# <sp> allora di l� a destra ci dovrebbe essere una riga 
 
 
p1#203:  s� la continuazione della sella
 
 
p2#204:  no
 
 
p1#205:  no
 
 
p2#206:  no <sp> parallelo <sp> alla gamba <sp> all<ll>
 
 
p1#207:  #<p2#208> parallelo alla ga+# /

 
p2#208:  #<p2#207> a dove la gamba gira# perch� � seduto l'omino ci #<p1#209> dovrebbe essere <sp> a met� <sp> una 
	
	riga#
 
 
p1#209:  #<p2#208> <inspiration> <mh> <breath>#
 
 
p2#210:  tu falla m+ <sp> #<p1#211> un po' a muzzo [dialect]#     
 
 
p1#211:  #<p2#210> <boh> e segno# io non ho #<p2#212> capito comunque#
 
 
p2#212:  #<p1#211> <NOISE> e tu {[laughing] segna# l+ / #<p1#213> segna l� una ga+ / <sp> una riga}#
 
 
p1#213:  #<p2#212> � tipo cos� <breath> {[laughing] va# bene s�}
 
 
p2#214:  {<NOISE> okay <sp> andiamo a #<p1#215> vedere la casetta ?}#
 
 
p1#215:  #<p2#214> <boh> <sp> <tongue-click> <sp> <eh># #<p2#216> s�#
 
 
p2#216:  #<p1#215> {<NOISE> il tetto ?}#
 
 
p1#217:  #<p2#218> s�#
 
 
p2#218:  #<p1#217> <NOISE># <tongue-click> #<p1#219> allora ci sono#
 
 
p1#219:  #<p2#218> <vocal> <sp> ha tante righe ?#
 
 
p2#220:  <NOISE> o dobbiam {<NOISE> contarle} #<p1#221> <NOISE>#
 
 
p1#221:  #<p2#220> uno due tre# quattro cinque sei sette 
 
 
p2#222:  s� <sp> sette #<p1#223> <breath> c'hai un'antenna e due caminetti ?#
 
 
p1#223:  #<p2#222> <inspiration> poi so+ / <lp># s�<ii> <breath> <sp> l'antenna � fatta<aa>
 
 
p2#224:  con #<p1#225> una<aa> ri+# /
 
 
p1#225:  #<p2#224> tipo da albero# *ndi Natale ?
 
 
p2#226:  <vocal>
 
 
p1#227:  ha due stanghette
 
 
p2#228:  s� <sp> una riga orizzontale e due verticali
 
 
p1#229:  s� <breath> #<p2#230> okay <sp> <NOISE>#
 
 
p2#230:  #<p1#229> invece i# caminetti sono a casetta ?
 
 
p1#231:  s�
 
 
p2#232:  #<G233> poi quante f+# /
 
 
p1#233:  #<F232> u+ / <sp> alte# uguali ? 
 
 
p2#234:  no, uno � un po' pi� piccolo e uno � un po' #<p1#235> pi� grande#

 
p1#235:  #<p2#234> s� okay#
 
 
p2#236:  <NOISE> #<p1#237> okay#
 
 
p1#237:  #<p2#236> finestre ?#
 
 
p2#238:  <inspiration> ce n'�<ee> <tongue-click> *cinqua <sp> due <vocal> e una
 
 
p1#239:  okay <breath> <lp> <NOISE> <sp> tutte / cio� messe<ee> <sp> tre verso<oo> la statua <sp> due <sp> e poi tre<ee>
 

p2#240:  #<p1#241> orizzontali#

 
p1#241:  #<p2#240> okay# <NOISE> 
 
 
p2#242:  {<NOISE> l'albero}
 
 
p1#243:  s�
 
 
p2#244:  <tongue-click> allora #<p1#245> l'albero#
 
 
p1#245:  #<p2#244> <unclear># <sp> <eh>
 
 
p2#246:  sembra<aa> <lp> <tongue-click> #<p1#247> cu+# /
 
 
p1#247:  #<p2#246> ha / ha# / dunque ha il tronco un po' *stuerto #<p2#248> <laugh>#  
 
 
p2#248:  #<p1#247> s�<ii># 
 
 
p1#249:  #<p2#250> okay# 

 
p2#250:  #<p1#249> <vocal> <tongue-click> la# riga � d+ / <sp> pi� a #<p1#251> sinistra un po' storta#
 
 
p1#251:  #<p2#250> <vocal> s� s� s�#

 
p2#252:  <inspiration> e l'altra � quasi #<p1#253> dritta semp+ <unclear>#
 
 
p1#253:  #<p2#252> <tongue-click> poi ci# sono #<p2#254> tipo<oo>#
 
 
p2#254:  #<p1#253> <tongue-click># tre folte chiome
 
 
p1#255:  una due <sp> s� s�
 
 
p2#256:  <tongue-click> e<ee> quella pi� v+ / <vocal> pi� a sinistra ha un pezzo ugu+ / <sp> quasi  

 
p1#257:  che sembra una mano ?
 
 
p2#258:  verticale #<p1#259> e poi una# specie di pollicione
 
 
p1#259:  #<p2#258> s� <lp># s�  
 
 
p2#260:  <vocal> <sp> e poi ci sono le altre due <sp> quasi parallele
 
 
p1#261:  s�
 
 
p2#262:  e poi ?

 
p1#263:  poi basta 
 
 
p2#264:  <ah> no l+ / <sp> la gamba della panca
 
 
p1#265:  s�<ii> 
 
 
p2#266:  e<ee> <sp> com'� ?
 
 
p1#267:  fatta tipo<oo> a Elle
 
 
p2#268:  s� <sp> e invece la panca � scura o chiara da te ?
 
 
p1#269:  dunque dietro dove si appoggia<aa> � a righe
 
 
p2#270:  s� <sp> e dove � #<p1#271> seduto ?#
 
 
p1#271:  #<p2#270> e<ee># sotto � nero <breath>
 
 
p2#272:  nero anche il mio <lp> <inspiration> e tra<aa> la gamba della panca scura #<p1#273> <sp> e il guinzaglio# c'� una riga 
	orizzontale ?
 
 
p1#273:  #<p2#272> <eh> <lp># c'� una riga orizzontale
 
 
p2#274:  <eh> <sp> non c'� <sp> tu falla <lp> subito prima /  {<NOISE> <sp> de+ / <sp>} a+ / subito sopra il sedere del / <sp> del #<p1#275> 
	cane#
 
 
p1#275:  #<p2#274> <NOISE># s�
 
 
p2#276:  c'� una riga orizzontale che parte dalla gamba del / <sp> <vocal> della panca scura
 
 
p1#277: #<p2#278> s�#
 
 
p2#278:  #<p1#277> e# arriva al guinzaglio
 
 
p1#279:  ma cos'� il pantalone ?
 
 
p2#280:  {[laughing] s�<ii>} #<p1#281> <laugh>#
 
 
p1#281:  #<p2#280> {[laughing] e allora} <laugh># porca miseria <laugh> #<p2#282> e c'�<ee>#
 
 
p2#282:  #<p1#281> <vocal> <lp># #<G283> <laugh> <NOISE>#
 
 
p1#283:  #<p2#282> c'� anche disegnato un pezzo della<aa> / <sp> della scarpa del tizio ?#
 
 
p2#284:  dove <breath> ?
 
 
p1#285:  � a fianco al sedere del cane <breath> <NOISE>
 
 
p2#286:  che <vocal> / che spunta <sp> da dietro il sedere del cane s�
 
 
p1#287:  {<NOISE> okay}
 
 
p2#288:  #<p1#289> {<NOISE> <unclear>}#
 
 
p1#289:  #<p2#288> e poi<ii># <breath> basta <breath> abbiamo detto tutto <breath>
 
 
p2#290:  {<NOISE> <boh> <sp> � finito il tempo}
 
 
p1#291:  s�
 
