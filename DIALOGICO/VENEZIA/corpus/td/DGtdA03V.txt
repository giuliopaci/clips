TEXT_inf.

MAT:            td
MAP:            A
TRM:
Ndl:            03
Ntr:
Nls:
REG:            V


SPEAKERS_inf.

INp1: G. S., F, 19, Venezia, spontaneo, fluente 
INp2: E. S., M, 21, VENEZIA, spontaneo, fluente 


RECORDING_inf.

TYP: DAT
LOC: VENEZIA/CASA
DAT: 08/01/01
DUR: 10.09,323
CON: BUONE


TRANSCRIPTION_inf.

DAT: 21/05/01
CMT:
Nst: 286



p1#1:  vado ? <sp> Gaia Pi uno


p2#2:  Emanuele Pi due


p1#3:  partiamo dall'alto in<nn> / #<p2#4> sinistra#


p2#4:  #<p1#3> da+ / <tongue click> in alto a sinistra #<p1#5> <eh>#


p1#5:  #<p2#4> dai# vai


p2#6:  c'� l'albero ?


p1#7:  si


p2#8:  <tongue click> adesso <sp> #<p1#9> <mh>#


p1#9:  #<p2#8> partiamo# da quelle pi� / non so dalla persona che forse ha pi� 
       #<p2#10> robe#


p2#10:  #<p1#9> o dalla# casa se [dialect] #<p1#11> molto pi� semplice#


p1#11:  #<p2#10> e va be# 


p2#12:  *quanti finestre hai ?


p1#13:  una due tre quattro e cinque nella prima linea


p2#14:  <eh>


p1#15:  una due <sp> sotto


p2#16:  <eh>


p1#17:  per� sotto verso destra


p2#18:  <tongue click> s+ / <eh>


p1#19:  e una <sp> sotto sempre verso destra


p2#20:  <unclear> ne hai<ii> <sp> guardando dall'alto verso il basso tre due #<p1#21> 
        e una una una# <inspiration>


p1#21:  #<p2#20> esatto una una una # <sp> due camini


p2#22:  si<ii> #<p1#23> attaccati#


p1#23:  #<p2#22> fatti / fatti# tipo<oo> con un<nn> / un rettangolo e un triangolo 
        sopra tutti e due


p2#24:  <tongue click> si


p1#25:  poi c'� l'antenna 


p2#26:  <tongue click> <eh> come � fatta #<p1#27> l'antenna# ?


p1#27:  #<p2#26> � una# d+ / una<aa> / una stanga in piedi


p2#28:  si<ii>


p1#29:  e due di traverso


p2#30:  si <sp> #<p1#31> unclear <sp> uguale#


p1#31:  #<p2#30> <vocal> benissimo# insomma #<p2#32> <mh>#


p2#32:  #<p1#31> <laugh># #<p1#33> preciso <inspiration>#


p1#33:  #<p2#32> <inspiration> va be' <sp> diamo# / <sp> la persona


p2#34:  si <lp> <inspiration> 


p1#35:  <inspiration> allora c'ha lui / <sp> e raccontami te     


p2#36:  e la persona � / cominciamo dai capelli


p1#37:  <eh>


p2#38:  ha i capelli g+ / <sp> tipo<oo> <sp> e<ee>


p1#39:  tipo una elle 


p2#40:  <eh> si #<p1#41> visto<oo>#


p1#41:  #<p2#40> <mh> <sp># l'orecchio grande


p2#42:  l'orecchio grande con un altro tipo orecchio piccolo dentro #<p1#43> <breath> 
        <vocal>#


p1#43:  #<p2#42> si<ii> <sp> la sopracciglia# 


p2#44:  la sopracciglio #<p1#45> e<ee> <breath>#


p1#45:  #<p2#44> l'occhio#


p2#46:  e ma il sopracciglio � una elle anche #<p1#47> quello rovescia [dialect] <sp>  
        l'occhio � un puntino <inspiration> <sp> il naso#


p1#47:  #<p2#46> si <sp> si <sp> si <sp> il naso# � una u


p2#48:  <eeh> inspiration


p1#49:  dopo scende c'� la bocca che � seria #<p2#50> <sp> cio� una linea dritta#


p2#50:  #<p1#49> <mhmh> <lp> <tongue click> si


p1#51:  <inspiration> <lip click> e dopo scende un altro po' e c'� un mento che � 
        un'altra specie di u


p2#52:  si


p1#53:  e invece dietro la nuca


p2#54:  ha uno scalino


p1#55:  ha lo scalino <NOISE> #<p2#56> <sp> dopo <lp> dopo scendi c'ha la giacca 
        <sp> che e tipo un<nn> <sp> va be' insomma una giacca <tongue-click> <sp> 
        {<laughing> dopo dopo} nell'angolo / nella piega del braccio# 


p2#56:  #<p1#55> <inspiration> <mh> <tongue click> {<NOISE> dop+ <breath> <sp>
        <breath> <tongue click># c'� un<nn>} / #<p1#57> un / son due ?#


p1#57:  #<p2#56> c'� <sp># c'ha due segni


p2#58:  <eh>


p1#59:  uno che fa #<p2#60> una u verso l'altro <sp> verso l'alto#


p2#60:  #<p1#59> <inspiration> <eh> e una / e una linetta# un #<p1#61> po' curva#


p1#61:  #<p2#60> che scende


p2#62:  <eh>


p1#63:  dopo il telecomando c'ha l'antenna con tre pezzi e #<p2#64> in alto un puntino#


p2#64:  #<p1#63> de+ / <sp># <eh> #<p1#65> s�  e dopo c'hai anche una linea <sp> che va dal 
        telecomando verso sinistra# 


p1#65:  #<p2#64> dop+ / <sp> dove ? <sp># tipo una radiazone


p2#66:  no <sp> tipo che � aperto come liblo


p1#67:  e s�  che ha tipo un libro 'sta #<p2#68> roba#


p2#68:  #<p1#67> <eh>#


p1#69:  <eh>


p2#70:  e c'ha il cane dentro #<p1#71> <sp> almeno pare un# cane


p1#71:  #<p2#70> e il cane dentro# <lp> <eh> <sp> il cane com+ / come ha il naso ?


p2#72:  il naso � q+ quasi un cerchio #<p1#73> <breath> e un c+ /#


p1#73:  #<p2#72> � grande ?#


p2#74:  s�  


p1#75:  <eh> <sp> dopo c'� uno due tre quattro <tongue click> otto bottono neri <sp> #<p2#76> 
        messi in due file <sp> dopo un bottone grande bianco sotto cio� pi� grande degli altri
        e due piccoli bianchi sotto <sp> <inspiration> dopo c'� il dito sopra il telecomando 
        <sp> il pollice#


p2#76:  #<p1#75> s�  <inspiration> s�  <sp> <tongue click> e due pi� g+ / <eh> <inspiration> <ah>
        <inspiratio> la {<NOISE> <sp> s� } �# dritto ?


p1#77:  s�  <sp> dopo c'ha la cosa la la camicia che viene fuori dalla giacca


p2#78:  la camicia #<p1#79> che viene fuori dalla giacca <inspiration> i+ / i+#


p1#79:  #<p2#78> <tongue click> <eh> una giacca e come un altro pezzetto# che s�  aggiunge anche alla 
        camicia <lp> ce l'ha o non ce l'ha ?


p2#80:  <tongue click> e sto cercando di capire da dove


p1#81:  dalla giacca <sp> nella ma+ / nel polso


p2#82:  s�  s�  #<p1#83> <eh> <inspiration># 


p1#83:  #<p2#82> <tongue click> <eh> e ha tre dita#


p2#84:  <mh> 


p1#85:  <inspiration> #<p2#86> dop+ /#


p2#86:  #<p1#85> il cane# dentro ha tutte e due le orecchie o una � mozzata #<p1#87> tipo#


p1#87:  #<p2#86> una �# mozzata #<p2#88> tipo# 


p2#88:  #<p1#87> <eh># <sp> #<p1#89> <unclear> <inspiration>#


p1#89:  #<p2#88> dopo dal braccio gli parte una corda# che va gi� no , che � il #<p2#90> 
        guinzaglio#


p2#90:  #<p1#89> s� #


p1#91:  per� non si vede il primo pezzo del guinzaglio cio� parte da sotto il bra+ / il braccio 
        #<p2#92> la mia#


p2#92:  #<p1#91> s� # 


p1#93:  e scende e fa una curva quasi a novanta gradi


p2#94:  quasi a novanta gradi ? 


p1#95:  s� , il guinzaglio


p2#96:  s�  #<p1#97> <sp> no no <breath>#


p1#97:  #<p2#96> <eh> e va verso il cane


p2#98:  <eh> il mio no <sp> eccolo qua


p1#99:  e dove va ?   


p2#100:  <eh> va sulla coda


p1#101:  no allora #<p2#102> no# 


p2#102:  #<p1#101> cio�# sul culo scu+ / s� va be'     


p1#103:  no il mio vers+ / va verso il collare


p2#104:  c'� l'ha proprio sul collare ?


p1#105:  e ?


p2#106:  c'� l'ha proprio attaccato al collare ?


p1#107:  no un po' dietro <sp> per� non sulla coda


p2#108:  e no io c'� l'ho proprio sul sedere #<p1#109> <inspiration> hai sbagliato#


p1#109:  #<p2#108> e va be' <sp> dopo il mio# collare non � chiuso sopra   


p2#110:  <tongue click> neanche questo


p1#111:  e allora<aa> / <sp> <inspiration> dopo anche lui c'ha tipo uno scalino un naso per 
         {<NOISE> fuori} il cane / #<p2#112> la testa del cane dietro c'ha le due orecchie a 
         {<NOISE> punta}#


p2#112:  #<p1#111> s�  <breath> <NOISE># di cui una �<ee> <tongue click>


p1#113:  di cui una � dietro


p2#114:  <ah>


p1#115:  poi va be' ha il naso non ha i baffi il mio cane


p2#116:  no c'ha i puntini per� 


p1#117:  c'ha i puntini


p2#118:  c'ha quattro puntini #<p1#119> e tre <sp> poi c'� la bocca che � una linea dritta poi col 
         / torna verso su#


p1#119:  #<p2#118> s�  <sp> s�  <sp> s�  <lp> col sorrisino# 


p2#120:  #<p1#121> <eh> <inspiration>#


p1#121:  #<p2#120> <eh> dopo c'ha la# / ha la zampa che ha solo due segni #<p2#122> di / di / di robi#


p2#122:  #<p1#121> <tongue click> s�  e sono# tutte e due piegate nello stesso senso


p1#123:  s� � come se sta <unclear> come<ee> / s�  accucciato 


p2#124:  <eh>


p1#125:  e ha la coda dritta 


p2#126:  ecco no io ce l'ho che va verso su


p1#127:  no la mia � dritta #<p2#128> dopo la# sedia del tipo


p2#128:  #<p1#127> no <sp># <mh>


p1#129:  il / la gamba gi�


p2#130:  <mh>


p1#131:  com'� ?


p2#132:  piegata


p1#133:  piegata dritta normale ?


p2#134:  piegata <sp> e dopo va tipo<oo> / vai sul b+ / cio�


p1#135:  la mia dalla sedia verso terra va gi� dritta 


p2#136:  {<NOISE> come sarebbe}


p1#137:  <NOISE> allora #<p2#138> c'� la sedia del tipo e dopo c'� la# gamba della sedia <sp> 
         giusto ?


p2#138:  #<p1#137> <tongue click> <mh># <lp> c'� la sedia


p1#139:  <eh> <sp> la panchina


p2#140:  <eh> la panchina c'� la gamba


p1#141:  e+ / della panchina


p2#142:  <eh> ec+ / e tu ti tira una una linea cio� si chiude la o anche c'ha / tipo la la 
         zampetta ?


p1#143:  ha anche la zampetta #<p2#144> <sp># alla fine {<NOISE> dopo l'uomo} ha le gambe 
         accavallate


p2#144:  #<p1#143> <eh> <sp> <inspiration># s� 


p1#145:  e la scarpa com'�<ee> ?


p2#146:  e la scarpa � <inspiration> <sp> allora <sp> <eeh> <NOISE> parte va be' c'� la 
         caviglia <noise>


p1#147:  e ? <sp> va verso su


p2#148:  poi va verso #<p1#149> su fa il cerc+# 


p1#149:  #<p2#148> allungata# ?


p2#150:  e ?


p1#151:  allungata #<p2#152> la scarpa#


p2#152:  #<p1#151> s�  <sp># #<p1#153> al ce+# 


p1#153:  #<p2#152> e ha una# due tre tre pezzi di spighetta ?


p2#154:  s� #<p1#155> con una linea# <sp> con una linea in mezzo 


p1#155:  #<p2#154> � al tacco <sp> �# al tacco


p2#156:  s� 


p1#157:  <unclear> <sp> #<p2#158> macchina# ? 


p2#158:  #<p1#157> allora la linea# in mezzo ai tre<ee> 


p1#159:  s�  c'� l'ho / c'�


p2#160:  <tongue click> e / e ma gira verso destra


p1#161:  s� va {<NOISE> be' <sp> la macchina} <lp> c'ha lo specchietto retrovisore ?


p2#162:  <tongue click> ? s� 


p1#163:  c'ha lo specchietto laterale ?


p2#164:  no non c'ha lo specchietto {<NOISE> retrovisore}


p1#165:  il retrovisore � quello in alto #<p2#166> per guardare <eh># 


p2#166:  #<p1#165> s�  no / non# c'� #<p1#167> l'ha#  


p1#167:  #<p2#66 non c'�# l'ha <sp> due fanali


p2#168:  due s� 


p1#169:  <tongue click> il parafa+ / come si chiama il paradobi qua


p2#170:  s�  c'� l'ha una linea ?


p1#171:  � una linea #<p2#172> sopra dopo# la statua <sp> ha la spada


p2#172:  #<sovr> <eh> <lp># s� 


p1#173:  ha il cappello il tipo / l'elmo ? 


p2#174:  #<p1#175> s� <ii>#


p1#175:  #<p2#174> �# fatto un po' come l'omino ?


p2#176:  s�  <sp> #<p1#177> uguale#


p1#177:  #<p2#176> e c'ha# la la / come si chiama d+ / la sella ?


p2#178:  la sella s� tutta<aa> /


p1#179:  c'ha la sella <sp> #<p2#180> e il cavallo# ha le orecchie a punta


p2#180:  #<p1#179> s� <lp># s�  <sp> c'ha gli occhi il cavallo ?


p1#181:  no 


p2#182:  no


p1#183:  dopo ha uno zoccolo alzato ?


p2#184:  s� 


p1#185:  s� 


p2#186:  e l'altro dritto #<p1#187> invece# com'� fatto al tronco ? ha tanti rami ?


p1#187:  #<p2#186> <tongue click> <sp># no


p2#188:  c'ha solo il tronco ?


p1#189:  c'ha il tronco 


p2#190:  #<p1#191> <eh> <sp> dopo cosa c'� <ah> i+# /


p1#191:  #<p2#190> e il tuo no tu hai i rami# ?


p2#192:  no non c'ho i rami #<p1#193> e ha tipo tre# / uno due <sp> tre pezzi cio�<ee> / tre 
         linee grandi che ne segnano le foglie ? 

   
p1#193:  #<p2#192> allora <lp># <tongue click> s� #<p2#194> <sp> uno due e tre#


p2#194:  <eh> #<p1#193> <sp> dopo il piedistallo# del+ / della statua


p1#195:  <mh>


p2#196:  � formato da un rettangolo stretto sotto lo zoccolo del #<p1#197> cavallo#


p1#197:  #<p2#196> s�# <sp> da un rettangolo #<p2#198> grande#


p2#198:  #<p1#197> grande# con #<p1#199> un<nn>#  


p1#199:  #<p2#198> un# #<p2#200> quadrato#


p2#200:  #<p1#199> un quadrato# in mezzo e dopo da un piedistallo che scende come<ee> / #<p1#201>
         come s� chiama trapezio# ?


p1#201:  #<p2#200> tipo trap+ / <breath> s�#


p2#202:  <boh> <sp> #<p1#203> do+# / 


p1#203:  #<p2#202> poi c'�# l'erba <sp> #<p2#204> c'� l'erba <sp> ti d+# /


p2#204:  #<p1#203> l'erba e l'aiuola# circonda+ / ci sono fiori nella tua erba ?


p1#205:  no


p2#206:  � circondata tipo da un'altra ri+ / da #<p1#207> un'altra<aa> / da un altro# /


p1#207:  #<p2#206> s� d+ / da# due linee tonde


p2#208:  <eh> e dopo cosa c'� da vedere <ah> c'� il / la porta+ come s� chiama / la la porta
         sulla tua macchina ?


p1#209:  <tongue click> s� c'� la riga che fa elle 


p2#210:  <eeh> <sp> c'� una riga in mezzo che #<p1#211> passa# ?


p1#211:  #<p2#210> s�<ii># <sp> #<p2#212> e c'� un quadratino#


p2#212:  #<p1#211> e la ruota come � fatta# con due cerchi o con uno ? <lp> cio� nel cerchio 
         grande che segna la ruota e quello in mezzo che segna<aa> /


p1#213:  #<p2#212> <eh> <sp> <tongue click> s�#


p2#214:  s�<ii> 


p1#215:  e l'altra ?


p2#216:  e l'altra non s� vede cio� � sempre due cerchi per� s� vedono appena


p1#217:  � interrotta ?


p2#218:  s� 


p1#219:  <eh> 


p2#220:  c'ha i due fari ?


p1#221:  s�


p2#222:  e dopo cosa c'� che non / #<p1#223> tra tra tra<aa># <sp> <vocal>


p1#223:  #<p2#222> <unclear> <lp># la / la panchi+ / come � fatta la #<p2#224> panchina#


p2#224:  #<p1#223> <eh># <sp> la panchina � come una panchina normale <inspiration> <sp> #<p1#225>
         e<ee>#


p1#225:  #<p2#224> � piegata # da una parte all'altra ? 


p2#226:  come piegata ? 


p1#227:  in alto


p2#228:  in alto lo schienale va ver+ / � un po' ripiegato


p1#229:  <eh> e c'ha tutte le linee 


p2#230:  e la parte davanti � in ombra ? 


p1#231:  e' in ombra


p2#232:  <boh> 


p1#233:  <mhmh> 


p2#234:  e dopo cosa c'� da dire a s� no / ma ripeti {<NOISE> l'antenna e il telecomando ma � 
         possibile} che l'omino non abbia niente


p1#235:  <laugh> 


p2#236:  <inspiration> la scarpa / la scarpa gi� dell'omino


p1#237:  <eh> 


p2#238:  co+ / ha qualcosa di particolare � dietro il cane o davanti al cane ?


p1#239:  � sopra il cane 


p2#240:  sopra il cane ? 


p1#241:  cio� � accavallata <sp> #<p2#242> io la vedo<oo># /


p2#242:  #<p1#241> <eh> no# quella dietro


p1#243:  quella dietro ?


p2#244:  <eh>


p1#245:  e dietro il #<p2#246> cane# 


p2#246:  #<p1#245> non s�# vede ?


p1#247:  no


p2#248:  e le p+ / la p+ / le pieghe del pantalone della<aa> <mh> della<aa> della gamba sotto


p1#249:  <mh> 


p2#250:  sono due ?


p1#251:  sono due #<p2#252> una pi�# lunga dell'altra


p2#252:  #<p1#251> <vocal> <sp># <eh> <sp> e dopo cos'� quest'omino / l'uomo non ha niente di 
         particolare ? non #<p1#253> so#


p1#253:  #<p2#252> ma# no <sp> tu il colle+ / il colletto 


p2#254:  <unclear> continua tipo come il tronco


p1#255:  <eh>


p2#256:  <eh> ? 


p1#257:  e se [dialect] uguale 


p2#258:  e dopo cosa c'� <sp> e la casa non ha niente di strano ?


p1#259:  <tongue click> il tetto come � fatto ?


p2#260:  il tetto � fatto come un trapezio con tutte le righe  


p1#261:  <eh> <sp> #<p2#262> e se [dialect] uguale#


p2#262:  #<p1#261> no ma dimmi# / ma l'antenna del telecomando tipo


p1#263:  <eh>


p2#264:  la mia � fatta a tre pezzi


p1#265:  <tongue click> <eh> <sp> uno � nero


p2#266:  va be' ma a parte i colori c'� i+ / <sp> ma forse magari c'� qualcosa messo al 
         contrario ? <sp> <NOISE> <sp> la macchina dove guarda ?


p1#267:  verso sinistra 


p2#268:  verso sinistra e il cane ? 


p1#269:  il cane ? <sp> verso #<p2#270> destra#


p2#270:  #<p1#269> <eh> <sp># verso destra <sp> #<p1#271> e la statua#


p1#271:  #<p2#270> il naso# <sp> verso sinistra 


p2#272:  verso sinistra il cavallo anche 


p1#273:  s�


p2#274:  e / e la panchina dell'omino dov'� a destra o a sinistra ?


p1#275:  <tongue click> cosa vuol dire ?


p2#276:  cio� dov'� la u+ / l'uomo nella parte destra o nella parte #<p1#277> sinistra# ?


p1#277:  #<p2#276> parte# sinistra


p2#278:  <eh> <NOISE> <sp> #<p1#279> <boh>#


p1#279:  #<p2#278> e il naso# del cane ?


p2#280:  il naso del cane � una palla tonda nera #<p1#281> con# dentro la linetta bianca


p1#281:  #<p2#280> <eh> <lp># ma la linetta bianca fatta come ?


p2#282:  che<ee> / a u verso il basso 


p1#283:  s� <sp> {<NOISE> <tongue click> � precisa} #<p2#284> <inspiration># e<ee>


p2#284:  #<p1#283> <mh> <lp># � tanto grande l'uomo ?


p1#285:  s�


p2#286:  occup+        
