TEXT_inf.

MAT:  LF
MAP:  A
TRM:
Ndl:  
Ntr:
Nls:  03
REG:  D


SPEAKERS_inf.

INp1:  M. G., F, 20, Trescore Balneario (BG)


RECORDING_inf.

TYP:  DAT
LOC:  Bergamo
DAT:  09/10/01
DUR:  2.15,503
CON:  buone


TRANSCRIPTION_inf.

DAT:  18/02/02
CMT:
Nst:  20


#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� attenta a scuola


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai <sp> cosa fare n� dove andare


#5:  ora basta <sp> non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  <tongue-click> la ragazza ch'� appena entrata non la conosco


#8:  <tongue-click> se mi lasci un'altra volta solo non ti parler� per tutta la
     vita


#9:  <tongue-click> quest'anno ho fatto un lavoro nuovo <sp> per� ho
     deciso di non cambiare pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  <tongue-click> Luisa fingeva di guardare da un'altra parte <sp> ma cercava
      il modo per farsi notare


#12:  <tongue-click> ho chiesto a Gianna di portare subito dell'acqua in un
      recipiente piccolo <sp> ma non � ancora arrivata


#13:  <inspiration> non bisogna credere che sia vero tutto quello che dice la
      gente <inspiration> tu non conosci ancora gli uomini non conosci il mondo


#14:  <tongue-click> chiamai il medico perch� avevo male agli occhi <sp> ma
      quando usc� mi accorsi di non {<laugh> sentire} neanche i rumori


#15:  <tongue-click> dopo tanto tempo non ricordo pi� dove ho messo quella bella
      foto <inspiration> ma se aspetti un po' <sp> la cerco <sp> e te la prendo


#16:  <inspiration> alla fine non sono riuscito a capire se Marco non voleva o
      non poteva venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit� <sp> ma tu non fargli vedere che pensi
      che sia un bugiardo


#18:  <tongue-click> questo tormento durer� ancora qualche ora forse un giorno
      <inspiration> poi tutto finir� e tu potrai tornare a casa <sp> nella tua 
      terra


#19:  Lucio era certo che sarebbe diventato una persona importante un uomo
      politico o magari un ministro <inspiration> aveva a cuore il bene della
      societ� <sp> rispettava la legge <inspiration> se teneva un discorso
      trovava le parole adatte ad ogni situazione <tongue-click> si sentiva
      proprio un buonitaliano


#20:  Stefano <sp> era un bel tipo <inspiration> gli piaceva farsi servire per
      primo e mangiava per due <sp> ma quando si trattava di pagare <sp>
      sembrava non vedere la mano che gli porgeva il conto <inspiration> il
      prezzo gli pareva sempre troppo alto <ispiration> eppure lavorava in
      un'azienda che dal punto di vista economico <inspiration> era la numero
      uno su scala nazionale
