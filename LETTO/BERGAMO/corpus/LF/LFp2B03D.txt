TEXT_inf.

MAT:		LF
MAP:		B
TRM:
Ndl:		
Ntr:
Nls:		03
REG:		D


SPEAKERS_inf.

INp2:		E. P., F, 20, Bergamo

 
RECORDING_inf.

TYP:		DAT
LOC:		Bergamo 
DAT:		29/10/01
DUR:		2.07,923
CON:		buone


TRANSCRIPTION_inf.

DAT:		18/12/02	
CMT:
Nst:		20


#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� attenta a scuola


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai cosa fare n� dove andare


#5:  ora basta ! <sp> non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  la ragazza che � appena entrata non la conosco


#8:  se mi lasci un'altra volta solo <sp> non ti parler� per tutta la vita


#9:  quest'anno ho fatto un lavoro nuovo <inspiration> per� ho deciso di non 
     cambiare pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano?


#11:  <inspiration> Luisa fingeva di guardare da un'altra parte <sp> ma cercava 
      il modo per farsi notare


#12:  <inspiration> ho chiesto a Gianna di portare subito dell'acqua in un 
      recipiente piccolo <sp> ma non � ancora arrivata


#13:  non bisogna credere che sia ve+ / <eeh> che sia vero tutto quello che dice 
      la gente <inspiration> tu non conosci ancora gli uomini <sp> non lo / non 
      conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi ma quando usc� mi accorsi 
      di non sentire neanche i rumori


#15:  <inspiration> dopo tanto tempo non ricordo pi� dove ho messo quella bella 
      foto ma se aspetti  <sp> un po' la cerco e te la prendo


#16:  alla fine non sono riuscito a capire se Marco non voleva o non poteva 
      venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit�, ma tu non fargli vedere che pensi che 
      sia un bugiardo


#18:  questo tormento durer� ancora qualche ora, forse un giorno <sp> poi tutto 
      finir� e tu potrai tornare a casa , nella tua terra


#19:  Lucio era certo che sarebbe diventato una persona importante un uomo 
      politico o magari un ministro <inspiration> aveva a cuore il bene della 
      societ� <lp> rispettava la legge <inspiration> se teneva un discorso  
      trovava le parole giuste <sp> ad ogni situazione <inspiration> si sentiva 
      proprio un buon italiano


#20:  Stefano era un bel tipo <sp> gli piaceva farsi servire per primo e 
      mangiava per due <inspiration> ma quando si trattava di pagare sembrava 
      non vedere la mano che gli porgeva il conto <inspiration> il prezzo gli 
      pareva sempre troppo alto <sp> eppure lavorava in una azienda che <sp> dal 
      punto di vista economico <sp> era la numero uno su scala nazionale
