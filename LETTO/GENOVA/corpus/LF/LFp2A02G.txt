TEXT_inf.

MAT:		LF
MAP:		A
TRM:
Ndl:			
Ntr:		
Nls:		02
REG:		G


SPEAKERS_inf.

INp2:		S. P., M, 21, Genova


RECORDING_inf.

TYP:		DAT
LOC:		Genova
DAT:		13/03/01
DUR:		1.56,309
CON:		buone


TRANSCRIPTION_inf.

DAT:		02/05/01
CMT:		
Nst:		20


#1:  un mese di vacanza passa in fretta


#2:  <vocal> Maria dovrebbe stare pi� attenta <sp> a scuola


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai cosa fare <sp> n� dove andare


#5:  ora basta ! <sp> non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  <vocal> la ragazza che � appena entrata <sp> non la #<NOISE> conosco#


#8:  se mi lasci un'altra volta solo <sp> non ti parler� per tutta la vita


#9:  <inspiration> quest'anno ho fatto un lavoro nuovo <sp> #<NOISE> per� ho 
     deciso di non cambiare# pi� attivit�


#10:  <vocal> � possibile che non trovi mai un momento per scrivere a un amico 
      lontano ?


#11:  Luisa fingeva di guardare da un'altra parte <sp> #<NOISE> ma cercava il 
      modo per farsi# notare


#12:  ho chiesto a Gianna di portare subito #<NOISE> dell'acqua in un 
      recipiente# piccolo <inspiration> ma non � ancora arrivata


#13:  non bisogna credere che sia vero tutto quello che dice la gente <breath>
      tu non conosci ancora gli uomini <sp> non conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi <sp> ma quando usc� mi
      accorsi di non sentire neanche i rumori


#15:  #<NOISE> dopo tanto tempo# non m+ \ ricordo pi� dove ho messo quella bella 
      foto <sp> ma se aspetti un po' la cerco e te la prendo


#16:  <inspiration> alla fine <sp> non sono riuscito a capire se Marco non 
      voleva o non #<NOISE> poteva# venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit� <sp> ma tu non fargli vedere che pensi 
      che sia un bugiardo


#18:  questo tormento durer� ancora qualche ora <breath> forse un giorno <sp> 
      poi tutto finir� e tu potrai tornare a casa nella tua terra


#19:  Lucio era certo che sarebbe diventato una persona importante <sp> <vocal> 
      un uomo politico o magari un ministro <vocal> <sp> aveva a cuore il 
      #<NOISE> bene della# societ� <sp> rispettava la legge <inspiration> se 
      teneva un discorso trovava le parole adatte ad ogni #<NOISE> situazione# 
      <sp> si sentiva proprio un buon italiano


#20:  Stefano era un bel tipo <sp> <vocal> gli piaceva farsi servire per primo e
      mangiava per due <sp> ma #<NOISE> quando si trattava di# pagare <sp> 
      sembrava non vedere la mano che gli porgeva il conto <sp> il prezzo gli 
      pareva sempre troppo alto <lp> eppure lavorava in una azienda che <sp> dal 
      punto di vista economico era la numero uno su scala nazionale
