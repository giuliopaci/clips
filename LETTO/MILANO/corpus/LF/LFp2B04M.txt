TEXT_inf.

MAT:          	LF
MAP:          	B
TRM:
Ndl:          
Ntr:          
Nls:          	04
REG:          	M


SPEAKERS_inf.

INp2:         	L. C., M, 21, Oristano


RECORDING_inf.

TYP:          	DAT
LOC:          	Milano/aula universitaria
DAT:          	03/01/01
DUR:          	2.30,574
CON:          	buone


TRANSCRIPTION_inf.

DAT:          	13/12/02
CMT:          	Il parlante legge anche i numeri relativi alle frasi
Nst:          	20


#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� attenta a scuola


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai cosa fare n� dove andare


#5:  ora basta ! <sp> non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  la ragazza che � appena entrata <sp> non la conosco


#8:  se mi lasci un'altra volta solo <sp> non ti parler� per tutta la vita


#9:  quest'anno ho fatto un lavoro nuovo <sp> per� <sp> ho deciso di non 
     cambiare pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  Luisa <sp> fingeva di guardare un'altra parte <inspiration> ma cercava il 
      modo per farsi notare


#12:  ho chiesto a Gianna di portare subito dell'acqua in un recipiente
      piccolo <sp> ma non � ancora arrivata


#13:  non bisogna credere che sia vero tutto quello che dice la gente 
      <sp> tu non conosci ancora gli uomini <sp> non conosci il
      mondo


#14:  chiamai il medico perch� avevo male agli occhi <sp> ma quando
      usc� <sp> mi accorsi di non sentire neanche i rumori


#15:  dopo tanto tempo non ricordo pi� dove ho messo quella bella foto 
      <sp> ma se aspetti un po' la (<NOISE> cerco( e te la prendo


#16:  alla fine <inspiration> non sono riuscito a capire se Marco non voleva o 
      non poteva venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit� <NOISE> ma tu non fargli vedere che 
      pensi che sia un bugiardo


#18:  questo tormento durer� ancora qualche ora <NOISE> forse un giorno <sp> poi 
      tutto finir� e tu potrai tornare a casa <breath> nella tua terra


#19:  Lucio era certo che sarebbe diventato una persona importante <inspiration> 
      un uomo politico o magari un ministro <sp> aveva a cuore il bene della
      societ� <sp> rispettava la legge <sp> se teneva un discorso trovava le 
      parole adatte ad ogni situazione <sp> si sentiva proprio un buon italiano


#20:  Stefano era un bel tipo <breath> gli piaceva farsi servire per primo 
      <inspiration> e mangiava per due <sp> ma quando si (<NOISE> trattava( di 
      pagare sembrava non vedere la mano che gli porgeva il conto <sp> 
      <inspiration> il prezzo gli pareva sempre troppo alto <sp> <NOISE> eppure 
      lavorava in un'azienda che <inspiration> dal punto di vista economico <sp> 
      era la numero uno su scala nazionale
