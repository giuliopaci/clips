TEXT_inf.

MAT:		LF
MAP:		A
TRM:
Ndl:		
Ntr:
Nls:		04
REG:		O


SPEAKERS_inf.

INp2:		R. V., M, 22, Perugia


RECORDING_inf.

TYP:		DAT
LOC:		Perugia/Universit�
DAT:		09/01/01
DUR:		2.21,721
CON:		buone


TRANSCRIPTION_inf.

DAT:		11/04/01		
CMT:
Nst:		20


#1:  un mese di vacanza <sp> passa in fretta


#2:  Maria dovrebbe stare pi� attenta a scuola


#3:  Luigi fa tutto quello che gli chiede


#4:  quei signori non sanno mai cosa fare n� dove andare


#5:  ora basta non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  la ragazza ch'� appena entrata non la conosco


#8:  se mi lasci un'altra volta solo non ti parler� per tutta la vita


#9:  quest'{[screaming] anno} <sp> ho fatto un lavoro nuovo <inspiration> per� ho deciso di non 
      cambiare pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  Luisa fingeva di guardare da un'altra parte ma cercava il modo per farsi notare


#12:  ho chiesto a Gianna di portare subito dell'acqua in un recipiente piccolo <sp> ma non � 
      ancora arrivata


#13:  non bisogna credere che sia  vero tutto quello che dice la gente <inspiration> tu non conosci 
      ancora  gli uomini , non conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi <sp> ma quando usc� mi accorsi di non 
      sentire neanche i rumori


#15:  dopo tanto tempo non ricordo pi� dove ho messo quella bella foto <sp> ma se aspetti un po' la 
      cerco e te la prendo


#16:  <NOISE> alla fine non sono riuscito a capire se Marco non voleva o non poteva venire<ee> a 
      casa di Sandra


#17:  quel ragazzo non dice mai la verit� <sp> ma tu non fargli vedere che pensi che sia un 
      bugiardo


#18:  questo tormento durer� ancora qualche ora <sp> forse un giorno <inspiration> poi tutto finir� 
      e tu potrai tornare a casa nella tua terra


#19:  Lucio era certo che sarebbe diventato una persona importante <inspiration> un uomo politico 
      <sp> o magari un ministro <inspiration> aveva a cuore il bene della societ� <inspiration> 
      rispettava la legge e  se teneva un discorso trovava le parole adatte ad ogni situazione 
      <inspiration> si sentiva proprio un buon italiano


#20:  <tongue-click> Stefano era un bel tipo <sp> <inspiration> gli piaceva farsi servire per primo e 
      mangiava per due <inspiration> ma quando si trattava di pagare <inspiration> {[screaming] 
      sembrava} non vedere la mano che gli porgeva il conto <inspiration> il prezzo gli pareva sempre 
      troppo alto <lp> eppure lavorava in un'azienda che <sp>, dal punto di vista economico , era la 
      numero u_no su scala nazionale
