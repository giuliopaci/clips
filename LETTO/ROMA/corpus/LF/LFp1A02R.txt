TEXT_inf.

MAT:		LF
MAP:		A		
TRM:
Ndl:		
Ntr:		
Nls:		02
REG:		R


SPEAKERS_inf.

INp1:		M. C., M, 29, Roma
		

RECORDING_inf.

TYP:		DAT
LOC:		Roma/casa
DAT:		02/12/00
DUR:		1.52,274
CON:		buone


TRANSCRIPTION_inf.

DAT:		03/05/01
CMT:		
Nst:		20


#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� atenta [dialect] <sp> a scuola


#3:  Luigi fa tutto che gli si chiede


#4:  quei signori non sanno mai cosa fare n� dove andare


#5:  ora basta ! non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  la ragazza che � appena entrata , non la conosco


#8:  se mi lasci un'altra volta solo , non ti parler� per tutta la vita


#9:  quest'anno ho fatto un lavoro nuovo <sp> , per� ho deciso di non cambiare
     pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  Luisa fingeva di guardare da un'altra parte , ma cercava il modo per farsi
      notare

#12:  ho chiesto a Gianna di portare subito dell'acqua in un recipiente piccolo
      , ma non � ancora arrivata


#13:  non bisogna credere che sia vero tutto quello che dice la gente <sp> tu
      non conosci ancora gli uomini , non conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi <sp> , ma quando usc� mi
      accorsi di non sentire neanche i rumori


#15:  dopo tanto tempo non ricordo pi� dove ho messo quella bella foto <sp> , ma
      se aspetti un po' la cerco e te la prendo


#16:  alla fine , non sono riuscito a capire se Marco non voleva o non poteva
      venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit� <sp> , ma tu non fargli vedere che
      pensi che sia un bugiardo


#18:  questo tormento durer� ancora qualche ora , forse un giorno <sp> poi tutto
      finir� e tu potrai tornare a casa , nella tua terra


#19:  Lucio era certo che sarebbe diventato una persona importante , un uomo
      politico o magari un ministro <sp> aveva a cuore il bene della societ� ,
      rispettava la legge , se teneva un discorso  trovava le parole adatte ad
      ogni situazione <sp> si sentiva proprio un buon italiano


#20:  Stefano era un bel tipo <sp> gli piaceva farsi servire per primo e
      mangiava per due , ma quando si trattava di pagare , sembrava non vedere
      la mano che gli porgeva il conto , il prezzo gli pareva sempre troppo alto
      <sp> eppure lavorava in una azienda che , dal punto di vista economico ,
      era la numero uno su scala nazionale
