TEXT_inf.

MAT: 		LF
MAP: 		B
TRM:
Ndl: 
Ntr:
Nls: 		03
REG: 		T


SPEAKERS_inf.

INp2: 		S. S., M, 25, Torino
 

RECORDING_inf.

TYP: 		DAT
LOC: 		TORINO/ABITAZIONE
DAT:		31/01/01
DUR:		2.42,097
CON:		buone 


TRANSCRIPTION_inf.

DAT:		07/08/02
CMT:		
Nst:		20


#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� (<NOISE> attenta( a scuola 


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai cosa fare <sp> n� dove andare


#5:  ora basta ! <sp> non ti dar� pi� un soldo


#6:  nel grande parco <sp> un bambino giocava con suo padre


#7:  la ragazza che � appena entrata <sp> non la conosco


#8:  se mi lasci un'altra volta solo <inspiration> non ti parler� per tutta la 
     vita


#9:  quest'anno ho fatto un lavoro nuovo <inspiration> per� ho deciso di non 
     cambiare pi� attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  Luisa fingeva di guardare da un'altra parte <sp> ma cercava il modo per 
      farsi notare


#12:  ho chiesto a Gianna di portare subito dell'acqua in un recipiente piccolo 
      <inspiration> ma non � ancora arrivata


#13:  non bisogna credere che sia vero tutto quello che dice la gente 
      <inspiration> tu non conosci ancora gli uomini <sp> non conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi <sp> ma quando usc� 
      <inspiration> mi accorsi di non sentire neanche i rumori


#15:  dopo tanto tempo non ricordo pi� dove ho messo quella bella foto 
      <inspiration> ma se aspetti un po' la cerco e te la prendo


#16:  alla fine <sp> non sono riuscito a capire se Marco non voleva 
      <inspiration> o non poteva venire a casa di Sandra


#17:  <tongue-click> quel ragazzo non dice mai la verit� <sp> ma tu non fargli 
      vedere che pensi che sia un bugiardo


#18:  questo tormento durer� ancora qualche ora forse un giorno <sp> 
      <tongue-click> poi tutto finir� <sp> e tu potrai tornare a casa nella tua 
      terra


#19:  Lucio <sp> era certo che sarebbe diventato una persona importante un 
      uomo politico <sp> o magari un ministro <inspiration> aveva a cuore il 
      bene della societ� <sp> rispettava la legge <inspiration> se teneva un 
      discorso trovava le parole adatte ad ogni situazione <sp> si sentiva 
      proprio un buon italiano


#20:  Stefano era un bel tipo <sp> gli piaceva farsi servire per primo e 
      mangiava per due <inspiration> ma quando si trattava di pagare <sp> 
      sembrava non vedere la mano che gli porgeva il conto <sp> il prezzo gli 
      pareva sempre troppo alto <sp> eppure lavorava in un'azienda che <sp> dal 
      punto di vista economico <inspiration> era la numero (<NOISE> uno( su
      scala nazionale

