TEXT_inf.

MAT:          LF
MAP:          A
TRM:
Ndl:          
Ntr:          
Nls:          03
REG:          V


SPEAKERS_inf.

INp1:         G. S., F, 19, Venezia


RECORDING_inf.

TYP:          DAT
LOC:          Venezia, stanza di casa privata
DAT:          08/01/2001
DUR:          2.34,736
CON:          buone


TRANSCRIPTION_inf.

DAT:          30/07/02
CMT:          Il parlante legge i numeri relativi alle frasi tranne che per le 
              prime due
Nst:          20



#1:  un mese di vacanza passa in fretta


#2:  Maria dovrebbe stare pi� attenta a scuola


#3:  Luigi fa tutto quello che gli si chiede


#4:  quei signori non sanno mai cosa fare <sp> n� dove andare


#5:  ora basta ! <sp> non ti dar� pi� un soldo


#6:  nel grande parco un bambino giocava con suo padre


#7:  la ragazza che � appena entrata <sp> non la conosco


#8:  se mi lasci un'altra volta solo <sp> non ti parler� per tutta la vita


#9:  quest'anno ho fatto un lavoro nuovo <sp> per� ho deciso di non cambiare pi� 
     attivit�


#10:  � possibile che non trovi mai un momento per scrivere a un amico lontano ?


#11:  Luisa fingeva di guardare da un'altra parte <inspiration> ma cercava il 
      modo per farsi notare


#12:  ho chiesto a Gianna di portare subito dell'acqua in un recipiente piccolo
      <sp> ma non � ancora arrivata


#13:  non bisogna credere che sia vero tutto quello che dice la gente 
      <inspiration> tu non conosci ancora gli uomini <sp> non conosci il mondo


#14:  chiamai il medico perch� avevo male agli occhi <inspiration> ma quando 
      usc� <sp> mi accorsi di non sentire neanche i rumori


#15:  dopo (<NOISE> tanto( tempo <sp> non ricordo pi� dove ho messo quella bella 
      foto <inspiration> ma se aspetti un po' la cerco e te la prendo


#16:  alla fine non sono riuscito a capire se Marco non voleva o non poteva 
      venire a casa di Sandra


#17:  quel ragazzo non dice mai la verit� <sp> ma tu non fargli vedere che pensi
      che sia un bugiardo


#18:  questo tormento durer� ancora qualche ora <sp> forse un giorno
      <inspiration> poi tutto finir� e tu potrai tornare a casa <sp> nella tua 
      terra


#19:  Lucio era certo che sarebbe diventato una persona importante <inspiration> 
      un uomo politico o magari un ministro <sp> <inspiration> aveva a cuore il
      bene della societ� <sp> rispettava la legge <inspiration> se teneva un 
      discorso trovava le parole adatte ad ogni situazione <inspiration> si 
      sentiva proprio un buon italiano


#20:  Stefano era un bel tipo <inspiration> gli piaceva farsi servire per primo 
      <sp> e mangiava per due <inspiration> ma quando si trattava di pagare <sp> 
      sembrava non vedere la mano che gli porgeva il conto <inspiration> il 
      prezzo gli pareva sempre troppo alto <sp> <inspiration> eppure lavorava in 
      un'azienda che dal {<NOISE> punto} di vista economico <sp> era la numero 
      uno su {<NOISE> scala} nazionale
