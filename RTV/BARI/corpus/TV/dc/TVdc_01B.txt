TEXT_inf.

MAT:		TV
MAP:		
TRM:		dc
Ndl:		
Ntr:		01
Nls:		
REG:		B


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		Bari
DAT:		09/12/01
DUR:		1.45,210
CON:		ottime


TRANSCRIPTION_inf.

DAT:		23/03/02
CMT:		sono stati trascritti 53s. (da 2" a 55")
Nst:		14



p0#1:  difesa delle piante


<lp>


p0#2:  si pu� dire ormai <sp> finita quest'annata <sp> duemila duemilaeuno , 
       siamo nella nuova annata


p0#3:  <inspiration> quindi bisogna prepararsi per metter le piante in 
       condizione di <sp>autodifese


<sp>


p0#4:  che significa questo <inspiration> che sia le lavorazioni <sp> sia le 
       concimazioni , sia la potatura


<sp>


p0#5:  tutte le operazioni di preparazione per il nuovo ciclo <sp> 
       vegeto-produttivo


<sp>


p0#6:  *dese+ deve essere fatto in modo di mettere la pianta , appunto


p0#7:  che pu� difendersi prima da s�


<sp>


p0#8:  e poi noi interveniamo quando un'avversit� parassitaria sconfina


<sp>


p0#9:  e quindi bisogna <sp> controllarla


p0#10:  <inspiration> allora <sp> qual � l'atteggiamento da tenere in senso 
        generale , per quanto riguarda le potature specialmente delle colture 
        arboree


p0#11:  <inspiration> � quello di pulire le piante


<sp>


p0#12:  � un'operazione fondamentale , perch� se uno <sp> <vocal> non so , un 
        albero di albicocco lo pulisce bene bene da quelli che sono gli esiti 
        delle malattie dell'annata precedente


p0#13:  gli attacchi parassitari <sp> nella nuova annata , saranno limitati


p0#14:  e cos� anche i <inspiration> i trattamenti antiparassitari
