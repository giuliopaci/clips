TEXT_inf.

MAT:  RD
MAP:  
TRM:  it
Ndl:  
Ntr:  14
Nls:
REG:  D


SPEAKERS_inf.

INp1:  F
INp2:  F
INp3:  F


RECORDING_inf.

TYP:  DAT
LOC:  Bergamo/abitazione 
DAT:  09/12/01
DUR:  1.34,093
CON:  buone


TRANSCRIPTION_inf.

DAT:  28/03/02
CMT:
Nst:  34


p1#1:  allora<aa> Erika ci ha<aa> comunicato il suo<oo> <inspiration> parere su
       questo film 


p1#2:  intanto io vorrei che anche i nostri due ospiti che sono arrivati
       adesso<oo> ci dicessero come si chiamano e trovassero una sedia un paio
       di cuffie


p1#3:  <inspiration> non ci sono sedie qua <eh>


p1#4:  <inspiration> allora mentre va la storia andiamo<oo> <inspiration> a
       recuperarle


p1#5:  intanto se vi avvicinate a un microfono ci potete dire <inspiration>
       almeno i vostri nomi <sp> <eh> ce li dic+ <NOISE> ce<ee> li dite ?


<sp>


p1#6:  s� prova prova <unclear>


p2#7:  io mi chiamo Carolina


p1#8:  ciao Carolina e tu chi sei ?


<sp>


p3#9:  Matilde


p1#10:  ciao Matilde sorelle ?


p2#11:  s�

il suono � molto flebile perch� il parlante � lontano dal microfono


p1#12:  sorelle<ee> <sp> quanti anni avete Matilde ?


<sp>


p3#13:  io dieci


p1#14:  e Carolina ?


<sp>


p2#15:  <eeh> quasi quattordici


p1#16:  <ah> l'hai accompagnata insomma oggi qua <sp> o tu sei un'ascoltatrice
        di Crapa Pelata ?


<sp>


p2#17:  ma io la se+ <eeh> ogni tanto la<aa> ascolto


p1#18:  #<p2#19> ogni#


p2#19:  #<p1#18> per�# non son qua di Milano


p2#20:  di solito sono in campagna quando<oo> <sp> #<p1#21> c'� la trasmissione#


p1#21:  #<p2#20> <ah> s� ? dove andate ?#


p1#22:  <inspiration> dove andate di bello la domeni+ / abitate fuori #<p2#23>
        Milano ?#


p2#23:  #<p1#22> s�


<sp>


p1#24:  dove abitate ?


p2#25:  provincia di Pavia nell'Oltrep� pavese


p1#26:  allora i bambini di Milano oggi si devono sentire veramente <eh> ? in
        colpa


p1#27:  loro arrivano da Varese da Malnate


p1#28:  voi arrivate <inspiration> da<aa> ?


<sp>


p2#29:  dalla provincia di Pavia


p1#30:  dalla provincia di Pavia e<ee> i bambini di Milano che abitano qui
        vicino a noi <ah> il cane mi {<laugh> sta portando via} <inspiration> i
        bambini<ii> di Milano invece<ee> se ne stanno a letto a poltrire


p1#31:  <eh> ? cosa dite ?


p1#32:  <inspiration> allora intanto<oo> <inspiration> <eeh> vi<ii> vi dico che
        fra poco partir� la storia una storia che parla di gatti <inspiration>


p1#33:  anche se i telefoni <ehm> brillano<oo> <vocal> Gianluca mi devi aiutare
        tu oggi in questa conduzione <inspiration>


p1#34:  perch�<ee> avendo lanciato la parola d'ordine Harry Potter qua adesso
        cominciano a chiamare
