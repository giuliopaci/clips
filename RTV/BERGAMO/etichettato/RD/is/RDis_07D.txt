TEXT_inf.

MAT:  RD
MAP:  
TRM:  is
Ndl:  
Ntr:  07
Nls:
REG:  D


SPEAKERS_inf.

INp1:  M
INp2:  F
INp3:  M


RECORDING_inf.

TYP:  DAT
LOC:  Bergamo 
DAT:  09/12/01
DUR:  1.35,815
CON:  buone


TRANSCRIPTION_inf.

DAT:  28/03/02
CMT:
Nst:  18


p1#1:  � scesa la colonnina di mercurio


p1#2:  nella notte le temperature sono rigide <sp>in tutta Italia i<ii>
       particolari nel servizio di Daniela Ubaldi


p2#3:  Italia divisa in due con il sole a nord e il cielo coperto al sud ma ad
       unire la penisola c'� il freddo intenso


p2#4:  il record sotto zero si � toccato in Friuli <inspiration> dove la
       temperatura � ulteriormente scesa nel corso di questa notte


p2#5:  <inspiration> raffiche di bora fino a cento chilometri orari a Trieste
       <inspiration>


p2#6:  freddo intenso ma senza abbondanti nevicate al nord con forti disagi
       sulle strade della Lombardia a causa di banchi di nebbia


p2#7:  <inspiration> dal Potentino in gi� si sono invece imbiancati i monti
       anche a quote basse


p2#8:  <inspiration> sul massiccio del Pollino il manto nevoso ha raggiunto i
       sei sette centimetri


p2#9:  <inspiration> stagione invernale iniziata in Abruzzo dove la stazione
       sciistica di Campo Imperatore <inspiration> � rimasta bloccata da una
       bufera di vento


p2#10:  <inspiration> circolazione con catene da ieri anche sul tratto lucano
        della -A- tre Salerno Reggio Calabria


p2#11:  <inspiration> pericolosa per il ghiaccio anche la circolazione sui
        viadotti


p2#12:  <inspiration> in Sicilia il mare forza sette e il vento a ventinove nodi
        <inspiration> hanno causato al largo delle coste siracusane il naufragio
        di una nave turca <inspiration> mentre un altro mercantile maltese �
        stato investito da un'ondata molto forte


p2#13:  <inspiration> un marinaio indiano � morto e un suo connazionale �
        rimasto ferito


p1#14:  andiamo a chiudere questa edizione di Reporter Flash dando la linea alla
        redazione sportiva


p3#15:  {<MUSIC> il pi� cordiale} saluto dalla redazione sportiva


p3#16:  quanto avvenuto ieri a Piacenza nella sfida con il Bologna ha
        dell'incredibile


p3#17:  una partita interamente giocata in una nebbia fittissima che ha impedito
        a spettatori sugli spalti e pubblico televisivo di vedere alcunch�


p3#18:  <inspiration> alla fine il tabellino recita due a zero per il Piacenza
        ma il verdetto del campo proprio per le condizioni climatiche �
        contestato dal tecnico del Bologna Francesco Guidolin
