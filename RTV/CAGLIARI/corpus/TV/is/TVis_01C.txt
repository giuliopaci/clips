TEXT_inf.

MAT:  TV
MAP:
TRM:  is
Ndl:
Ntr:  01
Nls:
REG:  C


SPEAKERS_inf.

INp1:  M
INp2:  M
INp3:  F
INp4:  M


RECORDING_inf.

TYP:  PC
LOC:  Cagliari/albergo
DAT:  10/12/01
DUR:  4.12,165
CON:  buone


TRANSCRIPTION_inf.

DAT:  20/04/02
CMT:  intervallo trascritto 2.257 - 2:43.771
Nst:  25



p1#1:  nel<ll> registro degli *inga_dagati � terminato anche il<ll> nome del
       direttore generale dell'Ente Nazionale Aviazione Civile


p1#2:  <inspiration> mentre a Cagliari sono cominciati i contatti tra<aa>
       Regione e rappresentanti delle<ee> attivit� imprenditoriali per studiare
       le possibilit� di applicare <inspiration> la continuit� territoriale
        anche per quanto riguarda il trasporto merci


p1#3:  i servizi di sono firmati da Valerio Vargiu <inspiration> e Teresa
       Piredda


<lp>


p2#4:  il pubblico ministero della Procura di Roma Vincenzo Roselli
       <inspiration> ha ipotizzato il reato di abuso d'ufficio a carico del
       direttore generale dell'Ente Nazionale per l'Aviazione Civile Pierluigi
       Di Palma <inspiration> e di tre componenti della commissione chiamata a
       valutare le offerte delle compagnie aeree <inspiration> che concorrevano
       all'aggiudicazione dell'appalto per le sei tratte da Cagliari Alghero e
       Olbia <inspiration> per Roma e Milano andata e ritorno


p2#5:  l'iscrizione nel registro degli indagati � un atto dovuto


p2#6:  in seguito alla denuncia presentata dall'Airone


p2#7:  l'unica compagnia aerea del tutto esclusa dalla gara <inspiration> che ha
       decretato la vittoria di tutte le altre


p2#8:  Alitalia e Volare Airline si sono aggiudicate i collegamenti da e per
       Cagliari


p2#9:  <inspiration> Meridiana � l'esclusiva per Olbia


p2#10:  <inspiration> e con volare Airline <ss>si divider� le tratte per Roma e
        Milano da e per Alghero


p2#11:  <inspiration> ma per Airone <inspiration> non tutto si sarebbe svolto
        regolarmente


p2#12:  in particolare la commissione istituita dall'Ente Nazionale per
        l'Aviazione Civile <inspiration> nella seduta del diciassette aprile
        avrebbe prima preso visione delle offerte economiche <inspiration> e
        soltanto successivamente determinato i criteri di valutazione


p2#13:  in base alla denuncia presentata in Procura a fine agosto dall'avvocato
        Marcello Meandri


p2#14:  nel corso della gara ci sarebbero state <ff>fughe di notizie


p2#15:  e i risultati sarebbero stati curiosamente proprio quelli auspicati da
        alcuni ambienti politici e imprenditoriali


p2#16:  <inspiration> da qui gli sviluppi giudiziari e i primi quattro indagati
        nell'inchiesta aperta dalla procura capitolina per le presunte
        irregolarit� nell'assegnazione in monopolio per un triennio dei
        collegamenti aerei a prezzi agevolati tra la Sardegna e il continente


<lp>


p3#17:  la lotta per l'attuazione della continuit� territoriale sta proseguendo
        sull'altro fronte , quello del trasporto delle merci


p3#18:  <insoiration> a disposizione ci sono trenta miliardi di contributi
        statali l'anno ai quali va aggiunto almeno il cinquanta percento di
        cofinanziamento regionale


p3#19:  <inspiration> al momento per� tutto � fermo


p3#20:  perch� non sono stati messi in moto i meccanismi che consentono
        l'utilizzo dei fondi


p3#21:  <inspiration> nel tentativo di predisporre un piano comune gi� dalla
        scorsa settimana l'assessore dei trasporti Marco Carboni <inspiration>
        avviato gli incontri con i rappresentanti delle categorie produttive


p3#22:  la strada che si vorrebbe seguire anche in questo caso � qell degli
        oneri di servizio


<sp>


p4#23:  se attualmente si paga <inspiration> <eeh> una determinata somma a<aa>
        metro lineare di merce<ee> trasportata su<uu> semirimorchio


p4#24:  si potr� pagare quella somma meno il venti meno il trenta percento


p4#25:  questo dipende dalle risorse che abbiamo a disposizione <inspiration>
