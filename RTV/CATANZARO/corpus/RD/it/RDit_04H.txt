TEXT_inf.

MAT:		RD
MAP: 
TRM:		it
Ndl: 
Ntr:		04
Nls:
REG:		H


SPEAKERS_inf.

INp0:		M 


RECORDING_inf.

TYP:		PC
LOC:		Catanzaro Centro
DAT:		04/04/01
DUR:		1.08,981
CON: 


TRANSCRIPTION_inf.

DAT:		29/06/01 
CMT:		volume basso
Nst:		11


p0#1:  <NOISE> altro che chiacchere da ascensore <sp> non ci sono pi� 
       le stagioni di una volta


p0#2:  qui tra<aa> <eeh> formula uno <laugh> ma soprattutto Erre-
       Ci auto tariffe impazzite cartelle pazze del fisco 
       <inspiration> ed elezioni <eeh> che incombono <sp> veramente 
       c'� da perdere <inspiration> <eh> la bussola ancora 


p0#3:  intanto non si sa quanti e quali siano i simboli elettorali 


p0#4:  <inspiration> <eh> che sono stati ricusati dall'apposita <eeh> 
       commissione falcidiatrice no esaminatrice l� alla<aa> Ministero 
       dell'Interno alla base 


p0#5:  <inspiration> tra gli esclusi eccellenti secondo quanto si 
       apprende ci potrebbe essere la lista della fiamma tricolore di 
       Pino Rauti 


p0#6:  non � un caso di <inspiration> discriminazione assolutamente ma 
       � <mh> contro l'utilizzazione del simbolo che era stato dell'Emme-Esse-I 


p0#7:  <inspiration> Alleanza Nazionale precedentemente esattamente 
       <inspiration> aveva fatto ricorso al tribunale di Roma 


p0#8:  che il ventinove marzo dell'anno scorso <inspiration> aveva 
       accolto la richiesta avanzata per Alleanza Nazionale dal 
       segretario Fini 


p0#9:  di inibire al partito <inspiration> di Pino Rauti l'uso del 
       nome movimento sociale e del simbolo <sp> della fiamma 


p0#10:  la commissione avrebbe quindi escluso <inspiration> il simbolo 
        dalla rosa dei contrassegni *emmessi alla prossima 
        consultazione elettorale <sp> 


p0#11:  chiaramente anche questa informazione <inspiration> � da <eeh> 
        confermare 
