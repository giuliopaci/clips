TEXT_inf.

MAT:		RD 
MAP:		
TRM:		it
Ndl: 
Ntr:		12
Nls:
REG:		H


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		Catanzaro Centro
DAT:		04/04/01
DUR:		0.58,951
CON:		buone


TRANSCRIPTION_inf.

DAT:		29/06/01
CMT: 
Nst:		9


p0#1:  cari radio ascoltatori di Catanzaro Centro il servizio meteo 
       parap� dell'aereonautica militare <eeh> ha deciso di comunicare 
       le previsioni del tempo sull'Italia 


p0#2:  per una situazione che vede <inspiration> una perturbazione 
       atlantica proveniente dalla Francia muoversi verso l'Italia 
       settentrionale 


p0#3:  un corpo nuvoloso in progressivo dissolvimento in movimento 
       verso nord est transiter� invece sulle regioni <inspiration> 
       centro settentrionali 


p0#4:  ed ecco il bullett�n [dialect] del tempo previsto fino alle 
       ventiquattro di quest'oggi 


p0#5:  <inspiration> al sud e sulla Sicilia in particolar modo cielo 
       sereno o poco nuvoloso <sp> 


p0#6:  <mb�> ? vorrei anche vedere un giorno <laugh> 


p0#7:  comunque temperatura <inspiration> <eeh> in lieve aumento le 
       massime e in lieve flessione <sp> le minime sembra quasi un 
       rompicapo 


p0#8:  per domani <inspiration> il tempo al sud della penisola e sulla 
       Sicilia <inspiration> vedr� un cielo con nuvolosit� variabile 
       ma con prevalenza di schiarite 


p0#9:  soltanto la temperatura sar� <inspiration> in lieve <eeh> 
       diminuzione e venti moderati in intensificazione da ovest nord 
       ovest
