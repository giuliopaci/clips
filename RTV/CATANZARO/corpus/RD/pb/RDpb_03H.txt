TEXT_inf.

MAT:		RD
MAP: 
TRM:		pb
Ndl: 
Ntr:		03
Nls:
REG:		H


SPEAKERS_inf.

INp0:		F 


RECORDING_inf.

TYP:		PC
LOC:		Catanzaro Centro
DAT:		04/04/01
DUR:		0.30,559
CON: 


TRANSCRIPTION_inf.

DAT:		29/06/01 
CMT:		musica in sottofondo
Nst:		5


p0#1:  freddo d'inverno caldo d'estate ? <sp> la fabbrica fratelli 
       Giordano � specializzata nella produzione e montaggio di 
       splendidi infissi e doppi infissi <sp> 


p0#2:  il tutto seguendo le tecnologie pi� all'avanguardia <sp> 


p0#3:  gli infissi della fabbrica fratelli Giordano <sp> sono infatti 
       in alluminio anodizzato all'esterno <sp> e rifiniti in legno 
       pregiato all'interno <sp> 


p0#4:  vi garantiscono cos� massima protezione e risparmio energetico 
       <sp> con un tocco di vera eleganza per il vostro appartamento 
       <lp> 


p0#5:  fabbrica fratelli Giordano <sp> via Carlo quinto 
       centocinquantatr� -Bi- Catanzaro <sp> telefono settantaquattro 
       cinquantatr� settantasei 
