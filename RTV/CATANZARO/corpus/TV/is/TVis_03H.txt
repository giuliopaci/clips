TEXT_inf.

MAT:  TV
MAP: 
TRM:  is
Ndl: 
Ntr:  03
Nls:
REG:  H


SPEAKERS_inf.

INp0:  F


RECORDING_inf.

TYP:  VRC
LOC:  Catanzaro Centro
DAT:  24/03/01
DUR:  0.35,702
CON:  buone con una lieve interferenza negli ultimi sec. 


TRANSCRIPTION_inf.

DAT:  14/06/01
CMT: 
Nst:  6


p0#1:  sono milleequattrocentoventisei i nuovi casi di -A- -I- Di- -      
       Esse- diagnosticati in Italia nel duemila


p0#2:  mentre i decessi sono stati trecentoventisei la met� rispetto  
       all'anno precedente 


p0#3:  <inspiration> dal millenovecentoottantadue anno della prima 
       diagnosi della malattia in Italia i casi notificati al centro 
       operativo -A- -I- Di- -Esse- ammontano a 
       quarantasettemilacinquecentotre 


p0#4:  <inspiration> <sp> la Lombardia resta in testa alle regioni pi� 
       colpite <inspiration> con quattrocentoventuno nuovi ammalati 


p0#5:  <inspiration> <sp> negli ultimi diciotto anni invece 
       complessivamente la stessa Lombardia ha accusato circa <sp>
       quindicimila casi 


p0#6:  <inspiration> <sp> in Calabria <sp> i casi registrati sono in tutto 
       quattrocentocinquantatre 
