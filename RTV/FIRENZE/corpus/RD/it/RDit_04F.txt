TEXT_inf.

MAT:		RD
MAP:		
TRM:		it
Ndl:		
Ntr:		04
Nls:		
REG:		F


SPEAKERS_inf.

INp0:		F


RECORDING_inf.

TYP:		PC
LOC:		Firenze/abitazione
DAT:		13/02/01
DUR:		0.33,134
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto dall'inizio a 19"312ms
Nst:		3


p0#1:  sono <sp> imbarcati su un volo che li ha portati da Londra <sp> a New 
       York


p0#2:  <inspiration> <sp> se riusciranno <sp> a stare <sp> uniti <sp> fino al 
       giorno di San Valentino senza chiedere le chiavi per *libela+ <sp> li+ 
       <sp> libelalsi <sp> {<laugh> in Cina} <sp> <inspiration> <sp> in Italia 
       <sp> liberarsi


p0#3:  <inspiration> vinceranno un premio di cinquemila sterline ciascuno 
       <inspiration> messo in palio <sp> <inspiration> da questa {<MUSIC> radio}
