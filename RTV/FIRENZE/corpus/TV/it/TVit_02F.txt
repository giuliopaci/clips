TEXT_inf.

MAT:		TV
MAP:		
TRM:		it
Ndl:		
Ntr:		02
Nls:		
REG:		F


SPEAKERS_inf.

INp1:		M
INp2:		M


RECORDING_inf.

TYP:		PC
LOC:		Firenze/abitazione
DAT:		
DUR:		5.12,500
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto da 0 a 1'59"178, totale 1'59"178
Nst:		15


p1#1:  <unclear> o sono {<NOISE> proprietari o stanno d'intorno} <NOISE> a <eeh> 
       certe terre perch� dicano proprio a noi


p1#2:  sostanzialmente forse � {[dialect] 'un proprio a noi} <sp> questo � un 
       po' brutale come <inspiration> <sp> <eeh> #<NOISE> espressione ma insomma 
       ora sentiremo <sp> il presidente#


p1#3:  la volta scorsa <inspiration> #<NOISE> l'assessore <vocal> <sp> 
       provinciale# <sp> <inspiration> <sp> <eeh> Romiti ci aveva <sp> in 
       qualche modo <inspiration> <sp> <eeh> spiegato tutte le scelte 


p1#4:  e anche i {<NOISE> tecnici hanno} scelto , pare <inspiration> <NOISE> tra 
       tutto ci� che <eeh> tocca <sp> <eeh> <inspiration> <sp> a<aa> a 
       impegnarsi alla provincia di Pistoia come le altre province della Toscana 
       proprio <sp> le #<NOISE> zone <sp> dove <inspiration> il danno <sp> 
       sarebbe stato <sp> minore insomma# <inspiration> il male minore 


p1#5:  e per di pi� , ci ha detto #<NOISE> se non avete seguito# <inspiration> 
       <sp> per chi non ha s+ / avesse seguito lo rammento <inspiration>


p1#6:  queste sono zone #<NOISE> che oltretutto <sp> verrebbero# <inspiration> 
       pagate <eeh> remunerate in qualche modo 


p1#7:  <inspiration> e oltretutto non lasciate l� inerti a morire , ma 
       <inspiration> ci possono essere fatti <inspiration> d'intorno e su quei 
       #<NOISE> terreni# <inspiration> delle attivit� di sviluppo di diversa 
       <sp> natura<aa> , attivit� sportive , attivit� ludiche e<ee> e cos� via 


p1#8:  <inspiration> <sp> e anche addirittura qualche <sp> insediamento 
       <inspiration> <sp> <ehm> di cost+ / costruito per #<NOISE> appunto queste 
       attivit�# sociali <inspiration> <sp> e pubbliche che <eeh> eventualmente 
       si ritiene opportuno 


p1#9:  #<NOISE> ecco questo � un pochino il quadro# 


p1#10:  allora per� <inspiration> <sp> il presidente del guado <inspiration> mi 
        pare <sp> insieme qui ci sono anche altri che vengono a sostenere 
        <inspiration> <sp> questa operazione 


p1#11:  mi pare che <sp> non siate d'accordo


p1#12:  e ovv�a , sentiamo perch�


<sp>


p2#13:  allora <sp> noi<ii> siamo venuti<ii> proprio<oo> in relazione alla 
        trasmissione di venerd� scorso
 

p2#14:  #<NOISE> perch�<ee># non ci troviamo d'accordo con la<aa> versione<ee> 
        dei fatti<ii> <eeh> enunciati dai dai vari<ii> <sp> <inspiration> <sp> 
        dalle varie asso+ #<NOISE> associazioni# sindacali


p1#15:  <inspiration <sp> e in pi�<uu> <eeh> in parte ci troviamo d'accordo 
        con<nn> <eeh> con l'assessore Romiti , ma non ci troviamo<oo> sulla 
        sulla stessa linea<aa> d'onda in quando c+ / quando dice che le case non 
        verranno fatte
