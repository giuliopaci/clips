TEXT_inf.

MAT:		TV
MAP:		
TRM:		it
Ndl:		
Ntr:		03
Nls:		
REG:		F


SPEAKERS_inf.

INp1:		M
INp2:		F


RECORDING_inf.

TYP:		PC
LOC:		Firenze/abitazione
DAT:		
DUR:		14.00,796
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto da 24"113ms a 1'55"398ms , totale 1'31"285ms
Nst:		10


p1#1:  il<ll> presidente per� <sp> attenzione questa � una risposta che non 
       tocca a te , tocca a<aa> chi ha la #<NOISE> responsabilit� diretta , 
       diceva una cosa# <inspiration> molto importante che � <sp> secondo me un 
       segno<oo> <sp> di grande evoluzione della gente di /


<sp> 


p1#2:  io mi permetto di dire di quella zona ma insomma , diciamo , della gente
       in questo caso che se fossero {<NOISE> convinti} <inspiration> che
       comunque questo loro <inspiration> tributo da pagare <inspiration>
       servisse


p1#3:  e tecnicamente gli fosse dimostrato in maniera <inspiration> netta e
       chiara <sp> e che poi , immagino <inspiration> la loro zona <inspiration>
       � <sp> comunque , fatti {<NOISE> tutti i} conti , preferibile ad altre
       zone


p1#4:  <unclear> da ultimo si rassegnerebbero anche / gli manca , manca a loro 
       <inspiration> questa convinzione


p1#5:  quindi <inspiration> mi pare che non ragionino <sp> egoisticamente e 
       basta , #<p2#6> ragionano com+ <sp> <eh> !#


p2#6:  #<p1#5> no , ci manca<aa># che su dei dati di fatto<oo> dopo gli ultimi 
       eventi alluvionali<ii> in zona delle Caserane dove esa+ esiste gi� una 
       cassa di espansione


p2#7:  <inspiration> che <eeh> / perch� bisogna dividere il problema dalle acque 
       alte alle acque basse perch� sono due cose completamente diverse


p2#8:  <inspiration> e quindi qui si sta parlando per quanto riguarda la nostra 
       cassa di acque alte


p2#9:  <inspiration> e quindi <NOISE> <sp> il problema di poter alluvionare 
       <inspiration> diciamo<oo> <ehm> Poggio <eeh> queste zone / Firenze <sp> 
       perch� <sp> parliamoci chiaro e tondo


p2#10:  <inspiration> questi tremilacinquecento miliardi , perch� sono 
        tremilacinquececento miliardi stanziati <inspiration> per l'edificazione 
        delle casse di espansione <inspiration> sono per salvaguardare Pisa <sp> 
        e Firenze
