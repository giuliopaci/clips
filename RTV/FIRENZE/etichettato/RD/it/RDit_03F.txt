TEXT_inf.

MAT:		RD
MAP:		
TRM:		it
Ndl:		
Ntr:		03
Nls:		
REG:		F


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		
DAT:		
DUR:		1.12,325
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
VER:		
REV:		
CMT:		trascritto da 0 sec. a 58 sec., totale 58 sec.
Nst:		7


p0#1:  a proposito<oo> volevo<oo> ribadire <sp> e soprattutto incitare tutti gli 
       ascoltatori <sp> <inspiration> <sp> a passare proprio in questi giorni 
       dal <eeh> CPA <ehm> nel viale Giannotti in Gavinana <sp> <inspiration> 
       <sp> per <eeh> dare solidariet� , sostegno 

p0#2:  c'� bisogno di tutti <sp> <inspiration> <sp> contro<oo> appunto lo 
       sgombero del CPA e anche soprattutto contro <sp> <inspiration> <sp> <eeh> 
       la realizzazione del <eeh> centro commerciale proprio in quel di Gavinana 

p0#3:  <sp> <inspiration> <sp> il mio quartiere che <sp> stanno <sp> per <sp> 
       distruggermelo


<MUSIC>


p0#4:  e quindi l'invito � ovviamente<ee> rivolto a tutti , a tutti coloro che 
       ci credono che credono in quello <ehm> in cui il CPA ha portato avanti in 
       <eeh> undici anni <sp> <inspiration> <sp> di attivit� 


p0#5:  e che credono che <sp> <inspiration> <sp> un <eeh> centro commerciale in 
       Gavinana sia <sp> una cosa veramente assurda 

p0#6:  e a questo proposito vi ricordo anche<ee> la {<NOISE> manifestazione} per 
       il <eeh> CPA contro lo sgombero e contro il centro commerciale che si 
       terr� sabato <sp> diciassette due <sp> <inspiration> <sp> <ehm>


p0#7:  con il ritrovo con <eeh> il concentramento in piazza Santa Croce alle ore 
       sedici

