TEXT_inf.

MAT:		RD
MAP:		
TRM:		it
Ndl:		
Ntr:		05
Nls:		
REG:		F


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		Firenze/abitazione
DAT:		13/02/01
DUR:		0.49,315
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto dall'inizio a 48"255ms
Nst:		8


p0#1:  <eeh> dal Gi <sp> no<oo> , s� , dal Gi , sembra<aa> troppo semplice qui 
       <inspiration> dal vostro caro dolce amaro amato odiato umano e disumano 
       <inspiration> <sp> amico Gi <inspiration> 

p0#2:  <eeh> s� {[whispering] <ehm> <ehm> <ehm> ho capito}


p0#3:  <ah> vabb� <inspiration> <oh> come va ? tutto bene ? s� <eeh> e noi 
       continuiamo la nostra <inspiration> <sp> {<NOISE> <eeh>} pes+ / 
       passeggiatina all'interno dei vecchi Sanremi <inspiration> Sanremi 


p0#4:  <inspiration> cercando personaggi<ii> che sono rimasti / che erano e poi 
       sono rimasti anche <inspiration> sconosciuti , no ? <eh> ? <inspiration> 


p0#5:  e<ee> qui <eeh> trovo Consoli , Consoli , Consoli ? <sp> ma Consoli � 
       diventata famosa ! <sp>

p0#6:  no , ma questa <sp> questo no <sp> questa o
       questo <inspiration> non � <eeh> la Consoli Consoli , � il Consoli ,
       il Consoli ? <sp> <eh> ? <inspiration>


p0#7:  ma a parte che anche la Consoli � di molto il<ll> , ma insomma 


p0#8:  <inspiration> questo si chiama Fabrizio Consoli ecco <inspiration> nel 
       novantacinque si present� <inspiration> a San Remo , Quando Saprai <eeh> 
       e invece non s'� mai saputo



