TEXT_inf.

MAT:		TV
MAP:
TRM:		it
Ndl:
Ntr:		02
Nls:
REG:		L


SPEAKERS_inf.

INp1:		F
INp2:		M


RECORDING_inf.

TYP:		PC
LOC:		Lecce
DAT:		
DUR:		5.54,305		
CON:		buone


TRANSCRIPTION_inf.
 
DAT:		16/03/02
CMT:            la trascrizione inizia da 23" e prosegue fino a 4'50" ca. 	 
Nst:	 	41





p1#1:  allora vi vorrei ricordare , una volta in pi� <sp>
       qualora ce ne fosse bisogno , i miei due opuscoli


<lp>


p1#2:  ecco <lp> allora <lp> questo , � un opuscolo <sp> che serve


p1#3:  cio� vi illustra , dei metodi pratici delle vere e proprie ricette 
       <inspiration> su come , riconoscere gli attacchi esterni di negativit� 
       volgarmente detti malocchio <sp> o volgarmente chiamati fatture


<sp>


p1#4:  si intitola come fare disfare difendersi dal malocchio


<inspiration> 


p1#5:  esiste in due versioni <lp> una <sp> ridotta , e una pi� 
       allargata


p1#6:  quella ridotta , costa proprio una fesseria <eh> giusto <unclear> le 
       spese {<NOISE> della} carta dell'inchiostro


<inspiration> 


p1#7:  ecco <eeh> praticamente quella ridotta <ehm> contiene tutti 
       i metodi per riconoscere il malocchio <inspiration> e i vari <eeh> 
       diciamo<oo> metodi pratici per poterlo contrastare per poterlo 
       combattere


<sp>


p1#8:  la versione pi� allargata , contiene anche <eeh> dei<ii> sistemi e dei metodi ,
       su come riconoscere il malocchio d'amore <sp> come contrastarlo


<inspiration> 


p1#9:  e poi contiene dei piccoli rituali , sono proprio dei 
       rituali veri e propri <inspiration> alcuni dei quali , in forma pi� 
       complessa adoperati da<aa> da da da dagli dagli esperti del settore , per 
       avvicinare o riavvicinare la persona amata


<sp>


p1#10:  io <nn>non posso proprio promettervi e garantirvi al cen+ 
        / per� vi posso garantire una cosa 


<sp>


p1#11:  perch� io di questi , ne ho venduti tantissimi <eh> ma <tongue-click> 
        cos� tanti <inspiration> che se questo fosse un bidone a quest'ora , mi 
        avrebbero gi� <lp> <vocal> dico / penso che <inspiration> mi avrebbero 
        {<breath> gi�} <inspiration> per modo di dire sparata in testa , cosa 
        che invece non � successa non succeder� mai 


<inspiration> 


p1#12:  io vi posso garantire , che specialmente per i rituali 
        d'amore <lp> <tongue-click> se voi <inspiration> una cosa la fate per 
        curiosit� , cos� tanto per <lp> anzi non riesce proprio , fatene a meno


<inspiration> 


p1#13:  ma se , un rituale voi lo eseguite convinti , proprio 
        convinti di quello che fate con massima seriet� e col massimo rispetto 
        per quello che state leggendo e per quello che state facendo io vi posso 
        garantire <inspiration> che il risultato 


p1#14:  va be' non posso dire al cento per  cento perch� c'� sempre un margine 
        di imprevedibilit� in tutte le cose


<inspiration>

 
p1#15:  per� vi posso garantire che dei risultati<ii> presto o 
        tardi li av+ <unclear> li avrete <eh> 


p1#16:  perch� dico a me sapete quanta gente ha telefonato per dire grazie
        <inspiration> al tuo opuscolo io ho risolto questo questo questo e 
        questo , questo mi fa piacere 


<inspiration> 


p1#17:  perch� vuol dire <inspiration> che tutte le notti insonni 
        che io ho speso per elaborare e rielaborare <inspiration> anche *delli 
        dei rituali <sp> presenti su altri testi, per vedere di semplificarli 
        <inspiration> per vedere di cambiare magari un certo materiale da 
        impiegare


p1#18:  no perch� qui , vengono illustrati metodi con materiali facilmente 
        reperibili e poco costosi 


<inspiration> 


p1#19:  quindi tutti i rituali anche se sono stati attinti da 
        altre fonti sono stati tutti da me rielaborati


<inspiration> 


p1#20:  poi c'� quest'altro / ogni giorno mi sto dimenticando di  
        porta+ / perch� poi mi trovo comoda che mi sistemo tutto , vabb�


<inspiration>

 
p1#21:  questa � una copertina provvisoria , l'altra , quella<aa> 
        quella definitiva � molto simile a questa , per� � molto pi� bella , io 
        ogni giorno mi sto dimenticando ,


p1#22:  si intitola metodi e sistemi per vincere al Lotto 


<inspiration> 


p1#23:  � una cosa che io ho fatto su vostra richiesta 
        <inspiration> perch� molti di voi hanno detto ma perch� non pubblichi 
        qualcosa che <inspiration> magari riguardi il gioco / riguarda il gioco 
        del Lotto , come vincere come elaborare dei calcoli , poi ci sono delle 
        persone che lo fanno per hobby


<inspiration> 


p1#24:  piace fare calcoli di matematica gli  piace cas+ / pasticciare gli piace elaborare


<inspiration> 


p1#25:  ed io devo essere sincera , ci sono state pure delle 
        persone che hanno vinto <eh> grazie a questa<aa> <inspiration> <vocal> 
        grazie a questa elaborazione <eh> che voi non troverete mai n� su 
        nessuna rivista n� su nessun giornale


p1#26:  ora , se no le amiche mie si offendono <inspiration> perch� dice 
        [dialect] ma noi ti scriviamo sempre tu non leggi mai niente , allora

Nel parlato quotidiano dialettale spesso viene utilizzata la forma verbale alla terza persona singolare nonostante il soggetto sia alla terza persona plurale


<inspiration> 


p1#27:  intanto prego la regia di inquadrare queste lettere con i 
        timbri postali proprio per far vedere che sono autentiche 


<sp>


p1#28:  potete anche / non ci sono / il mittente sta dietro <eh> potete<ee> 
        stringere un pochettino l'allargatura 


p2#29:  #<p1#30> {[whispering] <unclear>}# <unclear>


p1#30:  #<p2#28> non si pu� mi#


<sp>


p1#31:  allora devo fare , come ? {<NOISE> cos�}?


<lp>


p1#32:  vabb� a una a una , dai <NOISE>


<lp>


p1#33:  vado bene? 


<lp>


p1#34:  ecco <sp> il timbro postale � questo <eh> non si vede molto bene per� 
        ecco questo <eh> <sp> s�


<sp>


p1#35:  e poi c'� questa , questa � arrivata con una lettera semplice ecco 
        vedete il timbro , vedete , sono lettere vere <eh> 


<sp> 


p1#36:  non<nn> <lp> non sono delle buste {<NOISE> cos� preparate ad hoc} , 
        allora


<lp>


p1#37:  state tranquilli tanto il mittente sta dietro <eh> in tutte e due 
        quindi <lp> c'� 


<sp> <inspiration> 


p1#38:  allora <sp> Francavilla Fontana <lp> e poi c'� la data 
        va bene 


<inspiration> 


p1#39:  gentilissima signora sono <lp> e poi dice il nome , io 
        non ve lo posso ripetere


p1#40:  il , e qua dice la qualifica professionale , di Francavilla Fontana 


<inspiration>


p1#41:  la devo ringraziare di cuore , con  l'estrazione di 
        sabato scorso ho vinto quasi lire cinquecentomila.
