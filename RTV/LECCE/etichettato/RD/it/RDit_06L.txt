TEXT_inf.

MAT:		RD
MAP:        
TRM:		it
Ndl:
Ntr:		06
Nls:
REG:		L


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		Lecce
DAT:
DUR:		1.02,025
CON:


TRANSCRIPTION_inf.

DAT:		23/03/02
CMT:
Nst:		11


p0#1:  io<oo> quando ero pi� piccolino ascoltavo , anche se ormai erano un pochettino passati di moda <sp> i Beatles 
       {<MUSIC> da l� son partito io praticamente son partito da l�}


<sp>


p0#2:  <laugh> {[laughing] +stalgia i Bi+ <laugh>}


p0#3:  ho detto una cosa Sandro , gi� subito mi mandi la canzone 


<inspiration> <sp>


p0#4:  e i Beatle+ cosa serissima guarda che <eeh>


p0#5:  George Harrison<nn> , sapete un po' tutti insomma ci ha lasciato un altro
       dei Beatles


<sp>


p0#6:  ormai so' rimasti Paul McCartney <inspiration> e Ringo Star George Harrison era <inspiration> 
       <eeh> quello<oo> <ehm> il <sp> pi� diciamo il pi� <sp> timido il pi� pi� restio a interviste , a farsi vedere era era 
       forse quello quello pi� umano


<inspiration> 


p0#7:  e<ee> quello che magari<ii> scriveva meno canzoni rispetto a Paul McCartney e a 
       John Lennon , loro ne scrivevano tantissime


<sp>


p0#8:  per�<oo> le canzoni che scriveva lui <sp> erano <sp> bellissime e so' rimaste nella 
       storia della musica <inspiration>


p0#9:  proprio come <sp> la canzone che ho deciso di <sp> di farvi ascoltare 
       per<rr> ricordare il grande George


<inspiration>


p0#10:  �<ee> una<aa> canzone che<ee> un'altra grande voce della musica internazionale un certo Frank Sinatra 
        ha definito la pi� bella canzone d'amore mai scritta si chiama Something 


<sp> 


p0#11:  un omaggio <sp> a George Harrison


