TEXT_inf.

MAT:		RD
MAP:
TRM:		dc
Ndl:
Ntr:		04
Nls:
REG:		N


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC
LOC:		Napoli 
DAT:		10/02/01
DUR:		0.41,270
CON:		molto buone


TRANSCRIPTION_inf.

DAT:		10/05/01
CMT:
Nst:		6


p0#1:  <inspiration> un saluto<oo> a tutta Ischia e una buona giornata una
       buona mattinata mentre noi parliamo
       adesso <inspiration> di Bill Clinton che ha restituito parte dei regali <tongue-click>


p0#2:  <inspiration> <tongue-click> la casa bianca e' rientrata in possesso di parte dei regali
       offerti a Bill Clinton <inspiration> durante gli otto anni del doppio mandato alla
       presidenza degli Stati Uniti


       <sp>


p0#3:  <inspiration> la restituzione comprende mobili per un valore di cinquanta milioni


       <sp>


p0#4:  <tongue-click> {<inspiration> i} Clinton li avevano portati con s� insieme ad altri doni <inspiration>
       per un valore complessivo di circa quattrocento milioni quando lasciarono la casa bianca il venti gennaio


       <sp>


p0#5:  <inspiration> l'ex presidente e sua moglie , hanno insistito sul fatto che si tratta 
       di doni fatti a loro personalmente


p0#6:  <tongue-click> <inspiration> hai capito l'antifona ?
