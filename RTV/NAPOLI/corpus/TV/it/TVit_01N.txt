TEXT_inf.

MAT:		TV
MAP:
TRM:		it
Ndl:
Ntr:		01
Nls:
REG:		N


SPEAKERS_inf.

INp1:		F
INp2:		M


RECORDING_inf.

TYP:		PC
LOC:		Napoli
DAT:		08/02/01
DUR:		9.55,827			
CON:		buone


TRANSCRIPTION_inf.

DAT:		10/05/01
CMT:		la trascrizione va da 33 sec. a 5m 50s per un totale di 5m 17s 
Nst:		44


p1#1:  <inspiration> una brevissima riflessione prima di incominciare la nostra puntata <inspiration>


p1#2:  in genere <sp> <tongue-click> <sp> <tongue-click> i fatti <sp> di cronaca nera <sp> <tongue-click> interessano sempre moltissimo


p1#3:  <inspiration> e<ee> i giornali ne sono pieni <sp> <tongue-click> <sp> le<ee> televisioni <sp> <cough> <sp> 
       lasciano molto spazio <sp> a queste notizie <sp> e forse � anche un po' colpa nostra 
       che <sp> <tongue-click> ci interessiamo molto a queste cose 


p1#4:  <inspiration> e quindi <tongue-click> sentiamo per minuti minuti minuti parlare <tongue-click> di grossi personaggi , che sono scomparsi
       <inspiration> in circostanze oscure


p1#5:  <tongue-click> e quindi si indaga , se ne parla , ma per giorni giorni e giorni <NOISE>


p1#6:  volete provare a fare una cosa <sp> cos� come m'� capitata a me questa 
       mattina al risvegli+ <inspiration>


p1#7:  provate a pensare <sp> questa notte <sp> in Italia <sp> in Campania <sp> <tongue-click> forse in tutto il mondo <sp>
       quanti <lp> cosiddetti barboni <sp> <tongue-click> sono morti per il freddo <lp> per la fame 
       <sp> nessuno lo sa


p1#8:  <tongue-click> nessuno ne parla <lp> forse non sanno neanche dove metterli dopo morti


p1#9:  e passano cos� <sp> da un silenzio <sp> all'altro <sp> <inspiration> il silenzio della loro vita <sp> al silenzio
       della loro vita <sp> ormai eterna


p1#10:  <tongue-click> pensiamo un attimo a loro , anche se non li conosciam+


p1#11:  ma rivolgiamo un pensierino anche a loro


