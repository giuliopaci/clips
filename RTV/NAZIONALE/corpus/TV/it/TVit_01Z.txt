TEXT_inf.

MAT: 		TV
MAP:
TRM:		it
Ndl:
Ntr:		01
Nls:
REG:		Z


SPEAKERS_inf.

INp1: 	        M
INp2:		M


RECORDING_inf.

TYP:	PC
LOC:	Roma
DAT:	11/02/01
DUR:	4.37,606
CON: 	buone


TRANSCRIPTION_inf.

DAT:		27/04/01
CMT:		qualche rara sovrapposizione tra i parlanti
Nst:		52



p1#1:  <NOISE> ci fa fare le ore piccole <sp> ed � il direttore pi� imitato 
       della televisione italiana <sp> oltre che il pi� prolifico <sp> 
       <inspiration> a livello di<ii> scrittura <inspiration> � Gabriele La 
       Porta


p1#2:  mi vie+ mi fa piacere dire buongiorno Gabriele <sp> buongiorno


p2#3:  <eh> beh certo finalmente <NOISE>


p1#4:  {<NOISE> direttore di <sp> Rai Notte <sp> nonch� autore <lp> di un libro 
       che ha portato qui che si intitola <sp> Donne magiche <sp> Eri Editore 
       <inspiration>}


p1#5:  sono storie di donne


p2#6:  sono storie di donne<ee> come spesso nei miei libri <sp> ma di donne 
       con <eeh> come dire quel senso di brillantanza


p2#7:  insomma <sp> sono donne con grandi intuizioni con 
       facolt� che possiamo definire <inspiration> sommariamente un po' 
       parapsicologiche insomma


p2#8:  sono storie <sp> al limite , ai, al confine


p1#9:  ma quanto ti piace questo sottile confine tra <sp> <inspiration> realt� , 
       e<ee>, qualcosa che sfugge all'umana comprensione


p2#10:  Beh intanto Shakespeare <sp> <inspiration> 
        {[dialect] c'arricorda} <sp> <eh> come si dice a Roma , {[dialect] 
        c'arricorda} che noi siamo <sp> <inspiration> fatti dello stesso tessuto
        <sp> dei sogni


p2#11:  <sp> e a parte la battuta <ii>in romanesco <clear-throat> in effetti 
        <cough> io non sono , sono /


p2#12:  certo <sp> che la vita non � soltanto<oo> <sp> un momento razionale


p2#13:  <eeh> la vita � *fatto di altro , fatto di grandi intuizioni , di 
        grandi sentimenti , � *fatto anche di<ii> <sp> <tongue-click> / la vita 
        <sp> ha un tessuto <lp> che � *inespiegabile , e qu+ e questo , *inespiegabile ,
        � la cosa che pi� mi interessa


p2#14:  � il mistero insomma


p1#15:  e <unclear> tu m'hai fatto conoscere un altro libro <sp> niente succede 
        per #<p2#16> caso#


p2#16:  <eh> #<p1#15> s�#


p1#17:  ma � vero ?


p2#18:  <eeh> come no ! <unclear> che Jung <sp> tutti <sp> lo citano giustamente come <sp>
        uno dei padri della psicanalisi , ma nel ma , ci si dimetica che gran 
        parte del suo lavoro era stato dedicato alle coincidenze <lp> appunto 
        <sp> nulla , accade per caso

la sequenza intellegibile potrebbe essere un pensate o immaginate ma viene pronunciato a bassa voce e in modo molto confuso


p1#19:  +ulla accade per caso


p2#20:  e questo libro � pieno / donne magiche / di coincidenze <eh> !


p1#21:  e infatti , e non solo coincidenze , <sp> ma anche <sp> immagini <eeh> 
        immagini che caratterizzano graficamente anche<ee> <sp> anche il libro


p1#22:  <eeh> abbiamo la tempesta del Giorgione , la via lattea di Stevens <ehm> 
        che fanno diciamo da da giusto contr+ contrappunto <eh> ? <inspiration> 
        alle tue alle tue , parole e ai ricordi e ai racconti di queste 
        donne


p1#23:   secondo te , veramente hanno una marcia in pi� le donne come<ee> 
        intuizione <unclear> ?


<unclear>


p2#24:  io sono per il matriarcato l'ho scritto anche in altri volumi


p2#25:  sono convinto che le donne hanno pi� intuizione <sp> <inspiration> sono 
        <sp> tra l'altro pi� gentili <sp> e che l / il che non guasta sono pi� rispettose <unclear>


p1#26:  magari <unclear> non al volante


p2#27:  no , ma questo � un luogo comune <laugh> ma ma <unclear> facciamole 
        portare anche i camion


p1#28:  no , ma per carit� � solamente che ormai hanno reazioni<ii> <uu>uguali 
        se non maggiori a quelle degli uomini in mezzo alla strada


<unclear>


p2#29:  io non sto parlando di imitazioni dell'uomo non<nn> c'� nessun bisogno 
        di imitare l'uomo , <nn>noi ci abbiamo gi� le scatole piene degli uomini


p1#30:  s� <sp> no cio� <unclear> parliamo di queste<ee> di queste donne magiche 
        , di quest'intuizione 


p2#31:  s� , <inspiration> beh dunque l'intuizione <sp> � una facolt� 
        extrasensoriale obbiettivamente <sp> � solo che noi l'abbiamo 
        dimenticata completamente anzi ce ne vergognamo molto , <inspiration> e
        molte di queste protagoniste e queste sono trentatre storie 
        <inspiration> hanno avuto dei problemi proprio perch� avevano queste 
        facolt�<aa> , queste possibilit�


p1#32:  senti , prima mi stavi<ii> raccontando una cosa che � molto bella 
        <inspiration> <eeh> diciamola anche a chi ci sta seguendo da casa


p2#33:  <unclear> <sp> stavamo <eh> ?


p1#34:  stavamo chiacchierando


p2#35:  tra queste storie una che ti � piaciuta particolarmente<ee> avvenne in 
        una <inspiration> citt� dell'Umbria dove c'� una signora che aveva 
        vent'anni all'epoca , la lettera me l'ha scritta molti anni dopo


p2#36:  aveva vent'anni quando capita questo <sp> <inspiration> episodio <sp> 


p2#37:  <eeh> sta in attesa di un'amica <sp> <eeh> sotto <sp> un palazzo


p2#38:  si avvicina una bambina <sp> che <sp> la fa spostare senza dire una 
        parola la fa spostare di un <sp> un metro circa <sp>


p2#39:  il tempo che si sposta <sp> cade un cornicione del palazzo <sp> e <sp> 
        la manca per un poco <sp>


p2#40:  se la bambina non l'avesse spostata <sp> <eeh> sarebbe morta


p1#41:  certo


p2#42:  fino a qui � tutto semplice , poi in realt� il percorso � molto 
        dettegliato adesso qui salto


p1#43:  il flashback


p2#44:  il flashback � questo che la signora ha oltre settant'anni <inspiration> 
        quindi oltre mezzo secolo dopo questo episodio <inspiration> <sp> capita 
        , per una serie di coincidenze , in una casa di campagna <inspiration> 
        <lp> e ve+ vede un dipinto <sp> dove c'era rappresentata proprio quella 
        bambina che l'ha salvata


p2#45:  il punto � questo , <sp> che quando la bambina la salv� <sp> era gi�  
        morta da circa un secolo <lp>


p2#46:  questo arriva al punto

la parola arriva � detta in modo velocissimo


p2#47:  ora , questa storia , che mi ha raccontata ce ne sono molte io ho notato 
        <sp> in tutte le latitudini <sp> storie che si assomigliano e perch� 
        evidentemente questi fatti accadono molto di pi� di quanto noi pensiamo 
        , soltanto che non<nn> si desta nessuna attenzione <sp> #<p1#48> noi 
        cadiamo perfino fino a pensare#


p1#48:  #<p2#47> magari ci si vergogna# anche<ee> #<p2#49> <sp># a raccontare 
        per cui 


p2#49:  s� <unclear>


p1#50:  pu� essere un modo per per ritrovarsi e cercare di capire qual � la 
        trama di questo tessuto chiamato vita


p1#51:  donne magiche <sp> Gabriele La Porta <sp> grazie Gabriele


p2#52:  grazie a te , grazie a te <NOISE>
