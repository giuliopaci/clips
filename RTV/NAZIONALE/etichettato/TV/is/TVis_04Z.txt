TEXT_inf.

MAT: 		TV
MAP:
TRM:		is
Ndl:		
Ntr:		04
Nls:
REG:		Z


SPEAKERS_inf.

INp0: 	        M


RECORDING_inf.

TYP:	PC
LOC:	Roma
DAT:	12/02/01
DUR:	1.00,613
CON: 	buone


TRANSCRIPTION_inf.

DAT:		14/04/01
CMT:		
Nst:		9


p0#1:  stava rientrando a casa dopo una serata in discoteca Giovanni Putignano   
       di ventitre anni che la scorsa notte ha perso la vita sulla statale Noci-   
       Putignano in provincia <sp> di Bari


p0#2:  il giovane in compagnia di un amico viaggiava a velocit� sostenuta 
       <inspiration> ha perso il controllo dell'auto s'� cappottata e poi � 
       finita contro il guardrail 


p0#3:  nulla da fare per il conducente morto sul colpo <sp> illeso <sp> l'amico


p0#4:  per introdursi negli appartamenti <sp> <inspiration> e per rubare <sp>   
       non usava solo il tradizionale piede di porco ma scalava pareti o si 	 
       calava dall'alto


p0#5:  cos� il ladro arrestato <inspiration> a Padova <sp> agiva � un uomo di  
       cinquantaquattro anni lo vene+ lo vedete , ritenuto responsabile di una 
       serie di furti � stato subito <inspiration> soprannominato anche al di l� 
       del suo aspetto l'uomo ragno


p0#6:  a casa sua i carabinieri hanno trovato mille chiavi di appartamenti , una 
       pistola <sp> attrezzatura per scalare pareti <inspiration> e forzare 
       porte


p0#7:  recuperata <sp> quella che vedete <sp> che � parte <sp> della <sp> refurtiva


p0#8:  domani prima domenica a piedi del duemilauno in 
       centocinquanta citt� <sp> italiane


p0#9:  quasi diciassette milioni gli italiani <sp> <inspiration> che lasceranno 
       a casa l'auto fra questi i romani che non potranno <sp> usarla 
       <inspiration> per entrare <sp> nel centro storico 
