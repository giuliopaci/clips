TEXT_inf.

MAT: 		TV
MAP:
TRM:		it
Ndl:
Ntr:		03
Nls:
REG:		Z


SPEAKERS_inf.

INp1: 	        M
INp2:		F
INp3:		M
INp4:		F
INp5:		F


RECORDING_inf.

TYP:	PC
LOC:	Roma
DAT:	11/02/01
DUR:	5.31,046
CON: 	buone


TRANSCRIPTION_inf.

DAT:		29/04/01		
CMT:		qualche sovrapposizione di applausi o risate in alcuni punti; p1 e p5 hanno un leggero accento regionale
Nst:		85

p1#1:  vogliamo scegliere il segno ?


p2#2:  vergine

la presa di parola � molto imprecisa nell'articolazione e risente probabilmente dell'accento della locutrice


p1#3:  oh scegliamolo vah ! <eh> ?


p2#4:  s�


<MUSIC>


p1#5:  quale ? <sp> come hai detto ? non ho capito


p2#6:  vergine


p1#7:  <MUSIC> vergine <lp> vediamo vediamo <sp> sole ! <MUSIC> <oh> {<NOISE> 
       ciao , ciao <sp> ciao}


p2#8:  {<NOISE> grazie molte


p1#9:  <unclear>


p3#10:  allora in questo momento ti sembra di avere tutto e ti sembra di non 
        avere nulla <sp> 


p3#11:  � un periodo un po' particolare , conflittuale , perch� abbiamo <sp> 
        <inspiration> uno splendido Saturno che � un pianeta che d� molta 
        energia , <inspiration> che regala anche successo <inspiration>


p3#12:  i Vergine che hanno soprattutto un'attivit� indipendente sotto questo 
        punto di vista <inspiration> dovrebbero essere soddisfatti


p3#13:  dall'altra per� questo <sp> <inspiration> Giove difficile che in qualche 
        modo ti dice non ti accontentare <inspiration> puoi avere di pi� , o che 
        comunque <inspiration> pone un limite a certe soddisfazioni segno 
        evidente che entro tre quattro mesi <inspiration> tu dovrai 
        rincominciare <sp> a contrattare <eeh> dovrai di nuovo <inspiration> 
        stringere nuovi patti e nuove alleanze <inspiration>


p3#14:  attenzione alla salute ! <inspiration> <sp> 


p3#15:  l'unico punto debole di questo oroscopo <inspiration> cerca di 
        riguardarti di pi� , perch� non sei di ferro / nessuno di noi / 
        <inspiration>


p3#16:  naturalmente hai energie illimitate , ultimamente ti sei dato troppo da 
        fare , troppo stressato <inspiration> proprio per il lavoro la famiglia 
        , mi raccomando


p1#17:  mi raccomando <sp> <aa>altra telefonata ! <NOISE> pronto ? 


p4#18:  pronto ? Carlo ?


p1#19:  s�


p4#20:  ciao , sono Nicoletta , chiamo da Bari


p1#21:  Nicoletta <sp> ho capito il tuo nome , <nn>non ho capito da dove ci 
        chiami 


p4#22:  da Bari


p1#23:  da Bari , tutto bene ?


p4#24:  s�


p1#25:  <ah> <NOISE>


p4#26:  <unclear>


p1#27:  allora forza ! <NOISE> forza ! scegliamo il segno , vai ! <sp>


p4#28:  Capricorno


p1#29:  Capricorno <MUSIC> <sp> Capricorno ? <NOISE> ahi ahi ahi <NOISE> ahi ahi 
        ahi <sp> ciao Nicoletta


p3#30:  allora <inspiration> oh <NOISE> capricorno penseranno che io <inspiration> <eeh>  
        l'abbia con questo segno , invece non � cos� , tra l'altro anche il mio 
        ascendente � Capricorno quindi mi darei la zappa sui piedi <inspiration>


p3#31:  la cosa che per� noto � che questi capricorno adesso <inspiration> con 
        la scusa della pausa di riflessione stanno prendendo tempo e non 
        riescono poi invece <inspiration> a <sp> prendere delle decisioni


p3#32:  ecco perch� ti trovi <inspiration> <sp> in una condizione di stallo 
        <inspiration> <sp> ed � qualcosa che ti d� molto fastidio <inspiration> 
        <sp> 


p3#33:  tu ami la tranquillit� e attorno a te non c'� la tranquillit� 
        <inspiration> <sp>


p3#34:  tu vorresti che alcune persone ti aiutassero <inspiration> a fare
        determinate cose , a portare avanti determinati tuoi progetti 
        <inspiration> e poi alla fine stringi stringi ti ritrovi da solo
        <inspiration>


p3#35:  ecco c'� questa strana <inspiration> <sp> sensazione <sp> di lottare 
        quasi inutilmente <inspiration> 


p3#36:  la prossima settimana vede un luned� , la giornata di domani , non 
        eccellente <inspiration> <sp> marted� e mercoled� grande energia 
        <inspiration> <sp> domenica prossima , super energia <inspiration> 
        con una luna che si trover� proprio nel tuo segno zodiacale , {<NOISE> 
        una domenica ideale per amare , mi raccomando}

la sequenza mi raccomando � detta velocemente e molto disturbata dal rumore sottostante


<MUSIC>


p1#37:  pronto ? <NOISE>


p5#38:  {<NOISE> pronto}


p1#39:  {<NOISE> chi � in linea ?}


p5#40:  buonasera Carlo


p1#41:  buonasera ! Senti che voce allegra e pimpante !


p5#42:  sei bellissimo


p1#43:  chi ?


p5#44:  {<NOISE> mi piaci molto , sei un simpaticone}


p1#45:  da dove ? <NOISE>


p5#46:  � vero , � vero


p1#47:  da dove ci chiami ? 


p5#48:  da Avellino


p1#49:  {<NOISE> <ah> la vedi male raiuno allora}


p5#50:  {<NOISE> no , la vedo benissimo} sei <ss>stupendo


p1#51:  no , no troppo buo+


p5#52:  te lo dico col cuore


p1#53:  da dove ? <vocal>


p5#54:  provincia di Avellino


p1#55:  dev'esse' la mia zia , che c'ho una zia <NOISE>


p5#56:  <laugh> <oh> mi fai cos� vecchia !


p1#57:  no , no , no , no


p5#58:  io sono giovanissima <laugh>


p1#59:  sche+ scherzo <sp> scegliamo il segno di in bocca al lupo , vai !


p5#60:  s� <sp> io<oo> ho preso il mio segno<oo> <sp> , quello della bilancia , 
        e speriamo bene


<MUSIC>

p1#61:  e vai , vai <sp> <MUSIC> vediamo un po' cosa nasconde la bilancia<aa> 
        <NOISE> <sp> il grande Renato Zero <ah> <ah> <sp> che � nato il trenta 
        settembre <inspiration>

la parola bilancia ha un significativo allungamento della A finale


p1#62:  per la gioia di tutti i sorcini di Roma <sp> per ora � stato inaugurato 
        proprio in questi giorni lo Zero folle <lp> <clear-throat> che 
        consentir� ai numerosi fans del grande Renato Zero di <sp> farsi un giro
        by night della capitale ascoltando le sue canzoni <inspiration> di 
        maggior successo , standosene comodamente seduti <MUSIC> 
        <ss>sorseggiando un bel drink

lo schiarimento di voce � appena percettibile


p1#63:  ma , <sp> su quale mezzo <sp> avviene tutto questo ?


p5#64:  aiutami , dai<ii> !


p1#65:  <eh> pensa un po' , se uno <sp> girella per Roma 


p5#66:  girella ?


p1#67:  pensa al carrozzone per� <sp> #<p5#68> fallo <unclear># aspetta<aa> ho 
        fa+ , fam+ , hai detto carrozzella no , ha detto , carrozzella no stavi 
        per di' carrozzella no <sp> <NOISE> no ?


p5#68:  #<p1#67> carrozzella# s� , no no stavo dicendo stavo cercando / ad alta 
        voce pensavo


p1#69:  <oh> ecco ! <sp> allora <lp> notaio , <eeh> pensava ad alta voce <oh> 
        <eeh> <sp> allora , pensa al carrozzone , ma <sp> fai diventare moderno 
        il mezzo <lp>


p5#70:  <ah> <mh> <sp> quella<aa> grossa macchina<aa> <mh> <lp> <NOISE> <lp> 
        <NOISE> <ah> ! ma<aa> , che dicevi i+ il pattino , il monopattino ?


p1#71:  <NOISE> ora scusa <eh> , <sp> ora te mi devi spiegare <sp> come si fa ad 
        andare in tanti <sp> e bere un cocktail ascoltando la musica sul 
        monopattino


p5#72:  <ah> ! io non avevo #<p1#73> capito in tanti# , io non avevo capito in 
        tanti


p1#73:  #<p5#75> ma come si fa ?# in tanti ! si sale sopra , si paga il 
        biglietto , e poi si seguono


p5#74:  il treno !


p1#75:  per Roma ? <sp> per Roma !


p5#76:  il trenino


p1#77:  il tr+ tr+ 


p5#78:  trenino


p1#79:  <vocal> tr+ <vocal> <laugh> tr+ <vocal> come te lo devo dire ?


p5#80:  il tram<mm> ! {<NOISE> il tram<mm> <sp> il tram} il tram


p1#81:  il <tt>tram ! punto 


p5#82:  tram


p1#83:  <NOISE> l'accettiamo ? <sp> s� , l'accettiamo <NOISE>


p5#84:  <laugh>


p1#85:  brava !

