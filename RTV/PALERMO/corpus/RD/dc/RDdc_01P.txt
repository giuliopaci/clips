TEXT_inf.

MAT: RD
MAP:
TRM: dc
Ndl:
Ntr: 01
Nls:
REG: P


SPEAKERS_inf.

INp1: M
INp2: F


RECORDING_inf.

TYP: DAT
LOC: Palermo
DAT:          
DUR: 2.15,053          
CON:


TRANSCRIPTION_inf.

DAT: 03/07/02
CMT:
Nst: 7


p1#1:   vediamo Dante <sp> se � maestro anche <sp> nel suggerirci questa 
        tempestivit� <inspiration> nelle nostre <sp> azioni


<sp>


p1#2:   <inspiration> <vocal> Dante <breath> <sp> per capire un p� le parole 
        del divino poeta <sp> <vocal> si trova nell'antipurgatorio <sp>           


p1#3:   <inspiration> e <sp> la posizione di Dante � una posizione 
        particolare <sp> perch� <sp> mentre le anime non proiettano l'ombra <breath> <sp>
        <eh> perch� sono <sp> diafane <sp> sono <sp> impalpabili


p1#4:   <vocal> Dante <sp> essendo corpo proietta l'ombra <vocal> e ci� <sp>       
        suscita stupore tra<aa> i penitenti


p1#5:   <vocal> ed � naturale � un paradosso <sp> cio� nell<ll>'aldil� <sp>  
        <vocal> un corpo <sp> crea lo stesso stupore che sulla terra   
        creerebbe in noi la presenza <sp> #<p2#6> di un'anima#


p2#6:   #<p1#5> di uno {<laugh>spirito}# 


p1#7:   {<laugh> e allora} dicono subito <sp> ma guarda ma <unclear>       
        <breath> chi � colui ? <sp> guarda proietta l'ombra







 
