TEXT_inf.

MAT:		TV
MAP:  
TRM:		is
Ndl:  
Ntr:		01
Nls:
REG:		E


SPEAKERS_inf.

INp1:		M
INp2:		M


RECORDING_inf.

TYP:		PC
LOC:		Parma
DAT:  
DUR:		1.12,500
CON:		buone


TRANSCRIPTION_inf.

DAT:  
CMT:		trascritto da 5"008ms a 1'05"613ms (totale 1'00"605ms)
Nst:		7


p1#1:  ecco da questo punto di vista la la normativa prevede una forte 
       <inspiration> una forte riduzione di questi 


p1#2:  in particolare <inspiration> come dicevo per quanto riguarda i<ii> 
       commercianti e gli artigiani nel senso che per quanto riguarda gli 
       artigiani <sp> <tongue-click> si provveder� al rilascio solamente 
       <inspiration> a coloro che svolgono operazioni di <ehm> pronto intervento 
       <inspiration> <eeh> quindi pronto intervento la manutenzione e rep+ / e la 
       riparazione


p1#3:  per quanto riguarda invece i commerciamti <inspiration> <eeh> se prima il 
       permesso riguardava <inspiration> <eeh> tutta la giornata , riguardava il 
       singolo commerciante a questo punto {<NOISE> riguarder�} solamente <inspiration> 
       <eeh> e sar� <eeh> consentito solo per *otperazioni di <sp> carico 
       scarico <sp> e solamente in due fasce orarie


p1#4:  <inspiration> che sono una al <eeh> nel+ nella prima mattina 
       <inspiration> e un'altra nel primissimo <sp> pomeriggio 


p1#5:  <inspiration> e questo per cercare naturalmente di <eeh> razionalizzare , 
       � chiaro che poi <inspiration> <sp> comunque qualche eccezione esiste 
       anche per quanto riguarda i commercianti e gli artigiani , in particolare 
       per chi trasporta beni deperibili e quant'altro , che non avr� questa 
       limitazione delle due fasce orarie


p1#6:  <inspiration> quindi queste son le due categorie <sp> <tongue-click> 
       molto probabilmente dove ci sar� la razionalizzazione maggiore


p2#7:  quindi i tagli si annunciano<oo> imponenti , migliaia di permessi non 
       saranno rinnovati
