TEXT_inf.

MAT:		RD
MAP:		
TRM:		dc
Ndl:		
Ntr:		02
Nls:		
REG:		R


SPEAKERS_inf.

INp1:		M
INp2:		F


RECORDING_inf.

TYP:		PC
LOC:		Roma
DAT:		17/01/01
DUR:		1.47,197
CON:		buone


TRANSCRIPTION_inf.

DAT:		06/05/01
CMT:		della durata totale del file sono stati trascritti circa 54 sec
Nst:		6


p1#1:  la vita in fabbrica , a parte l'idea del posto fisso , che in quegli 
       anni l� era fortissima nel nostro paese ancora <inspiration> <eeh> ma la 
       vita in fabbrica costituiva anche un momento di aggregazione molto 
       importante


p1#2:  perch� soprattutto negli anni settanta le lotte che c'erano state avevano 
       costituito un tessuto sociale dentro queste grandi fabbriche 
       <inspiration>


p1#3:  e da un giorno all'altro <sp> delle persone <sp> normali <sp> che 
       avevano<oo> una vita normale una famiglia delle abitudini , si 
       ritrovano <sp> assolutamente senza niente <inspiration> <sp>


p1#4:  senza pi� quel tipo di socialit� , senza PI� uno stipendio , senza pi� un 
       futuro <inspiration>


p1#5:  e molte molte <unclear> sono , un centinaio<oo> per quanto ne so , molte 
       di queste persone , non hanno retto <sp> questo impatto e sono 
       morte <inspiration>


p1#6:  sono morte perch� si sono tolte la vita


p2#7:  certo non � il caso di aprire qui un'altra parentesi , per� purtroppo � 
       una situazione vissuta negli anni ottanta ma che ancora oggi 
       <inspiration> , non con queste<ee> cifre per� ha un'attualit�<aa> 
       sconvolgente


