TEXT_inf.

MAT: 		RD
MAP:		
TRM:		it
Ndl:		
Ntr:		04
Nls:		
REG:		R


SPEAKERS_inf.

INp1:		M 
INp2:		M 
INp3:		M 		


RECORDING_inf.

TYP:		PC
LOC:		Roma
DAT:		22/01/01
DUR:		8.02,377
CON:		abbastanza buone


TRANSCRIPTION_inf.

DAT:		30/01/02
CMT:		leggero fruscio; della durata totale del file sono stati trascritti 2m 58s
Nst:		31


p1#1:  <eh> Daniele se devo dirti proprio<oo> la canzone Losing my religion 
       proprio cos� la / � stata <ss>sicuramente<ee> la canzone che ha 
       caratterizzato la mia adolescenza con cui <inspiration>


p1#2:  che � legata soprattutto questa canzone a <sp> ad una ragazza ovviamente 
       quindi insomma una cosa abbastanza<aa> <sp>


p1#3:  quando sento Losing my religion veramente rivivo a pieno <sp> tutta la 
       mia <inspiration> adolescenza invece se devo dirti proprio il cantante<ee> <sp> 
       Fabrizio De Andr� <sp>


p1#4:  abbiamo ricordato <sp> due venerd� fa <sp> � stato il cantante della <sp> 
       della mia adolescenza per� la canzone <sp> � stata Losing my religion


p2#5:  ma io non vorrei soffermarmi <sp> su una canzone bens� di andare forse anche 
       un po' pi� indietro nel senso / al periodo delle medie <sp> all'atmosfera 
       delle feste delle medie in <sp> in cui <sp> ogni <eeh> canzone aveva il suo 
       momento particolare


p2#6:  quando si ballava tutti insieme quando c'era <sp> il lento quando si 
       facevano <sp> i giochi


p2#7:  ecco quella � un'atmosfera secondo me quella delle feste delle medie 
       che<ee> <lp> ricordo con piacere della mia adolescenza proprio perch� <sp> � 
       forse l'unica che veramente non si pu� ricreare <ehm> sotto nessun 
       aspetto 


p1#8:  <eh> ! s�, soprattutto l'innocenza dei primi balli cio� il momento del <sp> 
       chiedere se vuoi <sp> vuoi vuoi #<p2#9> ballare con me ?# <lp> #<p3#10> 
       balleresti con me?#


p2#9:  #<p1#8> <laugh> la tenzione anche#


p3#10:  #<p1#8> e soprattutto la distanza tra# ragazzo e ragazza nei #<p1#11> 
        balli# di coppia 


p1#11:  #<p3#10> <eh> ? <lp> s�# assolutamente freddi rigidi <inspiration> 
        #<p3#12> <eheh> ! due#


p3#12:  #<p1#11> terrificanti# 


p1#13:  due ghiaccioli


p3#14:  <ah> e invece <sp> <vocal> pensando a questo mi � venuto in mente forse il 
        perch� della mia antipatia nei confronti<ii> del povero Baglioni che in 
        realt� non ha fatto niente di male <inspiration>


p3#15:  solo in quel per+ / mio periodo di <sp> inadeguatezza <lp> <eeh> tutte 
        le ragazze quando sentivano la canzone di Baglioni che so Piccolo grande 
        amore <sp> sceglievano il bello della situazione per <eeh> andare a 
        ballare e io <sp> ovviamente rimanevo fuori da questa <inspiration> da 
        questa scelta


p3#16:  per cui il mio odio per quella canzone <sp> era spassionato mentrre <sp> 
        <eeh> la prima canzone pi� cos� <sp> {<MUSIC> che ti<ii> ricorda i primi 
        innammoramenti � una delle prime canzoni di Madonna degli anni ottanta 
        queste canzoni qui}


p2#17:  {<MUSIC> � vero avrei detto anch'io pe+ / se se avessi dovuto dire un 
        cantante Madonna} <sp> cio� era proprio la cantante di quegli anni 
        almeno della mia adolescenza


p1#18:  comunque volevo <sp> dire soltanto una cosa alle nostre <sp> 
        ascoltatrici perch� Daniele parla continuamente del suo essere brutto 
        nel periodo adolescenziale <sp> #<p3#19> e volevo / ma#


p3#19:  #<p1#18> confermare# che so' #<p1#20> rimasto uguale#


p1#20:  #<p3#19> no no <sp> volevo# <lp> volevo tranquillizzarle <sp>


p1#21:  cio� <sp> assolutamente � migliorato nel corso degli anni perch� visto 
        che lo ripeti continuamente <inspiration> magari anche <sp> la mamma se 
        � a casa signora suo figliolo <sp> � un bel figliolo


p3#22:  <vocal> #<p2#22> io<oo>#


p2#23:  #<p3#22> questa# � una scelta della radio per�


p3#24:  temevo che potesse<ee> dire che invece non � cambiato affatto da<aa> 
        dall'epoca <inspiration>


p3#25:  no e comunque proprio le canzoni sono quelle che hanno segnato dicevamo 
        maggiormente <sp> credo <sp>


p3#26:  se ricorda il primo<oo> <sp> il primo amore la<aa> <vocal> c'� qu+ / in 
        voi questa associazione <sp> di idee tra <sp> la canzone <sp> un po' 
        alla suona la nostra canzone Sam questa <lp> questo modo di fare <lp> 
        ricordare una ragazza associata ad una canzone ?


p1#27:  s� l'ho fatto<oo> l'ho fatto prima <sp> una ragazza a Losing my religion 


p3#28:  #<p2#29> per quanto riguarda#


p2#29:  #<p3#28> no , io non# ho un ricordo <lp> legato all'adolescenza ad una 
        canzone e ad una ragazza <sp>


p2#30:  su questo <lp> non {<NOISE> l'ho per�} questo ovviamente<ee> � una mia 
        questione <inspiration> personale ecco pi� l'atmosfera e quella colonna 
        sonora degli anni dell'adolescenza senza dubbio Madonna<aa> <lp> i Duran 
        Duran <sp>


p2#31:  <NOISE> insomma quei cantanti un po' cos� degli anni ottanta 
