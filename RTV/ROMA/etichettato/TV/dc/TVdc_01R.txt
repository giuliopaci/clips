TEXT_inf.

MAT: 		TV
MAP:		
TRM:		dc
Ndl:		
Ntr:		01
Nls:		
REG:		R


SPEAKERS_inf.

INp1:		M
INp2:		F


RECORDING_inf.

TYP:		PC
LOC:		Roma
DAT:		23/01/01
DUR:		2.36,415
CON:		buone


TRANSCRIPTION_inf.

DAT:		01/05/01
CMT:		della durata totale del file sono stati trascritti 51 sec circa
Nst:		11

p1#1:  <inspiration> per esempio nell'artrosi questa testa � deformata �
       ovalizzata


p1#2:  <inspiration> e ci sono gli osteofiti


p1#3:  <inspiration> sono i fenomeni che si verificano in qualsiasi<ii> <sp>
       sede colpita da artrosi


p1#4:  <inspiration> oppure per esempio <sp> <inspiration> <eeh> pu� essere
       osteoporotica


<sp>


p1#5:  e allora dobbiamo pensare a una malattia particolare che � la sindrome a
       spalla mano <inspiration>


p1#6:  oppure #<p2#7> <eeh> spalla mano# , sindrome / � un'algodistrofia che
       colpisce la mano con un <inspiration> una <sp> particolare forma di
       osteoporosi <inspiration>


p2#7:  #<p1#6> sindrome spalla mano#


p1#8:  e pu� colpire contemporaneamente la spalla <inspiration> senza
       interessare il gomito <inspiration>


p1#9:  quindi salta completamente il gomito , � un'algodistrofia <inspiration>
       dal meccanismo patogenetico molto complesso <inspiration> ancora non
       perfettamente chiarito <inspiration> e che comporta un dolore , una
       tumefazione della mano <inspiration> e anche un dolore <sp> <inspiration>
       della spalla <sp> corrispondente <sp> omolaterale <inspiration>


p1#10:  quindi anche questo aspetto va valutato <inspiration> <sp>


p2#11:  poi <sp> pu� essere importante vedere <inspiration> la presenza di
        calcificazioni

