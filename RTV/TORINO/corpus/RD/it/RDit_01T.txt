TEXT_inf.

MAT:		RD
MAP:
TRM:		it
Ndl:
Ntr:		01
Nls:
REG:		T


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC		
LOC:		Torino
DAT:		08/02/01
DUR:		1.52,500
CON:


TRANSCRIPTION_inf.

DAT:		11/03/03
CMT:		
Nst:		15


p0#1:  *lto spesso gentile e inaspettato


p0#2:  sono esperienze che di solito cerchiamo di dimenticare pi� presto 
       possibile <sp> e che ci <eeh> guardiamo bene dal raccontare agli altri 
       perch� riconosciamo anche noi che abbiamo agito da gonzi o stupidi


p0#3:  in maniera veramente ingiustificabile


<lp> 


p0#4:  ma<aa> vi � anche un altro lato amaro delle nostre esperienze <sp> gli
       esperti nelle truffe se ve ne sono dicono


p0#5:  che nessuno pu� essere raggirato se non possiede anche lui una certa dose 
       di disonest� <inspiration> nel proprio cuore 


<sp> 


p0#6:  insomma , quell'uomo che ti offre una bella giacca di pelle all'angolo 
       della strada


p0#7:  o quel tizio che ti offre un orologio d'oro in un parcheggio 
       dell'autostrada <inspiration> pi� o meno ti fa capire senza dirtelo in 
       tante parole <inspiration> che ci� che ti vende se l'� procurato per vie
       traverse 


p0#8:  e che se tu non sperassi di approfittare del fatto che lui � un ladro che
       deve disfarsi <inspiration> a poco prezzo di ci� che ha rubato 
       <inspiration> e che<ee> / e lo dovevi fare <sp> immediatamente mentre 
       nessuno ti guardava


p0#9:  non ci saresti cascato


<sp> 


p0#10:  perci� quando si dice che avvengono in Italia almeno due 
        milioni di raggiri e imbrogli all'anno <inspiration> ti devi domandare        
        se il vero numero di furbi coinvolti � composto soltanto di quelli


p0#11:  che hanno <sp> perpretato l'inganno o anche di un numero uguale di 
      furbi che l'hanno subi+ <sp> subito credendo di essere


p0#12:  uno pi� furbo dell'altro 


<sp>


p0#13:  <tongue-click> alle volte mi sono 
        domandato se non fa parte del nostro carattere non soltanto credere che 
        noi siamo pi� furbi degli altri <inspiration> ma anche se non crediamo 
        addirittura di essere pi� furbi di Dio 


<lp>


p0#14:  <tongue-click> qual � per 
        esempio la vera relazione <inspiration> fra il numero di peccati 
        commessi <inspiration> e il numero di peccati confessati ? 


<lp>


p0#15:  a me sembra che 
        si tratti forse<ee> <sp> inconsciamente di credere

