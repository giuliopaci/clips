TEXT_inf.

MAT:		RD
MAP:
TRM:		it
Ndl:
Ntr:		02
Nls:
REG:		T


SPEAKERS_inf.

INp0:		M


RECORDING_inf.

TYP:		PC				
LOC:		Torino
DAT:		
DUR:		1.15,625
CON:		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto da 38"950ms a 1'15"625ms , totale 36"674ms
Nst:		5
	

p0#1:  {<MUSIC> <inspiration> Testa, nato a Torino nel millenovecentodiciassette 
       e morto nella sua citt� nel novantadue <inspiration> � considerato il 
       capostipite della moderna pubblicit� italiana <breath> esploratore dei 
       pi� diversi} comunicazione visiva, inventore di nuovi e iniziatore di 
       nuovi percorsi


p0#2:  <inspiration> alcune delle sue immagini e dei suoi slogan sono diventati 
       un po' il simbolo dell'era di Carosello come appuntamento Yes <sp> 
       appuntamento con Punt & Mess 


p0#3:  <inspiration> semplice ed efficace, due caratteristiche da lui sempre 
       perorate 


p0#4:  <inspiration> amici ascoltatori <sp> � tutto per il nostro notiziario 
       regionale

       
<sp>


p0#5:  a tutti voi <sp> un buon proseguimento di serata
