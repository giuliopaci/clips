TEXT_inf.

MAT:		RD
MAP:
TRM:		pb
Ndl:
Ntr:		01
Nls:
REG:		T


SPEAKERS_inf.

INp1:		M
INp2:		M
INp3:		F
INp4:		M


RECORDING_inf.

TYP:		PC				
LOC:		Torino
DAT:		
DUR:		2.41,500
CON:		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto da 30"220ms a 55"101ms , totale 24"880ms
Nst:		9


p1#1:  la carta per terra ?


<sp>


p1#2:  ma <sp> <laugh> <vocal> lo fa anche la mamma <sp> <vocal>


<sp>


p2#3:  la buccia sul prato ? 


p2#4:  tanto <vocal> <sp> passano gli spazzini <vocal> 


<sp>


p3#5:  la cacca di Fuffi ?


p3#6:  beh ma <laugh> <vocal> <sp> � cos� piccola non c'� bisogno di usare la 
       paletta


<sp>


p4#7:  l'igiene urbana <sp> non ammette scuse 


p4#8:  con un po' di attenzione tutti i cittadini possono collaborare 


p4#9:  se teniamo pulita la citt� <sp> sar� poi pi� facile pulirla <sp> e avere 
       la coscienza pulita
