TEXT_inf.

MAT:		TV
MAP:		
TRM:		is
Ndl:		
Ntr:		01
Nls:		
REG:		V


SPEAKERS_inf.

INp1:		M
INp2:		M


RECORDING_inf.

TYP:		PC
LOC:		
DAT:		
DUR:		1.51,500
CON: 		buone


TRANSCRIPTION_inf.

DAT:		
CMT:		trascritto da 0 a 1'34"946ms , totale 1'34"946ms
Nst:		11


p1#1:  aveva detto ai suoi parrocchiani di non {<NOISE> fare offerte 
       <inspiration> ma di impegnarsi invece} a trovare un lavoro a quanti 
       specialmente tra gli extracomunitari in regola col permesso di soggiorno 
       <inspiration> faticano a regolarizzare la loro posizione


p1#2:  e raccogliendo la proposta del parroco <inspiration> in molti hanno dato 
       un'occhiata pi� attenta alle vetrine <inspiration> cercando qualche 
       annuncio assieme ai regali


p1#3:  il risultato della notte di San Silvestro a San Salvador parrocchia 
       veneziana nelle mani di Don Natalino Bonazza <inspiration> sono state 
       dieci segnalazioni <inspiration> alle quali vanno aggiunte tredici 
       offerte in piena regola


p1#4:  che altrettante aziende hanno fatto via fax 


<sp> 


p1#5:  e quella che era nata come una provocazione positiva <sp> si sta facendo 
       strada come iniziativa permanente visto che intanto durer� fino ai primi 
       di febbraio


p2#6:  <inspiration> <tongue-click> se � lecito un sogno all'inizio dell'anno � 
       che magari <inspiration> ecco<oo> possano<oo> nascere altre<ee> <eeh> 
       <inspiration> possano esserci anche altre piccole cassette qua e l� 
       <inspiration> nel nostro territorio 


p2#7:  perch� in fondo noi abbiam fatto<oo> u+ una cosa molto semplice in s� 


p2#8:  <inspiration> �<ee> la risposta che merita attenzione <inspiration> non 
       tanto <eeh> l'aver inventato questa cosa 


p2#9:  <inspiration> e<ee> le risposte allora possono moltiplicarsi se e<ee> 
       <eeh> ci sono appunto <inspiration> <eeh> pi� punti magari dove poter 
       <inspiration> raccogliere queste segnalazioni <inspiration>


p1#10:  tutto il materiale � stato intanto passato allo sportello per 
        l'orientamento al lavoro <sp> che l'associazione Venezia Paese di Pace 
        <sp>  promotrice col parroco della particolare raccolta natalizia 
        gestisce al civico cinque nove cinque Bi di Santa Croce un solo giorno 
        alla settimana il venerd� <sp> dalle quattordici alle sedici <sp>
       

p1#11:  <inspiration> nessuno vuole sostituirsi alle istituzioni o agli uffici 
        di collocamento � stato spiegato

