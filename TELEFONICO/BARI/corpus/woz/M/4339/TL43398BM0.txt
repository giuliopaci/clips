TEXT_inf.

MAT:  w
MAP:  43398
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  B


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  04/05/01 14.32.06
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 19/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  7


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <eeh> pronto vorrei disdire una camera matrimoniale <inspiration>
S#3: A che nome � la prenotazione?
U#4: Giuseppe Cannone <inspiration>
S#5: Qual e' il suo numero di prenotazione? 
U#6: <tongue-click> tre <inspiration> <clear-throat> <inspiration> <tongue-click>
S#7: Pu� dirmi la data di arrivo? 
U#8: <vocal> dodici febbraio <NOISE> <inspiration> <clear-throat> <vocal>
S#9: Pu� dirmi la data di partenza? 
U#10: <recording-failure>
S#11: Pu� dirmi la data di partenza? 
U#12: <vocal> <inspiration> primo maggio <inspiration> <i.talkers>
S#13: Scusi, puo� dirmi cosa non e� di suo gradimento? 
U#14: <tongue-click> niente <inspiration>
S#15: Va bene. Arrivederci.
S#16: Arrivederci,
