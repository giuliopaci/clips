TEXT_inf.

MAT:  w
MAP:  40615
TRM:  F
Ndl:  1
Ntr:
Nls:
REG:  D


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  27/04/01 15.16.20
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  11/12/01
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  9


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <vocal> mi trovo nella camera d'albergo per cortesia vorrei sapere a che ora � disponibile il servizio ristorante <inspiration> e<ee> se mi pu� richiamare alla duecento e due grazie
S#3: Formuli nuovamente la sua richiesta. Grazie.
U#4: mi trovo nella camera duecento e due del vostro albergo per cortesia mi pu� dire a che ora � disponibile il servizio ristorante ? grazie 
S#5: Di mattina o di sera? 
U#6: <NOISE> di mattin+ / di<ii> di sera <NOISE>
S#7: Pu� ripetere, per favore?
U#8: <tongue-click> di sera
S#9: Risponda dopo il segnale acustico.
U#10: <inspiration> di sera
S#11: Per quante persone vuole prenotare il tavolo? 
U#12: per quattro persone
S#13: Vuole un tavolo per fumatori o non fumatori? 
U#14: *fumatorio
S#15: Risponda dopo il segnale acustico.
U#16: tavolo per fumatori grazie
S#17: Risponda dopo il segnale acustico.
U#18: un tavolo per fumatori grazie <i.talkers>
S#19: Il ristorante � aperto dalle dodici e trenta alle quindici, e dalle diciannove e trenta alle ventitre. 
S#20: Arrivederci,
