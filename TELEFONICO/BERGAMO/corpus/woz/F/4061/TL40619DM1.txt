TEXT_inf.

MAT:  w
MAP:  40619
TRM:  F
Ndl:  1
Ntr:
Nls:
REG:  D


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  27/04/01 15.24.10
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  11/12/01
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  7


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> sono nella camera quarantasei del vostro albergo � rotto<oo> il water lo scarico del water per favore <eeh> pu� mandarlo a riparare ? grazie
S#3: Pu� ripetere, per favore?
U#4: <vocal> alloggio nella camera di un vostro albergo alla quarantasei c'� il water rotto � possibile farlo riparare ? grazie
S#5: Ha un numero di prenotazione? 
U#6: s� trentacinque
S#7: Risponda dopo il segnale acustico.
U#8: <inspiration> trentacinque
S#9: Scusi, puo� dirmi cosa non e� di suo gradimento? 
U#10: c'� lo scarico del water rotto <sp> e quindi mi disturba grazie
S#11: Per quando le serve la babysitter? 
U#12: <NOISE> a me non occorre la babysitter <lp> c'� lo scarico del water che � rotto <NOISE>
S#13: Pu� ripetere, per favore?
U#14: ora <inspiration> scusi <eh> ! <vocal> <ehm> nella  mia camera c'� lo scarico del water rotto e quindi se � possibile farlo riparare dal momento in cui mi disturba moltissimo grazie
S#15: Sono spiacente. Avviso subito l'addetto. 
S#16: Arrivederci,
