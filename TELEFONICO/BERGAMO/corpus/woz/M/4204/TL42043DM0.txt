TEXT_inf.

MAT:  w
MAP:  42043
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  D


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  04/05/01 14.57.10
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  20/01/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <i.talkers> <tongue-click> volevo sapere se dal ventitr� al venticinque settembre � disponibile una camera doppia <inspiration> con televisione con bagno per non fumatori con frigobar e aria condizionata <i.talkers>
S#3: Che tipo di stanza vuole? 
U#4: <tongue-click> una camera doppia <inspiration> <i.talkers>
S#5: Pu� dirmi la data di arrivo? 
U#6: <i.talkers> <inspiration> ventitr� settembre <NOISE> <i.talkers> <inspiration> <NOISE>
S#7: Pu� dirmi la data di partenza? 
U#8: <recording-failure>
S#9: Pu� dirmi la data di partenza? 
U#10: venticinque settembre <NOISE> <i.talkers>
S#11: Desidera una camera con vista?
U#12: <i.talkers> s� grazie <i.talkers>
S#13: Vuole una stanza per fumatori o per non fumatori? 
U#14: non fumatori {<i.talkers> no non fumatori <i.talkers>} <i.talkers>
S#15: Qual e' il numero della sua carta di credito? 
U#16: {<i.talkers> cinque cinque cinque tre uno tre uno sette tre <i.talkers>} <i.talkers> <inspiration> <i.talkers>
S#17: Si, � disponibile 
S#18: Arrivederci,
