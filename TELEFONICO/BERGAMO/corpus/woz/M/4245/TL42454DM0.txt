TEXT_inf.

MAT:  w
MAP:  42454
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  D


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  09/05/01 12.00.40
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  18/02/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  4


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: {<i.talkers> <inspiration>} volevo disdire la colazione in camera <i.talkers> <NOISE> <i.talkers>
S#3: Qual e' il suo numero di prenotazione? 
U#4: <i.talkers> <tongue-click> otto <i.talkers>
S#5: Scusi, puo� dirmi cosa non e� di suo gradimento? 
U#6: <tongue-click> {<i.talkers> <inspiration>} troppo presto <NOISE> <i.talkers>
S#7: Pu� ripetere, per favore?
U#8: {<i.talkers> <inspiration>} mi viene servita troppo presto <i.talkers>
S#9: Va bene. Non c'� problema
S#10: Arrivederci,
