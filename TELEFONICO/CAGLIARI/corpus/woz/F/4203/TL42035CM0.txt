TEXT_inf.

MAT:  w
MAP:  42035
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  C


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  17/05/01 8.39.52
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  20/01/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  10


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <eeh> pronto buongiorno vorrei disdire la camera matrimoniale <sp>  con vista con doccia con cassaforte <inspiration> per il dodici febbraio <sp> che era stata prenotata
S#3: A che nome � la prenotazione?
U#4: <unclear> Gina Bassi
S#5: Risponda dopo il segnale acustico.
U#6: <NOISE> {<NOISE> Gina} Bassi
S#7: Qual e' il suo numero di prenotazione? 
U#8: dieci
S#9: Pu� dirmi la data di arrivo? 
U#10: <recording-failure>
S#11: Pu� dirmi la data di arrivo? 
U#12: <inspiration> venticinque maggio <NOISE>
S#13: Pu� dirmi la data di partenza? 
U#14: <recording-failure>
S#15: Pu� dirmi la data di partenza? 
U#16: {<NOISE> quindici giugno <NOISE>}
S#17: Pu� ripetere, per favore?
U#18: *ci giugno
S#19: Risponda dopo il segnale acustico.
U#20: <inspiration> quindici giugno
S#21: Va bene. Non c'� problema
S#22: Arrivederci,
