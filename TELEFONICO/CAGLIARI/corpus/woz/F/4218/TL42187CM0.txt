TEXT_inf.

MAT:  w
MAP:  42187
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  C


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  02/05/01 12.51.00
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  12/02/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> sto chiamando per richiedere la prenotazione di un tavolo al ristorante <inspiration> alle ventuno e trenta <inspiration>
S#3: Per che ora vuole il taxi? 
U#4: <NOISE> ventuno
S#5: Formuli nuovamente la sua richiesta. Grazie.
U#6: <inspiration> sto telefonando dall'albergo Hilton di Berlino per chiedere <sp> un tavolo al ristorante <sp> per le ventuno e trenta
S#7: Per che ora vuole prenotare il tavolo? 
U#8: per le ventuno e trenta
S#9: Per quante persone vuole prenotare il tavolo? 
U#10: <recording-failure>
S#11: Per quante persone vuole prenotare il tavolo? 
U#12: <tongue-click> una persona
S#13: A che nome � la prenotazione?
U#14: <inspiration> Cocco
S#15: Vuole un tavolo per fumatori o non fumatori? 
U#16: <inspiration> non fumatori
S#17: Il suo tavolo e� prenotato. Arrivederci
S#18: Arrivederci,
