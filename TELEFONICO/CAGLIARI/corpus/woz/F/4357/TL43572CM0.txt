TEXT_inf.

MAT:  w
MAP:  43572
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  C


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  10/05/01 14.45.12
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 27/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  3


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> a che ora � {<NOISE> disponibile il servizio navetta per l'aeroporto ? <NOISE>}
S#3: Di mattina o di sera? 
U#4: <inspiration> di mattina
S#5: Qual e' il suo numero di prenotazione? 
U#6: <NOISE> cinque tre uno
S#7: La navetta parte ogni ora dalle sette fino alle ventidue. 
S#8: Arrivederci,
