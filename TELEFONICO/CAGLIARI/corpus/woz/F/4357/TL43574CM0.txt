TEXT_inf.

MAT:  w
MAP:  43574
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  C


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  10/05/01 14.48.46
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 27/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  15


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> allora <eeh> vorrei sapere se avete una camera tripla con vista con tivv� <inspiration> con doccia per non fumatori con cassaforte per la {<NOISE> settimana} di Natale
S#3: A che nome � la prenotazione?
U#4: <recording-failure>
S#5: A che nome � la prenotazione?
U#6: <tongue-click> <inspiration> Rossi <NOISE>
S#7: Abbiamo stanze singole, doppie, matrimoniali o triple. Quale vuole? 
S#8: Pu� dirmi la data di arrivo? 
U#9: <recording-failure>
U#10:  <inspiration> <NOISE> triple <inspiration> <NOISE>
S#11: Pu� dirmi la data di arrivo? 
U#12: <tongue-click> <inspiration> undici quattro duemila e uno <inspiration>
S#13: Pu� dirmi la data di partenza? 
U#14: <inspiration> quindici quattro duemila e uno <inspiration>
S#15: Desidera una camera con vista?
U#16: <tongue-click> s� <inspiration>
S#17: Preferisce la doccia o la vasca nel bagno? 
U#18: <inspiration> doccia <inspiration>
S#19: Mi puo' dare un numero di carta di credito? 
U#20: <inspiration> sette cinque tre sette zero uno zero zero <inspiration> uno tre due uno otto sei sei zero
S#21: Vuole una camera con cassaforte? 
U#22: s� <inspiration> <NOISE>
S#23: Vuole una stanza per fumatori o per non fumatori? 
U#24: *on fumatori <NOISE>
S#25: Preferisce la doccia o la vasca nel bagno? 
U#26: doccia <inspiration>
S#27: Qual � l�orario d�arrivo previsto? 
U#28: <inspiration> alle otto <NOISE> <inspiration>
S#29: Di mattina o di sera? 
U#30: mattina <inspiration> <NOISE>
S#31: La stanza � confermata per il periodo richiesto.
S#32: Arrivederci,
