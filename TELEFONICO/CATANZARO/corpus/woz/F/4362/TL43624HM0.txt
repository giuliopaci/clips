TEXT_inf.

MAT:  w
MAP:  43624
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  H


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  18/05/01 14.25.52
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 28/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  7


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <tongue-click> volevo {<NOISE> richiedere} la colazione in camera <NOISE>
S#3: Qual e' il suo numero di prenotazione? 
U#4: <NOISE> ventotto <NOISE> <i.talkers>
S#5: A che nome � la prenotazione?
U#6: {<i.talkers> Loprete} <NOISE>
S#7: A che ora? 
U#8: <tongue-click> alle otto
S#9: Di mattina o di sera? 
U#10: <tongue-click> di mattina <NOISE>
S#11: Per quante persone? 
U#12: dieci <inspiration> <NOISE>
S#13: Qual e' il numero della sua carta di credito? 
U#14: <inspiration> <vocal> ottantaquattro sedici undici quattordici zero otto cinquantaquattro novantatr� quindici 
S#15: Provvediamo subito.
S#16: Arrivederci,
