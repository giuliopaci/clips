TEXT_inf.

MAT:  w
MAP:  42955
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  F


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  15/05/01 12.52.10
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 18/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  7


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> s� buongiorno sto telefonando dall'albergo Hilton di Parigi perch� ho richiesto un servizio babysitter alle nove <inspiration> per�<oo> vorrei spostarlo invece alle diciassette
S#3: Per che ora aveva richiesto la babysitter? 
U#4: <tongue-click> alle nove
S#5: A che ora desidera la sveglia? 
U#6: <tongue-click> alle cinque
S#7: Per quanto tempo le serve la babysitter?
U#8: <recording-failure>
S#9: Per quando le serve la babysitter? 
U#10: <tongue-click> alle cinque
S#11: Per quanto tempo le serve la babysitter?
U#12: <tongue-click> un paio d'ore <NOISE>
S#13: Qual e' il numero della sua carta di credito? 
U#14: uno uno due due tre tre quattro quattro
S#15: Il servizio babysitter � prenotato per il nuovo orario. 
S#16: Arrivederci,
