TEXT_inf.

MAT:  w
MAP:  43177
TRM:  F
Ndl:  1
Ntr:
Nls:
REG:  F


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  16/05/01 10.16.56
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  29/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  6


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <vocal> s� buongiorno volevo richiedere il servizio fotocopie
S#3: Di mattina o di sera? 
U#4: di mattina
S#5: A che ora? 
U#6: <vocal> nove e venti
S#7: Quante fotocopie deve fare? 
U#8: <recording-failure>
S#9: Quante fotocopie deve fare? 
U#10: <tongue-click> cinquanta
S#11: Qual e' il numero della sua carta di credito? 
U#12: <tongue-click> tre tre quattro quattro cinque cinque
S#13: Provvediamo subito.
S#14: Arrivederci,
