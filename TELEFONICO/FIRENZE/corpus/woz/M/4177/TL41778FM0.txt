TEXT_inf.

MAT:  w
MAP:  41778
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  F


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  27/04/01 15.44.30
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  08/01/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  3


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <NOISE> <inspiration> buongiorno <ehm> telefono da una camera dell'albergo la numero cinquantaquattro qui dell'albergo Excelsior di Roma <inspiration> e il mio nome � Marco Di Stefano e vorrei disdire il servizio babysitter che avevo prenotato per questa sera alle ore ventuno
S#3: Pu� ripetere, per favore?
U#4: <inspiration> s� buongiorno reception telefono dalla camera cinquantaquattro dell'albergo Excelsior di Roma <inspiration> vorrei disdire il servizio babysitter <inspiration> da me prenotato per stasera alle ore ventuno il mio nome � Marco Di Stefano
S#5: Per che ora aveva richiesto la babysitter? 
U#6: <tongue-click> per le ore ventuno
S#7: Va bene. Arrivederci.
S#8: Arrivederci,
