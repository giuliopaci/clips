TEXT_inf.

MAT:  w
MAP:  42312
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  F


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  24/05/01 13.10.08
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  15/02/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  3


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> a che ora � {<NOISE> disponibile} il servizio ristorante ?
S#3: Di mattina o di sera? 
U#4: <NOISE> di {<NOISE> mattina} <NOISE>
S#5: Per quante persone? 
U#6: due <NOISE>
S#7: Il ristorante � aperto dalle dodici e trenta alle quindici, e dalle diciannove e trenta alle ventitre. 
S#8: Arrivederci,
