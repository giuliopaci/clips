TEXT_inf.

MAT:  w
MAP:  42228
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  G


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  30/04/01 12.05.48
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  13/02/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  6


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> <eeh> vorrei disdire il servizio sveglia <inspiration>
S#3: Per che ora aveva richiesto la babysitter? 
U#4: <inspiration> alle ore diciassette
S#5: Di mattina o di sera? 
U#6: sera
S#7: Formuli nuovamente la sua richiesta. Grazie.
U#8: vorrei disdire il servizio sveglia
S#9: Per che ora aveva richiesto la sveglia? 
U#10: alle ore diciassette <inspiration>
S#11: Di mattina o di sera? 
U#12: di sera
S#13: Va bene. Arrivederci.
S#14: Arrivederci,
