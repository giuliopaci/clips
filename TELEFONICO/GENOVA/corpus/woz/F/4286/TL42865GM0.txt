TEXT_inf.

MAT:  w
MAP:  42865
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  G


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  07/05/01 10.54.16
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 07/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <tongue-click> buonasera <NOISE> <ehm> io avevo prenotato il servizio babysitter per domani alle diciannove e trenta ma avrei bisogno di spostarlo <inspiration> sempre domani per� alle dieci e cinquanta , � possibile ?
S#3: A che nome � la prenotazione?
U#4: <tongue-click> Giugliani
S#5: Per che ora aveva richiesto la babysitter? 
U#6: nove e t+ / <inspiration> diciannove e trenta <inspiration> <NOISE>
S#7: Per quando le serve la babysitter? 
U#8: <tongue-click> domani alle ore dieci e cinquanta
S#9: Per quanto tempo le serve la babysitter?
U#10: <recording-failure>
S#11: Di mattina o di sera? 
U#12: *ti mattina
S#13: Risponda dopo il segnale acustico.
U#14: <inspiration> {<NOISE> mattina}
S#15: Per quanto tempo le serve la babysitter?
U#16: <tongue-click> due ore <NOISE>
S#17: Il servizio babysitter � prenotato per il nuovo orario. 
S#18: Arrivederci,
