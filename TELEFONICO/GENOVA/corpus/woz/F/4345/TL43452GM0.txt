TEXT_inf.

MAT:  w
MAP:  43452
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  G


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  04/05/01 11.39.54
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  20/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  3


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <tongue-click> buongiorno mi scusi volevo sapere a che ora � disponibile la navetta <ehm> che fa il servizio verso l'aeroporto dal vostro albergo <inspiration> oggi pomeriggio s� grazie <inspiration> <vocal>
S#3: A che ora? 
U#4: <inspiration> <tongue-click> <eeh> dalle quattordici in poi visto che l'aereo parte alle sedici <inspiration> <vocal>
S#5: Pu� dirmi la sua destinazione? 
U#6: <inspiration> <tongue-click> Roma <inspiration> <vocal>
S#7: La navetta parte ogni ora dalle sette fino alle ventidue. 
S#8: Arrivederci,
