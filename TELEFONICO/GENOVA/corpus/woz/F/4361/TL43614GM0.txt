TEXT_inf.

MAT:  w
MAP:  43614
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  G


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  08/05/01 10.28.04
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 28/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  4


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> buongiorno chiamo dall'albergo Hilton di {<NOISE> Berlino} volevo richiedere la colazione in camera
S#3: Per quante persone? 
U#4: <recording-failure>
S#5: Per quante persone? 
U#6: due persone stanza trentaquattro
S#7: A che ora? 
U#8: <vocal> alle dieci
S#9: Provvediamo subito.
S#10: Arrivederci,
