TEXT_inf.

MAT:  w
MAP:  44163
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  G


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  09/05/2001 12.17.22
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  20/03/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  10


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> sto telefonando dalla reception dell'albergo di Berlino per sapere la disponibilit� di una camera doppia con tivv� con bagno per non fumatori <inspiration> con aria condizionata dal ventitr� al venticinque settembre
S#3: Abbiamo stanze singole, doppie, matrimoniali o triple. Quale vuole? 
U#4: una camera doppia
S#5: Pu� dirmi la data di arrivo? 
U#6: <inspiration> ventitr� settembre
S#7: Pu� dirmi la data di partenza? 
U#8: venticinque settembre
S#9: Desidera una camera con vista?
U#10: si
S#11: Preferisce la doccia o la vasca nel bagno? 
U#12: la doccia <NOISE>
S#13: Vuole una camera con cassaforte? 
U#14: si <inspiration> 
S#15: Pu� ripetere, per favore?
U#16: <trash>
S#17: Vuole una stanza per fumatori o per non fumatori? 
U#18: per non fumatori
S#19: Qual � l�orario d�arrivo previsto? 
U#20: ore diciannove
S#21: Si, � disponibile 
S#22: Arrivederci,
