TEXT_inf.

MAT:  w
MAP:  42496
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  L


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  26/04/01 13.40.38
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  04/03/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> buongiorno sto per partire per Amsterdam e vorrei sapere quanto *costi da voi una stanza tripla con doccia frigobar cassaforte vista ed aria condizionata <inspiration>
S#3: Abbiamo stanze singole, doppie, matrimoniali o triple. Quale vuole? 
U#4: <tongue-click> una stanza tripla <tongue-click> con doccia frigobar cassaforte vista ed aria condizionata gentilmente <inspiration>
S#5: Pu� dirmi la data di arrivo? 
U#6: <recording-failure>
S#7: Pu� dirmi la data di arrivo? 
U#8: <tongue-click> domani mattina
S#9: Pu� dirmi la data di partenza? 
U#10: <tongue-click> questa sera <inspiration>
S#11: Pu� ripetere, per favore?
U#12: questa sera <inspiration> <NOISE>
S#13: Pu� dirmi la data di partenza? 
U#14: <recording-failure>
S#15: Desidera una camera con vista?
U#16: s� gentilmente <NOISE>
S#17: Il costo della camera � di centoventimila lire al giorno.  
S#18: Arrivederci,
