TEXT_inf.

MAT:  w
MAP:  43041
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  L


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  08/05/01 14.38.58
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 22/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  11


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: {<i.talkers> <inspiration>} buonasera sono la signora Trofo vorrei avere un preventivo circa il costo di una {<NOISE> stanza} tripla con doccia <inspiration> e frigobar vista e aria e aria condizionata e magari anche la cassaforte <NOISE>
S#3: Che tipo di stanza vuole? 
U#4: <NOISE> <tongue-click> <inspiration> una stanza<aa> <ehm> tripla con do+ / <ehm> doccia frigobar vista e aria {<NOISE> condizionata}
S#5: Pu� dirmi la data di arrivo? 
U#6: <recording-failure>
S#7: Pu� dirmi la data di arrivo? 
S#8: Pu� dirmi la data di partenza? 
U#9: <recording-failure>
U#10: <inspiration> <ehm> il tredici<ii> cinque<ee> duemila e uno <NOISE>
S#11: Pu� dirmi la data di partenza? 
U#12: <tongue-click> <eeh> partiremo sempre il {<NOISE> tredici} cinque duemila e uno <NOISE>
S#13: Desidera una camera con vista?
U#14: <tongue-click> {<NOISE> s�} <NOISE>
S#15: Preferisce la doccia o la vasca nel bagno? 
U#16: {<NOISE> <inspiration>} <eeh> s� <lp> la doccia <NOISE>
S#17: Vuole una camera con cassaforte? 
U#18: s� <NOISE>
S#19: Vuole una stanza per fumatori o per non fumatori? 
U#20: <NOISE> non fumatori <NOISE>
S#21: Qual � l�orario d�arrivo previsto? 
U#22: le diciassette e trenta <NOISE>
S#23: Il costo della camera � di centoventimila lire al giorno.  
S#24: Arrivederci,
