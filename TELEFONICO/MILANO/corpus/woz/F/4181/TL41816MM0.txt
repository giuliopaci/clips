TEXT_inf.

MAT:  w
MAP:  41816
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  M


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  02/05/01 9.09.50
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 05/02/2003
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  3


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> buongiorno <eeh> mi trovo all'aeroporto <inspiration> e<ee> desideravo <eeh> lamentarmi per il ritardo della navetta che mi porta all'albergo
S#3: Risponda dopo il segnale acustico.
U#4: <inspiration> sto telefonando dall'aereoporto <inspiration>  {<NOISE> e<ee>  volevo} <sp> dire che <ehm> {<NOISE> questa navetta} che mi deve portare all'albergo<oo> <sp> non � ancora arrivata <lp> � in ritardo
S#5: Scusi, puo� dirmi cosa non le e� stato fornito? 
U#6: <NOISE> la navetta <lp> per arrivare all'albergo
S#7: Ci scusi, ma c'� molto traffico in citt�. La navetta arriver� a momenti.
S#8: Arrivederci,
