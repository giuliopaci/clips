TEXT_inf.

MAT:  w
MAP:  43005
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  M


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  08/05/01 13.40.30
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 19/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  6


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: {<NOISE> <inspiration> <eeh> buongiorno � la reception dell'hotel Hilton di Parigi ? <inspiration> senta io avevo richiesto il servizio babysitter alle dodici e quaranta <tongue-click> ma vorrei se possibile spostarlo alle quattro e dieci <NOISE>}
S#3: A che nome � la prenotazione?
U#4: {<NOISE> <eeh> Ciceri Rosy <NOISE>}
S#5: Per che ora aveva richiesto la babysitter? 
U#6: {<NOISE> <eeh> l'avevo richiesta per le dodici e quaranta ma vorrei appunto spostarla alle quattro <NOISE>} e dieci <i.talkers>
S#7: Per quando le serve la babysitter? 
U#8: <inspiration> alle quattro e dieci <i.talkers>
S#9: Per quanto tempo le serve la babysitter?
U#10: <NOISE> ma facciamo un sei ore <i.talkers>
S#11: Qual e' il numero della sua carta di credito? 
U#12: {<i.talkers> <eeh> trentasette novantadue cinquantacinque ventitr� <i.talkers>}
S#13: Il servizio babysitter � prenotato per il nuovo orario. 
S#14: Arrivederci,
