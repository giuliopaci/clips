TEXT_inf.

MAT:  w
MAP:  41954
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  M


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  04/05/01 15.17.00
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 15/01/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: vorrei una camera matrimoniale con <NOISE> vista televisione doccia <sp> cassaforte per / dal quattro al dieci settembre <i.talkers>
S#3: A che nome � la prenotazione?
U#4: Tiritelli <NOISE>
S#5: Pu� ripetere, per favore?
U#6: Scotti
S#7: Che tipo di stanza vuole? 
U#8: matrimoniale <i.talkers> <NOISE> <i.talkers> <NOISE>
S#9: Pu� dirmi la data di arrivo? 
U#10: <recording-failure>
S#11: Pu� dirmi la data di arrivo? 
U#12: <NOISE> quattro settembre <NOISE>
S#13: Mi puo' dare un numero di carta di credito? 
U#14: due quattro quattro due otto tre {<i.talkers> sei nove otto sei uno <i.talkers>} sei cinque uno nove otto <i.talkers>
S#15: Pu� ripetere, per favore?
U#16: due {<i.talkers> quattro quattro <i.talkers>} {<NOISE> due} otto tre sei <NOISE> nove otto sei uno sei cinque uno nove otto <NOISE>
S#17: La stanza � confermata per il periodo richiesto.
S#18: Arrivederci,
