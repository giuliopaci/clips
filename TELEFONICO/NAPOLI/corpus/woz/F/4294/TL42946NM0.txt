TEXT_inf.

MAT:  w
MAP:  42946
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  N


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  07/05/01 16.23.52
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  17/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  4


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> buonasera vorrei sapere a che ora � disponibile il serv+ / il servizio navetta per la stazione <i.talkers>
S#3: Qual e' il suo numero di prenotazione? 
U#4: <tongue-click> duecento <i.talkers>
S#5: Pu� dirmi la sua destinazione? 
U#6: <i.talkers> <eeh> Berlino ovest <i.talkers>
S#7: Pu� ripetere, per favore?
U#8: {<i.talkers> la stazione pi� vicina <i.talkers>}
S#9: La navetta parte ogni ora dalle sette fino alle ventidue. 
S#10: Arrivederci,
