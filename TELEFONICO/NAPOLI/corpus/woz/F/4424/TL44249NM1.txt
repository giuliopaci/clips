TEXT_inf.

MAT:  w
MAP:  44249
TRM:  F
Ndl:  1
Ntr:
Nls:
REG:  N


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  07/05/2001 12.13.00
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  11/04/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <eeh> senta vorrei spostare il tavolo che ho prenotato al ristorante per le tredici e cinquanta lo vorrei spostare possibilmente alle quattordici e trenta
S#3: Pu� ripetere, per favore?
U#4: <inspiration> avevo prenotato un tavolo al ristorante per le tredici e cinquanta <sp> ma vorrei spostarlo alle quattordici e trenta <sp> grazie
S#5: A che nome � la prenotazione?
U#6: eeh> <mmh> Prisco
S#7: Per che ora aveva prenotato il ristorante? 
U#8: per le tredici e cinquanta
S#9: Per che ora vuole prenotare il tavolo? 
U#10: quattordici e trenta
S#11: Per quante persone vuole prenotare il tavolo? 
U#12: otto
S#13: Vuole un tavolo per fumatori o non fumatori? 
U#14: non fumatori
S#15: Ho bisogno del numero della sua carta di credito per effettuare la prenotazione. 
U#16: numero<oo> quarantacinque zero<oo> sei
S#17: Il suo tavolo e� prenotato. Arrivederci
S#18: Arrivederci,
