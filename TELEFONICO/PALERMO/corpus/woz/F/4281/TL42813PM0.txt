TEXT_inf.

MAT:  w
MAP:  42813
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  P


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  30/04/01 13.39.30
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 06/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  9


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <eeh> buongiorno sono la signora Caruso stanza ventitr� cortesemente vorrei prenotare un tavolo per le tredici e dieci al {<NOISE> ristorante}
S#3: Per che ora vuole prenotare il tavolo? 
U#4: per le tredici e dieci <NOISE>
S#5: Pu� ripetere, per favore?
U#6: le tredici <NOISE> e dieci <NOISE>
S#7: Per quante persone vuole prenotare il tavolo? 
U#8: <recording-failure>
S#9: Per quante persone vuole prenotare il tavolo? 
U#10: per due <NOISE>
S#11: A che nome � la prenotazione?
U#12: <inspiration> Caruso
S#13: Risponda dopo il segnale acustico.
U#14: <inspiration> Caruso
S#15: Vuole un tavolo per fumatori o non fumatori? 
U#16: <NOISE> non fumatori
S#17: Mi puo' dare un numero di carta di credito? 
U#18: <tongue-click> <inspiration> <eh> s� <eeh> otto otto due quattro cinque sei <inspiration> due nove <NOISE> quattro tre quattro otto {<NOISE> uno nove <NOISE>}
S#19: Il suo tavolo e� prenotato. Arrivederci
S#20: Arrivederci,
