TEXT_inf.

MAT:  w
MAP:  43965
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  P


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  02/05/2001 12.15.06
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 14/03/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  9


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <eeh> b+ senta buongiorno volevo disdire<ee> / avevo prenotato una camera tripla con bagno <inspiration> <eeh> per la settimana di Natale vorrei disdirla
S#3: A che nome � la prenotazione?
U#4: <recording-failure>
S#5: A che nome � la prenotazione?
U#6: Lamberti
S#7: Ha un numero di prenotazione? 
U#8: <eeh> si il quattro tre nove
S#9: Risponda dopo il segnale acustico.
U#10: il quattro tre nove
S#11: Pu� dirmi la data di arrivo? 
U#12: <recording-failure>
S#13: Pu� dirmi la data di arrivo? 
U#14: e allora io dovevo arrivare il venti_tr� dicembre
S#15: Pu� dirmi la data di partenza? 
U#16: <inspiration> e dovevo ripartire <eeh> il ventotto <sp> dicembre
S#17: Scusi, puo� dirmi cosa non e� di suo gradimento? 
U#18: <eeh> no <nn>non non c'� qualcosa di non gradimento purtroppo ho dovuto rinviare<ee> l+ la partenza per problemi di lavoro
S#19: Il nostro albergo � di prima prima categoria.
S#20: Arrivederci,
