TEXT_inf.

MAT:  w
MAP:  41478
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  P


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  26/04/01 12.18.38
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  12/12/01
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  17


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: {<i.talkers> sto telefonando alla reception dell'albergo *Excelsiors di Parigi per prenotare una camera tripla con <sp> vista con tivv� con doccia per non fumatori <inspiration> con cassaforte <inspiration> per la settimana di Natale <inspiration> a nome di tre amici <lp> con la<aa> / pago con la carta di credito <i.talkers>}
S#3: A che nome � la prenotazione?
U#4: <recording-failure>
S#5: A che nome � la prenotazione?
U#6: {<i.talkers> Bianco}
S#7: Abbiamo stanze singole, doppie, matrimoniali o triple. Quale vuole? 
U#8: {<i.talkers> tripla}
S#9: Pu� dirmi la data di arrivo? 
U#10: <trash>
S#11: Pu� ripetere, per favore?
U#12: {<i.talkers> il ventitr� dicembre <i.talkers>}
S#13: Pu� dirmi la data di partenza? 
U#14: {<i.talkers> ventidue dicembre <i.talkers>} <NOISE> <i.talkers>
S#15: Pu� ripetere, per favore?
U#16: ventidue {<i.talkers> dicembre} <i.talkers>
S#17: Pu� dirmi la data di arrivo? 
U#18: {<i.talkers> ventitr� dicembre <i.talkers>}
S#19: Ho bisogno di un numero di carta di credito per effettuare la prenotazione. 
U#20: uno quattro tre uno uno tre sette uno sette sei tre cinque uno sette <NOISE> cinque quattro
S#21: Desidera una camera con vista?
U#22: s� <NOISE>
S#23: Preferisce la doccia o la vasca nel bagno? 
U#24: doccia
S#25: Pu� ripetere, per favore?
U#26: {<NOISE> doccia}
S#27: Vuole una stanza per fumatori o per non fumatori? 
U#28: non fumatori
S#29: Vuole una camera con cassaforte? 
U#30: <tongue-click> s�
S#31: Qual � l�orario d�arrivo previsto? 
U#32: <NOISE> diciassette e trenta
S#33: Di mattina o di sera? 
U#34: di sera <NOISE>
S#35: La stanza � confermata per il periodo richiesto.
S#36: Arrivederci,
