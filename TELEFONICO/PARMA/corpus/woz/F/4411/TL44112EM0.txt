TEXT_inf.

MAT:  w
MAP:  44112
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  E


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  27/04/2001 12.08.08
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  19/03/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  4


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> {[dialect] o a che ora si magna la matina ?}
S#3: Pu� ripetere, per favore?
U#4: <unclear> a che ora<aa> a che ora c'� la colazione ? <inspiration>
S#5: Formuli nuovamente la sua richiesta. Grazie.
U#6: <inspiration> si scusi da che ora a che ora servite la colazione la mattina ? <inspiration> 
S#7: Pu� ripetere, per favore?
U#8: <unclear> buongiorno! volevo sapere a che ora � disponibile la colazione
S#9: L'orario della colazione � dalle sette alle undici. Oltre quest'ora, pu� richiedere il servizio in camera. 
S#10: Arrivederci,
