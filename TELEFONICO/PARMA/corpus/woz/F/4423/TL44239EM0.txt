TEXT_inf.

MAT:  w
MAP:  44239
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  E


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  24/04/2001 11.51.38
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  25/03/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <eeh> vorrei cambiare l'orario<oo> del<ll> tavolo che ho prenotato al ristorante
S#3: Per che ora aveva prenotato il ristorante? 
U#4: undici e venti
S#5: Per che ora vuole prenotare il tavolo? 
U#6: ventitr� e quaranta
S#7: Per quante persone vuole prenotare il tavolo? 
U#8: due
S#9: Per che ora vuole prenotare il tavolo? 
U#10: <inspiration> ventitr� e quaranta
S#11: A che nome � la prenotazione?
U#12: <recording-failure>
S#13: A che nome � la prenotazione?
U#14: <tongue click> Rossi
S#15: Vuole un tavolo per fumatori o non fumatori? 
U#16: non fumatori
S#17: Il suo tavolo e� prenotato per il nuovo orario. Arrivederci.
S#18: Arrivederci,
