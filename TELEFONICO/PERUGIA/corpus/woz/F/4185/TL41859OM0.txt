TEXT_inf.

MAT:  w
MAP:  41859
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  O


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  24/04/01 12.15.12
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  11/01/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  6


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: {<i.talkers> <inspiration> buongiorno sto chiamando dalla camera centoquindici <inspiration> <eeh> ho richiesto il servizio sveglia alle ore quattro e dieci ma vorrei spostarlo alle ore tre <i.talkers>}
S#3: Per che ora aveva richiesto la sveglia? 
U#4: <NOISE> quattro e dieci <NOISE>
S#5: Di mattina o di sera? 
U#6: mattina
S#7: A che ora desidera la sveglia? 
U#8: <inspiration> tre
S#9: Di mattina o di sera? 
U#10: mattina
S#11: Pu� ripetere, per favore?
U#12: *tina mattina <i.talkers>
S#13: La sveglia e� prenotata per il nuovo orario.
S#14: Arrivederci,
