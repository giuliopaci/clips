TEXT_inf.

MAT:  w
MAP:  43348
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  O


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  24/04/01 15.32.58
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 09/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <i.talkers> <NOISE> vorrei {<NOISE> disdire} la camera matrimoniale <NOISE> con vista con {<NOISE> doccia} con cassaforte <inspiration> per il <NOISE> <ehm> quattro<oo> dal quattro al dieci settembre <NOISE> qui c'� scritto il dodici {<NOISE> febbraio} ma io gli avevo detto <NOISE> a settembre <inspiration> che devo rid� [dialect] a febbraio ? <i.talkers> devo rid� [dialect] a febbraio ? <unclear> a settembre ? <i.talkers> � uguale
S#3: A che nome � la prenotazione?
U#4: <recording-failure>
S#5: Pu� ripetere, per favore?
U#6: <unclear> <inspiration> allora sto telefonando per disdire la camera matrimoniale <NOISE>
S#7: A che nome � la prenotazione?
U#8: Bernardini <vocal>
S#9: Qual e' il suo numero di prenotazione? 
U#10: <i.talkers> il numero di prenotazione questo devo <NOISE> d� [dialect] <i.talkers>
S#11: Pu� dirmi la data di arrivo? 
U#12: <inspiration> il dodici febbraio <i.talkers>
S#13: Pu� dirmi la data di partenza? 
U#14: {<i.talkers> <unclear> il *dieciotto febbraio <i.talkers>}
S#15: Che tipo di stanza vuole? 
U#16: {<i.talkers> perch� me [dialect] chiede che tipo di stanza voglio che gli sto a d� [dialect] de [dialect] disdirla ? allo+ / lo de+ / con vista con doccia con cassaforte <i.talkers>}
S#17: Va bene. Arrivederci.
S#18: Arrivederci,
