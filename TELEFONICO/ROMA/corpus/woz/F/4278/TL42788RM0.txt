TEXT_inf.

MAT:  w
MAP:  42788
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  R


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  03/05/01 10.56.58
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 05/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  5


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <eeh> buongiorno � la reception ? <inspiration> sono nella stanza centoquindici <NOISE> <inspiration> sto scendendo ora e prevedo di ritornare questa sera <inspiration> e quindi disdico<oo> il tavolo al ristorante per l'una e venti � il tavolo<oo> dodici quattro persone grazie
S#3: Pu� ripetere, per favore?
U#4: <tongue-click> sono nella stanza centoquindici e sto uscendo quindi prevedo di non ritornare <inspiration> per il pranzo <inspiration> avevo il tavolo prenotato <NOISE> per le tredici e venti <inspiration> vorrei {<NOISE> disdirlo} � un tavolo per quattro persone <lp> il quindici
S#5: A che nome � la prenotazione?
U#6: *zi <NOISE>
S#7: Qual e' il suo numero di prenotazione? 
U#8: <NOISE> centododici
S#9: Per che ora aveva prenotato il ristorante? 
U#10: <inspiration> per le tredici e venti <inspiration>
S#11: Va bene. Arrivederci.
S#12: Arrivederci,
