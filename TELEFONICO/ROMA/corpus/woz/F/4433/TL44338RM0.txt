TEXT_inf.

MAT:  w
MAP:  44338
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  R


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  02/05/2001 17.15.44
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  09/04/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  12


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> buonasera volevo prenotare una camera tripla <sp> con vista <inspiration> sul mare con tivv� <sp> con doccia <inspiration> <eeh> per non fumatori con cassaforte <inspiration> e<ee> per la settimana di Natale a nome di tre amici <inspiration> il suo numero di carta di credito � nove cinque due nove zero sei tre zero zero zero nove uno zero zero quattro tre 
S#3: A che nome � la prenotazione?
U#4: <tongue click> Bianchi
S#5: Che tipo di stanza vuole? 
U#6: <inspiration> una camera tripla con vista<aa> sul mare <tongue click> con tivv� <tongue click> con doccia <sp> che sia per non fumatori <sp> con una cassaforte
S#7: Pu� dirmi la data di arrivo? 
U#8: <recording-failure>
S#9: Pu� dirmi la data di arrivo? 
U#10: dieci maggio
S#11: Pu� dirmi la data di partenza? 
U#12: <recording-failure>
S#13: Pu� dirmi la data di partenza? 
U#14: venti maggio
S#15: Desidera una camera con vista?
U#16: si
S#17: Preferisce la doccia o la vasca nel bagno? 
U#18: la doccia
S#19: Vuole una camera con cassaforte? 
U#20: si
S#21: Vuole una stanza per fumatori o per non fumatori? 
U#22: per non fumatori
S#23: Qual � l�orario d�arrivo previsto? 
U#24: <inspiration> dieci e trenta <inspiration> 
S#25: Mi spiace, l'albergo � pieno. 
S#26: Arrivederci,
