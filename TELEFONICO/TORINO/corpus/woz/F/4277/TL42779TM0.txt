TEXT_inf.

MAT:  w
MAP:  42779
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  T


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  27/04/01 14.40.48
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  05/04/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  7


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> vorrei <eeh> <NOISE> vorrei reclamare una coca cola che � stata servita in camera <sp> <eeh>
S#3: Formuli nuovamente la sua richiesta. Grazie.
U#4: <unclear> che non � stata gradita la coca cola servita in camera <NOISE>
S#5: Risponda dopo il segnale acustico.
U#6: <tongue-click> <inspiration> vorrei far presente che non � stata gradita una coca cola servita in camera
S#7: Scusi, puo� dirmi cosa non e� di suo gradimento? 
U#8: <NOISE> � stata / non � stata gradita una coca cola <NOISE>
S#9: Di mattina o di sera? 
U#10: <inspiration> di sera
S#11: Pu� ripetere, per favore?
U#12: <NOISE> non � stata gradita una coca cola servita in camera troppo fredda
S#13: Di mattina o di sera? 
U#14: di sera
S#15: Mi spiace.  Provvediamo subito. 
S#16: Arrivederci,
