TEXT_inf.

MAT:  w
MAP:  43534
TRM:  F
Ndl:  0
Ntr:
Nls:
REG:  T


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  02/05/01 10.56.22
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 25/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  15


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <tongue-click> <inspiration> <eeh> buongiorno � l'albergo Excelsior di Parigi ? <inspiration> vorrei prenotare una camera tripla con vista la televisione la doccia <eeh> per la settimana di Natale
S#3: A che nome � la prenotazione?
U#4: <tongue-click> <inspiration> <eeh> a nome<ee> <ehm> Paolo Bosio
S#5: Pu� ripetere, per favore?
U#6: <inspiration> Paolo Brosio e<ee> Leo Fabiano
S#7: Abbiamo stanze singole, doppie, matrimoniali o triple. Quale vuole? 
U#8: <vocal> tripla
S#9: Pu� ripetere, per favore?
U#10: <vocal> ho detto una camera tripla
S#11: Pu� dirmi la data di arrivo? 
U#12: <recording-failure>
S#13: Pu� dirmi la data di arrivo? 
U#14: <inspiration> <eeh> per la settimana di Natale
S#15: Pu� dirmi la data di partenza? 
U#16: <vocal> <inspiration> il ventotto {<NOISE> dicembre}
S#17: Ho bisogno di un numero di carta di credito per effettuare la prenotazione. 
U#18: <tongue-click> <inspiration> allora il numero di carta di credito � sette cinque <inspiration> zero sette <inspiration> nove nove quattro quattro nove quattro zero sette <inspiration> cinque tre cinque otto
S#19: Per favore parli pi� forte.
U#20: <inspiration> sette {<NOISE> cinque} zero sette nove nove quattro quattro nove quattro zero sette {<NOISE> cinque} tre {<NOISE> cinque} otto
S#21: Desidera una camera con vista?
U#22: s�
S#23: Vuole una camera con cassaforte? 
U#24: <vocal> s�
S#25: Preferisce la doccia o la vasca nel bagno? 
U#26: preferisco la doccia
S#27: Vuole una stanza per fumatori o per non fumatori? 
U#28: <tongue-click> una stanza per non fumatori
S#29: Qual � l�orario d�arrivo previsto? 
U#30: le diciassette
S#31: Mi spiace, l'albergo � pieno. 
S#32: Arrivederci,
