TEXT_inf.

MAT:  w
MAP:  43225
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  T


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  30/04/01 13.58.54
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 02/05/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  10


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> buongiorno <vocal> ho prenotato un tavolo al ristorante <vocal> alle dodici e trenta <inspiration> <tongue-click> vorrei spostare la prenotazione alle quattordici e cinquanta <vocal>
S#3: Formuli nuovamente la sua richiesta. Grazie.
U#4: <clear-throat> <NOISE> <inspiration> vorrei spostare alle quattordici e cinquanta la prenotazione che avevo fatto <inspiration> per le dodici e trenta di un tavolo al ristorante <NOISE>
S#5: Per che ora aveva prenotato il ristorante? 
U#6: <inspiration> alle dodici e {<NOISE> trenta} <NOISE>
S#7: Di mattina o di sera? 
U#8: <inspiration> di mattina <inspiration>
S#9: Per che ora vuole prenotare il tavolo? 
U#10: <NOISE> alle quattordici e cinquanta <NOISE>
S#11: Per quante persone vuole prenotare il tavolo? 
U#12: {<i.talkers> quattro} <i.talkers>
S#13: A che nome � la prenotazione?
U#14: <trash>
S#15: Pu� ripetere, per favore?
U#16: <vocal> prenotazione a nome di Ciccone <i.talkers> <NOISE>
S#17: Vuole un tavolo per fumatori o non fumatori? 
U#18: <recording-failure>
S#19: Vuole un tavolo per fumatori o non fumatori? 
U#20: fumatori <NOISE>
S#21: Il suo tavolo e� prenotato per il nuovo orario. Arrivederci.
S#22: Arrivederci,
