TEXT_inf.

MAT:  w
MAP:  43377
TRM:  M
Ndl:  0
Ntr:
Nls:
REG:  T


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  03/05/01 11.04.36
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT: 18/06/02
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  14


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> <eeh> buongiorno vorrei prenotare una camera matrimoniale <inspiration> � possibile ? <NOISE>
S#3: A che nome � la prenotazione?
U#4: <recording-failure>
S#5: A che nome � la prenotazione?
U#6: <inspiration> a<aa> nome Rossi
S#7: Pu� ripetere, per favore?
U#8: *csi
S#9: Che tipo di stanza vuole? 
U#10: <inspiration> una camera matrimoniale <clear-throat> <vocal> con vista <vocal> con Ti - Vi <vocal> con doccia e per non fumatori <sp> con cassaforte
S#11: Pu� dirmi la data di arrivo? 
U#12: <inspiration> il quattro settembre <NOISE>
S#13: Pu� dirmi la data di partenza? 
U#14: dieci settembre <NOISE>
S#15: Ho bisogno di un numero di carta di credito per effettuare la prenotazione. 
U#16: il numero di carta di credito � sei quattro otto sei <inspiration> sei <vocal> quattro zero quattro <inspiration> zero zero tre quattro <inspiration> uno zero otto zero
S#17: Desidera una camera con vista?
U#18: s� grazie <NOISE>
S#19: Preferisce la doccia o la vasca nel bagno? 
U#20: <tongue-click> <eeh> preferirei la doccia {<NOISE> <inspiration>}
S#21: Vuole una camera con cassaforte? 
U#22: s�
S#23: Vuole una stanza per fumatori o per non fumatori? 
U#24: <vocal> per non fumatori grazie <NOISE>
S#25: Qual � l�orario d�arrivo previsto? 
U#26: <inspiration> <ehm> verso le quindici e trenta <NOISE>
S#27: Di mattina o di sera? 
U#28: <NOISE> <tongue-click> di sera <inspiration> 
S#29: La stanza � confermata per il periodo richiesto.
S#30: Arrivederci,
