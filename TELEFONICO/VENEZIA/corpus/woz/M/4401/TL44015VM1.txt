TEXT_inf.

MAT:  w
MAP:  44015
TRM:  M
Ndl:  1
Ntr:
Nls:
REG:  V


SPEAKERS_inf.

INp1:


RECORDING_inf.

TYP:  Linea telefonica
LOC:  NAPOLI
DAT:  07/05/2001 12.13.10
DUR:
CON:  Manual


TRANSCRIPTION_inf.

DAT:  18/03/2002
CMT:  <Creato con TRASCRITTORE v1.1 By Leandro D'Anna >
Nst:  8


S#1: Faccia la sua richiesta dopo il segnale acustico.
U#2: <inspiration> vorrei disdire la camera tripla con bagno per non fumatori con aria condizionata per la settimana di Natale che era stata prenotata
S#3: A che nome � la prenotazione?
U#4: <tongue click> <eeh> Bullo
S#5: Qual e' il suo numero di prenotazione? 
U#6: duecentocinquantuno
S#7: Pu� dirmi la data di arrivo? 
U#8: <inspiration> cinque <sp> giugno <sp> duemila
S#9: Pu� ripetere, per favore?
U#10: cinque giugno duemila
S#11: Pu� dirmi la data di partenza? 
U#12: dodici giugno duemila
S#13: Pu� ripetere, per favore?
U#14: <unclear> dodici giugno duemila
S#15: Risponda dopo il segnale acustico.
U#16: <tongue click> dodici giugno duemila
S#17: Va bene. Arrivederci.
S#18: Arrivederci,
